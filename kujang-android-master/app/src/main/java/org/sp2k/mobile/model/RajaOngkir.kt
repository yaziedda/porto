package org.sp2k.mobile.model


import com.google.gson.annotations.SerializedName

data class RajaOngkir(
    @SerializedName("code")
    val code: String?,
    @SerializedName("costs")
    val rajaOngkirCosts: List<RajaOngkirCost>?,
    @SerializedName("name")
    val name: String?
)