package org.sp2k.mobile.model.news

data class Comment(
    val id: String,
    val id_user: String = "",
    val id_berita: String = "",
    val comment: String = "",
    val comment_id: String = "",
    var created_at: String = "",
    val updated_at: String = "",
    var send: Int = 0
)