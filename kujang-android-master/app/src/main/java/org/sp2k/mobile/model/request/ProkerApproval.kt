package org.sp2k.mobile.model.request

data class ProkerApproval(
    var id: String = "",
    var alasan: String = "",
    var status: String = "",
    var type_update: String = "approval",
)