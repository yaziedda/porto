package org.sp2k.mobile.screens.company

import android.annotation.SuppressLint
import android.widget.ScrollView
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseFragment
import org.sp2k.mobile.databinding.FragmentCompanyBinding
import org.sp2k.mobile.databinding.ItemCompanyPeopleBinding
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.company.CompanyAnggota
import org.sp2k.mobile.viewmodel.CompanyViewModel
import android.view.ViewTreeObserver.OnScrollChangedListener
import org.greenrobot.eventbus.EventBus
import org.sp2k.mobile.event.GoToMainPageEvent


class CompanyFragment: BaseFragment<FragmentCompanyBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_company
    private val viewModel by activityViewModels<CompanyViewModel>()
    private lateinit var recyclerCompanyLeadViewAdapter: RecyclerViewAdapter<CompanyAnggota, ItemCompanyPeopleBinding>
    private lateinit var recyclerCompanySCViewAdapter: RecyclerViewAdapter<CompanyAnggota, ItemCompanyPeopleBinding>

    override fun FragmentCompanyBinding.initializeView() {
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupUI()
        setupObserver()
        fetcher()
    }

    private fun FragmentCompanyBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                this?.let {
                    if (it) {
                        containerLoading.visible()
                        containerContent.gone()
                    } else {
                        containerLoading.gone()
                        containerContent.visible()
                        swipeContainer.isRefreshing = false
                    }
                }
            }
            company.observe {
                this?.let {
                    recyclerCompanyLeadViewAdapter.updateList(it.company_anggota_lead)
                    recyclerCompanySCViewAdapter.updateList(it.company_anggota_sc)

                    it.company_profile.apply {
                        companyDescription.setHtml(description)
                        companyMission.setHtml(mision)
                        companyVision.setHtml(vision)

                        Glide.with(requireActivity())
                            .load(structure_org)
                            .into(companyStructureOrg)

                        companyStructureOrg.setOnClickListener {
                            showImageBottomSheet(structure_org, onButtonCloseClicked = {

                            }, onImageClicked = {
                                goToShowImageActivity(structure_org)
                            })
                        }
                    }
                }
            }
        }
    }

    private fun FragmentCompanyBinding.setupUI() {
        setupLeadMemberAdapter()
        setupSCMemberAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        containerContent.viewTreeObserver.addOnScrollChangedListener {
            companyDetailUp.visible()
        }
        companyDetailUp.setOnClickListener {
            containerContent.fullScroll(ScrollView.FOCUS_UP);
        }

        companyProkerDownload.setOnClickListener {
            EventBus.getDefault().post(GoToMainPageEvent(GoToMainPageEvent.HOME))
        }
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchCompany()
        }
    }

    private fun FragmentCompanyBinding.setupLeadMemberAdapter() {
        recyclerCompanyLeadViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_company_people,
            BR.companyAnggota
        ) { itemView, itemModel ->

            itemView.itemMenuTvTitle.text = itemModel.name
            itemView.itemCompanyPeopleTvJabatan.text = itemModel.title

            Glide.with(requireActivity())
                .load(itemModel.image)
                .into(itemView.itemMenuIvImage)


        }

        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        companyLeadRecyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerCompanyLeadViewAdapter
            isNestedScrollingEnabled = false
        }
    }

    private fun FragmentCompanyBinding.setupSCMemberAdapter() {
        recyclerCompanySCViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_company_people,
            BR.companyAnggota
        ) { itemView, itemModel ->

            itemView.itemMenuTvTitle.text = itemModel.name
            itemView.itemCompanyPeopleTvJabatan.text = itemModel.title

            Glide.with(requireActivity())
                .load(itemModel.image)
                .into(itemView.itemMenuIvImage)


        }

        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        companyScRecyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerCompanySCViewAdapter
            isNestedScrollingEnabled = false
        }
    }
}