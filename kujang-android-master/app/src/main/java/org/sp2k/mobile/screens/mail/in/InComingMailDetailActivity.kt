package org.sp2k.mobile.screens.mail.`in`

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.krishna.fileloader.FileLoader
import com.krishna.fileloader.listener.FileRequestListener
import com.krishna.fileloader.pojo.FileResponse
import com.krishna.fileloader.request.FileLoadRequest
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityInComingMailDetailBinding
import org.sp2k.mobile.event.SuratEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.Mail
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.viewmodel.SuratMainViewModel
import java.io.File

@AndroidEntryPoint
class InComingMailDetailActivity : BaseActivity<ActivityInComingMailDetailBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_in_coming_mail_detail
    val viewModel by viewModels<SuratMainViewModel>()
    private var idSurat: String? = ""

    companion object {
        const val ID = "ID"
    }

    override fun ActivityInComingMailDetailBinding.initializeView(savedInstanceState: Bundle?) {
        getExtras()
        setupActionBar()
        setupObserver()
        swipeContainer.apply {
            isEnabled = false
            isRefreshing = true
            setOnRefreshListener {
                fetch()
            }
        }
        fetch()
    }

    private fun fetch() {
        viewModel.suratMasukById(idSurat ?: "")
    }

    private fun ActivityInComingMailDetailBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        activityInComingMailDetailDetailConfirmation.gone()
                        containerLoading.visible()
                    } else {
                        containerLoading.gone()
                    }
                }
            }

            suratMasukDetail.observe {
                this?.let {
                    loadFile(it)
                }
            }

            error.observe {
                showError(this)
            }
        }
    }

    private fun getExtras() {
        idSurat = intent?.extras?.getString(ID)
    }

    private fun ActivityInComingMailDetailBinding.loadFile(mailObject: Mail) {
        containerLoading.visible()
        activityInComingMailDetailDetailConfirmation.gone()
        FileLoader.with(this@InComingMailDetailActivity)
            .load(mailObject.dokumen, true)
            .fromDirectory("PDFView", FileLoader.DIR_INTERNAL)
            .asFile(object : FileRequestListener<File?> {
                override fun onLoad(request: FileLoadRequest, response: FileResponse<File?>) {

                    val pdfFile: File? = response.body

                    activityInComingMailDetailDetailPdf.enableAnnotationRendering(true)
                    activityInComingMailDetailDetailPdf.enableAntialiasing(true)
                    activityInComingMailDetailDetailPdf.fromFile(pdfFile)
                        .onLoad {
                            containerLoading.gone()
                            activityInComingMailDetailDetailPdf.visible()
                            activityInComingMailDetailDetailConfirmation.visible()
                            if (mailObject.status?.startsWith(Constants.MAIL_WAITING) == true && mailObject.buttonVisible == true) {
                                activityInComingMailDetailDetailConfirmation.visible()
                            } else {
                                activityInComingMailDetailDetailConfirmation.gone()
                            }
                            activityInComingMailDetailDetailAccept.setOnClickListener {
                                startActivity(
                                    Intent(
                                        applicationContext,
                                        InComingMailDisposisiActivity::class.java
                                    ).apply {
                                        putExtra(
                                            InComingMailDisposisiActivity.ID_SURAT,
                                            mailObject.id
                                        )
                                    }
                                )
                            }
                        }
                        .onPageError { _, t ->
                            showMessage(t.message)
                            finish()
                        }.load()

                }

                override fun onError(request: FileLoadRequest, t: Throwable) {
                    showMessage(t.message)
                    finish()
                }
            })
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityInComingMailDetailBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "Detail Surat Masuk"
        }
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: SuratEvent?) {
        finish()
    }
}