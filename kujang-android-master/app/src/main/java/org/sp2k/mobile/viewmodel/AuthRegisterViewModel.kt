package org.sp2k.mobile.viewmodel

import android.content.Context
import android.text.Selection
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.material.textfield.TextInputEditText
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.extensions.afterTextChanged
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import org.sp2k.mobile.utils.Validator
import kotlinx.coroutines.launch
import okhttp3.RequestBody
import timber.log.Timber

class AuthRegisterViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private var validNIKFormat: Boolean = false
    private var validEmailFormat: Boolean = false
    private var validNoHpFormat: Boolean = false
    private var validAlamatFormat: Boolean = false
    private var validPasswordFormat: Boolean = false
    private var validPasswordConfirmationFormat: Boolean = false
    private var validImageKTP: Boolean = false
    private var validImageSelfie: Boolean = false

    private var onChangedDisabled: Boolean = false

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _staffData: MutableLiveData<Staff?> by lazy {
        MutableLiveData(null)
    }
    val staffData: MutableLiveData<Staff?>
        get() = _staffData

    private val _nikValue = MutableLiveData<String>()
    val nikValue: MutableLiveData<String>
        get() = _nikValue
    private val _hideErrorNIKFormat = MutableLiveData<Boolean>()
    val hideErrorNIKFormat: LiveData<Boolean>
        get() = _hideErrorNIKFormat

    private val _emailValue = MutableLiveData<String>()
    val emailValue: MutableLiveData<String>
        get() = _emailValue
    private val _hideErrorEmailFormat = MutableLiveData<Boolean>()
    val hideErrorEmailFormat: LiveData<Boolean>
        get() = _hideErrorEmailFormat

    private val _alamatValue = MutableLiveData<String>()
    val alamatValue: MutableLiveData<String>
        get() = _alamatValue
    private val _hideErrorAlamatFormat = MutableLiveData<Boolean>()
    val hideErrorAlamatFormat: LiveData<Boolean>
        get() = _hideErrorAlamatFormat

    private val _noHpValue = MutableLiveData<String>()
    val noHpValue: MutableLiveData<String>
        get() = _noHpValue
    private val _hideErrorNoHpFormat = MutableLiveData<Boolean>()
    val hideErrorNoHpFormat: LiveData<Boolean>
        get() = _hideErrorNoHpFormat

    private val _passwordValue = MutableLiveData<String>()
    private val passwordValue: MutableLiveData<String>
        get() = _passwordValue
    private val _hideErrorPasswordFormat = MutableLiveData<Boolean>()
    val hideErrorPasswordFormat: LiveData<Boolean>
        get() = _hideErrorPasswordFormat

    private val _passwordConfirmationValue = MutableLiveData<String>()
    private val passwordConfirmationValue: MutableLiveData<String>
        get() = _passwordConfirmationValue
    private val _hideErrorPasswordConfirmationFormat = MutableLiveData<Boolean>()
    val hideErrorPasswordConfirmationFormat: LiveData<Boolean>
        get() = _hideErrorPasswordConfirmationFormat

    private val _isValid = MutableLiveData<Boolean>()
    val isValid: LiveData<Boolean>
        get() = _isValid

    fun resetState() {
        setError(null)
    }

    fun attachInputFormat(
        nikInputEditText: TextInputEditText,
        emailInputEditText: TextInputEditText,
        noHpInputEditText: TextInputEditText,
        passwordInputEditText: TextInputEditText,
        passwordConfirmationInputEditText: TextInputEditText,
        alamatInputEditText: TextInputEditText
    ) {
        nikInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _nikValue.value = this
                    setText(nikValue.value)
                    validNIKFormat = length() == 16
                    _hideErrorNIKFormat.value = validNIKFormat
                    _isValid.value = checkValidation()
                    Selection.setSelection(nikInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }


        emailInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _emailValue.value = this
                    setText(emailValue.value)
                    validEmailFormat = Validator.isValidEmail(emailValue.value ?: "")
                    _hideErrorEmailFormat.value = validEmailFormat
                    _isValid.value = checkValidation()
                    Selection.setSelection(emailInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }

        noHpInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _noHpValue.value = this
                    setText(noHpValue.value)
                    validNoHpFormat = length() >= 9
                    _hideErrorNoHpFormat.value = validNoHpFormat
                    _isValid.value = checkValidation()
                    Selection.setSelection(noHpInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }

        alamatInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _alamatValue.value = this
                    setText(alamatValue.value)
                    validAlamatFormat = length() >= 9
                    _hideErrorAlamatFormat.value = validAlamatFormat
                    _isValid.value = checkValidation()
                    Selection.setSelection(alamatInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }

        passwordInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _passwordValue.value = this
                    setText(passwordValue.value)
                    validPasswordFormat = length() >= 6
                    _hideErrorPasswordFormat.value = validPasswordFormat
                    _isValid.value = checkValidation()
                    Selection.setSelection(passwordInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }

        passwordConfirmationInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _passwordConfirmationValue.value = this
                    setText(passwordConfirmationValue.value)
                    validPasswordConfirmationFormat = this == passwordValue.value
                    _hideErrorPasswordConfirmationFormat.value = validPasswordConfirmationFormat
                    _isValid.value = checkValidation()
                    Selection.setSelection(passwordConfirmationInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }
    }

    private fun checkValidation() =
        validNIKFormat && validEmailFormat && validNoHpFormat && validPasswordFormat && validPasswordConfirmationFormat && validImageKTP && validImageSelfie && validAlamatFormat

    fun register(map: RequestBody, context: Context) = viewModelScope.launch {
        _loading.value = true
        try {
//            val response = remoteRepository.authRegister(map)
//            if (response.state) {
//                val userData = mappingObject(response.data, UserToken::class.java)
//                PreferenceManager(context).saveUser(userData.user)
//                PreferenceManager(context).setToken(userData.token)
//                _userData.value = userData.user
//            } else {
//                _userData.value = null
//                showError(GeneralException(response.message))
//            }
            _loading.value = false
        } catch (exception: Exception) {
            exception.printStackTrace()
            Timber.tag(this::class.java.simpleName).e(exception)
            _loading.value = false
            showError(exception)
        }
    }

    fun setImageKtp(boolean: Boolean) {
        validImageKTP = boolean
        _isValid.value = checkValidation()
    }

    fun setImageSelfie(boolean: Boolean) {
        validImageSelfie = boolean
        _isValid.value = checkValidation()
    }
}