package org.sp2k.mobile.utils

import androidx.fragment.app.Fragment
import org.sp2k.mobile.transition.TransitionType

interface FragmentHelper<Result> {
    val fragment: Fragment
    var animation: TransitionType
    var fragmentCallback: FragmentCallback<Result>?
    var isAlreadyAdded: Boolean

    fun closeFragment() {
        fragment.parentFragmentManager.beginTransaction().setCustomAnimations(
            animation.enterTransition,
            animation.exitTransition
        ).remove(fragment).commit()
        isAlreadyAdded = false
    }
}