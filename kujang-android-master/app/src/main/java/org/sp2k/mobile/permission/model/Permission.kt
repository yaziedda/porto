package org.sp2k.mobile.permission.model

class Permission(var name: String, var granted: Boolean = false, var shouldShowRequestPermissionRationale: Boolean = false)