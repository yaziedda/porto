package org.sp2k.mobile.screens.auth

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import dagger.hilt.android.AndroidEntryPoint
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityRegisterVerificationBinding
import org.sp2k.mobile.extensions.click
import org.sp2k.mobile.extensions.color
import org.sp2k.mobile.extensions.underline
import org.sp2k.mobile.screens.MainActivity
import org.sp2k.mobile.utils.PreferenceManager
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.RegistrationVerificationViewModel

@AndroidEntryPoint
class RegisterVerificationActivity : BaseActivity<ActivityRegisterVerificationBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_register_verification
    private val viewModel by viewModels<RegistrationVerificationViewModel>()
    private lateinit var countDownTimer: CountDownTimer
    private val errorMessage: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(getString(R.string.label_prefix_blocking_description))
            .append(
                getString(R.string.label_here).color(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.colorPrimary
                    )
                )
                    .underline()
                    .click { }
            )
    }

    override fun ActivityRegisterVerificationBinding.initializeView(savedInstanceState: Bundle?) {
        StatusBar.setLightStatusBar(window)
        fetcher()
        setupObserver()
        setupUI()
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            sendVerification(applicationContext)
        }
    }

    private fun ActivityRegisterVerificationBinding.setupUI() {
        otpView.setOtpCompletionListener {
            btnLogin.isEnabled = true
            viewModel.validateOtp(applicationContext, it)
        }
        btnLogin.setOnClickListener {
            viewModel.validateOtp(applicationContext, otpView.text.toString())
        }
    }

    private fun ActivityRegisterVerificationBinding.startCountDown() {
        countDownTimer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val secs = millisUntilFinished / 1000
                tvLabelRetry.text =
                    "Anda dapat mengirim email kembali dalam waktu\n$secs detik lagi"
                tvRetry.visibility = View.GONE
            }

            override fun onFinish() {
                tvLabelRetry.text = "Belum mendapatkan kode verifikasi? "
                tvRetry.visibility = View.VISIBLE
                tvRetry.text = "Kirim ulang"
                tvRetry.setOnClickListener {
                    viewModel.sendVerification(this@RegisterVerificationActivity.applicationContext)
                    countDownTimer.start()
                }
            }
        }.start()
    }

    private fun ActivityRegisterVerificationBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    btnLogin.visibility = View.GONE
                    containerLoading.visibility = View.VISIBLE
                } else {
                    btnLogin.visibility = View.VISIBLE
                    containerLoading.visibility = View.GONE
                }
            }
            startCountDown.observe {
                if (this) {
                    startCountDown()
                }
            }
            error.observe {
                if (this != null) {
                    this@RegisterVerificationActivity.showDialog(
                        message ?: errorMessage,
                        ContextCompat.getDrawable(
                            applicationContext,
                            R.drawable.ic_not_logged_in
                        )
                    ) {
                        btnLogin.isEnabled = false
                        otpView.setText("")
                    }
                }
            }
            otpIsValid.observe {
                if (this) {
                    val user = PreferenceManager(applicationContext).getUser()
                    Toast.makeText(
                        applicationContext,
                        "Berhasil registrasi, selamat datang " + (user?.staff?.admin_nama?:"")+" di "+getString(R.string.app_name),
                        Toast.LENGTH_LONG
                    ).show()
                    startActivity(
                        Intent(
                            applicationContext,
                            MainActivity::class.java
                        )
                    )
                }
            }
        }
    }

    override fun onBackPressed() {

    }

    override fun onDestroy() {
        super.onDestroy()
        if (::countDownTimer.isInitialized)
            countDownTimer.cancel()
    }
}