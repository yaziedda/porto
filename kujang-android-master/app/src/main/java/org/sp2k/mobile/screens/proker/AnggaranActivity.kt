package org.sp2k.mobile.screens.proker

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityAnggaranBinding
import org.sp2k.mobile.databinding.ItemAnggaranBinding
import org.sp2k.mobile.event.AnggaranEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.toCurrency
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.Anggaran
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.ProkerViewModel

@AndroidEntryPoint
class AnggaranActivity : BaseActivity<ActivityAnggaranBinding>() {

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter<Anggaran, ItemAnggaranBinding>
    val viewModel by viewModels<ProkerViewModel>()

    override fun getLayoutId(): Int = R.layout.activity_anggaran

    override fun ActivityAnggaranBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupUI()
        setupObserver()
        fetchData()
    }

    private fun ActivityAnggaranBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        emptyState.gone()
                        recyclerView.gone()
                        containerLoading.visible()
                    } else {
                        recyclerView.visible()
                        containerLoading.gone()
                    }
                }
            }

            anggaranBidang.observe {
                this?.let {
                    anggranTotal.text = it.saldo.toString().toCurrency()
                    if (viewModel.getUser()?.role?.role_code.equals("KETUA_BIDANG")) {
                        anggranTotalBidangWrapper.gone()
                        anggranTotalBidang.text =
                            it.anggaran.getOrNull(0)?.anggaran_limit.toString().toCurrency()
                    } else {
                        anggranTotalBidangWrapper.gone()
                    }
                    recyclerViewAdapter.updateList(it.anggaran)
                    if (it.anggaran.isEmpty()) {
                        recyclerView.gone()
                        emptyState.visible()
                    } else {
                        recyclerView.visible()
                        emptyState.gone()
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityAnggaranBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "Anggaran"
        }
    }

    private fun ActivityAnggaranBinding.setupUI() {
        StatusBar.setLightStatusBar(window)
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetchData()
            }
        }
        if (viewModel.getUser()?.role?.role_code.equals("KETUA_BIDANG")) {
            anggranTotalTitle.text =
                getString(R.string.anggaran_title_bidang, viewModel.getUser()?.bidang?.name)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityAnggaranBinding.setupAdapter() {
        recyclerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_anggaran,
            BR.anggaran
        ) { itemView, itemModel ->

            itemView.itemAnggaranTitle.text = "BIDANG ${itemModel.name}"
            itemView.itemAnggaranJumlah.text = itemModel.saldo.toString().toCurrency()
            itemView.itemAnggaranJumlahSisa.text = itemModel.sisa.toString().toCurrency()
            itemView.itemAnggaranLimit.text = itemModel.anggaran_limit.toString().toCurrency()
        }

        val linearLayoutManager = LinearLayoutManager(
            this@AnggaranActivity,
            LinearLayoutManager.VERTICAL,
            false
        )

        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerViewAdapter
        }
    }


    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: AnggaranEvent?) {
        fetchData()
    }

    private fun fetchData() {
        viewModel.fetchAnggaranList()
    }
}