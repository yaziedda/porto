package org.sp2k.mobile.model

data class NewsDetail(
    val news: News?,
    val image: List<String> = arrayListOf()
)