package org.sp2k.mobile.screens.jasa

import android.content.Intent
import android.net.Uri
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseFragment
import org.sp2k.mobile.databinding.FragmentJasaBinding
import org.sp2k.mobile.databinding.ItemJasaBinding
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.model.Jasa
import org.sp2k.mobile.viewmodel.JasaViewModel
import timber.log.Timber
import java.net.URLEncoder


class JasaFragment : BaseFragment<FragmentJasaBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_jasa
    private lateinit var recyclerBlogViewAdapter: RecyclerViewAdapter<Jasa, ItemJasaBinding>
    private val viewModel by activityViewModels<JasaViewModel>()

    override fun FragmentJasaBinding.initializeView() {
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupNewsAdapter()
        setupObserver()
        fetcher()
    }

    private fun FragmentJasaBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    swipeContainer.isRefreshing = true
                    skeletonLayout.showSkeleton()
                } else {
                    skeletonLayout.showOriginal()
                    swipeContainer.isRefreshing = false
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        println("asu error mulu " + this.message)
                        fetcher()
                    }
                }
            }

            list.observe {
                recyclerBlogViewAdapter.updateList(this)
            }
        }
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            resetState()
            fetch()
        }
    }

    private fun FragmentJasaBinding.setupNewsAdapter() {
        recyclerBlogViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_jasa,
            BR.kasa
        ) { itemView, itemModel ->
            itemView.itemJasaTvTitle.text = itemModel.namaJasa
            itemView.itemJasaTitle3.text = itemModel.alamat
            itemView.itemJasaTitle2.gone()
            Glide.with(requireActivity())
                .load(itemModel.file)
                .into(itemView.itemJasaIvImage)
            itemView.chat.setOnClickListener {
                var phone = ""
                if (itemModel.noTelp?.startsWith("0") == true) {
                    phone = "62" + itemModel.noTelp.substring(1)
                }
                onClickWhatsApp(
                    "Hallo ${itemModel.namaJasa} saya tertarik dengan jasanya di Aplikasi " + getString(
                        R.string.app_name
                    ),
                    phone
                )
            }
        }

        val linearLayoutManager = LinearLayoutManager(requireActivity())
        rvBerita.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerBlogViewAdapter
        }
    }

    private fun onClickWhatsApp(string: String, phone: String) {
        val url = "whatsapp://send?text=${string}&phone=${phone}"
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            val appPackageName = "com.whatsapp"
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$appPackageName")
                    )
                )
            } catch (e: android.content.ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }
        }
    }

}