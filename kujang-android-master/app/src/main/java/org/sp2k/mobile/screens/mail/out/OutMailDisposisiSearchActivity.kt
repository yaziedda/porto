package org.sp2k.mobile.screens.mail.out

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.greenrobot.eventbus.EventBus
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityOutMailDisposisiSearchBinding
import org.sp2k.mobile.databinding.ItemMailDisposisiSearchBinding
import org.sp2k.mobile.event.SuratEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.request.DisposisiRequest
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.SuratMainViewModel

@AndroidEntryPoint
class OutMailDisposisiSearchActivity : BaseActivity<ActivityOutMailDisposisiSearchBinding>() {

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter<Staff, ItemMailDisposisiSearchBinding>
    val viewModel by viewModels<SuratMainViewModel>()
    private val userSelectedMap: HashMap<String, String> = hashMapOf()
    private var idSurat: Int = 0

    companion object {
        const val ID_SURAT = "ID_SURAT"
    }

    override fun getLayoutId(): Int = R.layout.activity_out_mail_disposisi_search

    override fun ActivityOutMailDisposisiSearchBinding.initializeView(savedInstanceState: Bundle?) {
        getExtras()
        setupActionBar()
        setupUI()
        setupObserver()
        viewModel.searchGroup("")
        activityMailDisposisiSearchText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.searchGroup(activityMailDisposisiSearchText.text.toString())
                return@OnEditorActionListener true
            }
            false
        })
    }

    private fun getExtras() {
        idSurat = intent?.extras?.getInt(OutComingMailDisposisiActivity.ID_SURAT) ?: 0
    }

    private fun ActivityOutMailDisposisiSearchBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        activityMailDisposisiSearchRecycler.gone()
                        activityMailDisposisiSearchAccept.gone()
                        containerLoading.visible()
                    } else {
                        activityMailDisposisiSearchRecycler.visible()
                        containerLoading.gone()
                        checkAcceptButtonUI()
                    }
                }
            }

            disposisiSuccess.observe {
                this?.let {
                    if (it) {
                        showDialog("Berhasil melakukan disposisi") {
                            EventBus.getDefault().post(SuratEvent(SuratEvent.KEY))
                            finish()
                        }
                    }
                }
            }

            error.observe {
                showError(this)
            }

            staffList.observe {
                this?.let {
                    recyclerViewAdapter.updateList(it)
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityOutMailDisposisiSearchBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "Cari Anggota Tertentu"
        }
    }

    private fun ActivityOutMailDisposisiSearchBinding.setupUI() {
        StatusBar.setLightStatusBar(window)
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                viewModel.fetchGroups()
            }
        }
        activityMailDisposisiSearchAccept.setOnClickListener {
            showDialogOptionBottomSheet("Apakah anda yakin?", yeButtonClick = {
                val userList: MutableList<String> = arrayListOf()
                userSelectedMap.forEach {
                    userList.add(it.value)
                }
                val disposisiRequest = DisposisiRequest(
                    id_surat = idSurat.toString(),
                    id_group = "-1",
                    users = userList
                )

                viewModel.sendDisposisiKeluar(disposisiRequest)
            })
        }
    }

    private fun ActivityOutMailDisposisiSearchBinding.setupAdapter() {
        recyclerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_mail_disposisi_search,
            BR.userSearch
        ) { itemView, itemModel ->
            itemView.itemMailDisposisiSearchtext.text = itemModel.admin_nama
            itemView.itemMailDisposisiSearchradio.isChecked = userSelectedMap[itemModel.id.toString()] != null && userSelectedMap[itemModel.id.toString()]?.isNotEmpty() == true
            itemView.itemMailDisposisiSearchradio.setOnCheckedChangeListener { _, b ->
                if (b) {
                    userSelectedMap[itemModel.id.toString()] = itemModel.id.toString()
                } else {
                    userSelectedMap.remove(itemModel.id.toString())
                }
                checkAcceptButtonUI()
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            this@OutMailDisposisiSearchActivity,
            LinearLayoutManager.VERTICAL,
            false
        )

        activityMailDisposisiSearchRecycler.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerViewAdapter
        }
    }

    private fun ActivityOutMailDisposisiSearchBinding.checkAcceptButtonUI() {
        if (userSelectedMap.size > 0) {
            activityMailDisposisiSearchAccept.visible()
        } else {
            activityMailDisposisiSearchAccept.gone()
        }
    }

}