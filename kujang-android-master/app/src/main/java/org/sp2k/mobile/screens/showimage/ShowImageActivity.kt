package org.sp2k.mobile.screens.showimage

import android.os.Bundle
import com.bumptech.glide.Glide
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityShowImageBinding
import kotlinx.android.synthetic.main.view_toolbar_activity.*
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*

class ShowImageActivity : BaseActivity<ActivityShowImageBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_show_image
    override fun getSecurityFlag(): Boolean = true

    companion object {
        const val IMAGE_URL = "IMAGE_URL"
        const val IMAGE_TAKE_SCREENSHOT = "IMAGE_TAKE_SCREENSHOT"
    }

    override fun ActivityShowImageBinding.initializeView(savedInstanceState: Bundle?) {
        with(showImageToolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                showImageAppBar
            )
        }
        val imageUrl = intent?.extras?.getString(IMAGE_URL) ?: ""
        val takeCapture = intent?.extras?.getString(IMAGE_TAKE_SCREENSHOT) ?: ""

        if (imageUrl.isNotEmpty()) {
            Glide.with(this@ShowImageActivity)
                .load(imageUrl)
                .into(showImageView)
        } else {
            showDialog(
                "Gambar tidak valid"
            )
        }
    }

}