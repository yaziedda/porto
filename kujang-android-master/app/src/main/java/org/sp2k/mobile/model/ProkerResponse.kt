package org.sp2k.mobile.model

import org.sp2k.mobile.model.prokeranggaran.ProkerData

data class ProkerResponse (
    val list: List<ProkerData> = arrayListOf(),
    val empty: EmptyProker
)

data class EmptyProker (
    val usulan: Empty,
    val proker: Empty,
    val lpj: Empty,
)