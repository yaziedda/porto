package org.sp2k.mobile.model.news

import org.sp2k.mobile.model.user.Staff

data class NewsComment(
    val comment: Comment,
    val user: Staff,
    var send: Int = 0
)