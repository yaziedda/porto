package org.sp2k.mobile.screens.donation

import android.content.Intent
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseFragment
import org.sp2k.mobile.databinding.FragmentDonationBinding
import org.sp2k.mobile.databinding.ItemDonationBinding
import org.sp2k.mobile.extensions.toCurrency
import org.sp2k.mobile.model.donation.Donation
import org.sp2k.mobile.viewmodel.DonationViewModel
import timber.log.Timber

class DonationFragment : BaseFragment<FragmentDonationBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_donation
    private lateinit var recyclerDonationAdapter: RecyclerViewAdapter<Donation, ItemDonationBinding>
    private val viewModel by activityViewModels<DonationViewModel>()

    override fun FragmentDonationBinding.initializeView() {
        setupDonationAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupObserver()
        fetcher()
    }

    private fun FragmentDonationBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    swipeContainer.isRefreshing = true
                    skeletonLayout.showSkeleton()
                } else {
                    skeletonLayout.showOriginal()
                    swipeContainer.isRefreshing = false
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }

            donationList.observe {
                recyclerDonationAdapter.updateList(this)
            }
        }
    }

    private fun FragmentDonationBinding.setupDonationAdapter() {
        recyclerDonationAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_donation,
            BR.donation
        ) { itemView, itemModel ->
            itemView.itemHomeDonationContAmount.text =
                itemModel.prosesDonasi.toString().toCurrency()
            itemView.itemHomeDonationContAmountTotal.text =
                getString(R.string.donasi_terkumpul).format(
                    itemModel.targetDonasi.toString().toCurrency()
                )
            itemView.itemHomeDonationTvTitle.text = itemModel.judul

            val percent = calculatePercentage(
                itemModel.prosesDonasi ?: 0, itemModel.targetDonasi ?: 0
            ).toInt()

            itemView.itemHomeDonationProgres.progress = percent

            Glide.with(requireActivity())
                .load(itemModel.image)
                .into(itemView.itemHomeDonationIvImage)

            itemView.root.setOnClickListener {
                startActivity(
                    Intent(requireContext(), DonationDetailActivity::class.java).apply {
                        putExtra(DonationDetailActivity.KEY_ID, itemModel.id.toString())
                    }
                )
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerDonationAdapter
        }
    }

    private fun calculatePercentage(obtained: Long, total: Long): Long {
        return obtained * 100 / total
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchDonation()
        }
    }

}