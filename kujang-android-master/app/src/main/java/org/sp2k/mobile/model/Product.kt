package org.sp2k.mobile.model


import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("berat")
    val berat: Int = 0,
    @SerializedName("created_at")
    val createdAt: String? = "",
    @SerializedName("created_by")
    val createdBy: Int? = 0,
    @SerializedName("description")
    val description: String? = "",
    @SerializedName("id")
    val id: Int?,
    @SerializedName("kode_barang")
    val kodeBarang: String? = "",
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("price")
    val price: Double = 0.0,
    @SerializedName("qty")
    val qty: Int? = 0,
    @SerializedName("status")
    val status: Int? = 0,
    @SerializedName("updated_at")
    val updatedAt: String? = "",
    @SerializedName("updated_by")
    val updatedBy: Int? = 0,
    @SerializedName("images")
    val images: List<String>? = arrayListOf(),
    @SerializedName("image")
    val image: String? = ""
)