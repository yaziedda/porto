package org.sp2k.mobile.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.exception.GeneralException
import org.sp2k.mobile.model.*
import org.sp2k.mobile.model.company.CompanyData
import org.sp2k.mobile.model.donation.Donation
import org.sp2k.mobile.model.home.Catalogue
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import javax.inject.Inject

@HiltViewModel
class CompanyViewModel @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _company = MutableLiveData<CompanyData?>()
    val company: LiveData<CompanyData?>
        get() = _company


    fun resetState() {
        setError(null)
    }

    fun fetchCompany() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.company()
            if (response.state && response.data != null) {
                _company.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _company.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }

    }

}