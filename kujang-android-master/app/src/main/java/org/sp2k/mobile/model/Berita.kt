package org.sp2k.mobile.model

import org.sp2k.mobile.model.news.NewsCategory

data class Berita(
    val created_at: String = "",
    val created_by: String = "",
    val id: String = "",
    val image: String = "",
    val isi: String = "",
    val judul: String = "",
    val kategori_id: Int = 0,
    val post_type: String = "",
    val tanggal: String = "",
    val terbit: String  = "",
    val ringkasan: String? = "",
    val updated_at: String = "",
    val updated_by: String = "",
    val views: Int = 0
)