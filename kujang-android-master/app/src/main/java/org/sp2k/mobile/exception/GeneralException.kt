package org.sp2k.mobile.exception

class GeneralException(message: String = "Error") : Exception(message)