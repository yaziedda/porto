package org.sp2k.mobile.screens.mail.out

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.customview.customView
import com.krishna.fileloader.FileLoader
import com.krishna.fileloader.listener.FileRequestListener
import com.krishna.fileloader.pojo.FileResponse
import com.krishna.fileloader.request.FileLoadRequest
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.greenrobot.eventbus.EventBus
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityOutComingMailDetailBinding
import org.sp2k.mobile.event.SuratEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.SuratKeluar
import org.sp2k.mobile.model.request.SuratKeluarApproveRequest
import org.sp2k.mobile.model.request.SuratKeluarRejectRequest
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.viewmodel.SuratMainViewModel
import java.io.File

@AndroidEntryPoint
class OutComingMailDetailActivity : BaseActivity<ActivityOutComingMailDetailBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_out_coming_mail_detail
    private var idSurat: String? = ""
    private lateinit var mailObject: SuratKeluar
    val viewModel by viewModels<SuratMainViewModel>()

    companion object {
        const val ID = "ID"
    }

    override fun ActivityOutComingMailDetailBinding.initializeView(savedInstanceState: Bundle?) {
        getExtras()
        setupActionBar()
        setupObserver()
        swipeContainer.apply {
            isEnabled = false
            isRefreshing = true
            setOnRefreshListener {
                fetch()
            }
        }
        fetch()
    }

    private fun fetch() {
        idSurat?.let {
            viewModel.suratKeluarById(it)
        }
    }

    private fun ActivityOutComingMailDetailBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        showProgressDialog()
                    } else {
                        dissmissProgressDialog()
                    }
                }
            }
            approveSuratKeluarSuccess.observe {
                this?.let {
                    getUser()?.group?.code.let {
                        if (it?.equals("1") == true) {
                            mailObject?.status = "A1"
                        } else {
                            mailObject?.status = "A2"
                        }
                    }
                    showMessage("Sukses menyetujui surat keluar")
                    EventBus.getDefault().post(SuratEvent(SuratEvent.KEY))
                    loadFile(mailObject)
                }
            }

            rejectSuratKeluarSuccess.observe {
                this?.let {
                    showMessage("Sukses menolak surat keluar")
                    EventBus.getDefault().post(SuratEvent(SuratEvent.KEY))
                    finish()
                }
            }

            suratKeluarDetail.observe {
                this?.let {
                    mailObject = it
                    loadFile(mailObject)
                }
            }

            error.observe {
                showError(this)
            }
        }
    }

    private fun getExtras() {
        idSurat = intent?.extras?.getString(ID)
    }

    private fun ActivityOutComingMailDetailBinding.loadFile(mailObject: SuratKeluar) {
        containerLoading.visible()
        activityInComingMailDetailDetailConfirmation.gone()
        FileLoader.with(this@OutComingMailDetailActivity)
            .load(mailObject.dokumen, true)
            .fromDirectory("PDFView", FileLoader.DIR_INTERNAL)
            .asFile(object : FileRequestListener<File?> {
                @SuppressLint("SetTextI18n")
                override fun onLoad(request: FileLoadRequest, response: FileResponse<File?>) {

                    val pdfFile: File? = response.body

                    activityInComingMailDetailDetailPdf.enableAnnotationRendering(true)
                    activityInComingMailDetailDetailPdf.enableAntialiasing(true)
                    activityInComingMailDetailDetailPdf.fromFile(pdfFile)
                        .onLoad {
                            containerLoading.gone()
                            activityInComingMailDetailDetailPdf.visible()
                            activityInComingMailDetailDetailConfirmation.visible()
                            activityInComingMailDetailDetailTitleTolak.gone()
                            activityInComingMailDetailDetailSubTitleTolak.gone()
                            activityInComingMailDetailDetailTitle.text = "Detail Surat"
                            var description = "PERIHAL : ${mailObject.perihal_surat ?: ""}\n" +
                                    "TUJUAN : ${mailObject.tujuan_surat ?: ""}\n" +
                                    "DISPOSISI : ${mailObject.status_disposisi}\n"

                            if (mailObject.status_disposisi?.contains("ANGGOTA TERTENTU") == true) {
                                mailObject.anggotaTertentu?.forEachIndexed { index, staff ->
                                    description += "${index + 1}. ${staff.admin_nama} (${staff.admin_username})\n"
                                }
                            }
                            activityInComingMailDetailDetailSubTitle.text = description

                            if (mailObject.status?.startsWith(Constants.MAIL_WAITING) == true && mailObject.buttonVisible == true) {
                                setMenunggu()
                            } else if (mailObject.status?.startsWith(Constants.MAIL_APPROVE) == true && mailObject.buttonVisible == true) {
                                if (viewModel.getUser()?.group?.code.equals("1")) {
                                    mailObject.status.let {
                                        if (it.equals("A1")) {
                                            setDisetujui()
                                        } else {
                                            setMenunggu()
                                        }
                                    }
                                } else {
                                    activityInComingMailDetailDetailAccept.gone()
                                    activityInComingMailDetailDetailReject.gone()
                                }

                            } else if (mailObject?.status?.startsWith(Constants.MAIL_REJECT) == true) {
                                activityInComingMailDetailDetailTitleTolak.visible()
                                activityInComingMailDetailDetailSubTitleTolak.visible()
                                activityInComingMailDetailDetailConfirmation.visible()
                                activityInComingMailDetailDetailAccept.gone()
                                activityInComingMailDetailDetailReject.gone()
                                activityInComingMailDetailDetailTitleTolak.text =
                                    "Alasan ditolak : "
                                activityInComingMailDetailDetailSubTitleTolak.text =
                                    mailObject?.alasan ?: ""
                            } else {
                                activityInComingMailDetailDetailAccept.gone()
                                activityInComingMailDetailDetailReject.gone()
                            }
                        }
                        .onPageError { page, t ->
                            showMessage(t.message)
                            finish()
                        }
                        .load()
                }

                override fun onError(request: FileLoadRequest, t: Throwable) {
                    showMessage(t.message)
                    finish()
                }
            })
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityOutComingMailDetailBinding.setMenunggu() {
        activityInComingMailDetailDetailConfirmation.visible()
        activityInComingMailDetailDetailAccept.text = "Setujui"
        activityInComingMailDetailDetailReject.apply {
            visible()
            setOnClickListener {
                val dialog = MaterialDialog(
                    this@OutComingMailDetailActivity,
                    BottomSheet(LayoutMode.WRAP_CONTENT)
                )
                    .customView(R.layout.dialog_confirmation_input, scrollable = true)
                    .cornerRadius(resources.getDimension(R.dimen.card_corner_radius_normal))

                val editText = dialog.findViewById<AppCompatEditText>(R.id.dialog_edit_text)
                val btnYes = dialog.findViewById<AppCompatButton>(R.id.dialog_button_yes)
                btnYes.setOnClickListener {
                    val text = editText.text.toString()
                    if (text.isEmpty()) {
                        showMessage("Alasan harus diisi!")
                        return@setOnClickListener
                    }

                    val rejectRequest = SuratKeluarApproveRequest(
                        id_surat = mailObject.id.toString(),
                        alasan = text,
                        status_approve = "-1"
                    )
                    viewModel.approveSuratKeluar(rejectRequest)
                }
                dialog.show()
            }
        }

        activityInComingMailDetailDetailAccept.setOnClickListener {
            showDialogOptionBottomSheet(
                "Apakah anda yakin akan menyetujui surat ini?",
                yeButtonClick = {
                    val suratKeluarApproveRequest =
                        SuratKeluarApproveRequest(
                            id_surat = mailObject?.id.toString(),
                            status_approve = "1"
                        )
                    viewModel.approveSuratKeluar(suratKeluarApproveRequest)
                })
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityOutComingMailDetailBinding.setDisetujui() {
        activityInComingMailDetailDetailAccept.text = "Disposisi"
        activityInComingMailDetailDetailReject.gone()
//        if (viewModel.getUser()?.group?.code.equals("1")) {
//            activityInComingMailDetailDetailConfirmation.visible()
//        } else {
//
//        }
        activityInComingMailDetailDetailAccept.gone()
        activityInComingMailDetailDetailAccept.setOnClickListener {
            startActivity(
                Intent(
                    applicationContext,
                    OutComingMailDisposisiActivity::class.java
                ).apply {
                    putExtra(
                        OutComingMailDisposisiActivity.ID_SURAT,
                        mailObject.id
                    )
                }
            )
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityOutComingMailDetailBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "Detail Surat Keluar"
        }
    }

}