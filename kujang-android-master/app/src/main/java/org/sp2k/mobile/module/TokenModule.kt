package org.sp2k.mobile.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import org.sp2k.mobile.model.Token

@Module
@InstallIn(SingletonComponent::class)
class TokenModule {

    @Provides
    fun provideToken(): Token = Token()
}