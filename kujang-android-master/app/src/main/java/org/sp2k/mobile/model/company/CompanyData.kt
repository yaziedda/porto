package org.sp2k.mobile.model.company

data class CompanyData(
    val company_anggota_lead: List<CompanyAnggota> = arrayListOf(),
    val company_anggota_sc: List<CompanyAnggota> = arrayListOf(),
    val company_profile: CompanyProfile
)