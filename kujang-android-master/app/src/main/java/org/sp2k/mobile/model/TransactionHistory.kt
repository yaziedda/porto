package org.sp2k.mobile.model


import com.google.gson.annotations.SerializedName

data class TransactionHistory(
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("created_by")
    val createdBy: Int?,
    @SerializedName("delivery_amount")
    val deliveryAmount: Int?,
    @SerializedName("delivery_etd")
    val deliveryEtd: String?,
    @SerializedName("delivery_id")
    val deliveryId: String?,
    @SerializedName("delivery_service")
    val deliveryService: String?,
    @SerializedName("delivery_status")
    val deliveryStatus: String = "",
    @SerializedName("id")
    val id: Int?,
    @SerializedName("invoice_number")
    val invoiceNumber: String?,
    @SerializedName("payment_method")
    val paymentMethod: String = "",
    @SerializedName("payment_status")
    val paymentStatus: String = "",
    @SerializedName("payment_to")
    val paymentTo: String = "",
    @SerializedName("payment_url")
    val paymentUrl: String = "",
    @SerializedName("settlement_time")
    val settlementTime: Any?,
    @SerializedName("snap_token")
    val snapToken: String?,
    @SerializedName("status")
    val status: Int?,
    @SerializedName("total")
    val total: Int?,
    @SerializedName("total_amount")
    val totalAmount: Int?,
    @SerializedName("total_qty")
    val totalQty: Int?,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("updated_by")
    val updatedBy: Int?
)