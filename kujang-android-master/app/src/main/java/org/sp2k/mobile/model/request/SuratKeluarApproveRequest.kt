package org.sp2k.mobile.model.request

data class SuratKeluarApproveRequest(
    val id_surat: String = "",
    val status_approve: String = "",
    val alasan: String = "",
)