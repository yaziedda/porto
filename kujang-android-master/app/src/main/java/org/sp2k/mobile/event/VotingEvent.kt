package org.sp2k.mobile.event

open class VotingEvent(val value: String) {
    companion object {
        const val KEY = "KEY"
    }
}