package org.sp2k.mobile.screens.mail.`in`

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityInComingMailBinding
import org.sp2k.mobile.databinding.ItemInComingMailBinding
import org.sp2k.mobile.event.SuratEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.Mail
import org.sp2k.mobile.screens.mail.out.OutComingMailDetailActivity
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.SuratMainViewModel

@AndroidEntryPoint
class InComingMailActivity : BaseActivity<ActivityInComingMailBinding>() {

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter<Mail, ItemInComingMailBinding>
    val viewModel by viewModels<SuratMainViewModel>()

    override fun getLayoutId(): Int = R.layout.activity_in_coming_mail

    override fun ActivityInComingMailBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupUI()
        setupObserver()
        fetchData()
    }

    private fun ActivityInComingMailBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        emptyState.gone()
                        activityInComingMailRecycler.gone()
                        containerLoading.visible()
                    } else {
                        activityInComingMailRecycler.visible()
                        containerLoading.gone()
                    }
                }
            }

            inComingMailList.observe {
                this?.let {
                    recyclerViewAdapter.updateList(it)
                    if (it.isEmpty()) {
                        activityInComingMailRecycler.gone()
                        emptyState.visible()
                    } else {
                        activityInComingMailRecycler.visible()
                        emptyState.gone()
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityInComingMailBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "Surat Masuk"
        }
    }

    private fun ActivityInComingMailBinding.setupUI() {
        StatusBar.setLightStatusBar(window)
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetchData()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityInComingMailBinding.setupAdapter() {
        recyclerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_in_coming_mail,
            BR.mail
        ) { itemView, itemModel ->

            itemView.itemInComingMailTvTitle.text = itemModel.perihal_surat
            itemView.itemInComingMailDate.text = itemModel.created_at

            if (itemModel.buttonVisible == true) {
                itemView.itemInComingMailStatus.apply {
                    visible()
                    text = itemModel.status
                    itemModel.status?.apply {
                        when {
                            startsWith(Constants.MAIL_WAITING) -> {
                                text = "MENUNGGU"
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.mail_waiting
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_waiting
                                )
                            }
                            startsWith(Constants.MAIL_DISPOSISI) -> {
                                text = "DISPOSISI"
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.white
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_approved
                                )
                            }
                            startsWith(Constants.MAIL_REJECT) -> {
                                text = "DITOLAK"
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.mail_rejected
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_rejected
                                )
                            }
                        }
                    }
                }
            } else {
                itemView.itemInComingMailStatus.gone()
            }

            if (itemModel.type != null) {
                if (itemModel.type == "MASUK") {
                    itemView.itemInComingMailType.text = "Surat Masuk"
                    itemView.itemInComingMailTvSubTitle.text = "Asal Surat : ${itemModel.asal_surat}"
                } else if (itemModel.type == "KELUAR") {
                    itemView.itemInComingMailType.text = "Surat Masuk Internal"
                    itemView.itemInComingMailTvSubTitle.text = "Kepada : ${itemModel.asal_surat}"

                }
            } else {
                itemView.itemInComingMailType.text = "Surat Masuk"
            }

            itemView.root.setOnClickListener {
                if (itemModel.type != null) {
                    when (itemModel.type) {
                        "MASUK" -> {
                            goToInInCOmingMailActivity(itemModel)
                        }
                        "KELUAR" -> {
                            goToOutComingMail(itemModel)
                        }
                        else -> {
                            goToInInCOmingMailActivity(itemModel)
                        }
                    }
                } else {
                    goToInInCOmingMailActivity(itemModel)
                }
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            this@InComingMailActivity,
            LinearLayoutManager.VERTICAL,
            false
        )

        activityInComingMailRecycler.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerViewAdapter
        }
    }

    private fun goToOutComingMail(itemModel: Mail) {
        startActivity(
            Intent(
                applicationContext,
                OutComingMailDetailActivity::class.java
            ).apply {
                putExtra(OutComingMailDetailActivity.ID, itemModel.id.toString())
            })
    }

    private fun goToInInCOmingMailActivity(itemModel: Mail) {
        startActivity(
            Intent(
                applicationContext,
                InComingMailDetailActivity::class.java
            ).apply {
                putExtra(InComingMailDetailActivity.ID, itemModel.id.toString())
            })
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: SuratEvent?) {
        fetchData()
    }

    private fun fetchData() {
        viewModel.fetchInComingMail()
    }
}