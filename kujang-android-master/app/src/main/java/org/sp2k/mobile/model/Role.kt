package org.sp2k.mobile.model

data class Role(
    val id: String = "",
    val role_code: String = "",
    val role_nama: String = "",
)
