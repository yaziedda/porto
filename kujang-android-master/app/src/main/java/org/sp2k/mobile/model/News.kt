package org.sp2k.mobile.model

import org.sp2k.mobile.model.news.NewsCategory

data class News(
    val kategori: NewsCategory,
    val berita: Berita,
)