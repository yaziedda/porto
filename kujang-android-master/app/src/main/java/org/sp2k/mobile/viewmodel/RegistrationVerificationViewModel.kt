package org.sp2k.mobile.viewmodel

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import kotlinx.coroutines.launch
import kotlin.collections.HashMap

class RegistrationVerificationViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _startCountDown: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val startCountDown: MutableLiveData<Boolean>
        get() = _startCountDown

    private val _staffData: MutableLiveData<Staff?> by lazy {
        MutableLiveData(null)
    }
    val staffData: MutableLiveData<Staff?>
        get() = _staffData

    private val _isValid = MutableLiveData<Boolean>()
    val isValid: LiveData<Boolean>
        get() = _isValid

    private val _otpIsValid = MutableLiveData<Boolean>()
    val otpIsValid: LiveData<Boolean>
        get() = _otpIsValid

    fun resetState() {
        _staffData.value = null
        setError(null)
        _startCountDown.value = false
    }

    fun sendVerification(context: Context) = viewModelScope.launch {
        loading.value = true
        try {
//            val response = remoteRepository.sendMail()
//            if (response.state) {
//                val userData = mappingObject(response.data, UserToken::class.java)
//                PreferenceManager(context).setToken(userData.token)
//                _startCountDown.value = true
//                _userData.value = userData.user
//            } else {
//                _userData.value = null
//                showError(GeneralException(response.message))
//            }
            _loading.value = false
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun validateOtp(context: Context, otp: String) = viewModelScope.launch {
        loading.value = true
        try {
            val map = HashMap<String, String?>()
            map["otp"] = otp
//            val response = remoteRepository.registrationVerifyOTP(map)
//            if (response.state) {
//                val userData = mappingObject(response.data, UserToken::class.java)
//                PreferenceManager(context).setToken(userData.token)
//                getProfile(context)
//            } else {
//                _userData.value = null
//                showError(GeneralException(response.message))
//                _otpIsValid.value = false
//            }
//            _loading.value = false
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    private suspend fun getProfile(context: Context) {
        try {
//            val response = remoteRepository.profile()
//            if (response.state) {
//                val userData = mappingObject(response.data, User::class.java)
//                PreferenceManager(context).saveUser(userData)
//                PreferenceManager(context).saveLogin(true)
//                _userData.value = userData
//                _otpIsValid.value = true
//            } else {
//                _userData.value = null
//                showError(GeneralException(response.message))
//                _otpIsValid.value = false
//            }
            _loading.value = false
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

}