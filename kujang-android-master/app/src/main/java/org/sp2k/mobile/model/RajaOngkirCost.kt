package org.sp2k.mobile.model


import com.google.gson.annotations.SerializedName
import org.sp2k.mobile.extensions.toCurrency

data class RajaOngkirCost(
    @SerializedName("cost")
    val rajaOngkirCost: RajaOngkirCostX?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("service")
    val service: String?,
    var courier: String = ""
) {
    override fun toString(): String {
        return courier+"-"+service+"\n"+rajaOngkirCost?.value.toString().toCurrency()+"\nEstimasi : "+rajaOngkirCost?.etd
    }
}