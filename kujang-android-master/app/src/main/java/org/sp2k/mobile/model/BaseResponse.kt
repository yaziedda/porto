package org.sp2k.mobile.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class BaseResponse<T>(

    @field:SerializedName("state")
    val state: Boolean = false,
    @field:SerializedName("code")
    val code: Int = 0,
    @field:SerializedName("message")
    val message: String = "",
    @field:SerializedName("data")
    val data: T? = null,
)