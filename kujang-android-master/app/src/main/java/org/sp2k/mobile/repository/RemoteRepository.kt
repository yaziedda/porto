package org.sp2k.mobile.repository

import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.sp2k.mobile.model.*
import org.sp2k.mobile.model.blog.Blog
import org.sp2k.mobile.model.company.CompanyData
import org.sp2k.mobile.model.donation.Donation
import org.sp2k.mobile.model.news.NewsComment
import org.sp2k.mobile.model.news.NewsCommentData
import org.sp2k.mobile.model.polling.Polling
import org.sp2k.mobile.model.polling.PollingRequest
import org.sp2k.mobile.model.prokeranggaran.ProkerData
import org.sp2k.mobile.model.request.DisposisiRequest
import org.sp2k.mobile.model.request.ProkerApproval
import org.sp2k.mobile.model.request.SuratKeluarApproveRequest
import org.sp2k.mobile.model.request.SuratKeluarRejectRequest
import org.sp2k.mobile.model.user.AuthData
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.model.voting.VotingRes
import javax.inject.Inject

class RemoteRepository @Inject constructor(
    private val remoteRepositoryDao: RemoteRepositoryDao
) : RemoteRepositoryDao {
    override suspend fun getHome(): BaseResponse<Home> {
        return remoteRepositoryDao.getHome()
    }

    override suspend fun getDonation(): BaseResponse<List<Donation>> {
        return remoteRepositoryDao.getDonation()
    }

    override suspend fun getDonationById(id: String): BaseResponse<Donation> {
        return remoteRepositoryDao.getDonationById(id)
    }

    override suspend fun sendDonasi(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryDao.sendDonasi(map)
    }

    override suspend fun getProducts(): BaseResponse<List<Product>> {
        return remoteRepositoryDao.getProducts()
    }

    override suspend fun getProductById(id: String): BaseResponse<Product> {
        return remoteRepositoryDao.getProductById(id)
    }

    override suspend fun addToCart(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryDao.addToCart(map)
    }

    override suspend fun getCart(): BaseResponse<List<Cart>> {
        return remoteRepositoryDao.getCart()
    }

    override suspend fun cartUnchecked(id: String, status: String): BaseResponse<String> {
        return remoteRepositoryDao.cartUnchecked(id, status)
    }

    override suspend fun cartUpdateQty(id: String, qty: String): BaseResponse<String> {
        return remoteRepositoryDao.cartUpdateQty(id, qty)
    }

    override suspend fun province(): BaseResponse<List<Province>> {
        return remoteRepositoryDao.province()
    }

    override suspend fun city(id: String): BaseResponse<List<City>> {
        return remoteRepositoryDao.city(id)
    }

    override suspend fun cost(map: HashMap<String, String?>?): BaseResponse<RajaOngkir> {
        return remoteRepositoryDao.cost(map)
    }

    override suspend fun checkout(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryDao.checkout(map)
    }

    override suspend fun history(): BaseResponse<List<TransactionHistory>> {
        return remoteRepositoryDao.history()
    }

    override suspend fun blog(): BaseResponse<List<Blog>> {
        return remoteRepositoryDao.blog()
    }

    override suspend fun jasa(): BaseResponse<List<Jasa>> {
        return remoteRepositoryDao.jasa()
    }

    override suspend fun jasaById(id: String): BaseResponse<Jasa> {
        return remoteRepositoryDao.jasaById(id)
    }

    override suspend fun tokenUpdate(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryDao.tokenUpdate(map)
    }

    override suspend fun login(map: HashMap<String, String>): BaseResponse<AuthData> {
        return remoteRepositoryDao.login(map)
    }

    override suspend fun news(): BaseResponse<List<News>> {
        return remoteRepositoryDao.news()
    }

    override suspend fun company(): BaseResponse<CompanyData> {
        return remoteRepositoryDao.company()
    }

    override suspend fun newsById(id: String): BaseResponse<NewsDetail> {
        return remoteRepositoryDao.newsById(id)
    }

    override suspend fun newsCommentById(id: String, page: Int): BaseResponse<NewsCommentData> {
        return remoteRepositoryDao.newsCommentById(id, page)
    }

    override suspend fun newsCommentSend(map: HashMap<String, String>): BaseResponse<NewsComment> {
        return remoteRepositoryDao.newsCommentSend(map)
    }

    override suspend fun search(search: String): BaseResponse<List<Staff>> {
        return remoteRepositoryDao.search(search)
    }

    override suspend fun groups(): BaseResponse<List<GroupMail>> {
        return remoteRepositoryDao.groups()
    }

    override suspend fun suratMasuk(): BaseResponse<List<Mail>> {
        return remoteRepositoryDao.suratMasuk()
    }

    override suspend fun disposisi(disposisiRequest: DisposisiRequest): BaseResponse<Any> {
        return remoteRepositoryDao.disposisi(disposisiRequest)
    }

    override suspend fun suratKeluar(): BaseResponse<List<SuratKeluar>> {
        return remoteRepositoryDao.suratKeluar()
    }

    override suspend fun approveSuratKeluar(suratKeluarApproveRequest: SuratKeluarApproveRequest): BaseResponse<Any> {
        return remoteRepositoryDao.approveSuratKeluar(suratKeluarApproveRequest)
    }

    override suspend fun disposisiKeluar(disposisiRequest: DisposisiRequest): BaseResponse<Any> {
        return remoteRepositoryDao.disposisiKeluar(disposisiRequest)
    }

    override suspend fun rejectSuratKeluar(suratKeluarRejectRequest: SuratKeluarRejectRequest): BaseResponse<Any> {
        return remoteRepositoryDao.rejectSuratKeluar(suratKeluarRejectRequest)
    }

    override suspend fun deleteCommentById(id: String): BaseResponse<Any> {
        return remoteRepositoryDao.deleteCommentById(id)
    }

    override suspend fun prokerList(type: String): BaseResponse<ProkerResponse> {
        return remoteRepositoryDao.prokerList(type)
    }

    override suspend fun prokerById(id: String): BaseResponse<ProkerData> {
        return remoteRepositoryDao.prokerById(id)
    }

    override suspend fun prokerApproval(prokerApproval: ProkerApproval): BaseResponse<Any> {
        return remoteRepositoryDao.prokerApproval(prokerApproval)
    }

    override suspend fun anggaran(): BaseResponse<AnggaranBidang> {
        return remoteRepositoryDao.anggaran()
    }

    override suspend fun pollingList(): BaseResponse<List<Polling>> {
        return remoteRepositoryDao.pollingList()
    }

    override suspend fun votingList(): BaseResponse<VotingRes> {
        return remoteRepositoryDao.votingList()
    }

    override suspend fun pollingVote(pollingRequest: PollingRequest): BaseResponse<Any> {
        return remoteRepositoryDao.pollingVote(pollingRequest)
    }

    override suspend fun voteVote(status: RequestBody?, id: RequestBody?, file: MultipartBody.Part?): BaseResponse<Any> {
        return remoteRepositoryDao.voteVote(status, id, file)
    }

    override suspend fun changeProfile(file: MultipartBody.Part?): BaseResponse<String> {
        return remoteRepositoryDao.changeProfile(file)
    }

    override suspend fun changePassword(changePasswordRequest: ChangePasswordRequest): BaseResponse<Any> {
        return remoteRepositoryDao.changePassword(changePasswordRequest)
    }

    override suspend fun suratMasukByID(id: String): BaseResponse<Mail> {
        return remoteRepositoryDao.suratMasukByID(id)
    }

    override suspend fun suratKeluarByID(id: String): BaseResponse<SuratKeluar> {
        return remoteRepositoryDao.suratKeluarByID(id)
    }

    override suspend fun notificationList(): BaseResponse<List<KujangNotification>> {
        return remoteRepositoryDao.notificationList()
    }

}