package org.sp2k.mobile.utils

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class ResultRetrofit<out R> {
    
    data class Success<out T>(val data: T) : ResultRetrofit<T>()
    data class Error(val exception: Exception) : ResultRetrofit<Nothing>()
    object Loading : ResultRetrofit<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
            Loading -> "Loading"
        }
    }
}

/**
 * `true` if [Result] is of type [Success] & holds non-null [Success.data].
 */
val ResultRetrofit<*>.succeeded
    get() = this is ResultRetrofit.Success && data != null