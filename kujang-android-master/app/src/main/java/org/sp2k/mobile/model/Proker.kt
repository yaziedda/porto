package org.sp2k.mobile.model

data class Proker(
    val alasan: String,
    val anggaran: Int,
    val anggaran_file: String,
    val anggaran_send_status: Int,
    val approval_id: Int,
    val approval_status: Int,
    val created_at: String,
    val created_by: Int,
    val date: String,
    val `file`: String,
    val id: Int,
    val id_rab_proker: Int,
    val status: Int,
    val status_anggaran: Int,
    val status_anggaran_alasan: String,
    val status_code: String,
    val tanggal_pelaksanaan_end: String,
    val tanggal_pelaksanaan_start: String,
    val updated_at: String,
    val updated_by: Int
)