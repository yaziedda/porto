package org.sp2k.mobile.screens.polling

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.LinearLayout
import androidx.activity.viewModels
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.customview.customView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityPollingBinding
import org.sp2k.mobile.databinding.ItemPollingBinding
import org.sp2k.mobile.event.PollingEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.polling.Polling
import org.sp2k.mobile.model.polling.PollingRequest
import org.sp2k.mobile.model.request.SuratKeluarApproveRequest
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.DateUtils
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.VotePollViewModel
import org.sufficientlysecure.htmltextview.HtmlTextView
import kotlin.math.roundToInt

@AndroidEntryPoint
class PollingActivity : BaseActivity<ActivityPollingBinding>() {

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter<Polling, ItemPollingBinding>
    val viewModel by viewModels<VotePollViewModel>()

    override fun getLayoutId(): Int = R.layout.activity_polling

    override fun ActivityPollingBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupUI()
        setupObserver()
        fetchData()
    }

    private fun ActivityPollingBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        emptyState.gone()
                        recyclerView.gone()
                        containerLoading.visible()
                    } else {
                        recyclerView.visible()
                        containerLoading.gone()
                    }
                }
            }

            pollingList.observe {
                this?.let {
                    recyclerViewAdapter.updateList(it)
                    if (it.isNotEmpty()) {
                        recyclerView.visible()
                        emptyState.gone()
                    } else {
                        recyclerView.gone()
                        emptyState.visible()
                    }
                }
            }

            isSubmited.observe {
                this?.let {
                    if (it) {
                        showMessage("Anda berhasil melakukan polling")
                        fetchData()
                    }
                }
            }

            error.observe {
                showError(this) {
                    finish()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityPollingBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "E-Polling"
        }
    }

    private fun ActivityPollingBinding.setupUI() {
        StatusBar.setLightStatusBar(window)
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetchData()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityPollingBinding.setupAdapter() {
        recyclerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_polling,
            BR.polling
        ) { itemView, itemModel ->

            val startMillis =
                DateUtils.parseDate(itemModel.tgl_start, Constants.yyyy_MM_dd)?.time ?: 0
            val endMillis = DateUtils.parseDate(itemModel.tgl_end, Constants.yyyy_MM_dd)?.time ?: 0
            val isLive = DateUtils.getDateIsLive(startMillis.toString(), endMillis)
            itemView.itemPollingContent.apply {
                setHtml(itemModel.deskripsi)
                setOnClickATagListener { _, _, href ->
                    goToWebViewActivity(href ?: "")
                    return@setOnClickATagListener true
                }
                setOnClickListener {
                    sendEvent(itemModel, isLive)
                }
            }
            itemView.itemPollingTitle.text = "E-POLLING : ${itemModel.tentang}"
            itemView.itemPollingTime.text = "Berakhir Pada ${itemModel.tgl_end}"
            itemView.itemPollingCount.text = "${itemModel.count} orang"
            itemView.itemPollingDate.text = "Mulai : ${itemModel.tgl_start}"


            if (isLive) {
                itemView.itemPollingStatus.apply {
                    text = "Berjalan"
                    setTextColor(ContextCompat.getColor(applicationContext, R.color.mail_waiting))
                    background =
                        ContextCompat.getDrawable(applicationContext, R.drawable.btn_mail_waiting)
                }
            } else {
                itemView.itemPollingStatus.apply {
                    text = "Selesai"
                    setTextColor(ContextCompat.getColor(applicationContext, R.color.white))
                    background =
                        ContextCompat.getDrawable(applicationContext, R.drawable.btn_mail_approved)
                }
            }

            if (!isLive) {
                itemView.itemPollingYesPercentageWrapper.visible()
                itemView.itemPollingNoPercentageWrapper.visible()
                itemView.itemPollingYesPercentageProgress.progress =
                    itemModel.percentage_yes?.toDouble()?.roundToInt() ?: 0
                itemView.itemPollingYesPercentageCount.text = "${itemModel.percentage_yes} %"
                itemView.itemPollingNoPercentageProgress.progress =
                    itemModel.percentage_no?.toDouble()?.roundToInt() ?: 0
                itemView.itemPollingNoPercentageCount.text = "${itemModel.percentage_no} %"
            } else {
                itemView.itemPollingYesPercentageWrapper.gone()
                itemView.itemPollingNoPercentageWrapper.gone()
            }

            itemView.itemPollingWrapper.setOnClickListener {
                sendEvent(itemModel, isLive)
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            this@PollingActivity,
            LinearLayoutManager.VERTICAL,
            false
        )

        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerViewAdapter
        }
    }

    private fun sendEvent(itemModel: Polling, isLive: Boolean) {
        val pollingRequest = PollingRequest(
            id = itemModel.id.toString()
        )

        val dialog = MaterialDialog(
            this@PollingActivity,
            BottomSheet(LayoutMode.WRAP_CONTENT)
        )
            .customView(
                R.layout.dialog_confirmation_polling,
                scrollable = false
            )
            .cornerRadius(this@PollingActivity.resources.getDimension(R.dimen.card_corner_radius_normal))

        dialog.findViewById<AppCompatTextView>(R.id.dialog_finish_quiz_info_title)
            .apply {
                if (title?.isNotEmpty() == true) {
                    visible()
                    text = "E-polling : ${itemModel.tentang}"
                } else {
                    gone()
                }
            }

        dialog.findViewById<HtmlTextView>(R.id.dialog_finish_quiz_info).apply {
            setHtml(itemModel.deskripsi)
            setOnClickATagListener { _, _, href ->
                goToWebViewActivity(href ?: "")
                return@setOnClickATagListener true
            }
        }
        dialog.findViewById<AppCompatButton>(R.id.dialog_finish_quiz_button_yes)
            .apply {
                text = "SETUJU"
                setOnClickListener {
                    dialog.dismiss()
                    pollingRequest.status = "1"
                    viewModel.pollingPost(pollingRequest)
                }
            }
        dialog.findViewById<AppCompatButton>(R.id.dialog_finish_quiz_button_no)
            .apply {
                text = "TIDAK SETUJU"
                setOnClickListener {
                    dialog.dismiss()

                    val dialogAlasan = MaterialDialog(
                        this@PollingActivity,
                        BottomSheet(LayoutMode.WRAP_CONTENT)
                    )
                        .customView(R.layout.dialog_confirmation_input, scrollable = true)
                        .cornerRadius(resources.getDimension(R.dimen.card_corner_radius_normal))

                    val editText =
                        dialogAlasan.findViewById<AppCompatEditText>(R.id.dialog_edit_text)
                    val btnYes =
                        dialogAlasan.findViewById<AppCompatButton>(R.id.dialog_button_yes)
                    btnYes.setOnClickListener {
                        val text = editText.text.toString()
                        if (text.isEmpty()) {
                            showMessage("Alasan harus diisi!")
                            return@setOnClickListener
                        }

                        pollingRequest.status = "0"
                        pollingRequest.alasan = text
                        viewModel.pollingPost(pollingRequest)
                        dialogAlasan.dismiss()
                    }
                    dialogAlasan.show()
                }
            }

        dialog.findViewById<AppCompatImageView>(R.id.dialog_finish_quiz_close)
            .apply {
                setOnClickListener {
                    dialog.dismiss()
                }
            }
        dialog.show()
        dialog.findViewById<LinearLayout>(R.id.dialog_finish_quiz_button_wrapper).gone()
        if (isLive) {
            if (!itemModel.hasVoted) {
                dialog.findViewById<LinearLayout>(R.id.dialog_finish_quiz_button_wrapper)
                    .visible()
            } else {
                showDialog("Anda telah melakukan polling")
            }
        }
    }


    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: PollingEvent?) {
        fetchData()
    }

    private fun fetchData() {
        viewModel.pollingList()
    }
}