package org.sp2k.mobile.model

data class AnggaranBidang(
    val anggaran: List<Anggaran>,
    val saldo: Int
)