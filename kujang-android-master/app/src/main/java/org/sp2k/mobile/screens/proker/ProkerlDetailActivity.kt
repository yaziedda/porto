package org.sp2k.mobile.screens.proker

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.customview.customView
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityProkerDetailBinding
import org.sp2k.mobile.event.ProkerEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.showToast
import org.sp2k.mobile.extensions.toCurrency
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.prokeranggaran.ProkerData
import org.sp2k.mobile.model.request.ProkerApproval
import org.sp2k.mobile.screens.proker.StatusUseCase.Companion.constructPokerStatus
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.viewmodel.ProkerViewModel


@AndroidEntryPoint
class ProkerlDetailActivity : BaseActivity<ActivityProkerDetailBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_proker_detail

    val viewModel by viewModels<ProkerViewModel>()
    private var idUsulan: String? = ""

    companion object {
        const val ID = "ID"
    }

    override fun ActivityProkerDetailBinding.initializeView(savedInstanceState: Bundle?) {
        getExtras()
        setupActionBar()
        viewModel.apply {
            loading.observe {
                this?.let {
                    if (it) {
                        activityProkerDetailWrapper.gone()
                        activityProkerDetailDetailConfirmation.gone()
                        containerLoading.visible()
                    } else {
                        containerLoading.gone()
                    }
                }
            }
            isSubmited.observe {
                if (this) {
                    showToast("Berhasil Update")
                    EventBus.getDefault().post(ProkerEvent(ProkerEvent.KEY))
                }
            }

            prokerData.observe {
                this?.let {
                    activityProkerDetailWrapper.visible()
                    loadData(it)
                }
            }

            error.observe {
                showError(this) {
                    finish()
                }
            }
        }
        viewModel.fetchById(idUsulan ?: "")
    }

    private fun getExtras() {
        idUsulan = intent?.extras?.getString(ID)
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityProkerDetailBinding.loadData(prokerData: ProkerData) {
        val roleCode = viewModel.getUser()?.role?.role_code
        activityProkerDetailDetailConfirmation.gone()
        proposalAnggaranBuktiTransferWrapper.gone()
        expandableLayout.isExpanded = true
        expandableWrapperTitle.gone()
        activityProkerDetailCarousel.gone()
        htmlDetailContent.gone()
        expandableWrapperTitle.setOnClickListener {
            expandableLayout.isExpanded = !expandableLayout.isExpanded
            if (expandableLayout.isExpanded) {
                exandableArrow.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24)
            } else {
                exandableArrow.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24)
            }

        }
        prokerData.let {
            prokerNama.text = it.usulan?.name
            prokerBidang.text = it.bidang?.name
            prokerKetua.text = it.usulan?.keplak_name
            if (it.proker?.id != null && it.proker.id > 0) {
                prokerTanggalWrapper.visible()
                prokerTanggal.apply {
                    visible()
                    text =
                        "${it.proker.tanggal_pelaksanaan_start} - ${it.proker.tanggal_pelaksanaan_end}"
                }
                if (it.proker.status_code?.startsWith("N") == true) {
                    prokerAlasanTitle.visible()
                    prokerAlasan.apply {
                        visible()
                        text = it.proker.alasan
                    }
                }
                if (roleCode in Constants.CAN_SHOW_ANGGARAN) {
                    proposalWrapper.visible()
                    proposalWrapperLine.visible()
                    proposalAnggaran.text = it.proker.anggaran.toString().toCurrency()

                    val url = it.proker?.file ?: ""
                    proposalDokumentasi.setOnClickListener { _ ->
                        goToWebViewActivity(url, it.usulan?.name ?: "")
                    }
                } else {
                    proposalWrapper.gone()
                    proposalWrapperLine.gone()
                }
                prokerStatusTitle.text = "Status Proker"
            } else {
                prokerTanggalWrapper.gone()
                prokerTanggal.gone()
            }

            if (it.lpj?.id != null && it.lpj.id > 0) {
                lpjAnggaran.text = it.lpj.anggaran_terpakai.toString().toCurrency()
                if (it.lpj.anggaran_sisa ?: 0 > 0) {
                    lpjAnggaranSisaWrapper.visible()
                    lpjAnggaranSisa.text = it.lpj.anggaran_sisa.toString().toCurrency()
                    lpjAnggaranSisaBuktiTransfer.setOnClickListener { _ ->
                        goToWebViewActivity(it.lpj.anggaran_sisa_document ?: "")
                    }
                } else {
                    lpjAnggaranSisaWrapper.gone()
                }
                if (roleCode in Constants.CAN_SHOW_ANGGARAN) {
                    lpjWrapper.visible()
                    lpjWrapperLine.visible()
                    lpjKwitansiWrapper.visible()
                    lpjDokumentasiTitle.text = "Dokument LPJ"
                    lpjDokumentasi.setOnClickListener { _ ->
                        goToWebViewActivity(it.lpj.file ?: "", it.usulan?.name ?: "")
                    }
                    if (it.lpj.approval_status == 1) {
                        setApprovedLPJNews(it)
                    }
                    kwitansiWrapperLine.visible()
                    kwitansiContainer.visible()
                    lpjKwitansiWrapper.removeAllViews()
                    it.image_kwitansi.forEach { kwitansi ->
                        val inflater =
                            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                        val rowView: View = inflater.inflate(R.layout.view_lpj_kwitansi, null)
                        val text = rowView.findViewById<AppCompatTextView>(R.id.lpj_dokumentasi)
                        text.setOnClickListener {
                            goToWebViewActivity(kwitansi)
                        }
                        lpjKwitansiWrapper.addView(rowView, lpjKwitansiWrapper.childCount - 1)
                    }
                } else {
                    if (it.lpj.approval_status == 1) {
                        setApprovedLPJNews(it)
                        if (roleCode in Constants.CAN_SHOW_ANGGARAN) {
                            lpjWrapper.visible()
                            lpjAnggaranWrapper.visible()
                            lpjWrapperLine.visible()
                            lpjAnggaranSisaWrapper.visible()
                        } else {
                            lpjWrapper.visible()
                            lpjAnggaranWrapper.gone()
                            lpjWrapperLine.gone()
                            lpjAnggaranSisaWrapper.gone()
                        }
                    } else {
                        lpjWrapper.gone()
                        lpjKwitansiWrapper.gone()
                        lpjWrapperLine.gone()
                        lpjAnggaranSisaWrapper.gone()
                    }
                    lpjDokumentasiTitleWrapper.gone()
                    lpjDokumentasiTitle.text = "Dokumentasi"
                    lpjDokumentasi.setOnClickListener { _ ->
                        goToWebViewActivity(it.lpj.link_dokumentasi ?: "", it.usulan?.name ?: "")
                    }
                }
                prokerStatusTitle.text = "Status LPJ"

                if (it.lpj.status_code?.startsWith("N") == true) {
                    prokerAlasanTitle.visible()
                    prokerAlasan.apply {
                        visible()
                        text = it.lpj.alasan
                    }
                }
            } else {
                lpjWrapper.gone()
                lpjWrapperLine.gone()
            }
            prokerDeskripsi.setHtml(it.usulan?.description ?: "")

            prokerStatus.apply {
                constructPokerStatus(
                    viewModel.getUser()?.role?.role_code ?: "",
                    applicationContext,
                    it,
                    prokerStatusA
                )
            }
        }
        val prokerApproval = ProkerApproval()
        prokerApproval.id = prokerData?.proker?.id.toString()
        when (roleCode) {
            "KETUA_UMUM", "SEKUM" -> {
                if (prokerData?.proker?.status_anggaran == 1) {
                    proposalAnggaranBuktiTransferWrapper.visible()
                    proposalAnggaranBuktiTransfer.setOnClickListener {
                        goToWebViewActivity(
                            prokerData?.proker?.anggaran_file ?: "",
                            "Bukti Transfer " + prokerData?.usulan?.name
                        )
                    }
                }

                if (prokerData?.proker?.approval_status == 0 && prokerData?.proker?.status_code == "A3") {
                    activityProkerDetailDetailConfirmation.visible()
                    activityProkerDetailDetailAccept.setOnClickListener {
                        showDialogOptionBottomSheet("Apakah anda yakin akan menyetujui?", {
                            prokerApproval.status = "1"
                            viewModel.prokerApproval(prokerApproval)
                        })
                    }
                    activityProkerDetailDetailReject.setOnClickListener {
                        showDialogOptionBottomSheet("Apakah anda yakin akan menolak?", {
                            prokerApproval.status = "-1"
                            val dialog = MaterialDialog(
                                this@ProkerlDetailActivity,
                                BottomSheet(LayoutMode.WRAP_CONTENT)
                            )
                                .customView(R.layout.dialog_confirmation_input, scrollable = true)
                                .cornerRadius(resources.getDimension(R.dimen.card_corner_radius_normal))

                            val editText =
                                dialog.findViewById<AppCompatEditText>(R.id.dialog_edit_text)
                            val btnYes =
                                dialog.findViewById<AppCompatButton>(R.id.dialog_button_yes)
                            btnYes.setOnClickListener {
                                val text = editText.text.toString()
                                if (text.isEmpty()) {
                                    showMessage("Alasan harus diisi!")
                                    return@setOnClickListener
                                }
                                prokerApproval.alasan = text
                                viewModel.prokerApproval(prokerApproval)
                                dialog.dismiss()
                            }
                            dialog.show()
                        })
                    }
                }

                if (prokerData?.proker?.approval_status == 1 && prokerData?.proker?.status_anggaran == 1) {
                    proposalAnggaranBuktiTransferWrapper.visible()
                    proposalAnggaranBuktiTransfer.setOnClickListener {
                        goToWebViewActivity(
                            prokerData?.proker?.anggaran_file ?: "",
                            "Bukti Transfer " + prokerData?.usulan?.name
                        )
                    }
                }

                // LJP SECTION

                if (prokerData?.lpj?.approval_status == 0 && prokerData?.lpj?.status_code == "A3") {
                    activityProkerDetailDetailConfirmation.visible()
                    activityProkerDetailDetailAccept.setOnClickListener {
                        showDialogOptionBottomSheet("Apakah anda yakin akan menyetujui?", {
                            prokerApproval.id = prokerData?.lpj?.id.toString()
                            prokerApproval.type_update = "lpj"
                            prokerApproval.status = "1"
                            viewModel.prokerApproval(prokerApproval)
                        })
                    }
                    activityProkerDetailDetailReject.setOnClickListener {
                        showDialogOptionBottomSheet("Apakah anda yakin akan menolak?", {
                            prokerApproval.id = prokerData?.lpj?.id.toString()
                            prokerApproval.type_update = "lpj"
                            prokerApproval.status = "-1"
                            val dialog = MaterialDialog(
                                this@ProkerlDetailActivity,
                                BottomSheet(LayoutMode.WRAP_CONTENT)
                            )
                                .customView(R.layout.dialog_confirmation_input, scrollable = true)
                                .cornerRadius(resources.getDimension(R.dimen.card_corner_radius_normal))

                            val editText =
                                dialog.findViewById<AppCompatEditText>(R.id.dialog_edit_text)
                            val btnYes =
                                dialog.findViewById<AppCompatButton>(R.id.dialog_button_yes)
                            btnYes.setOnClickListener {
                                val text = editText.text.toString()
                                if (text.isEmpty()) {
                                    showMessage("Alasan harus diisi!")
                                    return@setOnClickListener
                                }
                                prokerApproval.alasan = text
                                viewModel.prokerApproval(prokerApproval)
                                dialog.dismiss()
                            }
                            dialog.show()
                        })
                    }
                }
            }

            "KETUA_BIDANG" -> {
                if (prokerData.proker?.approval_status == 0 && prokerData.proker.status_code == "00") {
                    activityProkerDetailDetailConfirmation.visible()
                    activityProkerDetailDetailAccept.setOnClickListener {
                        showDialogOptionBottomSheet("Apakah anda yakin akan menyetujui?", {
                            prokerApproval.status = "1"
                            viewModel.prokerApproval(prokerApproval)
                        })
                    }
                    activityProkerDetailDetailReject.setOnClickListener {
                        showDialogOptionBottomSheet("Apakah anda yakin akan menolak?", {
                            prokerApproval.status = "-1"
                            val dialog = MaterialDialog(
                                this@ProkerlDetailActivity,
                                BottomSheet(LayoutMode.WRAP_CONTENT)
                            )
                                .customView(R.layout.dialog_confirmation_input, scrollable = true)
                                .cornerRadius(resources.getDimension(R.dimen.card_corner_radius_normal))

                            val editText =
                                dialog.findViewById<AppCompatEditText>(R.id.dialog_edit_text)
                            val btnYes =
                                dialog.findViewById<AppCompatButton>(R.id.dialog_button_yes)
                            btnYes.setOnClickListener {
                                val text = editText.text.toString()
                                if (text.isEmpty()) {
                                    showMessage("Alasan harus diisi!")
                                    return@setOnClickListener
                                }
                                prokerApproval.alasan = text
                                viewModel.prokerApproval(prokerApproval)
                                dialog.dismiss()
                            }
                            dialog.show()
                        })
                    }
                }

                if (prokerData.proker?.approval_status == 1 && prokerData.proker.status_anggaran == 2) {
                    activityProkerDetailDetailConfirmation.visible()
                    confirmButtonTitle.text = "Konfirmasi Bukti Transfer"
                    prokerApproval.type_update = "anggaran"
                    proposalAnggaranBuktiTransferWrapper.visible()
                    proposalAnggaranBuktiTransfer.setOnClickListener {
                        goToWebViewActivity(
                            prokerData.proker.anggaran_file ?: "",
                            "Bukti Transfer " + prokerData.usulan?.name
                        )
                    }
                    activityProkerDetailDetailConfirmation.visible()
                    activityProkerDetailDetailAccept.setOnClickListener {
                        showDialogOptionBottomSheet("Apakah anda yakin akan menyetujui transfer?", {
                            prokerApproval.status = "1"
                            viewModel.prokerApproval(prokerApproval)
                        })
                    }
                    activityProkerDetailDetailReject.setOnClickListener {
                        showDialogOptionBottomSheet("Apakah anda yakin akan menyetujui transfer?", {
                            prokerApproval.status = "-1"
                            val dialog = MaterialDialog(
                                this@ProkerlDetailActivity,
                                BottomSheet(LayoutMode.WRAP_CONTENT)
                            )
                                .customView(R.layout.dialog_confirmation_input, scrollable = true)
                                .cornerRadius(resources.getDimension(R.dimen.card_corner_radius_normal))

                            val editText =
                                dialog.findViewById<AppCompatEditText>(R.id.dialog_edit_text)
                            val btnYes =
                                dialog.findViewById<AppCompatButton>(R.id.dialog_button_yes)
                            btnYes.setOnClickListener {
                                val text = editText.text.toString()
                                if (text.isEmpty()) {
                                    showMessage("Alasan harus diisi!")
                                    return@setOnClickListener
                                }
                                prokerApproval.alasan = text
                                viewModel.prokerApproval(prokerApproval)
                                dialog.dismiss()
                            }
                            dialog.show()
                        })
                    }
                }

                if (prokerData?.proker?.approval_status == 1 && prokerData?.proker?.status_anggaran == 1) {
                    proposalAnggaranBuktiTransferWrapper.visible()
                    proposalAnggaranBuktiTransfer.setOnClickListener {
                        goToWebViewActivity(
                            prokerData?.proker?.anggaran_file ?: "",
                            "Bukti Transfer " + prokerData?.usulan?.name
                        )
                    }
                }

                // LPJ SECTION
                if (prokerData?.lpj?.approval_status == 0 && prokerData?.lpj?.status_code == "00") {
                    activityProkerDetailDetailConfirmation.visible()
                    activityProkerDetailDetailAccept.setOnClickListener {
                        showDialogOptionBottomSheet("Apakah anda yakin akan menyetujui?", {
                            prokerApproval.id = prokerData?.lpj?.id.toString()
                            prokerApproval.type_update = "lpj"
                            prokerApproval.status = "1"
                            viewModel.prokerApproval(prokerApproval)
                        })
                    }
                    activityProkerDetailDetailReject.setOnClickListener {
                        showDialogOptionBottomSheet("Apakah anda yakin akan menolak?", {
                            prokerApproval.id = prokerData.lpj.id.toString()
                            prokerApproval.type_update = "lpj"
                            prokerApproval.status = "-1"
                            val dialog = MaterialDialog(
                                this@ProkerlDetailActivity,
                                BottomSheet(LayoutMode.WRAP_CONTENT)
                            )
                                .customView(R.layout.dialog_confirmation_input, scrollable = true)
                                .cornerRadius(resources.getDimension(R.dimen.card_corner_radius_normal))

                            val editText =
                                dialog.findViewById<AppCompatEditText>(R.id.dialog_edit_text)
                            val btnYes =
                                dialog.findViewById<AppCompatButton>(R.id.dialog_button_yes)
                            btnYes.setOnClickListener {
                                val text = editText.text.toString()
                                if (text.isEmpty()) {
                                    showMessage("Alasan harus diisi!")
                                    return@setOnClickListener
                                }
                                prokerApproval.alasan = text
                                viewModel.prokerApproval(prokerApproval)
                                dialog.dismiss()
                            }
                            dialog.show()
                        })
                    }
                }

            }
        }
    }

    private fun ActivityProkerDetailBinding.setApprovedLPJNews(prokerData: ProkerData) {
        expandableWrapperTitle.visible()
        expandableLayout.isExpanded = false
        htmlDetailContent.visible()
        prokerData.lpj?.isi_berita?.let { htmlDetailContent.setHtml(it) }

        if (prokerData.image_dokumentasi.isNotEmpty()) {
            activityProkerDetailCarousel.visible()
            activityProkerDetailCarousel.setImageListener { position, imageView ->
                val url = prokerData.image_dokumentasi.getOrNull(position) ?: ""
                Glide.with(applicationContext)
                    .load(url)
                    .into(imageView)

                imageView.setOnClickListener {
                    goToShowImageActivity(url)
                }
            }
            activityProkerDetailCarousel.pageCount = prokerData.image_dokumentasi.size
        } else {
            activityProkerDetailCarousel.gone()
        }


    }

    @SuppressLint("SetTextI18n")
    private fun ActivityProkerDetailBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "Detail Program Kerja"
        }
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: ProkerEvent?) {
        finish()
    }
}