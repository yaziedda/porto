package org.sp2k.mobile.model

data class GroupMail(
    val code: String,
    val id: String,
    val name: String,
    var isChecked: Boolean = false
)