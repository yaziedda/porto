package org.sp2k.mobile.model.news

data class NewsCommentData(
    val data: List<NewsComment>?
)