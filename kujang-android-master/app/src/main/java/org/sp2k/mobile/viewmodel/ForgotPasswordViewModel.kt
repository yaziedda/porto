package org.sp2k.mobile.viewmodel

import android.text.Selection
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.material.textfield.TextInputEditText
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.extensions.afterTextChanged
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository

class ForgotPasswordViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private var validNIKFormat: Boolean = false
    private var onChangedDisabled: Boolean = false

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _startCountDown: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val startCountDown: MutableLiveData<Boolean>
        get() = _startCountDown

    private val _staffData: MutableLiveData<Staff?> by lazy {
        MutableLiveData(null)
    }
    val staffData: MutableLiveData<Staff?>
        get() = _staffData

    private val _nikValue = MutableLiveData<String>()
    val nikValue: MutableLiveData<String>
        get() = _nikValue
    private val _hideErrorNIKformat = MutableLiveData<Boolean>()
    val hideErrorNIKformat: LiveData<Boolean>
        get() = _hideErrorNIKformat
    private val _isValid = MutableLiveData<Boolean>()
    val isValid: LiveData<Boolean>
        get() = _isValid

    private val _otpIsValid = MutableLiveData<Boolean>()
    val otpIsValid: LiveData<Boolean>
        get() = _otpIsValid

    fun resetState() {
        _staffData.value = null
        setError(null)
        _startCountDown.value = false
    }

    fun attachInputFormat(
        nikInputEditText: TextInputEditText
    ) {
        nikInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _nikValue.value = this
                    setText(nikValue.value)
                    validNIKFormat = length() == 16
                    _hideErrorNIKformat.value = validNIKFormat
                    _isValid.value = validNIKFormat
                    Selection.setSelection(nikInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }
    }


}