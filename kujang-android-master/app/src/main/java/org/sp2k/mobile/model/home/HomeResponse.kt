package org.sp2k.mobile.model.home

import com.google.gson.annotations.SerializedName

data class HomeResponse (
    @field:SerializedName("state")
    val state: Boolean,

    @field:SerializedName("data")
    val data: Home,

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("message")
    val message: String
)