package org.sp2k.mobile.model


import com.google.gson.annotations.SerializedName

data class RajaOngkirCostX(
    @SerializedName("etd")
    val etd: String?,
    @SerializedName("note")
    val note: String?,
    @SerializedName("value")
    val value: Double = 0.0
)