package org.sp2k.mobile.utils

interface FragmentCallback<T> {
    fun result(result: T) {}
    fun action() {}
}