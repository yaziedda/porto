package org.sp2k.mobile.event

open class GoToMainPageEvent(val page: Int) {
    companion object {
        const val HOME = 0
        const val NEWS = 1
        const val COMPANY = 2
        const val NOTIFICATION = 3
        const val PROFILE = 4
        const val PROKER = 5
    }
}