package org.sp2k.mobile.model.user

import java.io.Serializable

data class Staff(
    val created_at: String = "",
    val id: Int = 0,
    val status: Int = 0,
    val updated_at: String = "",
    val admin_alamat: String = "",
    val admin_email: String = "",
    val admin_id: String = "",
    val admin_last_login: String = "",
    val admin_nama: String = "",
    val admin_nik: String = "",
    var admin_foto: String = "",
    val admin_telepon: String = "",
    val admin_uid_google: String? = "",
    val admin_username: String = "",
    val admin_verified: Int = 0,
    val admin_verified_at: String = "",
    val admin_group_code: String = "",
    val admin_group_name: String = "",
): Serializable