package org.sp2k.mobile.model

import com.google.gson.annotations.SerializedName
import org.sp2k.mobile.model.donation.Donation

data class Home(
    @field:SerializedName("news")
    val news: List<News> = arrayListOf()
)