package org.sp2k.mobile.model.polling

data class PollingRequest(
    var status: String = "",
    var alasan: String = "",
    val id: String,
)