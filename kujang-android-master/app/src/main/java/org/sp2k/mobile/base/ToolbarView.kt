package org.sp2k.mobile.base

interface ToolbarView {
    fun showToolbar(title: CharSequence)
}