package org.sp2k.mobile.screens

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityMainBinding
import org.sp2k.mobile.event.GoToMainPageEvent
import org.sp2k.mobile.extensions.showToast
import org.sp2k.mobile.model.KujangNotification
import org.sp2k.mobile.screens.company.CompanyFragment
import org.sp2k.mobile.screens.home.HomeFragment
import org.sp2k.mobile.screens.mail.`in`.InComingMailDetailActivity
import org.sp2k.mobile.screens.mail.out.OutComingMailDetailActivity
import org.sp2k.mobile.screens.news.NewsFragment
import org.sp2k.mobile.screens.notification.NotificationFragment
import org.sp2k.mobile.screens.polling.PollingActivity
import org.sp2k.mobile.screens.proker.ProkerlDetailActivity
import org.sp2k.mobile.screens.setting.SettingFragment
import org.sp2k.mobile.screens.voting.VotingActivity
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.MainViewModel

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    companion object {
        const val FRAGMENT_DESTINATION = "FRAGMENT_DESTINATION"
    }

    override fun getLayoutId(): Int = R.layout.activity_main
    private val viewModel by viewModels<MainViewModel>()
    private var currentNavController: LiveData<NavController>? = null
    private val labelUpdateTitle by lazy { getString(R.string.label_update_title) }
    private val labelUpdateDescription by lazy { getString(R.string.label_update_description) }

    override fun ActivityMainBinding.initializeView(savedInstanceState: Bundle?) {
        val typeNotification = intent?.extras?.getString(Constants.NOTIFICATION_TYPE) ?: ""
        val keyNotification = intent?.extras?.getString(Constants.NOTIFICATION_KEY) ?: ""
        if (typeNotification in arrayListOf(
                "lpj",
                "proposal",
                "anggaran",
                "anggaran_bidang",
                "voting",
                "polling",
                "surat_masuk",
                "surat_keluar"
            )
        ) {
            when (typeNotification) {
                "lpj" -> {
                    goToProker(keyNotification, typeNotification)
                }
                "proposal" -> {
                    goToProker(keyNotification, typeNotification)
                }
                "anggaran" -> {
                    goToProker(keyNotification, typeNotification)
                }
                "anggaran_bidang" -> {
                    goToProker(keyNotification, typeNotification)
                }
                "voting" -> {
                    startActivity(
                        Intent(applicationContext, VotingActivity::class.java)
                    )
                }
                "polling" -> {
                    startActivity(
                        Intent(applicationContext, PollingActivity::class.java)
                    )
                }
                "surat_masuk" -> {
                    startActivity(
                        Intent(applicationContext, InComingMailDetailActivity::class.java).apply {
                            putExtra(InComingMailDetailActivity.ID, keyNotification)
                        }
                    )
                }
                "surat_keluar" -> {
                    startActivity(
                        Intent(applicationContext, OutComingMailDetailActivity::class.java).apply {
                            putExtra(OutComingMailDetailActivity.ID, keyNotification)
                        }
                    )
                }
                else -> {

                }
            }

        }

        if (savedInstanceState == null) setupBottomNavigationBar()
        with(viewModel) {
            isNeedUpdate.observe {
                if (this) {
                    showDialog(
                        title = labelUpdateTitle,
                        message = labelUpdateDescription
                    ) {
                        val browserIntent =
                            Intent(Intent.ACTION_VIEW, Uri.parse(Constants.URL_PLAY_STORE))
                        startActivity(browserIntent)
                    }
                }
            }
            // TODO FORCE UPDATE
//            forceUpdate()
        }
        if (!viewModel.isAnonymous()) {
            FirebaseMessaging.getInstance().subscribeToTopic(viewModel.getEmail())
        }


        fabCompany.setOnClickListener {
            mainViewPager.currentItem = 2
        }

        FirebaseMessaging.getInstance().subscribeToTopic("3").addOnCompleteListener {
        }
        FirebaseMessaging.getInstance().subscribeToTopic(viewModel.getUser()?.group?.code ?: "")
            .addOnCompleteListener {
            }
    }

    private fun goToProker(keyNotification: String, typeNotification: String) {
        startActivity(
            Intent(applicationContext, ProkerlDetailActivity::class.java).apply {
                putExtra(Constants.NOTIFICATION_KEY, keyNotification)
                putExtra(Constants.NOTIFICATION_TYPE, typeNotification)
                putExtra(ProkerlDetailActivity.ID, keyNotification)
            }
        )
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        binding.apply {
            setupBottomNavigationBar()
        }
    }

    private fun ActivityMainBinding.setupBottomNavigationBar() {
        with(mainViewPager) {
            adapter = MainPagerAdapter(supportFragmentManager, this@MainActivity)
            mainBottomNav.setOnNavigationItemSelectedListener {
                when (it.itemId) {
                    R.id.home_nav -> {
                        currentItem = 0
                    }
                    R.id.news_nav -> {
                        currentItem = 1
                    }
                    R.id.company_nav -> {
                        currentItem = 2
                    }
                    R.id.notification_nav -> {
                        currentItem = 3
                    }
                    R.id.setting_nav -> {
                        currentItem = 4
                    }
                }
                true
            }

            addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    setStatusBarColor(position)
                }

                override fun onPageSelected(position: Int) {
                    with(mainBottomNav) {
                        when (position) {
                            0 -> {
                                selectedItemId = R.id.home_nav
                            }
                            1 -> {
                                selectedItemId = R.id.news_nav
                            }
                            2 -> {
                                selectedItemId = R.id.company_nav
                            }
                            3 -> {
                                selectedItemId = R.id.notification_nav
                            }
                            4 -> {
                                selectedItemId = R.id.setting_nav
                            }
                        }
                    }
                }

                override fun onPageScrollStateChanged(state: Int) {

                }
            })

            offscreenPageLimit = 4
        }
    }

    private fun setStatusBarColor(position: Int) {
        with(StatusBar) {
            when (position) {
                0 -> {
                    setStatusBarMainColor(window, R.color.colorPrimary)
                }
                1 -> {
                    setLightStatusBar(window)
                }
                2 -> {
                    setStatusBarMainColor(window, R.color.colorPrimary)
                }
                3 -> {
                    setLightStatusBar(window)
                }
                4 -> {
                    setLightStatusBar(window)
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return currentNavController?.value?.navigateUp() ?: false
    }

    private fun appInstalledOrNot(context: Context, uri: String): Boolean {
        val pm = context.packageManager
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
        }
        return false
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: GoToMainPageEvent?) {
        with(binding.mainViewPager) {
            event?.let {
                currentItem = it.page
            }
        }
    }

    var doubleBackToExitPressedOnce = false

    override fun onBackPressed() {
        with(binding.mainViewPager) {
            if (currentItem == 0) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed()
                    return
                }
                doubleBackToExitPressedOnce = true
                showToast("Please click BACK again to exit")
                Handler(Looper.getMainLooper()).postDelayed(
                    { doubleBackToExitPressedOnce = false },
                    2000
                )
            } else {
                currentItem = 0
            }
        }
    }
}

class MainPagerAdapter(fa: FragmentManager, val activity: MainActivity) :
    FragmentPagerAdapter(fa) {

    override fun getCount(): Int = 5

    override fun getItem(position: Int): Fragment {
        with(StatusBar) {
            return when (position) {
                0 -> {
                    setStatusBarMainColor(activity.window, R.color.colorPrimary)
                    HomeFragment()
                }
                1 -> {
                    setLightStatusBar(activity.window)
                    NewsFragment()
                }
                2 -> {
                    setStatusBarMainColor(activity.window, R.color.colorPrimary)
                    CompanyFragment()
                }
                3 -> {
                    setLightStatusBar(activity.window)
                    NotificationFragment()
                }
                4 -> {
                    setLightStatusBar(activity.window)
                    SettingFragment()
                }
                else -> Fragment()
            }
        }
    }
}