package org.sp2k.mobile.model.user

data class UserResponse(
    val `data`: Staff,
    val message: String,
    val state: Boolean,
    val token: String
)