package org.sp2k.mobile.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.exception.GeneralException
import org.sp2k.mobile.model.polling.Polling
import org.sp2k.mobile.model.polling.PollingRequest
import org.sp2k.mobile.model.voting.Voting
import org.sp2k.mobile.model.voting.VotingRes
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import org.sp2k.mobile.utils.PreferenceManager
import javax.inject.Inject

@HiltViewModel
class VotePollViewModel @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository,
    private val preferenceManager: PreferenceManager
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _pollingList = MutableLiveData<List<Polling>?>()
    val pollingList: LiveData<List<Polling>?>
        get() = _pollingList

    private val _votingList = MutableLiveData<VotingRes?>()
    val votingList: LiveData<VotingRes?>
        get() = _votingList

    private val _isSubmited: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val isSubmited: MutableLiveData<Boolean>
        get() = _isSubmited


    fun resetState() {
        setError(null)
        _pollingList.value = null
    }

    fun pollingList() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.pollingList()
            if (response.state && response.data != null) {
                _pollingList.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _pollingList.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _pollingList.value = null
            showError(exception)
        }
    }

    fun votingList() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.votingList()
            if (response.state && response.data != null) {
                _votingList.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _votingList.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _votingList.value = null
            showError(exception)
        }
    }

    fun pollingPost(pollingRequest: PollingRequest) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.pollingVote(pollingRequest)
            if (response.state) {
                _loading.value = false
                _isSubmited.value = true
            } else {
                _loading.value = false
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun votingPost(status: RequestBody?, id: RequestBody?, file: MultipartBody.Part?) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.voteVote(status, id, file)
            if (response.state) {
                _loading.value = false
                _isSubmited.value = true
            } else {
                _loading.value = false
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun getUser() = preferenceManager.getUser()

}