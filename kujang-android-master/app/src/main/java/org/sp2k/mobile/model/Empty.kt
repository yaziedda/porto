package org.sp2k.mobile.model

data class Empty (
    val title: String = "",
    val subTitle: String = ""
)