package org.sp2k.mobile.screens.proker

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityProkerBinding
import org.sp2k.mobile.databinding.ItemProkerBinding
import org.sp2k.mobile.event.ProkerEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.toCurrency
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.prokeranggaran.ProkerData
import org.sp2k.mobile.screens.proker.StatusUseCase.Companion.constructPokerStatus
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.ProkerViewModel

@AndroidEntryPoint
class ProkerActivity : BaseActivity<ActivityProkerBinding>() {

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter<ProkerData, ItemProkerBinding>
    val viewModel by viewModels<ProkerViewModel>()
    var selectionType = USULAN
    override fun getLayoutId(): Int = R.layout.activity_proker

    companion object {
        const val USULAN = "usulan"
        const val PROKER = "proker"
        const val LPJ = "lpj"
    }

    override fun ActivityProkerBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupUI()
        setupObserver()
        initCanShowUsulan()
        fetchData()
        initChip()
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityProkerBinding.initCanShowUsulan() {
        if (viewModel.getUser()?.role?.role_code in Constants.CAN_SHOW_ANGGARAN) {
            selectionType = USULAN
            chipProker.apply {
                visible()
                isChecked = true
            }
        } else {
            selectionType = PROKER
            chipProker.gone()
            chipBerjalan.apply {
                isChecked = true
                chipBerjalan.text = "Program Kerja"
            }
        }
    }

    private fun ActivityProkerBinding.initChip() {
        chipProker.apply {
            setOnClickListener {
                selectionType = USULAN
                fetchData()
            }
        }

        chipBerjalan.apply {
            setOnClickListener {
                selectionType = PROKER
                fetchData()
            }
        }

        chipSelesai.apply {
            setOnClickListener {
                selectionType = LPJ
                fetchData()
            }
        }
    }

    private fun ActivityProkerBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        emptyState.gone()
                        recyclerView.gone()
                        containerLoading.visible()
                    } else {
                        recyclerView.visible()
                        containerLoading.gone()
                    }
                }
            }

            prokerList.observe {
                this?.let {
                    recyclerViewAdapter.updateList(it.list)
                    if (it.list.isEmpty()) {
                        recyclerView.gone()
                        emptyState.visible()

                        when (selectionType) {
                            USULAN -> {
                                emptyStateTitle.text = it.empty.usulan.title
                                emptyStateSubTitle.text = it.empty.usulan.subTitle
                            }
                            PROKER -> {
                                emptyStateTitle.text = it.empty.proker.title
                                emptyStateSubTitle.text = it.empty.proker.subTitle
                            }
                            LPJ -> {
                                emptyStateTitle.text = it.empty.lpj.title
                                emptyStateSubTitle.text = it.empty.lpj.subTitle
                            }
                        }
                    } else {
                        recyclerView.visible()
                        emptyState.gone()
                    }
                }
            }

            anggaranBidang.observe {
                this?.let {
                    anggranTotal.text = it.saldo.toString().toCurrency()
                }
            }

            error.observe {
                showError(this) {
                    finish()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityProkerBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "Program Kerja"
        }
    }

    private fun ActivityProkerBinding.setupUI() {
        StatusBar.setLightStatusBar(window)
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetchData()
            }
        }
        if (viewModel.getUser()?.role?.role_code !in Constants.CAN_SHOW_ANGGARAN) {
            contentAnggaran.visible()
        } else {
            contentAnggaran.gone()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityProkerBinding.setupAdapter() {
        recyclerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_proker,
            BR.prokerData
        ) { itemView, itemModel ->

            itemView.itemProkerTvTitle.text = itemModel.usulan?.name
            itemView.itemProkerTvSubTitle.text = "Bidang : ${itemModel.bidang?.name} - ${itemModel.usulan?.created_at} "
            if (itemModel.proker?.id != null && itemModel.proker.id > 0) {
                itemView.itemProkerDate.visible()
                itemView.itemProkerDate.text =
                    "${itemModel.proker.tanggal_pelaksanaan_start} - ${itemModel.proker.tanggal_pelaksanaan_end}"
            } else {
                itemView.itemProkerDate.gone()
            }

            itemView.itemProkerStatusA.gone()
            itemView.itemProkerStatus.apply {
                constructPokerStatus(
                    viewModel.getUser()?.role?.role_code ?: "",
                    applicationContext,
                    itemModel,
                    itemView.itemProkerStatusA
                )
            }

            itemView.root.setOnClickListener {
                startActivity(
                    Intent(
                        applicationContext,
                        ProkerlDetailActivity::class.java
                    ).apply {
                        putExtra(ProkerlDetailActivity.ID, itemModel.usulan?.id.toString())
                    })
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            this@ProkerActivity,
            LinearLayoutManager.VERTICAL,
            false
        )

        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerViewAdapter
        }
    }


    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: ProkerEvent?) {
        fetchData()
    }

    private fun fetchData() {
        viewModel.apply {
            if (viewModel.getUser()?.role?.role_code !in Constants.CAN_SHOW_ANGGARAN) {
                fetchAnggaranList()
            }
            fetchList(selectionType)
        }
    }
}