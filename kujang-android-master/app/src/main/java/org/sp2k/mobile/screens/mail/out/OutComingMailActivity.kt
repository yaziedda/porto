package org.sp2k.mobile.screens.mail.out

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityOutComingMailBinding
import org.sp2k.mobile.databinding.ItemOutComingMailBinding
import org.sp2k.mobile.event.SuratEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.SuratKeluar
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.SuratMainViewModel

@AndroidEntryPoint
class OutComingMailActivity : BaseActivity<ActivityOutComingMailBinding>() {

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter<SuratKeluar, ItemOutComingMailBinding>
    val viewModel by viewModels<SuratMainViewModel>()

    override fun getLayoutId(): Int = R.layout.activity_out_coming_mail

    override fun ActivityOutComingMailBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupUI()
        setupObserver()
        fetchData()
    }

    private fun ActivityOutComingMailBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        emptyState  .gone()
                        activityInComingMailRecycler.gone()
                        containerLoading.visible()
                    } else {
                        activityInComingMailRecycler.visible()
                        containerLoading.gone()
                    }
                }
            }

            outComingMailList.observe {
                this?.let {
                    recyclerViewAdapter.updateList(it)
                    if (it.isEmpty()) {
                        activityInComingMailRecycler.gone()
                        emptyState.visible()
                    } else {
                        activityInComingMailRecycler.visible()
                        emptyState.gone()
                    }
                }
            }

            error.observe {
                showError(this)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityOutComingMailBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "Surat Keluar"
        }
    }

    private fun ActivityOutComingMailBinding.setupUI() {
        StatusBar.setLightStatusBar(window)
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetchData()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityOutComingMailBinding.setupAdapter() {
        recyclerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_out_coming_mail,
            BR.suratKeluar
        ) { itemView, itemModel ->

            itemView.itemInComingMailTvTitle.text = "Perihal : ${itemModel.perihal_surat}"
            itemView.itemInComingMailTvSubTitle.text = "Tujuan : ${itemModel.tujuan_surat}\n" +
                    "Kepada : ${itemModel.kepada}"
            itemView.itemInComingMailDate.text = itemModel.created_at

            if (itemModel.buttonVisible == true) {
                itemView.itemInComingMailStatus.apply {
                    visible()
                    text = itemModel.status
                    itemModel.status?.apply {
                        when {
                            startsWith(Constants.MAIL_WAITING) -> {
                                setMenunggu()
                            }
                            startsWith(Constants.MAIL_DISPOSISI) -> {
                                text = "DISPOSISI"
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.white
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_approved
                                )
                            }
                            startsWith(Constants.MAIL_APPROVE) -> {
                                if (viewModel.getUser()?.group?.code.equals("1")) {
                                    if (itemModel.status == "A1") {
                                        setDisetujui()
                                    } else {
                                        setMenunggu()
                                    }
                                } else {
                                    setDisetujui()
                                }
                            }
                            startsWith(Constants.MAIL_REJECT) -> {
                                text = "DITOLAK"
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.mail_rejected
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_rejected
                                )
                            }
                        }
                    }
                }
            } else {
                itemView.itemInComingMailStatus.gone()
            }

            itemView.root.setOnClickListener {
                startActivity(
                    Intent(
                        applicationContext,
                        OutComingMailDetailActivity::class.java
                    ).apply {
                        putExtra(OutComingMailDetailActivity.ID, itemModel.id.toString())
                    })
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            this@OutComingMailActivity,
            LinearLayoutManager.VERTICAL,
            false
        )

        activityInComingMailRecycler.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerViewAdapter
        }
    }

    private fun AppCompatButton.setDisetujui() {
        text = "DISETUJUI"
        setTextColor(
            ContextCompat.getColor(
                applicationContext,
                R.color.white
            )
        )
        background = ContextCompat.getDrawable(
            applicationContext,
            R.drawable.btn_mail_approved
        )
    }

    private fun AppCompatButton.setMenunggu() {
        text = "MENUNGGU"
        setTextColor(
            ContextCompat.getColor(
                applicationContext,
                R.color.mail_waiting
            )
        )
        background = ContextCompat.getDrawable(
            applicationContext,
            R.drawable.btn_mail_waiting
        )
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: SuratEvent?) {
        fetchData()
    }

    private fun fetchData() {
        viewModel.fetchOutComingMail()
    }
}