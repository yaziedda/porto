package org.sp2k.mobile.model

data class KujangNotification(
    val uid: Int,
    val body: String = "",
    val created_at: String = "",
    val image: String = "",
    val key: String = "",
    val sender: String = "",
    val title: String = "",
    val topic: String = "",
    val type: String = ""
)