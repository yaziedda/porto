package org.sp2k.mobile.screens.setting

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityChangePasswordBinding
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.ChangePasswordRequest
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.AccountViewModel

@AndroidEntryPoint
class ChangePasswordActivity : BaseActivity<ActivityChangePasswordBinding>() {

    val viewModel by viewModels<AccountViewModel>()

    override fun getLayoutId(): Int = R.layout.activity_change_password

    override fun ActivityChangePasswordBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupUI()
        setupObserver()
        viewModel.attachInputChangePasswordFormat(
            etPasswordOld,
            etPasswordNew,
            etPasswordNewConfirm
        )
        setPasswordToggle()
    }

    private fun ActivityChangePasswordBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        emptyState.gone()
                        containerContent.gone()
                        containerLoading.visible()
                    } else {
                        containerContent.visible()
                        containerLoading.gone()
                    }
                }
            }

            hideErrorPasswordOldFormat.observe {
                this.let {
                    passwordOldWarning.apply {
                        if (it) gone() else visible()
                    }
                }
            }

            hideErrorPasswordNewFormat.observe {
                this.let {
                    passwordNewWarning.apply {
                        if (it) gone() else visible()
                    }
                }
            }

            hideErrorPasswordNewConfirmFormat.observe {
                this.let {
                    passwordNewConfirmWarning.apply {
                        if (it) gone() else visible()
                    }
                }
            }

            hideErrorNotSamePasswordNewConfirmFormat.observe {
                this.let {
                    passwordNewConfirmNotSameWarning.apply {
                        if (it) gone() else visible()
                    }
                }
            }

            isPasswordChanged.observe {
                this?.let {
                    if (it) {
                        showMessage("Ganti Password Berhasil")
                        finish()
                    }
                }
            }

            error.observe {
                showError(this)
            }

            isValid.observe {
                if (this) {
                    btnChangePassword.visible()
                } else {
                    btnChangePassword.gone()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityChangePasswordBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "Ganti Password"
        }
    }

    private fun ActivityChangePasswordBinding.setupUI() {
        StatusBar.setLightStatusBar(window)
        swipeContainer.isEnabled = false

        btnChangePassword.setOnClickListener {
            val oldPassword = etPasswordOld.text.toString()
            val newPassword = etPasswordNew.text.toString()
            viewModel.changePassword(ChangePasswordRequest(oldPassword, newPassword))
        }
    }

    private fun ActivityChangePasswordBinding.setPasswordToggle() {
        var passwordOldHidden = false
        ivPasswordOldToggle.setOnClickListener {
            passwordOldHidden = if (passwordOldHidden) {
                passwordToggleVisibility(ivPasswordOldToggle, etPasswordOld, false)
            } else {
                passwordToggleVisibility(ivPasswordOldToggle, etPasswordOld, true)
            }
        }

        var passwordNewHidden = false
        ivPasswordNewToggle.setOnClickListener {
            passwordNewHidden = if (passwordNewHidden) {
                passwordToggleVisibility(ivPasswordNewToggle, etPasswordNew, false)
            } else {
                passwordToggleVisibility(ivPasswordNewToggle, etPasswordNew, true)
            }
        }

        var passwordNewConfirmHidden = false
        ivPasswordNewConfirmToggle.setOnClickListener {
            passwordNewConfirmHidden = if (passwordNewConfirmHidden) {
                passwordToggleVisibility(ivPasswordNewConfirmToggle, etPasswordNewConfirm, false)
            } else {
                passwordToggleVisibility(ivPasswordNewConfirmToggle, etPasswordNewConfirm, true)
            }
        }
    }


}