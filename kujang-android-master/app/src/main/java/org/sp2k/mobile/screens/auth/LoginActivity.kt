package org.sp2k.mobile.screens.auth

import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import dagger.hilt.android.AndroidEntryPoint
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityLoginBinding
import org.sp2k.mobile.exception.LoginException
import org.sp2k.mobile.screens.MainActivity
import org.sp2k.mobile.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.sp2k.mobile.extensions.*
import org.sp2k.mobile.utils.StatusBar

@AndroidEntryPoint
class LoginActivity : BaseActivity<ActivityLoginBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_login
    private val viewModel by viewModels<AuthViewModel>()
    private var isDirectToLogin: Boolean? = false

    private val errorMessage: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(getString(R.string.label_prefix_blocking_description))
            .append(
                getString(R.string.label_here).color(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.colorPrimary
                    )
                )
                    .underline()
                    .click { }
            )
    }

    companion object {
        const val KEY_IS_DIRECT_TO_LOGIN = "KEY_IS_DIRECT_TO_LOGIN"
    }

    override fun ActivityLoginBinding.initializeView(savedInstanceState: Bundle?) {
        StatusBar.setLightStatusBar(window)
        setupUI()
        setupObserver()
    }

    private fun ActivityLoginBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    etEmail.isEnabled = !it
                    etPassword.isEnabled = !it
                    if (it) {
                        btnLogin.gone()
                        containerContent.gone()
                        containerLoading.visible()
                    } else {
                        btnLogin.visible()
                        containerContent.visible()
                        containerLoading.gone()
                    }
                }
            }
            googleSignInIntent.observe(this@LoginActivity) { googleSignInIntent ->
                if (googleSignInIntent != null) {
                    startActivityForResult(googleSignInIntent, AuthViewModel.RC_GOOGLE_SIGN_IN)
                }
            }

            error.observe { if (this is LoginException) showBlockingPage { viewModel.loginWithGoogle() } }

            isValid.observe {
                btnLogin.isEnabled = this
            }

            hideErrorNIKformat.observe {
                when {
                    this -> nikWarning.visibility = View.GONE
                    etEmail.text?.isEmpty() == true -> nikWarning.visibility =
                        View.GONE
                    else -> nikWarning.visibility = View.VISIBLE
                }
            }

            hideErrorPasswordformat.observe {
                when {
                    this -> passwordWarning.visibility = View.GONE
                    etPassword.text?.isEmpty() == true -> passwordWarning.visibility =
                        View.GONE
                    else -> passwordWarning.visibility = View.VISIBLE
                }
            }

            error.observe {
                if (this != null) {
                    this@LoginActivity.showError(
                        this,
                        message ?: errorMessage,
                        ContextCompat.getDrawable(
                            applicationContext,
                            R.drawable.ic_not_logged_in
                        )
                    ) {

                    }
                }
            }

            staffData.observe {
                if (this != null) {
                    Toast.makeText(
                        applicationContext,
                        "Berhasil masuk, selamat datang " + (staff?.admin_nama
                            ?: "") + " di " + getString(R.string.app_name),
                        Toast.LENGTH_LONG
                    ).show()
                    goToHomeActivity()

                }
            }
        }
    }

    private fun ActivityLoginBinding.setupUI() {
        viewModel.attachInputFormat(etEmail, etPassword)
        var passwordHidden = false
        ivPasswordToggle.setOnClickListener {
            passwordHidden = if (passwordHidden) {
                passwordToggleVisibility(ivPasswordToggle, etPassword, false)
            } else {
                passwordToggleVisibility(ivPasswordToggle, etPassword, true)
            }
        }

        btnLoginGoogle.setOnClickListener {
            viewModel.loginWithGoogle()
        }

        btnLogin.setOnClickListener {
            val map: HashMap<String, String> = hashMapOf()
            map["username"] = etEmail.text.toString()
            map["password"] = etPassword.text.toString()
            viewModel.loginUser(map)
        }
    }

    private fun goToHomeActivity() {
        val intent = Intent(
            this,
            MainActivity::class.java
        ).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AuthViewModel.RC_GOOGLE_SIGN_IN) {
            if (data != null) {
                viewModel.onGoogleLoginActivityResult(data)
            }
        }
    }


}