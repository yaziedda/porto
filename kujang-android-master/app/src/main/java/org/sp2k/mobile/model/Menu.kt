package org.sp2k.mobile.model

data class Menu(
    val id: Int,
    val title: String,
    val icon: Int
)