package org.sp2k.mobile.screens

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivitySplashScreenBinding
import org.sp2k.mobile.screens.auth.LoginActivity
import org.sp2k.mobile.screens.proker.ProkerlDetailActivity
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.AuthViewModel

@AndroidEntryPoint
class SplashScreenActivity : BaseActivity<ActivitySplashScreenBinding>() {

    private val viewModel by viewModels<AuthViewModel>()

    override fun getLayoutId(): Int = R.layout.activity_splash_screen

    override fun ActivitySplashScreenBinding.initializeView(savedInstanceState: Bundle?) {
        val typeNotification = intent?.extras?.getString(Constants.NOTIFICATION_TYPE) ?: ""
        val keyNotification = intent?.extras?.getString(Constants.NOTIFICATION_KEY) ?: ""
        val actionNotification = intent?.action
        StatusBar.setLightStatusBar(window)
        Handler().postDelayed({
            applicationContext.let {
                if (viewModel.isAnonymous()) {
                    goToNextDestination(
                        Intent(it, LoginActivity::class.java)
                    )
                } else {
                    if (typeNotification in arrayListOf(
                            "lpj",
                            "proposal",
                            "anggaran",
                            "anggaran_bidang",
                            "voting",
                            "polling",
                            "surat_masuk",
                            "surat_keluar"
                        )
                    ) {
                        goToNextDestination(
                            Intent(it, MainActivity::class.java).apply {
                                putExtra(Constants.NOTIFICATION_KEY, keyNotification)
                                putExtra(Constants.NOTIFICATION_TYPE, typeNotification)
                                putExtra(ProkerlDetailActivity.ID, keyNotification)
                            }
                        )
                    } else {
                        goToNextDestination(
                            Intent(it, MainActivity::class.java)
                        )
                    }
                }
            }
        }, Constants.SPLASH_SCREEN_DELAY_DURATION)
    }

    private fun goToNextDestination(intent: Intent) {
        startActivity(intent)
        finish()
    }

    override fun onResume() {
        super.onResume()
        print("to do tampilkan pesan")
    }
}