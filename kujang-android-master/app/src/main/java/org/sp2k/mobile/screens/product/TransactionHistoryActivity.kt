package org.sp2k.mobile.screens.product

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityTransactionHistoryBinding
import org.sp2k.mobile.databinding.ItemTransactionHistoryBinding
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.toCurrency
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.TransactionHistory
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.ProductViewModel
import kotlinx.android.synthetic.main.loading_view.*
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import timber.log.Timber


@AndroidEntryPoint
class TransactionHistoryActivity : BaseActivity<ActivityTransactionHistoryBinding>() {
    private val viewModel by viewModels<ProductViewModel>()
    override fun getLayoutId(): Int = R.layout.activity_transaction_history
    private lateinit var recyclerAdapter: RecyclerViewAdapter<TransactionHistory, ItemTransactionHistoryBinding>

    override fun ActivityTransactionHistoryBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupObserver()
        fetcher()
    }

    private fun ActivityTransactionHistoryBinding.setupActionBar() {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.title = "Riwayat Transaksi"
        }
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchHistory()
        }
    }

    private fun ActivityTransactionHistoryBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    progressBar.visible()
                    swipeContainer.isRefreshing = true
                    recyclerView.gone()
                } else {
                    progressBar.gone()
                    skeletonLayout.showOriginal()
                    recyclerView.visible()
                    swipeContainer.isRefreshing = false
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }

            transactions.observe {
                recyclerAdapter.updateList(this)
            }
        }
    }

    private fun ActivityTransactionHistoryBinding.setupAdapter() {
        recyclerAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_transaction_history,
            BR.transactionHistory
        ) { itemView, itemModel ->
            itemView.itemTransactionHistoryInv.text = itemModel.invoiceNumber
            itemView.itemTransactionHistoryPrice.text = itemModel.total.toString().toCurrency()

            itemView.button.gone()
            when (itemModel.status) {
                1 -> {
                    itemView.itemTransactionHistoryStatus.text = "Pembayaran Berhasil"
                    itemView.itemTransactionHistoryStatus.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.done
                        )
                    )
                }
                0 -> {
                    itemView.itemTransactionHistoryStatus.text = "Menunggu Pembayaran"
                    itemView.itemTransactionHistoryStatus.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.proces
                        )
                    )
                    itemView.button.visible()
                    if (itemModel.paymentUrl.isNotEmpty()) {
                        itemView.button.setOnClickListener {
                            goToWebViewActivity(itemModel.paymentUrl)
                        }
                    } else {
                        setKadaluarsa(itemView)
                    }
                }
                -1 -> {
                    setKadaluarsa(itemView)
                }
            }
            itemView.root.setOnClickListener {
                startActivity(
                    Intent(applicationContext, ProductDetailActivity::class.java).apply {
                        putExtra(ProductDetailActivity.KEY_ID, itemModel.id.toString())
                    }
                )
            }
        }

        val linearLayoutManager = LinearLayoutManager(applicationContext)
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerAdapter
        }

    }

    private fun setKadaluarsa(itemView: ItemTransactionHistoryBinding) {
        itemView.itemTransactionHistoryStatus.text = "Transaksi Kadaluarsa"
        itemView.itemTransactionHistoryStatus.setTextColor(
            ContextCompat.getColor(
                applicationContext,
                R.color.color_error
            )
        )
    }

}