package org.sp2k.mobile.screens.voting

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityVotingBinding
import org.sp2k.mobile.databinding.ItemVotingBinding
import org.sp2k.mobile.event.VotingEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.voting.Voting
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.VotePollViewModel
import kotlin.math.roundToInt

@AndroidEntryPoint
class VotingActivity : BaseActivity<ActivityVotingBinding>() {

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter<Voting, ItemVotingBinding>
    val viewModel by viewModels<VotePollViewModel>()
    var resultIsPublish = false

    override fun getLayoutId(): Int = R.layout.activity_voting

    override fun ActivityVotingBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupUI()
        setupObserver()
        fetchData()
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityVotingBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        emptyState.gone()
                        recyclerView.gone()
                        containerLoading.visible()
                    } else {
                        recyclerView.visible()
                        containerLoading.gone()
                    }
                }
            }

            votingList.observe {
                this?.voting?.let {
                    recyclerViewAdapter.updateList(it)
                    if (it.isNotEmpty()) {
                        containerContent.visible()
                        recyclerView.visible()
                        emptyState.gone()
                        this.evotePublish?.let { evotePublish ->
                            resultIsPublish = evotePublish
                            if (evotePublish) {
                                activityVotingTitle.text = this.title
                                activityVotingSubTitle.text = this.subTitle
                            }
                        }

                        this.hasVoted.let { hasVoted ->
                            if (hasVoted == true) {
                                if (isLive == true) {
                                    activityVotingVoteButton.apply {
                                        visible()
                                        background = ContextCompat.getDrawable(
                                            applicationContext,
                                            R.drawable.btn_mail_blue
                                        )
                                        setTextColor(
                                            ContextCompat.getColor(
                                                applicationContext,
                                                R.color.white
                                            )
                                        )
                                        text = "Sudah Voting"
                                        setOnClickListener {

                                        }
                                    }
                                } else {
                                    activityVotingVoteButton.gone()
                                }
                            } else {
                                if (isClose == false) {
                                    activityVotingVoteButton.apply {
                                        visible()
                                        background = ContextCompat.getDrawable(
                                            applicationContext,
                                            R.drawable.btn_primary_full
                                        )
                                        setTextColor(
                                            ContextCompat.getColor(
                                                applicationContext,
                                                R.color.white
                                            )
                                        )
                                        text = "Vote Sekarang"
                                        setOnClickListener {
                                            startActivity(
                                                Intent(
                                                    applicationContext,
                                                    VotingGoVoteActivity::class.java
                                                )
                                            )
                                        }
                                    }
                                } else {
                                    activityVotingVoteButton.gone()
                                }
                            }
                        }
                    } else {
                        containerContent.gone()
                        recyclerView.gone()
                        emptyState.visible()
                        activityVotingVoteButton.gone()
                    }
                }
            }

            isSubmited.observe {
                this?.let {
                    if (it) {
                        showMessage("Anda berhasil melakukan voting")
                        fetchData()
                    }
                }
            }

            error.observe {
                showError(this) {
                    finish()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityVotingBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "E-Voting"
        }
    }

    private fun ActivityVotingBinding.setupUI() {
        StatusBar.setLightStatusBar(window)
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetchData()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityVotingBinding.setupAdapter() {
        recyclerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_voting,
            BR.voting
        ) { itemView, itemModel ->

            Glide.with(applicationContext)
                .load(itemModel.foto)
                .into(itemView.itemVotingIvImage)

            itemView.itemVotingTvTitle.text = "Calon ${itemModel.no_urut}"
            itemView.itemVotingTvUrut.text =
                "Ketua : ${itemModel.nama_ketua}\nSekum: ${itemModel.nama_sekum}"


            if (resultIsPublish) {
                itemView.itemPercentagePercentageWrapper.visible()
                itemView.itemPercentagePercentageProgress.progress =
                    itemModel.percentage?.toDouble()?.roundToInt() ?: 0
                itemView.itemPercentagePercentageCount.text = "${itemModel.percentage} %"

            } else {
                itemView.itemPercentagePercentageWrapper.gone()
            }

            itemView.itemVotingWrapper.setOnClickListener {
                val intent = Intent(applicationContext, VotingDetailActivity::class.java).apply {
                    putExtra(VotingDetailActivity.KETUM_NAMA, itemModel.nama_ketua)
                    putExtra(VotingDetailActivity.KETUM_NIK, itemModel.nik_ketua)
                    putExtra(VotingDetailActivity.KETUM_JABATAN_SEBELUMNYA, itemModel.jabatan_ketua)

                    putExtra(VotingDetailActivity.SEKUM_NAMA, itemModel.nama_sekum)
                    putExtra(VotingDetailActivity.SEKUM_NIK, itemModel.nik_sekum)
                    putExtra(VotingDetailActivity.SEKUM_JABATAN_SEBELUMNYA, itemModel.jabatan_sekum)

                    putExtra(VotingDetailActivity.URUT, itemModel.no_urut.toString())
                    putExtra(VotingDetailActivity.FOTO, itemModel.foto)
                    putExtra(VotingDetailActivity.DESKRIPSI, itemModel.deskripsi)
                }
                startActivity(intent)
            }
        }

        recyclerView.apply {
            layoutManager = GridLayoutManager(this@VotingActivity, 2)
            adapter = recyclerViewAdapter
        }
    }


    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: VotingEvent?) {
        fetchData()
    }

    private fun fetchData() {
        viewModel.votingList()
    }
}