package org.sp2k.mobile.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository

class SplashViewModel @ViewModelInject constructor(
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {
    companion object {
        const val DESTINATION_LOGIN = 1
        const val DESTINATION_MAIN = 2
    }

    private val _destination: MutableLiveData<Int> by lazy {
        MutableLiveData(0)
    }

    val destination: LiveData<Int>
        get() = _destination

    fun checkAuthenticated() {

        _destination.value = DESTINATION_MAIN

    }
}
