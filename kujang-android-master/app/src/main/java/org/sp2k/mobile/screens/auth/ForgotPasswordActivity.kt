package org.sp2k.mobile.screens.auth

import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import dagger.hilt.android.AndroidEntryPoint
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityForgotPasswordBinding
import org.sp2k.mobile.extensions.click
import org.sp2k.mobile.extensions.color
import org.sp2k.mobile.extensions.underline
import org.sp2k.mobile.utils.DialogUtils
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.ForgotPasswordViewModel

@AndroidEntryPoint
class ForgotPasswordActivity : BaseActivity<ActivityForgotPasswordBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_forgot_password
    private val viewModel by viewModels<ForgotPasswordViewModel>()
    private val errorMessage: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(getString(R.string.label_prefix_blocking_description))
            .append(
                getString(R.string.label_here).color(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.colorPrimary
                    )
                )
                    .underline()
                    .click { }
            )
    }

    override fun ActivityForgotPasswordBinding.initializeView(savedInstanceState: Bundle?) {
        StatusBar.setLightStatusBar(window)
        setupUI()
        setupObserver()
    }

    private fun ActivityForgotPasswordBinding.setupObserver() {
        viewModel.apply {
            isValid.observe {
                btnLogin.isEnabled = this
            }

            hideErrorNIKformat.observe {
                when {
                    this -> nikWarning.visibility = View.GONE
                    etNik.text?.isEmpty() == true -> nikWarning.visibility =
                        View.GONE
                    else -> nikWarning.visibility = View.VISIBLE
                }
            }


            error.observe {
                if (this != null) {
                    this@ForgotPasswordActivity.showError(
                        this,
                        message ?: errorMessage,
                        ContextCompat.getDrawable(
                            applicationContext,
                            R.drawable.ic_not_logged_in
                        )
                    ) {
                        submitPayload()
                    }
                }
            }

            loading.observe {
                if (this) {
                    containerLoading.visibility = View.VISIBLE
                    etNik.isEnabled = false
                    btnLogin.visibility = View.GONE
                } else {
                    containerLoading.visibility = View.GONE
                    etNik.isEnabled = true
                    btnLogin.visibility = View.VISIBLE
                }
            }

            staffData.observe {
                if (this != null) {
                    DialogUtils.showBasicAlertConfirmationDialog(
                        this@ForgotPasswordActivity,
                        ContextCompat.getDrawable(this@ForgotPasswordActivity.applicationContext, R.drawable.ic_not_logged_in),
                        "Dengan NIK $admin_nik, dan email $admin_email. Apakah anda yakin akan lupa password? Klik ya untuk mengirim kode verifikasi ke email anda.",
                        title = "Pengguna ditemukan"
                    ) {
                        goToVerificationActivity()
                    }

                }
            }
        }
    }

    private fun ActivityForgotPasswordBinding.setupUI() {
        viewModel.attachInputFormat(etNik)
        btnLogin.setOnClickListener { submitPayload() }
        etNik.setText("3215030702970001")
    }

    private fun ActivityForgotPasswordBinding.submitPayload() {

        val map = HashMap<String, String?>()
        map["nik"] = etNik.text.toString()
//        viewModel.forgotPasswordCheck(map, applicationContext)
    }

    private fun goToVerificationActivity() {
        val intent = Intent(
            this,
            ForgotPasswordVerificationActivity::class.java
        )
        startActivity(intent)
    }
}