package org.sp2k.mobile.model.prokeranggaran

import java.io.Serializable

data class Proker(
    val alasan: String? = "",
    val anggaran: Int? = 0,
    val anggaran_file: String? = "",
    val anggaran_send_status: Int? = 0,
    val approval_id: Int? = 0,
    val approval_status: Int? = 0,
    val created_at: String? = "",
    val created_by: Int? = 0,
    val date: String? = "",
    val `file`: String? = "",
    val id: Int? = 0,
    val id_rab_proker: Int? = 0,
    val is_lpj: Int? = 0,
    val status: Int? = 0,
    val status_anggaran: Int? = 0,
    val status_anggaran_alasan: String? = "",
    val status_code: String? = "",
    val tanggal_pelaksanaan_end: String? = "",
    val tanggal_pelaksanaan_start: String? = "",
    val updated_at: String? = "",
    val updated_by: Int
): Serializable