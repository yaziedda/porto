package org.sp2k.mobile.viewmodel

import android.content.Intent
import android.text.Selection
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.common.api.ApiException
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.exception.GeneralException
import org.sp2k.mobile.exception.LoginException
import org.sp2k.mobile.extensions.afterTextChanged
import org.sp2k.mobile.model.user.AuthData
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import org.sp2k.mobile.repository.ResourceRepository
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.PreferenceManager
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val resourceRepository: ResourceRepository,
    private val authenticationRepository: AuthenticationRepository,
    private val preferenceManager: PreferenceManager,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    companion object {
        const val LOGIN_PAGE = 0
        const val REGISTER_PAGE = 1
        const val RC_GOOGLE_SIGN_IN = 2
    }


    private var validNIKFormat: Boolean = false
    private var validPasswordFormat: Boolean = false
    private var onChangedDisabled: Boolean = false

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _staffData: MutableLiveData<AuthData?> by lazy {
        MutableLiveData(null)
    }
    val staffData: MutableLiveData<AuthData?>
        get() = _staffData

    private val _nikValue = MutableLiveData<String>()
    val nikValue: MutableLiveData<String>
        get() = _nikValue
    private val _hideErrorNIKformat = MutableLiveData<Boolean>()
    val hideErrorNIKformat: LiveData<Boolean>
        get() = _hideErrorNIKformat
    private val _passwordValue = MutableLiveData<String>()
    val passwordValue: MutableLiveData<String>
        get() = _passwordValue
    private val _hideErrorPasswordformat = MutableLiveData<Boolean>()
    val hideErrorPasswordformat: LiveData<Boolean>
        get() = _hideErrorPasswordformat
    private val _isValid = MutableLiveData<Boolean>()
    val isValid: LiveData<Boolean>
        get() = _isValid

    private val _isLoginFormValid: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    private val _isRegisterFormValid: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    private val _email: ObservableField<String> by lazy {
        ObservableField<String>("")
    }

    private val _phone: ObservableField<String> by lazy {
        ObservableField<String>("")
    }

    private val _password: ObservableField<String> by lazy {
        ObservableField<String>("")
    }

    private val _nameError: MutableLiveData<Exception?> by lazy {
        MutableLiveData<Exception?>(null)
    }

    private val _phoneError: MutableLiveData<Exception?> by lazy {
        MutableLiveData<Exception?>(null)
    }

    private val _emailError: MutableLiveData<Exception?> by lazy {
        MutableLiveData<Exception?>(null)
    }

    private val _passwordError: MutableLiveData<Exception?> by lazy {
        MutableLiveData<Exception?>(null)
    }

    private val _loginError: MutableLiveData<Exception?> by lazy {
        MutableLiveData<Exception?>(null)
    }

    private val _isSuccess: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    private val _page: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>(LOGIN_PAGE)
    }

    private val _googleSignInIntent: MutableLiveData<Intent> by lazy {
        MutableLiveData<Intent>(null)
    }

    val isLoginFormValid: LiveData<Boolean>
        get() = _isLoginFormValid

    val isRegisterFormValid: LiveData<Boolean>
        get() = _isRegisterFormValid


    val email: ObservableField<String>
        get() = _email

    val phone: ObservableField<String>
        get() = _phone

    val password: ObservableField<String>
        get() = _password

    val page: LiveData<Int>
        get() = _page


    val emailError: LiveData<Exception?>
        get() = _emailError

    val passwordError: LiveData<Exception?>
        get() = _passwordError

    val loginError: LiveData<Exception?>
        get() = _loginError

    val isSuccess: LiveData<Boolean>
        get() = _isSuccess

    val googleSignInIntent: LiveData<Intent>
        get() = _googleSignInIntent

    fun resetState() {
        setError(null)
        _page.value = 1
        _loading.value = false
        _loginError.value = null
        _emailError.value = null
        _passwordError.value = null
        _nameError.value = null
        _googleSignInIntent.value = null
        _isSuccess.value = false
        setError(null)
    }

    fun attachInputFormat(
        nikInputEditText: TextInputEditText,
        passwordInputEditText: TextInputEditText
    ) {
        nikInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _nikValue.value = this
                    setText(nikValue.value)
                    validNIKFormat = length() >= 6
                    _hideErrorNIKformat.value = validNIKFormat
                    _isValid.value = validNIKFormat && validPasswordFormat
                    Selection.setSelection(nikInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }

        passwordInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _passwordValue.value = this
                    setText(passwordValue.value)
                    validPasswordFormat = length() >= 6
                    _hideErrorPasswordformat.value = validPasswordFormat
                    _isValid.value = validNIKFormat && validPasswordFormat
                    Selection.setSelection(passwordInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }
    }

    fun isAnonymous(): Boolean {
        return !preferenceManager.isLoggedIn()
    }

    fun loginPage() {
        setError(null)
        _page.value = LOGIN_PAGE
    }

    fun registerPage() {
        setError(null)
        _page.value = REGISTER_PAGE
    }

    fun loginWithGoogle() {
        _loading.value = true
        setError(null)
        _googleSignInIntent.value = authenticationRepository.googleSignInIntent
    }

    fun onGoogleLoginActivityResult(data: Intent) {
        try {
            val callback: ((success: Boolean, err: Exception?) -> Unit) = { success, _ ->
                updateUniqueUser()
                updateView(success)
            }
            authenticationRepository.processGoogleSignInResult(data, callback)
        } catch (e: ApiException) {
            updateView(false)
        }
    }

    private fun updateView(success: Boolean) {
        _loading.value = false
        _isSuccess.value = success
        if (!success) {
            showError(LoginException())
        }
    }

    fun loginUser(map: HashMap<String, String>) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.login(map)
            if (response.state) {
                val user = response.data
                if (user != null) {
                    val callback: ((success: Boolean, err: Exception?) -> Unit) =
                        { success, err ->
                            if (success) {
                                updateUniqueUser()
                                viewModelScope.launch {
                                    try {
                                        val mapToken: HashMap<String, String?>? = hashMapOf()
                                        mapToken?.put("token_fcm", "")
                                        remoteRepository.tokenUpdate(mapToken)

                                        preferenceManager.saveUser(user)
                                        preferenceManager.saveLogin(true)

                                        FirebaseMessaging.getInstance().apply {
                                            val topicUser = Constants.PUSH_NOTIF_USER + user.staff?.admin_username.toString()
                                            val topicRole = Constants.PUSH_NOTIF_ROLE + user.role?.role_code
                                            val topicBidang = Constants.PUSH_NOTIF_BIDANG + user.bidang?.id
                                            val topicGroup = Constants.PUSH_NOTIF_GROUP + user.group?.code
                                            val topicGlobal = Constants.PUSH_NOTIF_GLOBAL
                                            subscribeToTopic(topicUser)
                                            subscribeToTopic(topicRole)
                                            if (user.bidang?.id != null) {
                                                subscribeToTopic(topicBidang)
                                            }
                                            subscribeToTopic(topicGroup)
                                            subscribeToTopic(topicGlobal)
                                        }
                                        _staffData.value = user
                                        _loading.value = false
                                    } catch (exception: Exception) {

                                    }
                                }

                            } else {
                                var exception: Exception
                                if (err is FirebaseAuthInvalidUserException) {
                                    prosesCreateUser(map = map)
                                }
                                if (err is FirebaseAuthInvalidCredentialsException) {
                                    _loading.value = false
                                    exception =
                                        Exception(resourceRepository.getString(R.string.login_gagal))
                                    _passwordError.value = exception
                                    setError(exception)
                                }
                                if (err is FirebaseAuthUserCollisionException) {
                                    prosesCreateUser(map)
                                }
                                if (err is FirebaseNetworkException) {
                                    _loading.value = false
                                    exception = LoginException()
                                    _loginError.value = exception
                                    setError(exception)
                                }
                                updateUniqueUser()
                            }
                        }
                    var email = map["username"] ?: ""
                    email += Constants.EMAIL_DUMMY_FB
                    authenticationRepository.signInWithEmailAndPassword(
                        email,
                        map["password"] ?: "",
                        callback
                    )
                } else {
                    _loading.value = false
                    showError(GeneralException(response.message))
                }
            } else {
                _loading.value = false
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    private fun prosesCreateUser(
        map: HashMap<String, String>,
    ) {
        val callback: ((success: Boolean, err: Exception?) -> Unit) = { success, err ->
            _loading.value = false
            if (success) {
                loginUser(map)
            } else {
                var exception: Exception = GeneralException("")
                if (err is FirebaseAuthInvalidUserException) {
                    exception =
                        Exception(resourceRepository.getString(R.string.user_with_email_not_found))
                    _emailError.value = exception
                }
                if (err is FirebaseAuthInvalidCredentialsException) {
                    exception = Exception(resourceRepository.getString(R.string.wrong_password))
                    _passwordError.value = exception
                }
                if (err is FirebaseAuthUserCollisionException) {
                    exception = Exception(resourceRepository.getString(R.string.email_exists))
                    _emailError.value = exception
                }
                if (err is FirebaseNetworkException) {
                    exception = LoginException()
                    _loginError.value = exception
                }
                showError(exception)
                updateUniqueUser()
            }
        }

        authenticationRepository.createUserWithEmailAndPassword(
            email = map["username"] + Constants.EMAIL_DUMMY_FB,
            password = map["password"] ?: "",
            callback = callback
        )
    }

    private fun tokenUpdate() = viewModelScope.launch {
        try {
            val map: HashMap<String, String?>? = hashMapOf()
            map?.put("token_fcm", "")
            remoteRepository.tokenUpdate(map)
        } catch (exception: Exception) {
        }
    }


}