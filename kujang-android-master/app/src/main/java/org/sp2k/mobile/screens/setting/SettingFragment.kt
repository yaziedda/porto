package org.sp2k.mobile.screens.setting

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItems
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.sp2k.mobile.BuildConfig
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseFragment
import org.sp2k.mobile.contract.GalleryContract
import org.sp2k.mobile.databinding.FragmentSettingBinding
import org.sp2k.mobile.extensions.*
import org.sp2k.mobile.permission.helper.PermissionHelper
import org.sp2k.mobile.permission.listener.PermissionListener
import org.sp2k.mobile.screens.SplashScreenActivity
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.DialogUtils
import org.sp2k.mobile.viewmodel.AccountViewModel
import java.io.File


@AndroidEntryPoint
class SettingFragment : BaseFragment<FragmentSettingBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_setting
    private val viewModel by activityViewModels<AccountViewModel>()
    private lateinit var fileImage: File
    private var imageUri: Uri? = null
    private val takePicture =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { _ ->
            if (fileImage.exists())
                lifecycleScope.launch {
                    imageUri?.let {
                        val compressedFile = requireContext().compressFile(fileImage)
                        val requestFile: RequestBody = RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            compressedFile
                        )
                        val image: MultipartBody.Part = MultipartBody.Part.createFormData(
                            "foto",
                            compressedFile.name,
                            requestFile
                        )
                        viewModel.changeProfile(image)
                    }
                }
        }

    private val getImageFromGallery = registerForActivityResult(GalleryContract()) { result ->
        result?.run {
            imageUri = requireContext().getRealPathFromUri(this)
            imageUri?.path?.let {
                fileImage = File(it)
            }
            imageUri?.let {
                lifecycleScope.launch {
                    val compressedFile = requireContext().compressFile(fileImage)
                    val requestFile: RequestBody = RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        compressedFile
                    )
                    val image: MultipartBody.Part = MultipartBody.Part.createFormData(
                        "foto",
                        compressedFile.name,
                        requestFile
                    )
                    viewModel.changeProfile(image)
                }
            }
        }
    }

    override fun FragmentSettingBinding.initializeView() {
        initUI()
        initObserver()
        checkAnonymous()
        initLogout()
    }

    @SuppressLint("SetTextI18n")
    private fun FragmentSettingBinding.initObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    if (it) {
                        showProgressDialog("Sedang upload photo")
                    } else {
                        dismissProgressDialog()
                    }
                }
            }
            isSubmited.observe {
                this?.let {
                    if (it) {
                        initUI()
                        showMessage("Foto profile berhasil di update")
                    }
                }
            }
            signOutSuccess.observe(viewLifecycleOwner) { signOutSuccess ->
                if (signOutSuccess) {
                    val intent = Intent(activity, SplashScreenActivity::class.java)
                    activity?.startActivity(intent)
                    activity?.finish()
                }
            }
        }
        versionTxt.text = "App Version ${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})"
    }

    @SuppressLint("SetTextI18n")
    private fun FragmentSettingBinding.initUI() {
        val user = viewModel.getUser()
        tvName.text = user?.staff?.admin_nama
        if (user?.role?.role_code.equals("KETUA_BIDANG")) {
            tvDesc.text =
                "No Anggota : ${user?.staff?.admin_username} - ${user?.role?.role_nama} ${user?.bidang?.name}"
        } else {
            tvDesc.text = "No Anggota : ${user?.staff?.admin_username} - ${user?.role?.role_nama}"
        }

        Glide.with(requireContext())
            .load(user?.staff?.admin_foto)
            .error(R.drawable.ic_profile_default)
            .into(ivProfile)

        ivProfile.setOnClickListener {
            doPick()
        }
        ivProfileChange.setOnClickListener {
            doPick()
        }
    }

    @SuppressLint("CheckResult")
    private fun doPick() {
        val url = viewModel.getUser()?.staff?.admin_foto ?: ""
        if (url.startsWith("http") || url.startsWith("https")) {
            val builder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
            builder.setTitle("Pilih")
            val options = arrayOf("Lihat Foto", "Ganti Profile")
            builder.setItems(
                options
            ) { _, which ->
                when (which) {
                    0 -> {
                        goToShowImageActivity(url)
                    }
                    1 -> showChangeProfileDialog()
                }
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        } else {
            showChangeProfileDialog()
        }
    }

    @SuppressLint("CheckResult")
    private fun showChangeProfileDialog() {
        showDialogOptionBottomSheet(
            "Apakah anda yakin akan ganti foto profile?",
            yeButtonClick = {
                val builder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
                builder.setTitle("Pilih")
                val options = arrayOf("Kamera", "Galery")
                builder.setItems(
                    options
                ) { _, which ->
                    when (which) {
                        0 -> {
                            pickImage {
                                with(requireContext()) {
                                    createImageFile().also {
                                        imageUri = getUriFromFile(it)
                                        fileImage = it
                                        takePicture.launch(imageUri)
                                    }
                                }
                            }
                        }
                        1 -> {
                            pickImage {
                                getImageFromGallery.launch(1)
                            }
                        }
                    }
                }
                val dialog: AlertDialog = builder.create()
                dialog.show()
            })
    }

    private fun FragmentSettingBinding.initLogout() {
        llAccountLogout.setOnClickListener {
            doLogout()
        }
        ivAccountLogout.setOnClickListener {
            doLogout()
        }
        tvAccountLogout.setOnClickListener {
            doLogout()
        }

        llAccountChangePassword.setOnClickListener {
            startActivity(Intent(requireContext(), ChangePasswordActivity::class.java))
        }
    }

    private fun doLogout() {
        requireContext().clearNotifications()
        DialogUtils.showBasicAlertConfirmationDialog(
            activity = requireActivity(),
            ContextCompat.getDrawable(requireContext(), R.drawable.logo),
            "Apakah anda yakin akan logout?"
        ) {
            val user = viewModel.getUser()
            FirebaseMessaging.getInstance().apply {
                unsubscribeFromTopic(Constants.PUSH_NOTIF_USER + user?.staff?.admin_username.toString())
                unsubscribeFromTopic(Constants.PUSH_NOTIF_ROLE + user?.role?.role_code)
                unsubscribeFromTopic(Constants.PUSH_NOTIF_BIDANG + user?.bidang?.id)
                unsubscribeFromTopic(Constants.PUSH_NOTIF_GROUP + user?.group?.code)
                unsubscribeFromTopic(Constants.PUSH_NOTIF_GLOBAL)
            }
            viewModel.signOut()
        }
    }

    private fun FragmentSettingBinding.checkAnonymous() {
        val isAnonymous = viewModel.isAnonymous()
        if (!isAnonymous) {
            contentMain.visibility = View.VISIBLE
            viewModel.getDisplayName()
        } else {
            contentMain.visibility = View.GONE
        }
    }

    private val permissionHelper by lazy { PermissionHelper(this) }
    private val requestedPermissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    private fun pickImage(action: () -> Unit) {
        permissionHelper.requests(requestedPermissions)
            .listener(object : PermissionListener.Request {
                override fun granted() = action.invoke()
                override fun showRequestPermissionRationale() = showDeniedPermission()
                override fun denied() = showDeniedPermission {
                    startActivity(
                        Intent(
                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                        )
                    )
                }
            })
    }

    private fun showDeniedPermission(action: (() -> Unit)? = null) {
        showDialog(getString(R.string.label_camera_permission)) {
            onBackPressed()
            action?.invoke()
        }
    }


}