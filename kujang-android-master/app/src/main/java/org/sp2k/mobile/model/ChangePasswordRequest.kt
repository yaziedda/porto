package org.sp2k.mobile.model

data class ChangePasswordRequest(
    var old_password: String = "",
    val new_password: String = "",
)