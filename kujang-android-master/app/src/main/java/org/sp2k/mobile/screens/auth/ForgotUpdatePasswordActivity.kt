package org.sp2k.mobile.screens.auth

import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import dagger.hilt.android.AndroidEntryPoint
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityForgotUpdatePasswordBinding
import org.sp2k.mobile.extensions.click
import org.sp2k.mobile.extensions.color
import org.sp2k.mobile.extensions.underline
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.ForgotUpdatePasswordModel

@AndroidEntryPoint
class ForgotUpdatePasswordActivity : BaseActivity<ActivityForgotUpdatePasswordBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_forgot_update_password
    private val viewModel by viewModels<ForgotUpdatePasswordModel>()

    private val errorMessage: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(getString(R.string.label_prefix_blocking_description))
            .append(
                getString(R.string.label_here).color(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.colorPrimary
                    )
                )
                    .underline()
                    .click { }
            )
    }

    override fun ActivityForgotUpdatePasswordBinding.initializeView(savedInstanceState: Bundle?) {
        StatusBar.setLightStatusBar(window)
        setupUI()
        setupObserver()

    }

    private fun ActivityForgotUpdatePasswordBinding.setupObserver() {
        viewModel.apply {
            isValid.observe {
                btnLogin.isEnabled = this
            }

            hideErrorPasswordformat.observe {
                when {
                    this -> passwordWarning.visibility = View.GONE
                    etPassword.text?.isEmpty() == true -> passwordWarning.visibility =
                        View.GONE
                    else -> passwordWarning.visibility = View.VISIBLE
                }
            }

            hideErrorPasswordConfirmationFormat.observe {
                when {
                    this -> passwordConfirmationWarning.visibility = View.GONE
                    etPassword.text?.isEmpty() == true -> passwordConfirmationWarning.visibility =
                        View.GONE
                    else -> passwordConfirmationWarning.visibility = View.VISIBLE
                }
            }

            error.observe {
                if (this != null) {
                    this@ForgotUpdatePasswordActivity.showError(
                        this,
                        message ?: errorMessage,
                        ContextCompat.getDrawable(
                            applicationContext,
                            R.drawable.ic_not_logged_in
                        )
                    ) {
                        submitPayload()
                    }
                }
            }

            loading.observe {
                if (this) {
                    containerLoading.visibility = View.VISIBLE
                    etPassword.isEnabled = false
                    etPasswordConfirm.isEnabled = false
                    btnLogin.visibility = View.GONE
                } else {
                    containerLoading.visibility = View.GONE
                    etPassword.isEnabled = true
                    etPasswordConfirm.isEnabled = true
                    btnLogin.visibility = View.VISIBLE
                }
            }

            passwordIsUpdate.observe {
                if (this) {
                    Toast.makeText(
                        applicationContext,
                        "Berhasil update password, silahkan login ke akun anda ",
                        Toast.LENGTH_LONG
                    ).show()
                    goToLoginActivity()
                }
            }
        }
    }

    private fun ActivityForgotUpdatePasswordBinding.setupUI() {
        viewModel.attachInputFormat(etPassword, etPasswordConfirm)
        btnLogin.setOnClickListener { submitPayload() }
        var passwordHidden = false
        ivPasswordToggle.setOnClickListener {
            passwordHidden = if (passwordHidden) {
                passwordToggleVisibility(ivPasswordToggle, etPassword, false)
            } else {
                passwordToggleVisibility(ivPasswordToggle, etPassword, true)
            }
        }
        var passwordConfirmationHidden = false

        ivPasswordConfirmToggle.setOnClickListener {
            passwordConfirmationHidden = if (passwordConfirmationHidden) {
                passwordToggleVisibility(ivPasswordConfirmToggle, etPasswordConfirm, false)
            } else {
                passwordToggleVisibility(ivPasswordConfirmToggle, etPasswordConfirm, true)
            }
        }
    }

    private fun ActivityForgotUpdatePasswordBinding.submitPayload() {

        val map = HashMap<String, String?>()
        map["password"] = etPassword.text.toString()
//        viewModel.updatePassword(map, applicationContext)
    }

    private fun goToLoginActivity() {
        val intent = Intent(
            this,
            LoginActivity::class.java
        ).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        startActivity(intent)
    }
}