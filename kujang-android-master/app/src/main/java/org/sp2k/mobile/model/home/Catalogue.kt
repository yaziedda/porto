package org.sp2k.mobile.model.home


import com.google.gson.annotations.SerializedName

data class Catalogue(
    @SerializedName("created_at")
    val createdAt: String?= "",
    @SerializedName("id")
    val id: Int?= 0,
    @SerializedName("image")
    val image: String?= "",
    @SerializedName("name")
    val name: String?= "",
    @SerializedName("squence")
    val squence: Int?= 0,
    @SerializedName("status")
    val status: Int?= 0,
    @SerializedName("type")
    val type: Int?= 0,
    @SerializedName("type_value")
    val typeValue: String?= "",
    @SerializedName("updated_at")
    val updatedAt: String?= "",
    @SerializedName("price")
    val price: String?= "",
)