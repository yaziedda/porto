package org.sp2k.mobile.model


import com.google.gson.annotations.SerializedName

data class Cart(
    @SerializedName("checked")
    var checked: Int? = 0,
    @SerializedName("created_at")
    val createdAt: String? = "",
    @SerializedName("created_by")
    val createdBy: Int? = 0,
    @SerializedName("id")
    val id: Int? = 0,
    @SerializedName("id_product")
    val idProduct: Int? = 0,
    @SerializedName("id_user")
    val idUser: Int? = 0,
    @SerializedName("qty")
    var qty: Int = 0,
    @SerializedName("updated_at")
    val updatedAt: String? = "",
    @SerializedName("updated_by")
    val updatedBy: Int? = 0,
    @SerializedName("barang")
    val barang: Product
)