package org.sp2k.mobile.model

import org.sp2k.mobile.model.user.AuthData
import org.sp2k.mobile.model.user.Staff
import java.io.Serializable

data class SuratKeluar(
    val alasan: String? = "",
    val kepada: String? = "",
    val buttonVisible: Boolean? = false,
    val created_at: String? = "",
    val created_by: String? = "",
    val dokumen: String? = "",
    val id: Int? = 0,
    val nama: String? = "",
    val perihal_surat: String? = "",
    var status: String? = "",
    var status_disposisi: String? = "",
    val tanggal: String? = "",
    val tujuan_surat: String? = "",
    val updated_at: String? = "",
    val anggotaTertentu: List<Staff>? = arrayListOf()
): Serializable