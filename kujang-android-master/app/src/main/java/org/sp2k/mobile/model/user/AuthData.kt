package org.sp2k.mobile.model.user

import org.sp2k.mobile.model.Bidang
import org.sp2k.mobile.model.Group
import org.sp2k.mobile.model.Role

data class AuthData(
    val staff: Staff? = null,
    val group: Group? = null,
    val role: Role? = null,
    val bidang: Bidang? = null,
)