package org.sp2k.mobile.model

data class Group(
    val id: String = "",
    val code: String = "",
    val name: String = "",
)
