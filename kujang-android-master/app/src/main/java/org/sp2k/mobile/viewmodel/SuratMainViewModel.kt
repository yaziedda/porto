package org.sp2k.mobile.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.exception.GeneralException
import org.sp2k.mobile.model.*
import org.sp2k.mobile.model.request.DisposisiRequest
import org.sp2k.mobile.model.request.SuratKeluarApproveRequest
import org.sp2k.mobile.model.request.SuratKeluarRejectRequest
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import org.sp2k.mobile.utils.PreferenceManager
import javax.inject.Inject

@HiltViewModel
class SuratMainViewModel @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository,
    private val preferenceManager: PreferenceManager
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _inComingMailList = MutableLiveData<List<Mail>?>()
    val inComingMailList: LiveData<List<Mail>?>
        get() = _inComingMailList

    private val _suratMasukDetail = MutableLiveData<Mail?>()
    val suratMasukDetail: LiveData<Mail?>
        get() = _suratMasukDetail

    private val _suratKeluarDetail = MutableLiveData<SuratKeluar?>()
    val suratKeluarDetail: LiveData<SuratKeluar?>
        get() = _suratKeluarDetail

    private val _outComingMailList = MutableLiveData<List<SuratKeluar>?>()
    val outComingMailList: LiveData<List<SuratKeluar>?>
        get() = _outComingMailList

    private val _groupList = MutableLiveData<List<GroupMail>?>()
    val groupList: LiveData<List<GroupMail>?>
        get() = _groupList

    private val _userList = MutableLiveData<List<Staff>?>()
    val staffList: LiveData<List<Staff>?>
        get() = _userList

    private val _disposisiSuccess = MutableLiveData<Boolean?>()
    val disposisiSuccess: LiveData<Boolean?>
        get() = _disposisiSuccess

    private val _approveSuratKeluarSuccess = MutableLiveData<Boolean?>()
    val approveSuratKeluarSuccess: LiveData<Boolean?>
        get() = _approveSuratKeluarSuccess

    private val _rejectSuratKeluarSuccess = MutableLiveData<Boolean?>()
    val rejectSuratKeluarSuccess: LiveData<Boolean?>
        get() = _rejectSuratKeluarSuccess

    fun resetState() {
        setError(null)
        _inComingMailList.value = null
        _disposisiSuccess.value = null
    }

    fun fetchInComingMail() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.suratMasuk()
            if (response.state && response.data != null) {
                _inComingMailList.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _inComingMailList.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _inComingMailList.value = null
            showError(exception)
        }

    }

    fun fetchOutComingMail() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.suratKeluar()
            if (response.state && response.data != null) {
                _outComingMailList.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _outComingMailList.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _outComingMailList.value = null
            showError(exception)
        }

    }

    fun suratMasukById(id: String) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.suratMasukByID(id)
            if (response.state && response.data != null) {
                _suratMasukDetail.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _suratMasukDetail.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _suratMasukDetail.value = null
            showError(exception)
        }
    }

    fun suratKeluarById(id: String) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.suratKeluarByID(id)
            if (response.state && response.data != null) {
                _suratKeluarDetail.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _suratKeluarDetail.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _suratKeluarDetail.value = null
            showError(exception)
        }
    }

    fun fetchGroups() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.groups()
            if (response.state && response.data != null) {
                _groupList.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _groupList.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _groupList.value = null
            showError(exception)
        }
    }

    fun searchGroup(search: String) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.search(search)
            if (response.state && response.data != null) {
                _userList.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _userList.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _userList.value = null
            showError(exception)
        }
    }

    fun sendDisposisi(disposisiRequest: DisposisiRequest) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.disposisi(disposisiRequest)
            if (response.state) {
                _loading.value = false
                _disposisiSuccess.value = true
            } else {
                _loading.value = false
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun approveSuratKeluar(suratKeluarApproveRequest: SuratKeluarApproveRequest) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.approveSuratKeluar(suratKeluarApproveRequest)
            if (response.state) {
                _loading.value = false
                if (suratKeluarApproveRequest.status_approve == "1") {
                    _approveSuratKeluarSuccess.value = true
                } else {
                    _rejectSuratKeluarSuccess.value = true
                }
            } else {
                _loading.value = false
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun rejectSuratKeluar(suratKeluarRejectRequest: SuratKeluarRejectRequest) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.rejectSuratKeluar(suratKeluarRejectRequest)
            if (response.state) {
                _loading.value = false
                _rejectSuratKeluarSuccess.value = true
            } else {
                _loading.value = false
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun sendDisposisiKeluar(disposisiRequest: DisposisiRequest) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.disposisiKeluar(disposisiRequest)
            if (response.state) {
                _loading.value = false
                _disposisiSuccess.value = true
            } else {
                _loading.value = false
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun getUser() = preferenceManager.getUser()

}