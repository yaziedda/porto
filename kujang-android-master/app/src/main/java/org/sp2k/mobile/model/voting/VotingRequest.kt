package org.sp2k.mobile.model.voting

data class VotingRequest(
    var status: String = "",
    val id: String,
)