package org.sp2k.mobile.model

import java.io.Serializable

data class Bidang(
    val id: String? = "",
    val code: String? = "",
    val name: String? = "",
): Serializable
