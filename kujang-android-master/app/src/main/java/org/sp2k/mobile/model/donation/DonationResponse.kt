package org.sp2k.mobile.model.donation


import com.google.gson.annotations.SerializedName

data class DonationResponse(
    @SerializedName("code")
    val code: Int? = 0,
    @SerializedName("data")
    val `data`: List<Donation>? = arrayListOf(),
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("state")
    val state: Boolean? = false
)