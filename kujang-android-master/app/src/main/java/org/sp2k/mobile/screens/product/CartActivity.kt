package org.sp2k.mobile.screens.product

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItems
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityCartBinding
import org.sp2k.mobile.databinding.ItemCartBinding
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.toCurrency
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.Cart
import org.sp2k.mobile.model.City
import org.sp2k.mobile.model.Province
import org.sp2k.mobile.model.RajaOngkirCost
import org.sp2k.mobile.screens.feature.WebViewActivity
import org.sp2k.mobile.viewmodel.ProductViewModel
import it.sephiroth.android.library.numberpicker.doOnProgressChanged
import kotlinx.android.synthetic.main.loading_view.*
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import timber.log.Timber

@AndroidEntryPoint
class CartActivity : BaseActivity<ActivityCartBinding>() {
    private val viewModel by viewModels<ProductViewModel>()
    override fun getLayoutId(): Int = R.layout.activity_cart
    private lateinit var recyclerAdapter: RecyclerViewAdapter<Cart, ItemCartBinding>
    val checkOutMap = HashMap<String, String?>()
    override fun ActivityCartBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupObserver()
        fetcher()
        binding.button.setOnClickListener {
            viewModel.province()
        }
    }

    private fun ActivityCartBinding.setupActionBar() {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.title = getString(R.string.cart)
        }
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchCarts()
        }
    }

    private fun ActivityCartBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    progressBar.visible()
                    swipeContainer.isRefreshing = true
                    recyclerView.gone()
                    totalWrapper.gone()
                } else {
                    progressBar.gone()
                    totalWrapper.visible()
                    recyclerView.visible()
                    swipeContainer.isRefreshing = false
                }
            }

            loadingCart.observe {
                if (this) {
                    showProgressDialog()
                } else {
                    dissmissProgressDialog()
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }

            cartList.observe {
                recyclerAdapter.updateList(this)
            }

            total.observe {
                if (this != null) {
                    binding.tvTotal.text = this.toString().toCurrency()
                    binding.button.isEnabled = this > 0
                }
            }

            provinces.observe {
                this?.showProvincesDialog()
            }

            cites.observe {
                this?.showCitiesDialog()
            }

            rajaOngkir.observe {
                this?.rajaOngkirCosts?.showRajaOngkirs(this.name ?: "")
            }

            midtransUrl.observe {
                if (this != null) {
                    val intent = Intent(applicationContext, WebViewActivity::class.java)
                    intent.putExtra("value", this)
                    intent.putExtra("name", "Pilih Pembayaran")
                    startActivity(intent)
                    finish()
                }
            }
        }
    }

    private fun List<Province>.showProvincesDialog() {
        val builderSingle: AlertDialog.Builder = AlertDialog.Builder(this@CartActivity)
        builderSingle.setIcon(R.drawable.logo)
        builderSingle.setTitle("Pilih Provinsi")
        val arrayAdapter =
            ArrayAdapter<Province>(
                applicationContext,
                android.R.layout.select_dialog_singlechoice
            )
        this.forEach {
            arrayAdapter.add(it)
        }
        builderSingle.setNegativeButton(
            "cancel"
        ) { dialog, _ -> dialog.dismiss() }

        builderSingle.setAdapter(
            arrayAdapter
        ) { _, which ->
            val model = arrayAdapter.getItem(which)
            viewModel.cities(model?.provinceId.toString())
        }
        builderSingle.show()
    }

    private fun List<City>.showCitiesDialog() {
        val builderSingle: AlertDialog.Builder = AlertDialog.Builder(this@CartActivity)
        builderSingle.setIcon(R.drawable.logo)
        builderSingle.setTitle("Pilih Kabupaten/Kota")
        val arrayAdapter =
            ArrayAdapter<City>(
                applicationContext,
                android.R.layout.select_dialog_singlechoice
            )
        this.forEach {
            arrayAdapter.add(it)
        }
        builderSingle.setNegativeButton(
            "cancel"
        ) { dialog, _ -> dialog.dismiss() }

        builderSingle.setAdapter(
            arrayAdapter
        ) { _, which ->
            val model = arrayAdapter.getItem(which)
            val myItems = listOf("JNE", "POS", "TIKI")
            MaterialDialog(this@CartActivity).show {
                listItems(items = myItems) { _, index, _ ->
                    when (index) {
                        0 -> {
                            getCost("jne", model)
                        }
                        1 -> {
                            getCost("pos", model)
                        }
                        2 -> {
                            getCost("tiki", model)

                        }
                    }
                }
            }
        }
        builderSingle.show()
    }

    private fun List<RajaOngkirCost>.showRajaOngkirs(courier: String) {
        val builderSingle: AlertDialog.Builder = AlertDialog.Builder(this@CartActivity)
        builderSingle.setIcon(R.drawable.logo)
        builderSingle.setTitle("Pilih Service")
        val arrayAdapter =
            ArrayAdapter<RajaOngkirCost>(
                applicationContext,
                android.R.layout.select_dialog_singlechoice
            )
        this.forEach {
            it.courier = courier
            arrayAdapter.add(it)
        }
        builderSingle.setNegativeButton(
            "cancel"
        ) { dialog, _ -> dialog.dismiss() }

        builderSingle.setAdapter(
            arrayAdapter
        ) { _, which ->
            val model = arrayAdapter.getItem(which)
            checkOutMap["delivery_service"] = model?.service
            checkOutMap["delivery_amount"] = model?.rajaOngkirCost?.value.toString()
            checkOutMap["delivery_status"] = model?.rajaOngkirCost?.etd
            viewModel.setOngkirTotal(model?.rajaOngkirCost?.value ?: 0.0)
            binding.button.text = "Checkout"
            binding.button.setOnClickListener {
                viewModel.checkout(checkOutMap)
            }
        }
        builderSingle.show()
    }

    private fun getCost(courier: String, model: City?) {
        val map = HashMap<String, String?>()
        map["origin"] = "171"
        map["destination"] = model?.cityId
        map["weight"] = viewModel.getWeight().toString()
        map["courier"] = courier
        checkOutMap["delivery_id"] = courier
        viewModel.cost(map)
    }

    private fun ActivityCartBinding.setupAdapter() {
        recyclerAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_cart,
            BR.cart
        ) { itemView, itemModel ->
            itemView.itemCartTvTitle.text = itemModel.barang.name
            itemView.itemCartContAmount.text = itemModel.barang.price.toString().toCurrency()
            Glide.with(applicationContext)
                .load(itemModel.barang.image)
                .into(itemView.itemCartIvImage)

            itemView.numberPicker.setProgress(itemModel.qty)
            itemView.numberPicker.doOnProgressChanged { numberPicker, progress, formUser ->
                itemModel.qty = progress
                viewModel.calculateTotal()
                viewModel.cartUpdateQty(itemModel.id.toString(), itemModel.qty.toString())
            }
            itemView.checkBox.isChecked = itemModel.checked == 1

            itemView.checkBox.setOnCheckedChangeListener { _, b ->
                if (b) {
                    itemModel.checked = 1
                } else {
                    itemModel.checked = 0
                }

                viewModel.cartUnchecked(itemModel.id.toString(), itemModel.checked.toString())
                viewModel.calculateTotal()
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            applicationContext,
            LinearLayoutManager.VERTICAL,
            false
        )
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerAdapter
        }
    }

}