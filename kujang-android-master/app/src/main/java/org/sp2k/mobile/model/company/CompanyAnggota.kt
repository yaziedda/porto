package org.sp2k.mobile.model.company

data class CompanyAnggota(
    val created_at: String,
    val id: Int,
    val image: String,
    val name: String,
    val title: String,
    val type: String,
    val updated_at: String
)