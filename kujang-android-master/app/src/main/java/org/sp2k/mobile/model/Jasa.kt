package org.sp2k.mobile.model


import com.google.gson.annotations.SerializedName

data class Jasa(
    @SerializedName("alamat")
    val alamat: String?,
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("created_by")
    val createdBy: Int?,
    @SerializedName("file")
    val `file`: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("latitude")
    val latitude: String?,
    @SerializedName("longitude")
    val longitude: String?,
    @SerializedName("nama_jasa")
    val namaJasa: String?,
    @SerializedName("no_telp")
    val noTelp: String?,
    @SerializedName("status")
    val status: Int?,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("updated_by")
    val updatedBy: Int?
)