package org.sp2k.mobile.screens.news

import android.annotation.SuppressLint
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.PopupMenu
import androidx.cardview.widget.CardView
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.sp2k.mobile.R
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.news.NewsComment
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.Constants.IS_PENGURUS_INTI
import org.sp2k.mobile.utils.DateUtils
import java.util.*


class NewsCommentAdapter(
    val newsDetailActivity: NewsDetailActivity,
    val model: NewsComment,
    val comment: String = "",
    val newsId: String = ""
) :
    AbstractItem<NewsCommentAdapter.ViewHolder>() {

    override val type: Int = R.id.view_news_comment_id
    override val layoutRes: Int = R.layout.item_news_comment

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    class ViewHolder(view: View) : FastAdapter.ViewHolder<NewsCommentAdapter>(view) {

        private val itemImageCircleBg =
            view.findViewById<CircleImageView>(R.id.item_news_comment_image_bg)
        private val itemName = view.findViewById<AppCompatTextView>(R.id.item_news_comment_people)
        private val itemTime = view.findViewById<AppCompatTextView>(R.id.item_news_comment_time)
        private val itemComment = view.findViewById<AppCompatTextView>(R.id.item_news_comment_text)
        private val itemCommentOption =
            view.findViewById<AppCompatImageView>(R.id.item_news_comment_option)

        @SuppressLint("SetTextI18n")
        override fun bindView(item: NewsCommentAdapter, payloads: List<Any>) {
            with(item) {
                itemName.text = model.user.admin_nama
                itemComment.text = model.comment.comment

                Glide.with(newsDetailActivity)
                    .load(model.user.admin_foto)
                    .error(R.drawable.ic_profile_default)
                    .into(itemImageCircleBg)

                itemTime.text = DateUtils.let { date ->
                    date.convertLongDateToAgoString(
                        date.parseDate(
                            model.comment.created_at,
                            Constants.yyyy_MM_dd_HH_mm_ss
                        )?.time ?: 0
                    )
                }

                initOptionItemDelete()

                if (model.send == -1) {
                    itemTime.text = "Mengirim.."
                    newsDetailActivity.viewModel.apply {
                        val map: HashMap<String, String> = hashMapOf()
                        map["comment"] = comment
                        map["id_berita"] = newsId
                        map["comment_id"] = model.comment.comment_id

                        sendIDComment.observe(newsDetailActivity) { data ->
                            data?.let {
                                if (it.comment.comment_id == model.comment.comment_id) {
                                    model.comment.created_at = it.comment.created_at
                                    model.send = 0
                                    viewModelScope.launch {
                                        delay(1000L)
                                        itemTime.text =
                                            DateUtils.let { date ->
                                                date.convertLongDateToAgoString(
                                                    date.parseDate(
                                                        model.comment.created_at,
                                                        Constants.yyyy_MM_dd_HH_mm_ss
                                                    )?.time ?: 0
                                                )
                                            }
                                        newsDetailActivity.showLoadMoreLoading(false)
                                        initOptionItemDelete()
                                    }
                                }
                            }
                        }
                        sendNewsComment(map)
                    }
                    itemCommentOption.gone()
                }

            }
        }

        private fun NewsCommentAdapter.initOptionItemDelete() {
            val userData = newsDetailActivity.getUser()?.staff
            val role = newsDetailActivity.getUser()?.role
            val group = newsDetailActivity.getUser()?.group
            if (userData?.id.toString() == model.comment.id_user || role?.role_code in Constants.CAN_DELETE_COMMENT) {
                itemCommentOption.apply {
                    visible()
                    setOnClickListener {
                        val popupMenu = PopupMenu(newsDetailActivity, it)

                        popupMenu.menuInflater.inflate(
                            R.menu.menu_delete_comment,
                            popupMenu.menu
                        )
                        popupMenu.setOnMenuItemClickListener(object :
                            android.widget.PopupMenu.OnMenuItemClickListener,
                            PopupMenu.OnMenuItemClickListener {
                            override fun onMenuItemClick(menu: MenuItem?): Boolean {
                                when (menu?.itemId) {
                                    R.id.delete -> {
                                        newsDetailActivity.showDialogOptionBottomSheet(
                                            "Apakah anda yakin akan menghapus komentar?",
                                            {
                                                newsDetailActivity.viewModel.deleteNewsComment(model.comment.comment_id)
                                            })
                                    }
                                }
                                return true
                            }

                        })
                        popupMenu.show()
                    }
                }
            } else {
                itemCommentOption.gone()
            }
        }

        override fun unbindView(item: NewsCommentAdapter) {
            itemName.text = ""
            itemComment.text = ""
            itemTime.text = ""

        }

    }
}