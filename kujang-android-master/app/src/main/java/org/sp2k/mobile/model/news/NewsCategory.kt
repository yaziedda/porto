package org.sp2k.mobile.model.news

data class NewsCategory(
    val created_at: String = "",
    val id: Int = 0,
    val kategori: String = "",
    val terbit: Int = 0,
    val updated_at: String = ""
)