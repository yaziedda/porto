package org.sp2k.mobile.screens.donation

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.andrognito.pinlockview.PinLockListener
import dagger.hilt.android.AndroidEntryPoint
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityDonationNumpadBinding
import org.sp2k.mobile.extensions.toCurrency
import org.sp2k.mobile.screens.feature.WebViewActivity
import org.sp2k.mobile.viewmodel.DonationViewModel
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*


@AndroidEntryPoint
class DonationNumpadActivity : BaseActivity<ActivityDonationNumpadBinding>() {
    override fun getLayoutId(): Int = R.layout.activity_donation_numpad
    private val viewModel by viewModels<DonationViewModel>()
    private lateinit var id: String

    companion object {
        const val KEY_ID = "KEY_ID"
    }

    override fun ActivityDonationNumpadBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        getExtras()
        initObserver()
        setupNumpad()
        buttonDonation.isEnabled = false
        buttonDonation.setOnClickListener {
            val map = HashMap<String, String?>()
            map["id_donasi"] = id
            map["amount"] = textNominal.tag.toString()
            viewModel.sendDonasi(map)
        }
    }

    private val mPinLockListener: PinLockListener = object : PinLockListener {
        override fun onComplete(pin: String) {
        }

        override fun onEmpty() {
            with(binding){
                textNominal.tag = "0"
                textNominal.text = "0".toCurrency()
                buttonDonation.isEnabled = false
            }
        }

        override fun onPinChange(pinLength: Int, intermediatePin: String) {
            with(binding) {
                textNominal.tag = intermediatePin
                textNominal.text = intermediatePin.toCurrency()
                buttonDonation.isEnabled = pinLength > 0
            }
        }
    }

    private fun ActivityDonationNumpadBinding.setupNumpad() {
        pinLockView.pinLength = 12
        pinLockView.setPinLockListener(mPinLockListener)
    }

    private fun getExtras() {
        id = intent?.extras?.getString(KEY_ID) ?: ""
    }

    private fun initObserver() {
        with(viewModel) {
            resetState()

            midtransUrl.observe {
                if (this != null) {
                    val intent = Intent(applicationContext, WebViewActivity::class.java)
                    intent.putExtra("value", this)
                    intent.putExtra("name", "Pilih Pembayaran")
                    startActivity(intent)
                    finish()
                }
            }

            loadingCart.observe {
                if (this) {
                    showProgressDialog()
                } else {
                    dissmissProgressDialog()
                }
            }
        }
    }


    private fun ActivityDonationNumpadBinding.setupActionBar() {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
        }
    }
}