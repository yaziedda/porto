package org.sp2k.mobile.screens.proker

import android.content.Context
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import org.sp2k.mobile.R
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.prokeranggaran.ProkerData
import org.sp2k.mobile.utils.Constants

class StatusUseCase {

    companion object {
        fun AppCompatButton.constructPokerStatus(
            role: String,
            applicationContext: Context,
            itemModel: ProkerData,
            itemProkerStatusA: AppCompatButton
        ) {
            gone()

            if (itemModel.usulan?.approval_status == 1) {
                visible()
                text = "Sudah Disahkan"
                setTextColor(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.white
                    )
                )
                background = ContextCompat.getDrawable(
                    applicationContext,
                    R.drawable.btn_mail_approved
                )
            }

            if (itemModel.proker?.id != null && itemModel.proker.id > 0) {
                visible()
                itemModel.proker.status_code?.apply {
                    when {
                        equals("00") -> {
                            text = "Menunggu Acc Bidang"
                            setTextColor(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.mail_waiting
                                )
                            )
                            background = ContextCompat.getDrawable(
                                applicationContext,
                                R.drawable.btn_mail_waiting
                            )
                        }

                        equals("A3") -> {
                            text = "Menunggu Acc Ketum/Sekum"
                            setTextColor(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.mail_waiting
                                )
                            )
                            background = ContextCompat.getDrawable(
                                applicationContext,
                                R.drawable.btn_mail_waiting
                            )

                            itemProkerStatusA.apply {
                                visible()
                                text = "Disetujui Bidang"
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.white
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_blue
                                )
                            }
                        }

                        equals("A1") || equals("A2") -> {
                            if (role in Constants.CAN_SHOW_ANGGARAN) {
                                text = "Disetujui "
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.white
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_approved
                                )
                                when (itemModel.proker.status_anggaran) {
                                    0 -> {
                                        itemProkerStatusA.apply {
                                            visible()
                                            text = "Belum Transfer"
                                            setTextColor(
                                                ContextCompat.getColor(
                                                    applicationContext,
                                                    R.color.mail_waiting
                                                )
                                            )
                                            background = ContextCompat.getDrawable(
                                                applicationContext,
                                                R.drawable.btn_mail_waiting
                                            )
                                        }
                                    }
                                    2 -> {
                                        itemProkerStatusA.apply {
                                            visible()
                                            text = "Anggaran Konfirmasi Bidang"
                                            setTextColor(
                                                ContextCompat.getColor(
                                                    applicationContext,
                                                    R.color.white
                                                )
                                            )
                                            background = ContextCompat.getDrawable(
                                                applicationContext,
                                                R.drawable.btn_mail_blue
                                            )
                                        }
                                    }
                                    -1 -> {
                                        itemProkerStatusA.apply {
                                            visible()
                                            text = "Transfer Ditolak"
                                            setTextColor(
                                                ContextCompat.getColor(
                                                    applicationContext,
                                                    R.color.white
                                                )
                                            )
                                            background = ContextCompat.getDrawable(
                                                applicationContext,
                                                R.drawable.btn_mail_rejected
                                            )
                                        }
                                    }
                                    1 -> {
                                        itemProkerStatusA.apply {
                                            visible()
                                            text = "Berjalan "
                                            setTextColor(
                                                ContextCompat.getColor(
                                                    applicationContext,
                                                    R.color.white
                                                )
                                            )
                                            background = ContextCompat.getDrawable(
                                                applicationContext,
                                                R.drawable.btn_mail_blue
                                            )
                                        }
                                    }
                                }
                            } else {
                                text = "Berjalan "
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.white
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_blue
                                )
                                itemProkerStatusA.gone()
                            }
                        }

                        startsWith("N") -> {
                            text = "DITOLAK"
                            setTextColor(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.mail_rejected
                                )
                            )
                            background = ContextCompat.getDrawable(
                                applicationContext,
                                R.drawable.btn_mail_rejected
                            )
                        }

                        role in arrayListOf(
                            "SEKUM",
                            "KETUA_UMUM",
                            "KETUA_BIDANG"
                        ) && (itemModel.proker.id == 0) -> {
                            text = "Belum Berjalan"
                            setTextColor(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.white
                                )
                            )
                            background = ContextCompat.getDrawable(
                                applicationContext,
                                R.drawable.btn_mail_blue
                            )
                        }
                    }
                }
            }

            if (itemModel.lpj?.id != null && itemModel.lpj.id > 0) {
                itemProkerStatusA.gone()
                visible()
                if (role in Constants.CAN_SHOW_ANGGARAN) {
                    itemModel.lpj.status_code?.apply {
                        when {
                            equals("00") -> {
                                text = "LPJ Menunggu Acc Bidang"
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.mail_waiting
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_waiting
                                )
                            }

                            equals("A3") -> {
                                text = "LPJ Menunggu Acc Ketum/Sekum"
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.mail_waiting
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_waiting
                                )

                                itemProkerStatusA.apply {
                                    visible()
                                    text = "LPJ Disetujui Bidang & Bendahara"
                                    setTextColor(
                                        ContextCompat.getColor(
                                            applicationContext,
                                            R.color.white
                                        )
                                    )
                                    background = ContextCompat.getDrawable(
                                        applicationContext,
                                        R.drawable.btn_mail_blue
                                    )
                                }
                            }

                            equals("A4") -> {
                                text = "LPJ Menunggu Acc Bendahara"
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.mail_waiting
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_waiting
                                )

                                itemProkerStatusA.apply {
                                    visible()
                                    text = "LPJ Disetujui Bidang"
                                    setTextColor(
                                        ContextCompat.getColor(
                                            applicationContext,
                                            R.color.white
                                        )
                                    )
                                    background = ContextCompat.getDrawable(
                                        applicationContext,
                                        R.drawable.btn_mail_blue
                                    )
                                }
                            }

                            equals("A1") || equals("A2") -> {
                                if (role in Constants.CAN_SHOW_ANGGARAN) {
                                    text = "LJP Disetujui "
                                    setTextColor(
                                        ContextCompat.getColor(
                                            applicationContext,
                                            R.color.white
                                        )
                                    )
                                    background = ContextCompat.getDrawable(
                                        applicationContext,
                                        R.drawable.btn_mail_approved
                                    )
                                }
                            }

                            startsWith("N") -> {
                                text = "DITOLAK"
                                setTextColor(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.mail_rejected
                                    )
                                )
                                background = ContextCompat.getDrawable(
                                    applicationContext,
                                    R.drawable.btn_mail_rejected
                                )
                            }
                        }
                    }
                } else {
                    if (itemModel.lpj.approval_status == 1) {
                        text = "Selesai"
                        setTextColor(
                            ContextCompat.getColor(
                                applicationContext,
                                R.color.white
                            )
                        )
                        background = ContextCompat.getDrawable(
                            applicationContext,
                            R.drawable.btn_mail_approved
                        )
                    } else {
                        text = "Berjalan"
                        setTextColor(
                            ContextCompat.getColor(
                                applicationContext,
                                R.color.white
                            )
                        )
                        background = ContextCompat.getDrawable(
                            applicationContext,
                            R.drawable.btn_mail_blue
                        )
                    }
                }
            }
        }
    }
}