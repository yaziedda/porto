package org.sp2k.mobile.model.prokeranggaran

import java.io.Serializable

data class Usulan(
    val approval_id: Int? = 0,
    val approval_status: Int? = 0,
    val created_at: String? = "",
    val created_by: Int? = 0,
    val date_end_estimate: String? = "",
    val date_start_estimate: String? = "",
    val description: String? = "",
    val `file`: String? = "",
    val id: Int? = 0,
    val id_bidang: Int? = 0,
    val keplak_name: String? = "",
    val name: String? = "",
    val status: Int? = 0,
    val updated_at: String? = "",
    val updated_by: Int? = 0,
    val usulan_alasan: String? = "",
    val usulan_status: String
) : Serializable