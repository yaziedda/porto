package org.sp2k.mobile.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.exception.GeneralException
import org.sp2k.mobile.model.AnggaranBidang
import org.sp2k.mobile.model.ProkerResponse
import org.sp2k.mobile.model.prokeranggaran.ProkerData
import org.sp2k.mobile.model.request.ProkerApproval
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import org.sp2k.mobile.utils.PreferenceManager
import javax.inject.Inject

@HiltViewModel
class ProkerViewModel @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository,
    private val preferenceManager: PreferenceManager
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _prokerList = MutableLiveData<ProkerResponse?>()
    val prokerList: LiveData<ProkerResponse?>
        get() = _prokerList

    private val _prokerData = MutableLiveData<ProkerData?>()
    val prokerData: LiveData<ProkerData?>
        get() = _prokerData

    private val _anggaranBidang = MutableLiveData<AnggaranBidang?>()
    val anggaranBidang: LiveData<AnggaranBidang?>
        get() = _anggaranBidang

    private val _isSubmited: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val isSubmited: MutableLiveData<Boolean>
        get() = _isSubmited


    fun resetState() {
        setError(null)
        _prokerList.value = null
    }

    fun fetchList(type: String) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.prokerList(type)
            if (response.state && response.data != null) {
                _prokerList.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _prokerList.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _prokerList.value = null
            showError(exception)
        }
    }

    fun fetchById(id: String) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.prokerById(id)
            if (response.state && response.data != null) {
                _prokerData.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _prokerData.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _prokerList.value = null
            showError(exception)
        }
    }

    fun prokerApproval(prokerApproval: ProkerApproval) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.prokerApproval(prokerApproval)
            if (response.state) {
                _loading.value = false
                _isSubmited.value = true
            } else {
                _loading.value = false
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun fetchAnggaranList() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.anggaran()
            if (response.state && response.data != null) {
                _anggaranBidang.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _anggaranBidang.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _anggaranBidang.value = null
            showError(exception)
        }
    }


    fun getUser() = preferenceManager.getUser()

}