package org.sp2k.mobile.repository

import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.sp2k.mobile.model.*
import org.sp2k.mobile.model.blog.Blog
import org.sp2k.mobile.model.company.CompanyData
import org.sp2k.mobile.model.donation.Donation
import org.sp2k.mobile.model.news.NewsComment
import org.sp2k.mobile.model.news.NewsCommentData
import org.sp2k.mobile.model.polling.Polling
import org.sp2k.mobile.model.polling.PollingRequest
import org.sp2k.mobile.model.prokeranggaran.ProkerData
import org.sp2k.mobile.model.request.DisposisiRequest
import org.sp2k.mobile.model.request.ProkerApproval
import org.sp2k.mobile.model.request.SuratKeluarApproveRequest
import org.sp2k.mobile.model.request.SuratKeluarRejectRequest
import org.sp2k.mobile.model.user.AuthData
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.model.voting.VotingRes
import javax.inject.Inject

class RemoteRepositoryDaoImpl @Inject constructor(
    private val remoteRepositoryService: RemoteRepositoryService
) : RemoteRepositoryDao {
    override suspend fun getHome(): BaseResponse<Home> {
        return remoteRepositoryService.getHome()
    }

    override suspend fun getDonation(): BaseResponse<List<Donation>> {
        return remoteRepositoryService.getDonasi()
    }

    override suspend fun getDonationById(id: String): BaseResponse<Donation> {
        return remoteRepositoryService.getDonasiById(id)
    }

    override suspend fun sendDonasi(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryService.sendDonasi(map)
    }

    override suspend fun getProducts(): BaseResponse<List<Product>> {
        return remoteRepositoryService.getProducts()
    }

    override suspend fun getProductById(id: String): BaseResponse<Product> {
        return remoteRepositoryService.getProductById(id)
    }

    override suspend fun addToCart(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryService.addToCart(map)
    }

    override suspend fun getCart(): BaseResponse<List<Cart>> {
        return remoteRepositoryService.getCart()
    }

    override suspend fun cartUnchecked(id: String, status: String): BaseResponse<String> {
        return remoteRepositoryService.cartUnchecked(id, status)
    }

    override suspend fun cartUpdateQty(id: String, qty: String): BaseResponse<String> {
        return remoteRepositoryService.cartUpdateQty(id, qty)
    }

    override suspend fun province(): BaseResponse<List<Province>> {
        return remoteRepositoryService.province()
    }

    override suspend fun city(id: String): BaseResponse<List<City>> {
        return remoteRepositoryService.city(id)
    }

    override suspend fun cost(map: HashMap<String, String?>?): BaseResponse<RajaOngkir> {
        return remoteRepositoryService.cost(map)
    }

    override suspend fun checkout(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryService.checkout(map)
    }

    override suspend fun history(): BaseResponse<List<TransactionHistory>> {
        return remoteRepositoryService.history()
    }

    override suspend fun blog(): BaseResponse<List<Blog>> {
        return remoteRepositoryService.blog()
    }

    override suspend fun jasa(): BaseResponse<List<Jasa>> {
        return remoteRepositoryService.jasa()
    }

    override suspend fun jasaById(id: String): BaseResponse<Jasa> {
        return remoteRepositoryService.jasaById(id)
    }

    override suspend fun tokenUpdate(map: HashMap<String, String?>?): BaseResponse<String> {
        return remoteRepositoryService.tokenUpdate(map)
    }

    override suspend fun login(map: HashMap<String, String>): BaseResponse<AuthData> {
        return remoteRepositoryService.login(map)
    }

    override suspend fun news(): BaseResponse<List<News>> {
        return remoteRepositoryService.news()
    }

    override suspend fun company(): BaseResponse<CompanyData> {
        return remoteRepositoryService.company()
    }

    override suspend fun newsById(id: String): BaseResponse<NewsDetail> {
        return remoteRepositoryService.newsById(id)
    }

    override suspend fun newsCommentById(id: String, page: Int): BaseResponse<NewsCommentData> {
        return remoteRepositoryService.newsCommentById(id, page)
    }

    override suspend fun newsCommentSend(map: HashMap<String, String>): BaseResponse<NewsComment> {
        return remoteRepositoryService.newsCommentSend(map)
    }

    override suspend fun search(search: String): BaseResponse<List<Staff>> {
        return remoteRepositoryService.search(search)
    }

    override suspend fun groups(): BaseResponse<List<GroupMail>> {
        return remoteRepositoryService.groups()
    }

    override suspend fun suratMasuk(): BaseResponse<List<Mail>> {
        return remoteRepositoryService.suratMasuk()
    }

    override suspend fun disposisi(disposisiRequest: DisposisiRequest): BaseResponse<Any> {
        return remoteRepositoryService.disposisi(disposisiRequest)
    }

    override suspend fun suratKeluar(): BaseResponse<List<SuratKeluar>> {
        return remoteRepositoryService.suratKeluar()
    }

    override suspend fun approveSuratKeluar(suratKeluarApproveRequest: SuratKeluarApproveRequest): BaseResponse<Any> {
        return remoteRepositoryService.approveSuratKeluar(suratKeluarApproveRequest)
    }

    override suspend fun disposisiKeluar(disposisiRequest: DisposisiRequest): BaseResponse<Any> {
        return remoteRepositoryService.disposisiKeluar(disposisiRequest)
    }

    override suspend fun rejectSuratKeluar(suratKeluarRejectRequest: SuratKeluarRejectRequest): BaseResponse<Any> {
        return remoteRepositoryService.rejectSuratKeluar(suratKeluarRejectRequest)
    }

    override suspend fun deleteCommentById(id: String): BaseResponse<Any> {
        return remoteRepositoryService.deleteCommentById(id)
    }

    override suspend fun prokerList(type: String): BaseResponse<ProkerResponse> {
        return remoteRepositoryService.prokerList(type)
    }

    override suspend fun prokerById(id: String): BaseResponse<ProkerData> {
        return remoteRepositoryService.prokerById(id)
    }

    override suspend fun prokerApproval(prokerApproval: ProkerApproval): BaseResponse<Any> {
        return remoteRepositoryService.prokerApproval(prokerApproval)
    }

    override suspend fun anggaran(): BaseResponse<AnggaranBidang> {
        return remoteRepositoryService.anggaran()
    }

    override suspend fun pollingList(): BaseResponse<List<Polling>> {
        return remoteRepositoryService.pollingList()
    }

    override suspend fun votingList(): BaseResponse<VotingRes> {
        return remoteRepositoryService.votingList()
    }

    override suspend fun pollingVote(pollingRequest: PollingRequest): BaseResponse<Any> {
        return remoteRepositoryService.pollingVote(pollingRequest)
    }

    override suspend fun voteVote(
        status: RequestBody?,
        id: RequestBody?,
        file: MultipartBody.Part?
    ): BaseResponse<Any> {
        return remoteRepositoryService.voteVote(status, id, file)
    }

    override suspend fun changeProfile(file: MultipartBody.Part?): BaseResponse<String> {
        return remoteRepositoryService.changeProfile(file)
    }

    override suspend fun changePassword(changePasswordRequest: ChangePasswordRequest): BaseResponse<Any> {
        return remoteRepositoryService.changePassword(changePasswordRequest)
    }

    override suspend fun suratMasukByID(id: String): BaseResponse<Mail> {
        return remoteRepositoryService.suratMasukById(id)
    }

    override suspend fun suratKeluarByID(id: String): BaseResponse<SuratKeluar> {
        return remoteRepositoryService.suratKeluarById(id)
    }

    override suspend fun notificationList(): BaseResponse<List<KujangNotification>> {
        return remoteRepositoryService.notificationList()
    }
}