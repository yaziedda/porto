package org.sp2k.mobile.viewmodel

import android.text.Selection
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.exception.GeneralException
import org.sp2k.mobile.extensions.afterTextChanged
import org.sp2k.mobile.model.ChangePasswordRequest
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import org.sp2k.mobile.utils.PreferenceManager
import javax.inject.Inject

@HiltViewModel
class AccountViewModel @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository,
    private val preferenceManager: PreferenceManager
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private var onChangedDisabled: Boolean = false
    private var validPasswordOldFormat: Boolean = false
    private var validPasswordNewFormat: Boolean = false
    private var validPasswordNewConfirmFormat: Boolean = false

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading


    private val _isSubmited: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val isSubmited: MutableLiveData<Boolean>
        get() = _isSubmited

    private val _newPhotoProfile: MutableLiveData<String?> by lazy {
        MutableLiveData(null)
    }

    val newPhotoProfile: MutableLiveData<String?>
        get() = _newPhotoProfile

    private val _isPasswordChanged: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val isPasswordChanged: MutableLiveData<Boolean>
        get() = _isPasswordChanged

    fun resetState() {
        setError(null)
        _isValid.value = false
    }

    private val _displayName = MutableLiveData<String>()
    val displayName: LiveData<String>
        get() = _displayName

    private val _signOutSuccess: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    val signOutSuccess: LiveData<Boolean>
        get() = _signOutSuccess

    private val _hideErrorPasswordOldFormat = MutableLiveData<Boolean>()
    val hideErrorPasswordOldFormat: LiveData<Boolean>
        get() = _hideErrorPasswordOldFormat

    private val _hideErrorPasswordNewFormat = MutableLiveData<Boolean>()
    val hideErrorPasswordNewFormat: LiveData<Boolean>
        get() = _hideErrorPasswordNewFormat

    private val _hideErrorPasswordNewConfirmFormat = MutableLiveData<Boolean>()
    val hideErrorPasswordNewConfirmFormat: LiveData<Boolean>
        get() = _hideErrorPasswordNewConfirmFormat

    private val _hideErrorNotSamePasswordNewConfirmFormat = MutableLiveData<Boolean>()
    val hideErrorNotSamePasswordNewConfirmFormat: LiveData<Boolean>
        get() = _hideErrorNotSamePasswordNewConfirmFormat

    private val _isValid = MutableLiveData<Boolean>()
    val isValid: LiveData<Boolean>
        get() = _isValid

    private val _oldPasswordValue = MutableLiveData<String>()
    val oldPasswordValue: MutableLiveData<String>
        get() = _oldPasswordValue

    private val _newPasswordValue = MutableLiveData<String>()
    val newPasswordValue: MutableLiveData<String>
        get() = _newPasswordValue

    private val _newConfirmPasswordValue = MutableLiveData<String>()
    val newConfirmPasswordValue: MutableLiveData<String>
        get() = _newConfirmPasswordValue

    fun isAnonymous(): Boolean {
        return authenticationRepository.isAnonymousUser()
    }

    fun getDisplayName() {
        _displayName.value = when {
            authenticationRepository.currentUser()?.displayName != null ->
                authenticationRepository.currentUser()?.displayName ?: ""
            authenticationRepository.currentUser()?.email != null ->
                authenticationRepository.currentUser()?.email ?: ""
            else -> authenticationRepository.currentUser()!!.uid.substring(0, 5)
        }
    }

    fun signOut() {
        preferenceManager.clear()
        authenticationRepository.signOut()
        _signOutSuccess.value = true
        updateUniqueUser()
    }

    fun changeProfile(file: MultipartBody.Part?) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.changeProfile(file)
            if (response.state) {
                _loading.value = false
                val user = getUser()
                val newUrl = response.data ?: ""
                user?.staff?.admin_foto = newUrl
                preferenceManager.saveUser(user)
                _isSubmited.value = true
            } else {
                _loading.value = false
                _isSubmited.value = false
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            _isSubmited.value = false
            showError(exception)
        }
    }

    fun changePassword(changePasswordRequest: ChangePasswordRequest) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.changePassword(changePasswordRequest)
            if (response.state) {
                _loading.value = false
                _isPasswordChanged.value = true
            } else {
                _loading.value = false
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun attachInputChangePasswordFormat(
        passwordOldInputEditText: TextInputEditText,
        passwordNewInputEditText: TextInputEditText,
        passwordNewConfirmInputEditText: TextInputEditText
    ) {
        passwordOldInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _oldPasswordValue.value = this
                    setText(oldPasswordValue.value)
                    validPasswordOldFormat = length() >= 6
                    _hideErrorPasswordOldFormat.value = validPasswordOldFormat
                    _isValid.value = validateValidation()
                    Selection.setSelection(passwordOldInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }

        passwordNewInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _newPasswordValue.value = this
                    setText(newPasswordValue.value)
                    val isSameNewPassword = (this == passwordNewConfirmInputEditText.text.toString())
                    val isValidFormat = length() >= 6
                    validPasswordNewFormat = isValidFormat && isSameNewPassword
                    _hideErrorPasswordNewFormat.value = isValidFormat
                    _hideErrorNotSamePasswordNewConfirmFormat.value = isSameNewPassword
                    _isValid.value = validateValidation()
                    Selection.setSelection(passwordNewInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }

        passwordNewConfirmInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _newConfirmPasswordValue.value = this
                    setText(newConfirmPasswordValue.value)
                    val isSameNewPassword = (this == passwordNewInputEditText.text.toString())
                    val isValidFormat = length() >= 6
                    validPasswordNewConfirmFormat = isValidFormat && isSameNewPassword
                    _hideErrorPasswordNewConfirmFormat.value = isValidFormat
                    _hideErrorNotSamePasswordNewConfirmFormat.value = isSameNewPassword
                    _isValid.value = validateValidation()
                    Selection.setSelection(passwordNewConfirmInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }
    }

    private fun validateValidation() =
        validPasswordOldFormat && validPasswordNewFormat && validPasswordNewConfirmFormat

    fun getUser() = preferenceManager.getUser()
}