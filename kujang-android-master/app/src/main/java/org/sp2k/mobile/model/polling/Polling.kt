package org.sp2k.mobile.model.polling

data class Polling(
    val created_at: String = "",
    val created_by: String = "",
    val deskripsi: String = "",
    val id: Int = 0,
    val count: Int = 0,
    val tentang: String = "",
    val tgl_end: String = "",
    val tgl_start: String = "",
    val updated_at: String = "",
    val percentage_yes: String? = "0",
    val percentage_no: String? = "0",
    val hasVoted: Boolean = false,
)