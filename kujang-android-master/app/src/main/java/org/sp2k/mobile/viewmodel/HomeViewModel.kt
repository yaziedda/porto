package org.sp2k.mobile.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.exception.GeneralException
import org.sp2k.mobile.model.Home
import org.sp2k.mobile.model.Menu
import org.sp2k.mobile.model.News
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.PreferenceManager
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository,
    private val preferenceManager: PreferenceManager
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _isNeedUpdate = MutableLiveData<Boolean>()
    val isNeedUpdate: LiveData<Boolean>
        get() = _isNeedUpdate

    private val _home = MutableLiveData<Home?>()
    val home: LiveData<Home?>
        get() = _home

    private val _menu = MutableLiveData<MutableList<Menu>>()
    val menu: LiveData<MutableList<Menu>>
        get() = _menu

    private val _news = MutableLiveData<MutableList<News>>()
    val news: LiveData<MutableList<News>>
        get() = _news


    fun resetState() {
        _menu.value = arrayListOf()
        _news.value = arrayListOf()
        _home.value = null
        setError(null)
    }

    fun fetchHome() = viewModelScope.launch {
        _loading.value = true
        val menuList: MutableList<Menu> = arrayListOf()
        populateMenu(menuList)
        _menu.value = menuList

        try {
            val response = remoteRepository.getHome()
            if (response.state) {
                val listNews: MutableList<News> = arrayListOf()
                listNews.addAll(response.data?.news ?: arrayListOf())
                _news.value = listNews
                _loading.value = false
            } else {
                _loading.value = false
                _home.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    private fun populateMenu(menuList: MutableList<Menu>) {
        menuList.apply {
            add(
                Menu(
                    id = 1,
                    title = "Program \n" +
                            "Kerja & RAB",
                    icon = R.drawable.ic_menu_1x_proker
                )
            )

            val userGroup = getUser()?.group
            if (userGroup?.id ?: "" in Constants.USER_GROUP_CODE_ACCEPTOR) {
                add(
                    Menu(
                        id = 2,
                        title = "Surat Menyurat",
                        icon = R.drawable.ic_menu_1x_surat
                    )
                )
            } else {
                add(
                    Menu(
                        id = 2,
                        title = "Surat Masuk",
                        icon = R.drawable.ic_menu_1x_surat
                    )
                )
            }

            add(
                Menu(
                    id = 3,
                    title = "Sejarah",
                    icon = R.drawable.ic_menu_1x_company_profile
                )
            )

            add(
                Menu(
                    id = 4,
                    title = "Media Berita",
                    icon = R.drawable.ic_menu_1x_news
                )
            )

            add(
                Menu(
                    id = 5,
                    title = "E-Polling",
                    icon = R.drawable.ic_menu_1x_polling
                )
            )

            add(
                Menu(
                    id = 6,
                    title = "E-Voting",
                    icon = R.drawable.ic_menu_1x_voting
                )
            )
        }
    }

    fun populateMenuPopUpSurat(): MutableList<Menu> {
        val menuList: MutableList<Menu> = arrayListOf()
        return menuList.apply {
            add(
                Menu(
                    id = 1,
                    title = "Surat Masuk",
                    icon = R.drawable.ic_menu_1x_surat_masuk
                )
            )

            add(
                Menu(
                    id = 2,
                    title = "Surat Keluar",
                    icon = R.drawable.ic_menu_1x_surat_keluar
                )
            )
        }

    }

    fun populateMenuPopUpProker(): MutableList<Menu> {
        val menuList: MutableList<Menu> = arrayListOf()
        return menuList.apply {
            add(
                Menu(
                    id = 1,
                    title = "Program Kerja",
                    icon = R.drawable.ic_menu_1x_proker
                )
            )

            add(
                Menu(
                    id = 2,
                    title = "Rencana Anggaran Biaya",
                    icon = R.drawable.ic_rab
                )
            )
        }

    }

    fun isAnonymous() = authenticationRepository.isAnonymousUser()
    fun getUser() = preferenceManager.getUser()

}