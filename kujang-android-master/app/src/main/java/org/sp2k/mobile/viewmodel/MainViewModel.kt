package org.sp2k.mobile.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.exception.GeneralException
import org.sp2k.mobile.model.KujangNotification
import org.sp2k.mobile.model.News
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import org.sp2k.mobile.utils.PreferenceManager
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository,
    private val preferenceManager: PreferenceManager,
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _isNeedUpdate = MutableLiveData<Boolean>()
    val isNeedUpdate: LiveData<Boolean>
        get() = _isNeedUpdate

    private val _notificationList = MutableLiveData<List<KujangNotification>?>()
    val notificationList: LiveData<List<KujangNotification>?>
        get() = _notificationList

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    fun forceUpdate() {
        _isNeedUpdate.value = true
    }

    fun tokenUpdate(map: HashMap<String, String?>?) = viewModelScope.launch {
        try {
            remoteRepository.tokenUpdate(map)
        } catch (exception: Exception) {
        }
    }

    fun resetState() {
        _notificationList.value = arrayListOf()
        setError(null)
    }

    fun getNotificationList() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.notificationList()
            if (response.state) {
                _notificationList.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _notificationList.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun isAnonymous() = authenticationRepository.isAnonymousUser()

    fun getEmail(): String {
        return authenticationRepository.currentUser()?.email ?: ""
    }

    fun getUID(): String {
        return authenticationRepository.currentUser()?.uid ?: ""
    }

    fun getUser() = preferenceManager.getUser()
}