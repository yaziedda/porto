package org.sp2k.mobile.utils

import android.annotation.SuppressLint
import android.text.format.DateUtils
import org.joda.time.DateTime
import org.joda.time.LocalDateTime
import org.sp2k.mobile.utils.Constants.DD_MMM
import org.sp2k.mobile.utils.Constants.HH_DOT_mm
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt


class DateUtils {
    companion object {
        fun getDateNow(): String {
            val dateNow = Calendar.getInstance().time
            val formatter = SimpleDateFormat(Constants.DATE_FORMATTED_LIVE_AT)
            return formatter.format(dateNow)
        }

        fun getDateTimeNow(): Long {
            return Calendar.getInstance().timeInMillis
        }

        @SuppressLint("SimpleDateFormat")
        fun getDateNow(format: String = Constants.DATE_FORMATTED_LIVE_AT): String {
            val dateNow = Calendar.getInstance().time
            val formatter = SimpleDateFormat(format)
            return formatter.format(dateNow)
        }

        fun getHumanCurrentDiffTime(number: Number?): String? {
            try {
                val time = number?.toLong() ?: 0
                val averageMonthInMillis: Long = android.text.format.DateUtils.DAY_IN_MILLIS * 30
                val now = Date().time
                val delta = now - time
                val resolution: Long
                when {
                    delta <= android.text.format.DateUtils.MINUTE_IN_MILLIS -> {
                        resolution = android.text.format.DateUtils.SECOND_IN_MILLIS
                    }
                    delta <= android.text.format.DateUtils.HOUR_IN_MILLIS -> {
                        resolution = android.text.format.DateUtils.MINUTE_IN_MILLIS
                    }
                    delta <= android.text.format.DateUtils.DAY_IN_MILLIS -> {
                        resolution = android.text.format.DateUtils.HOUR_IN_MILLIS
                    }
                    delta <= android.text.format.DateUtils.WEEK_IN_MILLIS -> {
                        resolution = android.text.format.DateUtils.DAY_IN_MILLIS
                    }
                    else -> return when {
                        delta <= averageMonthInMillis -> {
                            ((delta / android.text.format.DateUtils.WEEK_IN_MILLIS).toInt()).toString() + " weeks(s) ago"
                        }
                        delta <= android.text.format.DateUtils.YEAR_IN_MILLIS -> {
                            (delta / averageMonthInMillis).toInt().toString() + " month(s) ago"
                        }
                        else -> {
                            ((delta / android.text.format.DateUtils.YEAR_IN_MILLIS).toInt()).toString() + " year(s) ago"
                        }
                    }
                }
                return android.text.format.DateUtils.getRelativeTimeSpanString(
                    time,
                    now,
                    resolution
                ).toString()
            } catch (e: Exception) {
                Timber.tag(this::class.java.name).e(e)
                return null
            }
        }

        @SuppressLint("SimpleDateFormat")
        fun getDateTime(timeStamp: String, format: String = Constants.MM_DD_YYYY): String? {
            return try {
                val sdf = SimpleDateFormat(format)
                val netDate = Date(timeStamp.toLong())
                sdf.format(netDate)
            } catch (e: Exception) {
                e.toString()
            }
        }

        fun parseDate(date: String, format: String = HH_DOT_mm): Date? {
            val inputParser = SimpleDateFormat(format, Locale.getDefault())
            return try {
                inputParser.parse(date)
            } catch (e: ParseException) {
                Date(0)
            }
        }

        fun parseDateTimeOnly(date: String, format: String = HH_DOT_mm): Date? {
            val inputParser = SimpleDateFormat(format, Locale.getDefault())
            inputParser.calendar = Calendar.getInstance()
            return try {
                inputParser.parse(date)
            } catch (e: ParseException) {
                Date(0)
            }
        }

        fun getTimeIsLive(
            startTime: String? = System.currentTimeMillis().toString(),
            endTime: Long
        ): Boolean {
            return try {
                val startDate: Long = DateTime(startTime?.toLong()).millis
                val endDate: Long = DateTime(endTime).millis
                val localDateTime = LocalDateTime()
                val now: Long = localDateTime.toDateTime().millis

                now in startDate..endDate
            } catch (e: Exception) {
                false
            }
        }

        fun getDateIsLive(
            startTime: String? = System.currentTimeMillis().toString(),
            endTime: Long
        ): Boolean {
            return try {
                val startDate: Long = DateTime(startTime?.toLong()).millis
                val endDate: Long = DateTime(endTime).millis
                val localDateTime = LocalDateTime()

                val myDate = getDateNow(Constants.yyyy_MM_dd)
                val sdf = SimpleDateFormat("yyyy-MM-dd")
                val date = sdf.parse(myDate)
                val millis = date.time

                if (millis in startDate..endDate) {
                    return true
                }

                return false
            } catch (e: Exception) {
                false
            }
        }

        val dayOfWeeksIDList = arrayListOf(
            "MINGGGU",
            "SENIN",
            "SELASA",
            "RABU",
            "KAMIS",
            "JUMAT",
            "SABTU"
        )

        @SuppressLint("SimpleDateFormat", "DefaultLocale")
        fun getDayOfWeekID(timeStamp: String): String {
            return try {
                val netDate = Date(timeStamp.toLong())
                val c = Calendar.getInstance()
                c.time = netDate
                val dayInt = c.get(Calendar.DAY_OF_WEEK)
                val day = dayOfWeeksIDList.getOrNull(dayInt - 1) ?: ""
                day.toLowerCase().capitalize()
            } catch (e: Exception) {
                ""
            }
        }

        @SuppressLint("SimpleDateFormat", "DefaultLocale")
        fun getDayOfWeekIDInt(timeStamp: String): Int {
            return try {
                val netDate = Date(timeStamp.toLong())
                val c = Calendar.getInstance()
                c.time = netDate
                val dayInt = c.get(Calendar.DAY_OF_WEEK)
                dayInt
            } catch (e: Exception) {
                -1
            }
        }

        fun getHumanCurrentDiffTime(timeMillis: Long): String? {
            try {
                val averageMonthInMillis: Long = android.text.format.DateUtils.DAY_IN_MILLIS * 30
                val now = Date().time
                val delta = now - timeMillis
                val resolution: Long
                when {
                    delta <= android.text.format.DateUtils.MINUTE_IN_MILLIS -> {
                        resolution = android.text.format.DateUtils.SECOND_IN_MILLIS
                    }
                    delta <= android.text.format.DateUtils.HOUR_IN_MILLIS -> {
                        resolution = android.text.format.DateUtils.MINUTE_IN_MILLIS
                    }
                    delta <= android.text.format.DateUtils.DAY_IN_MILLIS -> {
                        resolution = android.text.format.DateUtils.HOUR_IN_MILLIS
                    }
                    delta <= android.text.format.DateUtils.WEEK_IN_MILLIS -> {
                        resolution = android.text.format.DateUtils.DAY_IN_MILLIS
                    }
                    else -> return when {
                        delta <= averageMonthInMillis -> {
                            ((delta / android.text.format.DateUtils.WEEK_IN_MILLIS).toInt()).toString() + " weeks(s) ago"
                        }
                        delta <= android.text.format.DateUtils.YEAR_IN_MILLIS -> {
                            (delta / averageMonthInMillis).toInt().toString() + " month(s) ago"
                        }
                        else -> {
                            ((delta / android.text.format.DateUtils.YEAR_IN_MILLIS).toInt()).toString() + " year(s) ago"
                        }
                    }
                }
                return android.text.format.DateUtils.getRelativeTimeSpanString(
                    timeMillis,
                    now,
                    resolution
                ).toString()
            } catch (e: Exception) {
                Timber.tag(this::class.java.name).e(e)
                return null
            }
        }

        fun getArrBulan(): ArrayList<String> {
            val arrBulan = ArrayList<String>()
            arrBulan.add("Jan")
            arrBulan.add("Feb")
            arrBulan.add("Mar")
            arrBulan.add("Apr")
            arrBulan.add("Mei")
            arrBulan.add("Jun")
            arrBulan.add("Jul")
            arrBulan.add("Agu")
            arrBulan.add("Sep")
            arrBulan.add("Okt")
            arrBulan.add("Nov")
            arrBulan.add("Des")
            return arrBulan
        }


        @SuppressLint("SimpleDateFormat")
        fun convertSimpleTimeAgo(
            date: String,
        ): String? {
            val parseDate = parseDate(date, Constants.yyyy_MM_dd_HH_mm_ss)
            return getHumanCurrentDiffTime(parseDate?.time ?: 0)
        }

        fun convertLongDateToAgoString(createdDate: Long): String {
            val timeNow = System.currentTimeMillis()
            val timeElapsed = timeNow - createdDate

            val oneMin = 60000L
            val oneHour = 3600000L
            val oneDay = 86400000L
            val oneWeek = 604800000L
            var finalString = "0sec"
            val unit: String
            when {
                timeElapsed < oneMin -> {
                    var seconds = (timeElapsed / 1000).toDouble()
                    seconds = seconds.roundToInt().toDouble()
                    unit = if (seconds == 1.0) {
                        " detik"
                    } else {
                        " detik"
                    }
                    finalString = String.format("%.0f ", seconds) + unit
                }
                timeElapsed < oneHour -> {
                    var minutes = (timeElapsed / 1000 / 60).toDouble()
                    minutes = minutes.roundToInt().toDouble()
                    unit = if (minutes == 1.0) {
                        " menit"
                    } else {
                        " menit"
                    }
                    finalString = String.format("%.0f ", minutes) + unit
                }
                timeElapsed < oneDay -> {
                    var hours = (timeElapsed / 1000 / 60 / 60).toDouble()
                    hours = hours.roundToInt().toDouble()
                    unit = if (hours == 1.0) {
                        " jam"
                    } else {
                        "jam"
                    }
                    finalString = String.format("%.0f ", hours) + unit
                }
                timeElapsed < oneWeek -> {
                    var days = (timeElapsed / 1000 / 60 / 60 / 24).toDouble()
                    days = days.roundToInt().toDouble()
                    unit = if (days == 1.0) {
                        " hari"
                    } else {
                        " hari"
                    }
                    finalString = String.format("%.0f ", days) + unit
                }
                timeElapsed > oneWeek -> {
                    var weeks = (timeElapsed / 1000 / 60 / 60 / 24 / 7).toDouble()
                    weeks = weeks.roundToInt().toDouble()
                    unit = if (weeks == 1.0) {
                        " minggu"
                    } else {
                        " minggu"
                    }
                    finalString = String.format("%.0f ", weeks) + unit
                }
            }
            return finalString
        }

        fun convertLongDateToDateStringNotification(createdDate: Long): String {
            val calendarUser = Calendar.getInstance()
            calendarUser.timeInMillis = createdDate

            val calendarMinus1Day = Calendar.getInstance().apply {
                timeInMillis = System.currentTimeMillis()
                add(Calendar.DATE, -1)
            }

            return when {
                DateUtils.isToday(createdDate) -> {
                    getDateTime(createdDate.toString(), HH_DOT_mm) ?: ""
                }
                getDateTime(
                    calendarUser.timeInMillis.toString(),
                    Constants.DD_MMM_yyyy
                ) == getDateTime(
                    calendarMinus1Day.timeInMillis.toString(),
                    Constants.DD_MMM_yyyy
                ) -> {
                    "Kemarin"
                }
                else -> {
                    getDateTime(createdDate.toString(), DD_MMM) ?: ""
                }
            }
        }

        fun dateIn24hours(createdDate: Long): Boolean {
            val timeNow = System.currentTimeMillis()
            val timeElapsed = createdDate - timeNow
            val oneDay = 86400000L
            if (timeElapsed < oneDay) {
                return true
            }
            return false
        }

        fun getDay(day: Int): String {
            return when (day) {
                1 -> "Minggu"
                2 -> "Senin"
                3 -> "Selasa"
                4 -> "Rabu"
                5 -> "Kamis"
                6 -> "Jumat"
                7 -> "Sabtu"
                else -> "Diinfokan lebih lanjut"
            }
        }
    }
}