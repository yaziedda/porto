package org.sp2k.mobile.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class GeneralResponse(

    @field:SerializedName("state")
    val state: Boolean,

    @field:SerializedName("data")
    val data: Any,

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("message")
    val message: String,
)