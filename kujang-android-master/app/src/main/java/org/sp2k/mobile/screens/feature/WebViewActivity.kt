package org.sp2k.mobile.screens.feature

import android.annotation.SuppressLint
import android.content.Intent
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.*
import com.krishna.fileloader.FileLoader
import com.krishna.fileloader.listener.FileRequestListener
import com.krishna.fileloader.pojo.FileResponse
import com.krishna.fileloader.request.FileLoadRequest
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityWebViewBinding
import kotlinx.android.synthetic.main.toolbar.view.*
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.utils.Constants
import java.io.File


class WebViewActivity : BaseActivity<ActivityWebViewBinding>() {

    private var url: String = ""
    private var title: String = ""

    override fun getLayoutId(): Int = R.layout.activity_web_view

    companion object {
        const val KEY_URL = "value"
        const val KEY_TITLE = "name"
    }

    override fun ActivityWebViewBinding.initializeView(savedInstanceState: Bundle?) {
        loading.max = 100
        settings()
        getExtras()
//        FinestWebView.Builder(this@WebViewActivity).show(url)
//        finish()
        setupActionBar()
        if (url.endsWith(".pdf")) {
            loading.visible()
            webViewPdf.visible()
            webViewContent.gone()
            FileLoader.with(this@WebViewActivity)
                .load(url)
                .fromDirectory("PDFView", FileLoader.DIR_INTERNAL)
                .asFile(object : FileRequestListener<File?> {
                    override fun onLoad(request: FileLoadRequest, response: FileResponse<File?>) {
                        val pdfFile: File? = response.body
                        webViewPdf.fromFile(pdfFile)
                            .onLoad {
                                loading.gone()
                            }
                            .onPageError { page, t ->
                                showMessage(t.message)
                                finish()
                            }.load()

                    }

                    override fun onError(request: FileLoadRequest, t: Throwable) {
                        showMessage(t.message)
                        finish()
                    }
                })
        } else if (url.endsWith(".jpg") || url.endsWith(".png") || url.endsWith(".jpeg")) {
            goToShowImageActivity(url)
            finish()
        } else {
            webViewPdf.gone()
            webViewContent.visible()
            loadWebview()
        }
    }

    private fun getExtras() {
        url = intent?.extras?.getString("value") ?: ""
        title = intent?.extras?.getString("name") ?: ""
    }

    private fun ActivityWebViewBinding.setupActionBar() {
        with(layoutToolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBarLayout
            )
            toolbar_text_title.text = title
        }
    }

    private fun setupWebView() {
        binding.webViewContent.loadUrl("file:///android_asset/privacypolice/privacy_police_content.html")
        binding.webViewContent.setPadding(10, 10, 10, 10)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun ActivityWebViewBinding.settings() {
        val webView = webViewContent.settings
        webView.javaScriptEnabled = true
        webView.allowContentAccess = true
        webView.useWideViewPort = true
        webView.loadsImagesAutomatically = true
        webView.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.setRenderPriority(WebSettings.RenderPriority.HIGH)
        webView.setEnableSmoothTransition(true)
        webView.domStorageEnabled = true
    }

    @SuppressLint("ObsoleteSdkInt")
    private fun ActivityWebViewBinding.loadWebview() {
        if (Build.VERSION.SDK_INT >= 19) {
            webViewContent.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        } else {
            webViewContent.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
        webViewContent.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                loading.visibility = View.VISIBLE
                loading.progress = newProgress
                if (newProgress == 100) {
                    loading.visibility = View.INVISIBLE
                }
                super.onProgressChanged(view, newProgress)
            }

        }
        webViewContent.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, URL: String?): Boolean {
                URL?.let { view?.loadUrl(it) }
                loading.visibility = View.VISIBLE
                return true
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    view?.loadUrl(request?.url.toString())
                }
                loading.visibility = View.VISIBLE
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                loading.visibility = View.INVISIBLE
            }

            override fun onReceivedSslError(
                view: WebView?,
                handler: SslErrorHandler,
                error: SslError?
            ) {
                handler.proceed()
            }
        }

        webViewContent.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webViewContent.loadUrl(url)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}