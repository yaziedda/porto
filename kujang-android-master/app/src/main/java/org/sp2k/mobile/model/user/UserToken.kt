package org.sp2k.mobile.model.user

data class UserToken(
    val staff: Staff,
    val token: String
)