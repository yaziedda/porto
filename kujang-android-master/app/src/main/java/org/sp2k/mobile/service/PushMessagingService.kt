package org.sp2k.mobile.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.PowerManager
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.sp2k.mobile.BuildConfig
import org.sp2k.mobile.R
import org.sp2k.mobile.screens.SplashScreenActivity
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.PreferenceManager
import java.util.*


class PushMessagingService : FirebaseMessagingService() {

    private val channelName = "kujang_notification"
    private val channelId = "${BuildConfig.APPLICATION_ID}$channelName"


    override fun onNewToken(token: String) {
        getSharedPreferences("", MODE_PRIVATE).edit().putString("fcm_token", token).apply()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val topic = remoteMessage.data["topic"].toString()
        val user = PreferenceManager(applicationContext).getUser()

        val actionKey = Random().nextInt().toString() + BuildConfig.APPLICATION_ID
        val intent = Intent(this, SplashScreenActivity::class.java).apply {
            putExtra(Constants.NOTIFICATION_KEY, remoteMessage.data["key"].toString())
            putExtra(Constants.NOTIFICATION_TYPE, remoteMessage.data["type"].toString())
            action = actionKey
        }
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val title = remoteMessage.data["title"].toString()
        val body = remoteMessage.data["body"].toString()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.generateNotificationChannel(channelId, "kujang_apps")
        }

        buildNotification(title, body, pendingIntent)

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    fun Context.createChannels(title: String, body: String) {
        val notificationChannel =
            NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
        notificationChannel.enableLights(true)
        notificationChannel.enableVibration(true)
        notificationChannel.description = "$title : $body"
        notificationChannel.lightColor = Color.GREEN
        notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC

        val soundMelody =
            Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + applicationContext.packageName + "/" + R.raw.bell)
        notificationChannel.setSound(
            soundMelody,
            AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
        )
        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(notificationChannel)
    }

    private fun Context.sendHighPriorityNotification(
        title: String?,
        body: String?,
        pendingIntent: PendingIntent
    ) {
        val notification: Notification =
            NotificationCompat.Builder(this, channelId).apply {
                setSmallIcon(R.drawable.ic_notification_kujang)
                setLargeIcon(
                    BitmapFactory.decodeResource(
                        resources,
                        R.drawable.ic_notification_kujang
                    )
                )
                priority = NotificationCompat.PRIORITY_HIGH
                setStyle(
                    NotificationCompat.BigTextStyle().setSummaryText(body)
                        .setBigContentTitle(title).bigText(body)
                )
                setContentTitle(title)
                setContentText(body)
                setContentIntent(pendingIntent)
                val soundMelody =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + applicationContext.packageName + "/" + R.raw.bell)
                setSound(soundMelody)
                setDefaults(Notification.DEFAULT_VIBRATE or Notification.FLAG_SHOW_LIGHTS)
                setAutoCancel(true)
            }.build()

        val notificationManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//        notificationManager.notify(Random().nextInt(), notification)
        NotificationManagerCompat.from(this).notify(Random().nextInt(), notification)
    }

    private fun NotificationManager.generateNotificationChannel(
        channelId: String,
        paramAppId: String
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel(
                channelId,
                paramAppId,
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                lightColor = Color.GREEN
                lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                setShowBadge(true)
                val soundMelody =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + BuildConfig.APPLICATION_ID + "/" + R.raw.bell)
                setSound(soundMelody, AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build())
                setBypassDnd(true)
                createNotificationChannel(this)
            }
        }
    }

    private fun buildNotification(
        title: String?,
        body: String?,
        pendingIntent: PendingIntent
    ) {
        CoroutineScope(Dispatchers.Main).launch {
            val requestCode = (System.currentTimeMillis() % 1000).toInt()
            val notificationBuilder =
                NotificationCompat.Builder(applicationContext, channelId)
                    .apply {
                        color = Color.GREEN
                        setSmallIcon(R.drawable.ic_notification_kujang)
                        setContentTitle(title)
                        setContentText(body)
                        setWhen(Calendar.getInstance().timeInMillis)
                        setAutoCancel(true)
                        setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        setContentIntent(pendingIntent)
                        setStyle(
                            NotificationCompat.BigTextStyle().bigText(body)
                        )
                        priority = NotificationCompat.PRIORITY_MAX
                        setCategory(NotificationCompat.CATEGORY_ALARM)
                    }

            val notificationManager =
                applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.apply {
                notify(requestCode, notificationBuilder.build())
            }
            val powerManager =
                applicationContext.getSystemService(Context.POWER_SERVICE) as PowerManager
            powerManager.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP,
                this::class.java.name
            ).apply { acquire(1240L) }
        }
    }

}