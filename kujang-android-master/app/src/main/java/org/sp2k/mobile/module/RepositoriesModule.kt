package org.sp2k.mobile.module

import NanoNanoUtils
import android.app.Application
import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GetTokenResult
import com.google.gson.GsonBuilder
import com.moe.pushlibrary.MoEHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.*
import org.sp2k.mobile.BuildConfig
import org.sp2k.mobile.model.Token
import org.sp2k.mobile.repository.*
import org.sp2k.mobile.utils.PreferenceManager
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.io.File
import java.net.HttpURLConnection
import java.util.*
import java.util.concurrent.TimeUnit

@Module
@InstallIn(SingletonComponent::class)
class RepositoriesModule {

    @Provides
    fun provideAuthenticationRepository(
        mGoogleSignInClient: GoogleSignInClient,
        auth: FirebaseAuth,
        token: Token
    ): AuthenticationRepository = AuthenticationRepository(mGoogleSignInClient, auth, token)

    @Provides
    fun provideAnalyticsRepository(
        firebaseAnalytics: FirebaseAnalytics,
        moEHelper: MoEHelper
    ): AnalyticsRepository = AnalyticsRepository(moEHelper, firebaseAnalytics)


    @Provides
    fun provideResourceRepository(
        application: Application
    ): ResourceRepository = ResourceRepository(application.baseContext)


    @Provides
    fun provideBaseUrl() = BuildConfig.BASE_URL

    @Provides
    fun cache(cacheFile: File): Cache = Cache(cacheFile, 10 * 1000 * 1000) //10MB Cahe

    @Provides
    fun cacheFile(@ApplicationContext context: Context): File =
        File(context.cacheDir, "okhttp_cache")

    @Provides
    fun provideOkHttpClient(
        @ApplicationContext context: Context,
        cache: Cache?,
        token: Token
    ): OkHttpClient {
        return OkHttpClient.Builder().apply {
            addInterceptor(AuthorizationInterceptor(token, context))
            if (BuildConfig.DEBUG || BuildConfig.FLAVOR == "development") {
                addInterceptor(
                    ChuckerInterceptor.Builder(context)
                        .collector(ChuckerCollector(context))
                        .maxContentLength(250000L)
                        .redactHeaders(emptySet())
                        .alwaysReadResponseBody(false)
                        .build()
                )
            }
            cache(cache)
            connectTimeout(180, TimeUnit.SECONDS)
            writeTimeout(180, TimeUnit.SECONDS)
            readTimeout(180, TimeUnit.SECONDS)
        }.build()
    }

    private class AuthorizationInterceptor(val token: Token, val context: Context) : Interceptor {
        fun getAuthorizationToken(refresh: Boolean): String? = try {
            val firebaseAuth = FirebaseAuth.getInstance().currentUser
            val task: Task<GetTokenResult>? = firebaseAuth?.getIdToken(refresh)
            val tokenResult: GetTokenResult = Tasks.await(task as Task<GetTokenResult>)
            token.token = tokenResult.token
            tokenResult.token
        } catch (exception: Exception) {
            Timber.tag(this::class.java.name).e(exception)
            ""
        }

        override fun intercept(chain: Interceptor.Chain): Response {
            var response: Response
            val newToken = when {
                token.token?.isNotEmpty() == true -> token.token
                else -> getAuthorizationToken(refresh = false)
            }
            response = createRequest(chain, newToken ?: "")
            try {
                if (response.code == HttpURLConnection.HTTP_UNAUTHORIZED || response.code == HttpURLConnection.HTTP_FORBIDDEN) {
                    response = createRequest(chain, getAuthorizationToken(refresh = true) ?: "")
                }
            } catch (exception: Exception) {
                Timber.tag(this::class.java.name).e(exception)
                response.close()
            }
            return response
        }

        private fun createRequest(chain: Interceptor.Chain, newToken: String): Response {
            val newRequest: Request = chain.request().newBuilder()
                .header("Authorization", newToken)
                .header("Accept-Language", Locale.getDefault().language)
                .header("sign", NanoNanoUtils().sign(context = context) ?: "")
                .build()
            return chain.proceed(newRequest)
        }
    }


    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, BASE_URL: String): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .build()

    @Provides
    fun provideRemoteRepositoryService(retrofit: Retrofit): RemoteRepositoryService =
        retrofit.create(RemoteRepositoryService::class.java)

    @Provides
    fun provideRemoteRepositoryImpl(remoteRepositoryImpl: RemoteRepositoryDaoImpl): RemoteRepositoryDao =
        remoteRepositoryImpl

    @Provides
    fun providePreferenceManager(application: Application): PreferenceManager = PreferenceManager(application.baseContext)

}