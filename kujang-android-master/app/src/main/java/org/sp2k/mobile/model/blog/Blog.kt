package org.sp2k.mobile.model.blog


import com.google.gson.annotations.SerializedName

data class Blog(
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("created_by")
    val createdBy: Int?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("images")
    val images: List<String>?,
    @SerializedName("isi_blog")
    val isiBlog: String?,
    @SerializedName("judul_blog")
    val judulBlog: String?,
    @SerializedName("status")
    val status: Int?,
    @SerializedName("status_video")
    val statusVideo: Int?,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("updated_by")
    val updatedBy: Int?,
    @SerializedName("url_blog")
    val urlBlog: String = "",
    @SerializedName("video_blog")
    val videoBlog: String?

)