package org.sp2k.mobile.event

open class PollingEvent(val value: String) {
    companion object {
        const val KEY = "KEY"
    }
}