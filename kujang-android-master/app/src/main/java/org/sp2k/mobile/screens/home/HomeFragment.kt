package org.sp2k.mobile.screens.home

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.customview.customView
import com.bumptech.glide.Glide
import org.greenrobot.eventbus.EventBus
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseFragment
import org.sp2k.mobile.databinding.*
import org.sp2k.mobile.event.GoToMainPageEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.Menu
import org.sp2k.mobile.model.News
import org.sp2k.mobile.screens.mail.`in`.InComingMailActivity
import org.sp2k.mobile.screens.mail.out.OutComingMailActivity
import org.sp2k.mobile.screens.news.NewsDetailActivity
import org.sp2k.mobile.screens.polling.PollingActivity
import org.sp2k.mobile.screens.proker.AnggaranActivity
import org.sp2k.mobile.screens.proker.ProkerActivity
import org.sp2k.mobile.screens.voting.VotingActivity
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.DateUtils
import org.sp2k.mobile.viewmodel.HomeViewModel
import timber.log.Timber

class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_home
    private val viewModel by activityViewModels<HomeViewModel>()
    private lateinit var recyclerMenuViewAdapter: RecyclerViewAdapter<Menu, ItemMenuBinding>
    private lateinit var recyclerNewsViewAdapter: RecyclerViewAdapter<News, ItemNewsBinding>

    override fun FragmentHomeBinding.initializeView() {
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        setupObserver()
        fetcher()
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchHome()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    binding.apply {
                        mainContainer.gone()
                        containerLoading.visible()
                    }
                } else {
                    binding.apply {
                        containerLoading.gone()
                        mainContainer.visible()
                        swipeContainer.isRefreshing = false
                    }
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }


            menu.observe {
                recyclerMenuViewAdapter.updateList(this)
            }

            news.observe {
                recyclerNewsViewAdapter.updateList(this)
            }

        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupUI() {
        binding.apply {
            setupMenuAdapter()
            setupNewsAdapter()
            val user = viewModel.getUser()
            username.text = user?.staff?.admin_nama
            if (user?.role?.role_code.equals("KETUA_BIDANG")) {
                jabatan.text =
                    "No Anggota : ${user?.staff?.admin_username} - ${user?.role?.role_nama} ${user?.bidang?.name}"
            } else {
                jabatan.text =
                    "No Anggota : ${user?.staff?.admin_username} - ${user?.role?.role_nama}"
            }
            navigationSeeAllNews.setOnClickListener {
                EventBus.getDefault().post(GoToMainPageEvent(GoToMainPageEvent.NEWS))
            }
        }
    }

    private fun FragmentHomeBinding.setupMenuAdapter() {
        recyclerMenuViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_menu,
            BR.menu
        ) { itemView, itemModel ->
            itemView.itemMenuTvTitle.text = itemModel.title
            Glide.with(requireActivity())
                .load(itemModel.icon)
                .into(itemView.itemMenuIvImage)

            itemView.root.setOnClickListener {
                when (itemModel.id) {
                    1 -> {
                        if (viewModel.getUser()?.role?.role_code !in Constants.CAN_SHOW_ANGGARAN) {
                            startActivity(
                                Intent(
                                    requireContext(),
                                    ProkerActivity::class.java
                                )
                            )
                        } else {
                            showPopUpProkerMenuDialog()
                        }
                    }
                    2 -> {
                        if (viewModel.getUser()?.group?.id ?: "" in Constants.USER_GROUP_CODE_ACCEPTOR) {
                            showPopUpSuratMenuDialog()
                        } else {
                            startActivity(
                                Intent(
                                    requireContext(),
                                    InComingMailActivity::class.java
                                )
                            )
                        }
                    }
                    3 -> {
                        EventBus.getDefault().post(GoToMainPageEvent(GoToMainPageEvent.COMPANY))
                    }
                    4 -> {
                        EventBus.getDefault().post(GoToMainPageEvent(GoToMainPageEvent.NEWS))
                    }
                    5 -> {
                        startActivity(
                            Intent(
                                requireContext(),
                                PollingActivity::class.java
                            )
                        )
                    }
                    6 -> {
                        startActivity(
                            Intent(
                                requireContext(),
                                VotingActivity::class.java
                            )
                        )
                    }
                }
            }
        }

        val layoutManagers = GridLayoutManager(requireContext(), 4)
        homeRecyclerviewMenu.apply {
            layoutManager = layoutManagers
            adapter = recyclerMenuViewAdapter
            isNestedScrollingEnabled = false
        }
    }

    private fun showPopUpSuratMenuDialog() {
        val dialog = MaterialDialog(requireActivity(), BottomSheet(LayoutMode.WRAP_CONTENT))
            .customView(R.layout.dialog_menu_popup, scrollable = false)
            .cornerRadius(requireActivity().resources.getDimension(R.dimen.card_corner_radius_large))

        val recyclerView = dialog.findViewById<RecyclerView>(R.id.dialog_menu_popup)
        setupPopUpSuratMenuList(recyclerView)

        val dialogButton = dialog.findViewById<AppCompatImageView>(R.id.dialog_menu_popup_finish)
        dialogButton.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun setupPopUpSuratMenuList(recyclerView: RecyclerView) {
        val recyclerMenuViewAdapter: RecyclerViewAdapter<Menu, ItemMenuBinding> =
            RecyclerViewAdapter(
                viewModel.populateMenuPopUpSurat(),
                R.layout.item_menu,
                BR.menu
            ) { itemView, itemModel ->
                itemView.itemMenuTvTitle.text = itemModel.title
                Glide.with(requireActivity())
                    .load(itemModel.icon)
                    .into(itemView.itemMenuIvImage)
                itemView.root.setOnClickListener {
                    when (itemModel.id) {
                        1 -> {
                            // GOTO MASUK
                            startActivity(
                                Intent(
                                    requireContext(),
                                    InComingMailActivity::class.java
                                )
                            )
                        }
                        2 -> {
                            // GOTO KELUAR
                            startActivity(
                                Intent(
                                    requireContext(),
                                    OutComingMailActivity::class.java
                                )
                            )
                        }

                    }
                }
            }

        val layoutManagers = GridLayoutManager(requireContext(), 4)
        recyclerView.apply {
            layoutManager = layoutManagers
            adapter = recyclerMenuViewAdapter
            isNestedScrollingEnabled = false
        }
    }

    private fun showPopUpProkerMenuDialog() {
        val dialog = MaterialDialog(requireActivity(), BottomSheet(LayoutMode.WRAP_CONTENT))
            .customView(R.layout.dialog_menu_popup, scrollable = false)
            .cornerRadius(requireActivity().resources.getDimension(R.dimen.card_corner_radius_large))

        val recyclerView = dialog.findViewById<RecyclerView>(R.id.dialog_menu_popup)
        setupPopUpProkerMenuList(recyclerView)

        val dialogButton = dialog.findViewById<AppCompatImageView>(R.id.dialog_menu_popup_finish)
        dialogButton.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun setupPopUpProkerMenuList(recyclerView: RecyclerView) {
        val recyclerMenuViewAdapter: RecyclerViewAdapter<Menu, ItemMenuBinding> =
            RecyclerViewAdapter(
                viewModel.populateMenuPopUpProker(),
                R.layout.item_menu,
                BR.menu
            ) { itemView, itemModel ->
                itemView.itemMenuTvTitle.text = itemModel.title
                Glide.with(requireActivity())
                    .load(itemModel.icon)
                    .into(itemView.itemMenuIvImage)
                itemView.root.setOnClickListener {
                    when (itemModel.id) {
                        1 -> {
                            startActivity(
                                Intent(
                                    requireContext(),
                                    ProkerActivity::class.java
                                )
                            )
                        }
                        2 -> {
                            startActivity(
                                Intent(
                                    requireContext(),
                                    AnggaranActivity::class.java
                                )
                            )
                        }

                    }
                }
            }

        val layoutManagers = GridLayoutManager(requireContext(), 4)
        recyclerView.apply {
            layoutManager = layoutManagers
            adapter = recyclerMenuViewAdapter
            isNestedScrollingEnabled = false
        }
    }


    @SuppressLint("SetTextI18n")
    private fun FragmentHomeBinding.setupNewsAdapter() {
        recyclerNewsViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_news,
            BR.news
        ) { itemView, itemModel ->

            itemView.itemNewsTvTitle.text = itemModel.berita.judul
            itemView.itemNewsTvSubTitle.text = if (itemModel.berita.ringkasan?.length ?: 0 > 39) {
                itemModel.berita.ringkasan?.substring(0, 39) + "..."
            } else {
                itemModel.berita.ringkasan
            }

            itemView.itemNewsTitle2.text = itemModel.kategori.kategori
            itemView.itemNewsTitle3.text =
                DateUtils.let { date ->
                    date.convertLongDateToAgoString(
                        date.parseDate(
                            itemModel.berita.updated_at,
                            Constants.yyyy_MM_dd_HH_mm_ss
                        )?.time ?: 0
                    )
                }

            Glide.with(requireActivity())
                .load(itemModel.berita.image)
                .into(itemView.itemNewsIvImage)

            itemView.root.setOnClickListener {
                startActivity(
                    Intent(requireContext(), NewsDetailActivity::class.java).apply {
                        putExtra(NewsDetailActivity.KEY_NEWS_ID, itemModel.berita.id)
                    }
                )
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )
        homeRecyclerviewNews.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerNewsViewAdapter
            isNestedScrollingEnabled = false
        }
    }

    private fun calculatePercentage(obtained: Long, total: Long): Long {
        return obtained * 100 / total
    }

}