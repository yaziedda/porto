package org.sp2k.mobile.event

open class ProkerEvent(val value: String) {
    companion object {
        const val KEY = "KEY"
    }
}