package org.sp2k.mobile.exception

class LoginException :
    Exception("Terjadi kesalahan saat login. Mungkin sedang terjadi kesalahan pada sistem kami.")