package org.sp2k.mobile

import android.app.Application
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.HiltAndroidApp
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.PreferenceManager
import timber.log.Timber

@HiltAndroidApp
class CoreApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        configureTimber()
    }

    private fun configureTimber() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }
}