package org.sp2k.mobile.model.blog


import com.google.gson.annotations.SerializedName

data class NewsResponse(
    @SerializedName("data")
    val blogData: List<Blog>,
    @SerializedName("state")
    val state: Boolean,
    @SerializedName("message")
    val message: String
)