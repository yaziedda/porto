package org.sp2k.mobile.repository

import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.sp2k.mobile.model.*
import org.sp2k.mobile.model.blog.Blog
import org.sp2k.mobile.model.company.CompanyData
import org.sp2k.mobile.model.donation.Donation
import org.sp2k.mobile.model.news.NewsComment
import org.sp2k.mobile.model.news.NewsCommentData
import org.sp2k.mobile.model.polling.Polling
import org.sp2k.mobile.model.polling.PollingRequest
import org.sp2k.mobile.model.prokeranggaran.ProkerData
import org.sp2k.mobile.model.request.DisposisiRequest
import org.sp2k.mobile.model.request.ProkerApproval
import org.sp2k.mobile.model.request.SuratKeluarApproveRequest
import org.sp2k.mobile.model.request.SuratKeluarRejectRequest
import org.sp2k.mobile.model.user.AuthData
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.model.voting.VotingRes

interface RemoteRepositoryDao {

    suspend fun getHome(): BaseResponse<Home>
    suspend fun getDonation(): BaseResponse<List<Donation>>
    suspend fun getDonationById(id: String): BaseResponse<Donation>
    suspend fun sendDonasi(map: HashMap<String, String?>?): BaseResponse<String>
    suspend fun getProducts(): BaseResponse<List<Product>>
    suspend fun getProductById(id: String): BaseResponse<Product>
    suspend fun addToCart(map: HashMap<String, String?>?): BaseResponse<String>
    suspend fun getCart(): BaseResponse<List<Cart>>
    suspend fun cartUnchecked(id: String, status: String): BaseResponse<String>
    suspend fun cartUpdateQty(id: String, qty: String): BaseResponse<String>
    suspend fun province(): BaseResponse<List<Province>>
    suspend fun city(id: String): BaseResponse<List<City>>
    suspend fun cost(map: HashMap<String, String?>?): BaseResponse<RajaOngkir>
    suspend fun checkout(map: HashMap<String, String?>?): BaseResponse<String>
    suspend fun history(): BaseResponse<List<TransactionHistory>>
    suspend fun blog(): BaseResponse<List<Blog>>
    suspend fun jasa(): BaseResponse<List<Jasa>>
    suspend fun jasaById(id: String): BaseResponse<Jasa>
    suspend fun tokenUpdate(map: HashMap<String, String?>?): BaseResponse<String>
    suspend fun login(map: HashMap<String, String>): BaseResponse<AuthData>
    suspend fun news(): BaseResponse<List<News>>
    suspend fun company(): BaseResponse<CompanyData>
    suspend fun newsById(id: String): BaseResponse<NewsDetail>
    suspend fun newsCommentById(id: String, page: Int): BaseResponse<NewsCommentData>
    suspend fun newsCommentSend(map: HashMap<String, String>): BaseResponse<NewsComment>
    suspend fun search(search: String): BaseResponse<List<Staff>>
    suspend fun groups(): BaseResponse<List<GroupMail>>
    suspend fun suratMasuk(): BaseResponse<List<Mail>>
    suspend fun disposisi(disposisiRequest: DisposisiRequest): BaseResponse<Any>
    suspend fun suratKeluar(): BaseResponse<List<SuratKeluar>>
    suspend fun approveSuratKeluar(suratKeluarApproveRequest: SuratKeluarApproveRequest): BaseResponse<Any>
    suspend fun disposisiKeluar(disposisiRequest: DisposisiRequest): BaseResponse<Any>
    suspend fun rejectSuratKeluar(suratKeluarRejectRequest: SuratKeluarRejectRequest): BaseResponse<Any>
    suspend fun deleteCommentById(id: String): BaseResponse<Any>
    suspend fun prokerList(type: String): BaseResponse<ProkerResponse>
    suspend fun prokerById(id: String): BaseResponse<ProkerData>
    suspend fun prokerApproval(prokerApproval: ProkerApproval): BaseResponse<Any>
    suspend fun anggaran(): BaseResponse<AnggaranBidang>
    suspend fun pollingList(): BaseResponse<List<Polling>>
    suspend fun votingList(): BaseResponse<VotingRes>
    suspend fun pollingVote(pollingRequest: PollingRequest): BaseResponse<Any>
    suspend fun voteVote(
        status: RequestBody?,
        id: RequestBody?,
        file: MultipartBody.Part?
    ): BaseResponse<Any>
    suspend fun changeProfile(file: MultipartBody.Part?): BaseResponse<String>
    suspend fun changePassword(changePasswordRequest: ChangePasswordRequest): BaseResponse<Any>
    suspend fun suratMasukByID(id: String): BaseResponse<Mail>
    suspend fun suratKeluarByID(id: String): BaseResponse<SuratKeluar>
    suspend fun notificationList(): BaseResponse<List<KujangNotification>>

}