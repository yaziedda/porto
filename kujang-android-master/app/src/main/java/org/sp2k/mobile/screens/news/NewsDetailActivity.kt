package org.sp2k.mobile.screens.news

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import androidx.activity.viewModels
import androidx.core.view.ViewCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.mikepenz.fastadapter.ui.items.ProgressItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.item_home_blog_horizontal.*
import kotlinx.coroutines.launch
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityNewsDetailBinding
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.hideKeyboard
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.news.Comment
import org.sp2k.mobile.model.news.NewsComment
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.DateUtils
import org.sp2k.mobile.utils.StatusBar.Companion.setLightStatusBar
import org.sp2k.mobile.viewmodel.NewsViewModel

@AndroidEntryPoint
class NewsDetailActivity : BaseActivity<ActivityNewsDetailBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_news_detail
    val viewModel by viewModels<NewsViewModel>()
    private var newsId: String = ""
    lateinit var commentsAdapter: FastItemAdapter<IItem<*>>
    lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener
    private var footerAdapter = ItemAdapter<ProgressItem>()
    private var page = 1

    companion object {
        const val KEY_NEWS_ID = "KEY_NEWS_ID"
    }

    override fun ActivityNewsDetailBinding.initializeView(savedInstanceState: Bundle?) {
        getExtras()
        setupUI()
        setupObserver()
        fetcher()
    }

    private fun getExtras() {
        newsId = intent?.extras?.getString(KEY_NEWS_ID) ?: ""
    }

    private fun ActivityNewsDetailBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    if (it) {
                        containerLoading.visible()
                        containerContent.gone()
                    } else {
                        containerLoading.gone()
                        containerContent.visible()
                        swipeContainer.isRefreshing = false
                        showLoadMoreLoading(false)
                    }
                }
            }

            newsDetail.observe {
                this?.let { newsDetail ->
                    newsDetail.news?.let {
                        newsDetailCoverContainerCarousel.visible()
                        newsDetailCoverContainerCarousel.setImageListener { position, imageView ->
                            val url = newsDetail.image.getOrNull(position) ?: ""
                            Glide.with(applicationContext)
                                .load(url)
                                .into(imageView)

                            imageView.setOnClickListener {
                                goToShowImageActivity(url)
                            }
                        }
                        newsDetailCoverContainerCarousel.pageCount = newsDetail.image.size

                        newsDetailCoverTitle.text = it.berita.judul
                        newsDetailCoverCategory.text = it.kategori.kategori
                        newsDetailCoverTime.text =
                            DateUtils.let { date ->
                                date.convertLongDateToAgoString(
                                    date.parseDate(
                                        it.berita.updated_at,
                                        Constants.yyyy_MM_dd_HH_mm_ss
                                    )?.time ?: 0
                                )
                            }
                        newsDetailCoverViews.text = it.berita.views.toString()
                        newsDetailContent.setHtml(it.berita.isi)
                        newsDetailContent.setOnClickATagListener { widget, spannedText, href ->
                            goToWebViewActivity(href ?: "")
                            return@setOnClickATagListener true
                        }
                    }
                }
            }

            newsComment.observe {
                if (this != null) {
                    if (this.isNotEmpty()) {
                        if (page == 1) commentsAdapter.clear()
                        val collections: MutableList<IItem<*>> = arrayListOf()
                        this.forEach {
                            collections.add(
                                NewsCommentAdapter(this@NewsDetailActivity, it)
                            )
                        }
                        commentsAdapter.add(collections)
                        if (page == 1 && this.size < 5) {
                            newsDetailCommentPaggingButton.gone()
                        } else {
                            newsDetailCommentPaggingButton.visible()
                        }
                    } else {
                        showLoadMoreLoading(false)
                        newsDetailCommentPaggingButton.gone()
                    }
                }
            }

            isRunning.observe {
                this?.let {
                    showLoadMoreLoading(it)
                }
            }

            successDeletedComment.observe {
                this?.let {
                    if (it) {
                        page = 1
                        fetcher()
                    }
                }
            }

        }
    }

    private fun ActivityNewsDetailBinding.setupUI() {
        setLightStatusBar(window)
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        newsDetailFinish.setOnClickListener {
            finish()
        }
        setupAdapter()
        newsDetailCommentSend.setOnClickListener {
            val comment = newsDetailCommentEditText.text.toString()
            if (comment.isEmpty()) {
                showMessage("Komentar tidak boleh kosong")
                return@setOnClickListener
            }

            lifecycleScope.launch {
                kotlinx.coroutines.delay(500L)
                val idUser = viewModel.getUser()?.staff?.id.toString()
                commentsAdapter.add(
                    NewsCommentAdapter(
                        this@NewsDetailActivity,
                        NewsComment(
                            comment = Comment(
                                id = System.currentTimeMillis().toString(),
                                comment = comment,
                                id_user = idUser,
                                comment_id = viewModel.generateCommentId(),
                                created_at = System.currentTimeMillis().toString(),
                            ),
                            user = Staff(
                                admin_id = idUser,
                                admin_nama = viewModel.getUser()?.staff?.admin_nama ?: "",
                                admin_foto = viewModel.getUser()?.staff?.admin_foto ?: "",
                            ),
                            send = -1
                        ),
                        comment,
                        newsId
                    )
                )
                containerContent.fullScroll(View.FOCUS_DOWN)
            }
            hideKeyboard()
            newsDetailCommentEditText.setText("")
            showLoadMoreLoading(false)
        }

        containerContent.viewTreeObserver.addOnScrollChangedListener {
            newsDetailUp.visible()
        }
        newsDetailUp.setOnClickListener {
            containerContent.fullScroll(ScrollView.FOCUS_UP);
        }
    }

    private var paginatedRV: RecyclerView? = null

    private fun enablePagination(
        nestedScrollView: NestedScrollView,
        recyclerView: RecyclerView,
    ) {
        ViewCompat.setNestedScrollingEnabled(recyclerView, false)
        nestedScrollView.viewTreeObserver?.addOnScrollChangedListener {
            if (paginatedRV == null) {
                val holder = nestedScrollView.getChildAt(0) as ViewGroup
                for (i in 0 until holder.childCount) {
                    if (holder.getChildAt(i).id == recyclerView.id) {
                        paginatedRV = holder.getChildAt(i) as RecyclerView
                        break
                    }
                }
            }
            paginatedRV?.let {
                if (it.bottom - (nestedScrollView.height + nestedScrollView.scrollY) == 0) {
                    fetchComment()
                }
            }
        }
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchNewsById(newsId)
            fetchNewsCommentById(newsId, page)
        }
    }

    private fun fetchComment() {
        page += 1
        viewModel.fetchNewsCommentById(newsId, page)
    }

    private fun ActivityNewsDetailBinding.setupAdapter(): FastItemAdapter<IItem<*>> {
        commentsAdapter = FastItemAdapter<IItem<*>>().apply {
            setHasStableIds(true)
            val eventHook = object : ClickEventHook<NewsCommentAdapter>() {
                override fun onBind(viewHolder: RecyclerView.ViewHolder): View? =
                    (viewHolder as? NewsCommentAdapter.ViewHolder)?.itemView

                override fun onClick(
                    v: View,
                    position: Int,
                    fastAdapter: FastAdapter<NewsCommentAdapter>,
                    item: NewsCommentAdapter
                ) {

                }
            }
            addEventHook(eventHook)
        }

        newsDetailCommentRecycler.apply {
            layoutManager = LinearLayoutManager(this@NewsDetailActivity)
            adapter = commentsAdapter
            endlessRecyclerOnScrollListener =
                object : EndlessRecyclerOnScrollListener(footerAdapter) {
                    override fun onLoadMore(currentPage: Int) {
                        if (viewModel.getIsRunning() == true) {
                            return
                        }
                        newsDetailCommentPaggingButton.apply {
                            visible()
                            setOnClickListener {
                                gone()
                                showLoadMoreLoading(true)
                                fetchComment()
                            }
                        }
                    }
                }
            addOnScrollListener(endlessRecyclerOnScrollListener)
            isNestedScrollingEnabled = false
        }
        return commentsAdapter
    }

    fun showLoadMoreLoading(loading: Boolean) {
        if (loading) {
            binding.newsDetailCommentLoading.visible()
        } else {
            binding.newsDetailCommentLoading.gone()
        }
    }

    fun getUser() = viewModel.getUser()
}