package org.sp2k.mobile.screens.auth

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.SpannableStringBuilder
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import dagger.hilt.android.AndroidEntryPoint
import org.sp2k.mobile.BuildConfig
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.contract.GalleryContract
import org.sp2k.mobile.databinding.ActivityRegisterBinding
import org.sp2k.mobile.extensions.*
import org.sp2k.mobile.permission.helper.PermissionHelper
import org.sp2k.mobile.permission.listener.PermissionListener
import org.sp2k.mobile.utils.DialogUtils
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.AuthRegisterViewModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File


@AndroidEntryPoint
class RegisterActivity : BaseActivity<ActivityRegisterBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_register
    private val viewModel by viewModels<AuthRegisterViewModel>()
    private val permissionHelper by lazy { PermissionHelper(this) }

    private val requestedPermissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    private val labelUploadPhoto: String by lazy { getString(R.string.label_upload_photo) }
    private val labelCamera: String by lazy { getString(R.string.label_camera) }
    private val labelGallery: String by lazy { getString(R.string.label_gallery) }
    private lateinit var fileUploadKTP: File
    private var imageUriKTP: Uri? = null
    private lateinit var fileUploadSelfie: File
    private var imageUriSelfie: Uri? = null
    private var valueGender = "LAKI-LAKI"
    private var valueAgama = "ISLAM"
    private var imageKtpPath = ""
    private var imageKtpSelfiePath = ""

    private val errorMessage: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(getString(R.string.label_prefix_blocking_description))
            .append(
                getString(R.string.label_here).color(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.colorPrimary
                    )
                )
                    .underline()
                    .click { }
            )
    }

    companion object {
        var UPLOAD_TYPE = 1
        const val UPLOAD_TYPE_KTP = 1
        const val UPLOAD_TYPE_SELFIE = 2
    }

    private fun getImageFromGallery() =
        registerForActivityResult(GalleryContract()) { result ->
            result.run {
                if (UPLOAD_TYPE == UPLOAD_TYPE_KTP) {
                    imageUriKTP = this?.let { applicationContext.getRealPathFromUri(it) }
                    imageUriKTP?.path?.let {
                        fileUploadKTP = File("image/jpeg", it)
                        setImage(it)
                    }
                } else if (UPLOAD_TYPE == UPLOAD_TYPE_SELFIE) {
                    imageUriSelfie = this?.let { applicationContext.getRealPathFromUri(it) }
                    imageUriSelfie?.path?.let {
                        fileUploadSelfie = File("image/jpeg", it)
                        setImage(it)
                    }
                }
            }
        }

    private val takePicture =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { _ ->
            if (UPLOAD_TYPE == UPLOAD_TYPE_KTP) {
                if (File(fileUploadKTP.path).exists())
                    imageUriKTP?.path?.let { setImage(it) }
            } else if (UPLOAD_TYPE == UPLOAD_TYPE_SELFIE) {
                if (File(fileUploadSelfie.path).exists())
                    imageUriSelfie?.path?.let { setImage(it) }
            }
        }

    private fun setImage(path: String) {
        with(binding) {
            val myBitmap = BitmapFactory.decodeFile(path)
            if (UPLOAD_TYPE == UPLOAD_TYPE_KTP) {
                imageKtpPath = path
                viewModel.setImageKtp(true)
            } else if (UPLOAD_TYPE == UPLOAD_TYPE_SELFIE) {
                imageKtpSelfiePath = path
                viewModel.setImageSelfie(true)
            }
        }
    }

    override fun ActivityRegisterBinding.initializeView(savedInstanceState: Bundle?) {
        StatusBar.setLightStatusBar(window)
        setupSpinner()
        setupAgamaSpinner()
        viewModel.attachInputFormat(
            etNik,
            etEmail,
            etNoHp,
            etPassword,
            etPasswordConfirm,
            etAlamat
        )
        setupObserver()
        btnRegister.setOnClickListener {
            DialogUtils.showBasicAlertConfirmationDialog(
                activity = this@RegisterActivity,
                ContextCompat.getDrawable(applicationContext, R.drawable.ic_not_logged_in),
                "Apakah anda yakin NIK dan Email anda benar? Kami akan mengirimkan email ke "+etEmail.text.toString()+" untuk memverifikasi."
            ) {
                submit()
            }
        }
    }

    private fun ActivityRegisterBinding.submit() {
        val builder: MultipartBody.Builder =
            MultipartBody.Builder().setType(MultipartBody.FORM);
        builder
            .addFormDataPart("nik", etNik.text.toString())
            .addFormDataPart("email", etEmail.text.toString())
            .addFormDataPart("jenis_kelamin", valueGender)
            .addFormDataPart("no_hp", etNoHp.text.toString())
            .addFormDataPart("alamat", etAlamat.text.toString())
            .addFormDataPart("agama", valueAgama)
            .addFormDataPart("password", etPassword.text.toString())

        val bmpKtp = BitmapFactory.decodeFile(imageKtpPath)
        val bosKtp = ByteArrayOutputStream()
        bmpKtp.compress(Bitmap.CompressFormat.JPEG, 30, bosKtp)

        builder.addFormDataPart(
            "image_ktp",
            fileUploadKTP.name,
            RequestBody.create(MultipartBody.FORM, bosKtp.toByteArray())
        )

        val bmpKtpSelfie = BitmapFactory.decodeFile(imageKtpPath)
        val bosKtpSelfie = ByteArrayOutputStream()
        bmpKtpSelfie.compress(Bitmap.CompressFormat.JPEG, 30, bosKtpSelfie)

        builder.addFormDataPart(
            "image_selfie",
            fileUploadSelfie.name,
            RequestBody.create(MultipartBody.FORM, bosKtpSelfie.toByteArray())
        )

        viewModel.register(builder.build(), applicationContext)
    }

    private fun ActivityRegisterBinding.setupObserver() {
        viewModel.apply {
            isValid.observe {
                btnRegister.isEnabled = this
            }

            hideErrorNIKFormat.observe {
                when {
                    this -> txtNIKError.visibility = View.GONE
                    etNik.text?.isEmpty() == true -> txtNIKError.visibility =
                        View.GONE
                    else -> txtNIKError.visibility = View.VISIBLE
                }
            }

            hideErrorEmailFormat.observe {
                when {
                    this -> txtEmailError.visibility = View.GONE
                    etEmail.text?.isEmpty() == true -> txtEmailError.visibility =
                        View.GONE
                    else -> txtEmailError.visibility = View.VISIBLE
                }
            }

            hideErrorNoHpFormat.observe {
                when {
                    this -> txtNoHPError.visibility = View.GONE
                    etNoHp.text?.isEmpty() == true -> txtNoHPError.visibility =
                        View.GONE
                    else -> txtNoHPError.visibility = View.VISIBLE
                }
            }

            hideErrorAlamatFormat.observe {
                when {
                    this -> txtAlamatError.visibility = View.GONE
                    etAlamat.text?.isEmpty() == true -> txtAlamatError.visibility =
                        View.GONE
                    else -> txtAlamatError.visibility = View.VISIBLE
                }
            }

            hideErrorPasswordFormat.observe {
                when {
                    this -> txtPasswordError.visibility = View.GONE
                    etPassword.text?.isEmpty() == true -> txtPasswordError.visibility =
                        View.GONE
                    else -> txtPasswordError.visibility = View.VISIBLE
                }
            }

            hideErrorPasswordConfirmationFormat.observe {
                when {
                    this -> txtPasswordConfirmError.visibility = View.GONE
                    etPasswordConfirm.text?.isEmpty() == true -> txtPasswordConfirmError.visibility =
                        View.GONE
                    else -> txtPasswordConfirmError.visibility = View.VISIBLE
                }
            }

            val loadingDialog = DialogUtils.loadingDialog(this@RegisterActivity, "Sedang melakukan upload data", "Loading..", false) {

            }

            loading.observe {
                if (this) {
                    loadingDialog.show()
                } else {
                    loadingDialog.dismiss()
                }
            }

            error.observe {
                if (this != null) {
                    this@RegisterActivity.showError(
                        this,
                        message ?: errorMessage,
                        ContextCompat.getDrawable(
                            applicationContext,
                            R.drawable.ic_not_logged_in
                        )
                    ) {
                        submit()
                    }
                }
            }

            staffData.observe {
                if (this != null) {
                    startActivity(Intent(applicationContext, RegisterVerificationActivity::class.java))
                }
            }
        }
    }

    private fun ActivityRegisterBinding.setupSpinner() {
        val arraySpinner = arrayOf(
            "LAKI-LAKI",
            "PEREMPUAN"
        )
        val adapter = object : ArrayAdapter<Any>(
            applicationContext,
            android.R.layout.simple_spinner_dropdown_item,
            arraySpinner
        ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                return super.getDropDownView(position, convertView, parent).also { view ->
                    if (position == spinnerGender.selectedItemPosition) {
                        view.setBackgroundColor(
                            ContextCompat.getColor(
                                applicationContext,
                                R.color.colorPrimary
                            )
                        )
                        (view as TextView).setTextColor(
                            ContextCompat.getColor(
                                applicationContext,
                                R.color.white
                            )
                        )
                    }
                    valueGender = (view as TextView).text.toString()
                }
            }
        }
        spinnerGender.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, view: View,
                pos: Int, id: Long
            ) {
                val string: String = parent.getItemAtPosition(pos) as String
                valueAgama = string
            }

            override fun onNothingSelected(arg0: AdapterView<*>?) {

            }
        }
        spinnerGender.adapter = adapter
        spinnerGender.apply {

        }

    }

    private fun ActivityRegisterBinding.setupAgamaSpinner() {
        val arraySpinner = arrayOf(
            "ISLAM",
            "PROTESTAN",
            "KATOLIK",
            "HINDU",
            "BUDHA",
            "KHONGHUCU"
        )
        val adapter = object : ArrayAdapter<Any>(
            applicationContext,
            android.R.layout.simple_spinner_dropdown_item,
            arraySpinner
        ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                return super.getDropDownView(position, convertView, parent).also { view ->
                    if (position == spinnerAgama.selectedItemPosition) {
                        view.setBackgroundColor(
                            ContextCompat.getColor(
                                applicationContext,
                                R.color.colorPrimary
                            )
                        )
                        (view as TextView).setTextColor(
                            ContextCompat.getColor(
                                applicationContext,
                                R.color.white
                            )
                        )
                    }
                    valueAgama = (view as TextView).text.toString()
                }
            }

        }
        spinnerAgama.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, view: View,
                pos: Int, id: Long
            ) {
                val string: String = parent.getItemAtPosition(pos) as String
                valueAgama = string
            }

            override fun onNothingSelected(arg0: AdapterView<*>?) {

            }
        }
        spinnerAgama.adapter = adapter
        spinnerAgama.apply {

        }
    }

    private fun selectImage(context: Context) {
        val options = arrayOf<CharSequence>(labelCamera, labelGallery)
        AlertDialog.Builder(context).apply {
            setTitle(labelUploadPhoto)
            setItems(options) { _, item ->
                when (options[item]) {
                    labelCamera -> pickImage {
                        with(applicationContext) {
                            createImageFile()?.also {
                                imageUriKTP = getUriFromFile(it)
                                fileUploadKTP = File("image/jpeg", it.path)
                                takePicture.launch(imageUriKTP)
                            }
                        }
                    }
                    labelGallery -> pickImage { getImageFromGallery().launch(1) }
                }
            }
        }.show()
    }

    private fun pickImage(action: () -> Unit) {
        permissionHelper.requests(requestedPermissions)
            .listener(object : PermissionListener.Request {
                override fun granted() = action.invoke()
                override fun showRequestPermissionRationale() = showDeniedPermission()
                override fun denied() = showDeniedPermission {
                    startActivity(
                        Intent(
                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                        )
                    )
                }
            })
    }

    private fun showDeniedPermission(action: (() -> Unit)? = null) {
        showDialog(getString(R.string.label_camera_permission)) {
            onBackPressed()
            action?.invoke()
        }
    }

}