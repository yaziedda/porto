package org.sp2k.mobile.utils

import java.text.NumberFormat
import java.util.*

class ConverterCurrency {
    companion object {
        fun rupiah(price: String): String{
            val number : Double = price.toDouble()
            val localeID =  Locale("in", "ID")
            val numberFormat = NumberFormat.getCurrencyInstance(localeID)
            return numberFormat.format(number).toString()
        }
    }
}