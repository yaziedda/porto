package org.sp2k.mobile.model.prokeranggaran

import java.io.Serializable

data class Lpj(
    val alasan: String? = "",
    val anggaran_terpakai: Int? = 0,
    val anggaran_sisa: Int? = 0,
    val approval_id: String? = "",
    val approval_status: Int? = 0,
    val created_at: String? = "",
    val created_by: Int? = 0,
    val `file`: String? = "",
    val id: Int? = 0,
    val id_t_proker: Int? = 0,
    val link_dokumentasi: String? = "",
    val anggaran_sisa_document: String? = "",
    val link_kwitansi: String? = "",
    val status: Int? = 0,
    val status_code: String? = "",
    val updated_at: String? = "",
    val isi_berita: String? = "",
    val updated_by: Int
): Serializable