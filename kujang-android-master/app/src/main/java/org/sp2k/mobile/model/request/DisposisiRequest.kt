package org.sp2k.mobile.model.request

data class DisposisiRequest(
    val id_surat: String = "",
    val id_group: String = "",
    val users: List<String> = arrayListOf(),
)