package org.sp2k.mobile.event

open class SuratEvent(val value: String) {
    companion object {
        const val KEY = "KEY"
    }
}