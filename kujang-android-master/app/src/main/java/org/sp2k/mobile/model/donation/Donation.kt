package org.sp2k.mobile.model.donation


import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class Donation(
    @SerializedName("created_at")
    val createdAt: String? = "",
    @SerializedName("created_by")
    val createdBy: Int?  = 0,
    @SerializedName("deskripsi")
    val deskripsi: String? = "",
    @SerializedName("file")
    val `file`: String? = "",
    @SerializedName("id")
    val id: Int? = 0,
    @SerializedName("is_publish")
    val isPublish: Int? = 0,
    @SerializedName("judul")
    val judul: String? = "",
    @SerializedName("proses_donasi")
    val prosesDonasi: Long? = 0,
    @SerializedName("status")
    val status: Int? = 0,
    @SerializedName("tanggal_akhir")
    val tanggalAkhir: String? = "",
    @SerializedName("tanggal_awal")
    val tanggalAwal: String? = "",
    @SerializedName("target_donasi")
    val targetDonasi: Long? = 0,
    @SerializedName("updated_at")
    val updatedAt: String? = "",
    @SerializedName("updated_by")
    val updatedBy: Int? = 0,
    @SerializedName("images")
    val images: List<String>? = arrayListOf(),
    @SerializedName("image")
    val image: String? = ""
)