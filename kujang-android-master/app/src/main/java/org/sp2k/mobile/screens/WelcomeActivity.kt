package org.sp2k.mobile.screens

import android.content.Intent
import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityWelcomeBinding
import org.sp2k.mobile.screens.auth.LoginActivity

@AndroidEntryPoint
class WelcomeActivity : BaseActivity<ActivityWelcomeBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_welcome

    override fun ActivityWelcomeBinding.initializeView(savedInstanceState: Bundle?) {
        welcomeButtonNext.setOnClickListener {
            startActivity(Intent(applicationContext, LoginActivity::class.java))
            finish()
        }
    }
}