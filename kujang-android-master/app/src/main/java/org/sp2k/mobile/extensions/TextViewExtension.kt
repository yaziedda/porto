package org.sp2k.mobile.extensions

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import androidx.appcompat.widget.AppCompatTextView
import org.sp2k.mobile.utils.NumberListSpanBuilder
import java.util.*

fun AppCompatTextView.toCurrency(number: CharSequence, locale: Locale = Locale("in", "ID")) {
    text = number.toCurrency(locale)
}

fun AppCompatTextView.setCustomText(value: SpannableStringBuilder) {
    movementMethod = LinkMovementMethod.getInstance()
    text = value
}

fun AppCompatTextView.setBulletNumber(items: List<String>) {
    val builder = SpannableStringBuilder()
    items.forEachIndexed { index, item ->
        val header = index + 1
        builder.append(
            item + "\n",
            NumberListSpanBuilder(50, "$header."),
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    this.text = builder
}