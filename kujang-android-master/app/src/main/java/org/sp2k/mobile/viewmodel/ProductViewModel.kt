package org.sp2k.mobile.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.exception.GeneralException
import org.sp2k.mobile.model.*
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import kotlinx.coroutines.launch

class ProductViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _loadingCart: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loadingCart: MutableLiveData<Boolean>
        get() = _loadingCart


    private val _product = MutableLiveData<Product?>()
    val product: LiveData<Product?>
        get() = _product

    private val _productList = MutableLiveData<List<Product>>()
    val productList: LiveData<List<Product>>
        get() = _productList

    private val _isSubmit = MutableLiveData<Boolean>()
    val isSubmit: LiveData<Boolean>
        get() = _isSubmit

    private val _cartList = MutableLiveData<List<Cart>>()
    val cartList: LiveData<List<Cart>>
        get() = _cartList

    private val _total = MutableLiveData<Double?>()
    val total: LiveData<Double?>
        get() = _total

    private val _weight = MutableLiveData<Int?>()
    val weight: LiveData<Int?>
        get() = _weight

    private val _provinces = MutableLiveData<List<Province>?>()
    val provinces: LiveData<List<Province>?>
        get() = _provinces


    private val _cites = MutableLiveData<List<City>?>()
    val cites: LiveData<List<City>?>
        get() = _cites

    private val _rajaOngkir = MutableLiveData<RajaOngkir?>()
    val rajaOngkir: LiveData<RajaOngkir?>
        get() = _rajaOngkir

    private val _midtransUrl = MutableLiveData<String?>()
    val midtransUrl: LiveData<String?>
        get() = _midtransUrl

    private val _transactions = MutableLiveData<List<TransactionHistory>>()
    val transactions: LiveData<List<TransactionHistory>>
        get() = _transactions

    var ongkir: Double = 0.0
    fun resetState() {
        setError(null)
        _product.value = null
        _isSubmit.value = false
    }

    fun isAnonymous(): Boolean = authenticationRepository.isAnonymousUser()

    fun fetchProducts() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.getProducts()
            _loading.value = false
            if (response.state) {
                val listProduct: MutableList<Product> = arrayListOf()
                listProduct.addAll(response.data ?: arrayListOf())
                _productList.value = listProduct
            } else {
                _product.value = null
                showError(GeneralException(response.message ?: ""))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun fetchProductById(id: String) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.getProductById(id)
            _loading.value = false
            if (response.state) {
                _product.value = response.data
            } else {
                _product.value = null
                showError(GeneralException(response.message ?: ""))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun addToCart(map: HashMap<String, String?>?) = viewModelScope.launch {
        _loadingCart.value = true
        try {
            val response = remoteRepository.addToCart(map)
            _loadingCart.value = false
            if (response.state) {
                _isSubmit.value = true
            } else {
                _isSubmit.value = false
                showError(GeneralException(response.message ?: ""))
            }
        } catch (exception: Exception) {
            _loadingCart.value = false
            showError(exception)
        }
    }

    fun fetchCarts() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.getCart()
            _loading.value = false
            if (response.state) {
                val list: MutableList<Cart> = arrayListOf()
                list.addAll(response.data ?: arrayListOf())
                _cartList.value = list
                calculateTotal()
            } else {
                _cartList.value = null
                showError(GeneralException(response.message ?: ""))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun calculateTotal() {
        val list = _cartList.value
        var total = 0.0
        var weight = 0
        list?.forEach {
            if (it.checked == 1) {
                val subTotal = it.barang.price * it.qty
                total += subTotal
                weight += it.barang.berat * it.qty
            }
        }
        _total.value = total + ongkir
        _weight.value = weight
    }

    fun cartUnchecked(id: String, status: String) = viewModelScope.launch {
        try {
            remoteRepository.cartUnchecked(id, status)
        } catch (exception: Exception) {
            showError(exception)
        }
    }

    fun cartUpdateQty(id: String, qty: String) = viewModelScope.launch {
        try {
            remoteRepository.cartUpdateQty(id, qty)
        } catch (exception: Exception) {
            showError(exception)
        }
    }

    fun province() = viewModelScope.launch {
        _loadingCart.value = true
        try {
            val response = remoteRepository.province()
            _loadingCart.value = false
            if (response.state) {
                val list: MutableList<Province> = arrayListOf()
                list.addAll(response.data ?: arrayListOf())
                _provinces.value = list
            } else {
                _provinces.value = null
                showError(GeneralException(response.message ?: ""))
            }
        } catch (exception: Exception) {
            _loadingCart.value = false
            showError(exception)
        }
    }

    fun cities(id: String) = viewModelScope.launch {
        _loadingCart.value = true
        try {
            val response = remoteRepository.city(id)
            _loadingCart.value = false
            if (response.state) {
                val list: MutableList<City> = arrayListOf()
                list.addAll(response.data ?: arrayListOf())
                _cites.value = list
            } else {
                _cites.value = null
                showError(GeneralException(response.message ?: ""))
            }
        } catch (exception: Exception) {
            _loadingCart.value = false
            showError(exception)
        }
    }

    fun cost(map: HashMap<String, String?>?) = viewModelScope.launch {
        _loadingCart.value = true
        try {
            val response = remoteRepository.cost(map)
            _loadingCart.value = false
            if (response.state) {
                _rajaOngkir.value = response.data
            } else {
                _rajaOngkir.value = null
                showError(GeneralException(response.message ?: ""))
            }
        } catch (exception: Exception) {
            _loadingCart.value = false
            showError(exception)
        }
    }

    fun getWeight(): Int {
        return _weight.value ?: 0
    }

    fun setOngkirTotal(cost: Double) {
        ongkir = cost
        calculateTotal()
    }

    fun checkout(map: HashMap<String, String?>?) = viewModelScope.launch {
        _loadingCart.value = true
        try {
            val response = remoteRepository.checkout(map)
            _loadingCart.value = false
            if (response.state) {
                _midtransUrl.value = response.data
            } else {
                _midtransUrl.value = null
                showError(GeneralException(response.message ?: ""))
            }
        } catch (exception: Exception) {
            _loadingCart.value = false
            showError(exception)
        }
    }

    fun fetchHistory() = viewModelScope.launch {
        _loadingCart.value = true
        try {
            val response = remoteRepository.history()
            _loadingCart.value = false
            if (response.state) {
                val list: MutableList<TransactionHistory> = arrayListOf()
                list.addAll(response.data ?: arrayListOf())
                _transactions.value = list
            } else {
                _transactions.value = null
                showError(GeneralException(response.message ?: ""))
            }
        } catch (exception: Exception) {
            _loadingCart.value = false
            showError(exception)
        }
    }

}