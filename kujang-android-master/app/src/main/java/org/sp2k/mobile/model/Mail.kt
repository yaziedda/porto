package org.sp2k.mobile.model

import java.io.Serializable

data class Mail(
    val asal_surat: String? = "",
    val created_at: String? = "",
    val created_by: String? = "",
    val dokumen: String? = "",
    val id: Int? = 0,
    val nama_penerima: String? = "",
    val nama_tujuan: String? = "",
    val no_surat: String? = "",
    val perihal_surat: String? = "",
    val type: String? = "",
    val status: String? = "",
    val tanggal_surat: String? = "",
    val updated_at: String? = "",
    val updated_by: String? = "",
    val buttonVisible: Boolean? = false,
) : Serializable