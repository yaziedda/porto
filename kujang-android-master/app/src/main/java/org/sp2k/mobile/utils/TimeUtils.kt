package org.sp2k.mobile.utils

import android.annotation.SuppressLint
import android.util.Log
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class TimeUtils {

    fun getArrBulan(): ArrayList<String> {
        val arrBulan = ArrayList<String>()
        arrBulan.add("Jan")
        arrBulan.add("Feb")
        arrBulan.add("Mar")
        arrBulan.add("Apr")
        arrBulan.add("Mei")
        arrBulan.add("Jun")
        arrBulan.add("Jul")
        arrBulan.add("Agu")
        arrBulan.add("Sep")
        arrBulan.add("Okt")
        arrBulan.add("Nov")
        arrBulan.add("Des")
        return arrBulan
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDate(tgl: String): String? {
        var tgl = tgl
        val bulan: ArrayList<String> = getArrBulan()
        val date = tgl
        return try {
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val past = format.parse(tgl)
            val now = Date()
            if (TimeUnit.MILLISECONDS.toSeconds(now.time - past.time) > 60) {
                if (TimeUnit.MILLISECONDS.toMinutes(now.time - past.time) > 60) {
                    if (TimeUnit.MILLISECONDS.toHours(now.time - past.time) > 24) {
                        if (TimeUnit.MILLISECONDS.toDays(now.time - past.time) > 7) {
                            val temp1 = tgl.split(" ".toRegex()).toTypedArray()
                            val temp2 = temp1[0].split("-".toRegex()).toTypedArray()
                            tgl = temp2[2] + " " + bulan[temp2[1].toInt() - 1] + " " + temp2[0]
                            Log.d("TANGGAL", temp2[2] + " " + temp2[1] + " " + temp2[0])
                            Log.d("TANGGAL", tgl)
                            tgl
                        } else {
                            tgl = TimeUnit.MILLISECONDS.toDays(now.time - past.time)
                                .toString() + " hari yang lalu"
                            println(
                                TimeUnit.MILLISECONDS.toDays(now.time - past.time)
                                    .toString() + " hari yang lalu"
                            )
                            tgl
                        }
                    } else {
                        tgl =
                            TimeUnit.MILLISECONDS.toHours(now.time - past.time).toString() + " jam"
                        println(
                            TimeUnit.MILLISECONDS.toHours(now.time - past.time).toString() + " jam"
                        )
                        tgl
                    }
                } else {
                    tgl =
                        TimeUnit.MILLISECONDS.toMinutes(now.time - past.time).toString() + " menit"
                    println(
                        TimeUnit.MILLISECONDS.toMinutes(now.time - past.time).toString() + " menit"
                    )
                    tgl
                }
            } else {
                tgl = TimeUnit.MILLISECONDS.toSeconds(now.time - past.time).toString() + " detik"
                println(TimeUnit.MILLISECONDS.toSeconds(now.time - past.time).toString() + " detik")
                if (tgl.contains("-")) {
                    tgl = "0"
                } else if (tgl == "0s") {
                    tgl = "Baru saja"
                }
                tgl
            }
        } catch (j: Exception) {
            j.printStackTrace()
            tgl
        }
    }

    fun getArrHari(): ArrayList<String> {
        val arrBulan = ArrayList<String>()
        arrBulan.add("Senin")
        arrBulan.add("Selasa")
        arrBulan.add("Rabu")
        arrBulan.add("Kamis")
        arrBulan.add("Jum'at")
        arrBulan.add("Sabtu")
        arrBulan.add("Minggu")
        return arrBulan
    }

    fun convertToHari(day: Int): String {
        val hari = getArrHari()
        when (day) {
            Calendar.SUNDAY -> return hari[6]
            Calendar.MONDAY -> return hari[0]
            Calendar.TUESDAY -> return hari[1]
            Calendar.WEDNESDAY -> return hari[2]
            Calendar.THURSDAY -> return hari[3]
            Calendar.FRIDAY -> return hari[4]
            Calendar.SATURDAY -> return hari[5]
        }
        return ""
    }

    fun getCurrentDate(): String? {
        val bulan: ArrayList<String> = getArrBulan()
        val hari: ArrayList<String> = getArrHari()
        val current = Date()
        val frmt = SimpleDateFormat("D, dd MM yyyy")
        val dateString = frmt.format(current)
        val calendar = Calendar.getInstance()
        calendar.time = current
        val day = calendar[Calendar.DAY_OF_WEEK]
        val year = calendar[Calendar.YEAR]
        val date = calendar[Calendar.DATE]
        val month = calendar[Calendar.MONTH]
        val hariName: String = convertToHari(day)
        val bulanName = bulan[Integer.valueOf(month)]
        return "$hariName, $date $bulanName $year"
    }
}