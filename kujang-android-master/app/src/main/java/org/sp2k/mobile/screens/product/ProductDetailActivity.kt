package org.sp2k.mobile.screens.product

import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityProductDetailBinding
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.toCurrency
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.Product
import org.sp2k.mobile.screens.auth.BlockingPageAnonymousFragment
import org.sp2k.mobile.utils.FragmentSupportManager
import org.sp2k.mobile.viewmodel.ProductViewModel
import kotlinx.android.synthetic.main.loading_view.*
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*

@AndroidEntryPoint
class ProductDetailActivity : BaseActivity<ActivityProductDetailBinding>() {
    override fun getLayoutId(): Int = R.layout.activity_product_detail
    private val viewModel by viewModels<ProductViewModel>()
    private lateinit var id: String

    companion object {
        const val KEY_ID = "KEY_ID"
    }

    override fun ActivityProductDetailBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        getExtras()
        initObserver()
        viewModel.fetchProductById(id)
    }

    private fun getExtras() {
        id = intent?.extras?.getString(KEY_ID) ?: ""
    }

    private fun ActivityProductDetailBinding.initObserver() {
        with(viewModel) {
            resetState()
            loading.observe {
                setLoading(this)
            }

            product.observe {
                if (this != null) {
                    setDetail(this)
                }
            }

            isSubmit.observe {
                if (this) {
                    startActivity(Intent(applicationContext, CartActivity::class.java))
                    finish()
                }
            }
            loadingCart.observe {
                if (this) {
                    showProgressDialog()
                } else {
                    dissmissProgressDialog()
                }
            }
        }
    }

    private fun ActivityProductDetailBinding.setDetail(product: Product) {
        toolbar.toolbar_support.toolbar_text_title.text = product.name
        if (product.images?.isNotEmpty() == true) {
            activityProductDetailCarousel.setImageListener { position, imageView ->
                val url = product.images.getOrNull(position) ?: ""
                Glide.with(applicationContext)
                    .load(url)
                    .into(imageView)

                imageView.setOnClickListener {
                    goToShowImageActivity(url)
                }
            }
            activityProductDetailCarousel.pageCount = product.images.size
        }

        activityProductDetailTvPrice.text = product.price.toString().toCurrency()
        activityProductDetailTvTitle.text = product.name
        activityProductDetailBerat.text = product.berat.toString()
        activityProductDetailDesc.isClickable = true
        activityProductDetailDesc.movementMethod = LinkMovementMethod.getInstance()
        activityProductDetailDesc.text = fromHtml(product.description)

        activityProductDetailBtnCart.setOnClickListener {
            if (!viewModel.isAnonymous()) {
                val map = HashMap<String, String?>()
                map["id_product"] = product.id.toString()
                map["qty"] = "1"
                viewModel.addToCart(map)
            } else {
                FragmentSupportManager(
                    this@ProductDetailActivity,
                    BlockingPageAnonymousFragment()
                ).show()
            }
        }
    }

    private fun ActivityProductDetailBinding.setLoading(
        b: Boolean
    ) {
        if (b) {
            progressBar.visible()
            activityProductDetailWrapper.gone()
        } else {
            progressBar.gone()
            activityProductDetailWrapper.visible()
        }
    }

    private fun ActivityProductDetailBinding.setupActionBar() {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
        }
    }
}