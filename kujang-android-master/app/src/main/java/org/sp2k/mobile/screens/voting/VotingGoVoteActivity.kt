package org.sp2k.mobile.screens.voting

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.FileUtils
import android.provider.Settings
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.sp2k.mobile.BR
import org.sp2k.mobile.BuildConfig
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityVotingGoVoteBinding
import org.sp2k.mobile.databinding.ItemVotingGoVoteBinding
import org.sp2k.mobile.event.ProkerEvent
import org.sp2k.mobile.event.VotingEvent
import org.sp2k.mobile.extensions.*
import org.sp2k.mobile.model.voting.Voting
import org.sp2k.mobile.permission.helper.PermissionHelper
import org.sp2k.mobile.permission.listener.PermissionListener
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.VotePollViewModel
import java.io.File


@AndroidEntryPoint
class VotingGoVoteActivity : BaseActivity<ActivityVotingGoVoteBinding>() {

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter<Voting, ItemVotingGoVoteBinding>
    val viewModel by viewModels<VotePollViewModel>()

    override fun getLayoutId(): Int = R.layout.activity_voting_go_vote
    private var idIdentity: String = ""
    private lateinit var fileImage: File
    private var imageUri: Uri? = null
    private val takePicture =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { _ ->
            if (fileImage.exists())
                lifecycleScope.launch {
                    imageUri?.let {
                        val compressedFile = compressFile(fileImage)
                        val requestFile: RequestBody = RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            compressedFile
                        )
                        val image: MultipartBody.Part = MultipartBody.Part.createFormData(
                            "foto",
                            compressedFile.name,
                            requestFile
                        )
                        val status: RequestBody =
                            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "1")
                        val id: RequestBody = RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            idIdentity
                        )
                        viewModel.votingPost(status, id, image)
                    }
                }
        }

    override fun ActivityVotingGoVoteBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupUI()
        setupObserver()
        fetchData()

        activityVotingGoVoteButton.setOnClickListener {
            showDialogOptionBottomSheet("Apakah anda sudah yakin dengan pilihan anda?", yeButtonClick = {
                pickImage {
                    with(applicationContext) {
                        createImageFile().also {
                            imageUri = getUriFromFile(it)
                            fileImage = it
                            takePicture.launch(imageUri)
                        }
                    }
                }
            })
        }
    }

    private fun ActivityVotingGoVoteBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        emptyState.gone()
                        recyclerView.gone()
                        activityVotingGoVoteButton.gone()
                        containerLoading.visible()
                    } else {
                        recyclerView.visible()
                        containerLoading.gone()
                    }
                }
            }

            votingList.observe {
                this?.voting?.let {
                    recyclerViewAdapter.updateList(it)
                    if (it.isNotEmpty()) {
                        recyclerView.visible()
                        emptyState.gone()
                    } else {
                        recyclerView.gone()
                        emptyState.visible()
                    }
                }
            }

            isSubmited.observe {
                this?.let {
                    if (it) {
                        EventBus.getDefault().post(VotingEvent(VotingEvent.KEY))
                        showMessage("Anda berhasil melakukan voting")
                        finish()
                    }
                }
            }

            error.observe {
                showError(this) {
                    finish()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityVotingGoVoteBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "E-Voting"
        }
    }

    private fun ActivityVotingGoVoteBinding.setupUI() {
        StatusBar.setLightStatusBar(window)
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetchData()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityVotingGoVoteBinding.setupAdapter() {
        recyclerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_voting_go_vote,
            BR.votingGoVote
        ) { itemView, itemModel ->

            itemView.itemVotingGoVoteRadio.isFocusable = false
            itemView.itemVotingGoVoteTitle.text =
                "${itemModel.nama_ketua} & ${itemModel.nama_sekum}"
            itemView.itemVotingGoVoteUrut.text = "Calon ${itemModel.no_urut}"

            if (itemModel.isChecked) {
                itemView.itemVotingGoVoteRadio.isChecked = true
                itemModel.isChecked = false
            } else {
                itemView.itemVotingGoVoteRadio.isChecked = false
            }

            itemView.itemVotingGoVoteRadio.setOnClickListener {
                setValidateChoiceSelection(itemModel)
            }

            itemView.root.setOnClickListener {
                setValidateChoiceSelection(itemModel)
            }

        }

        val linearLayoutManager = LinearLayoutManager(
            this@VotingGoVoteActivity,
            LinearLayoutManager.VERTICAL,
            false
        )

        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerViewAdapter
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun ActivityVotingGoVoteBinding.setValidateChoiceSelection(
        itemModel: Voting
    ) {
        activityVotingGoVoteButton.visible()
        idIdentity = itemModel.id.toString()
        itemModel.isChecked = true
        recyclerViewAdapter.notifyDataSetChanged()
    }

    private val permissionHelper by lazy { PermissionHelper(this) }
    private val requestedPermissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    private fun pickImage(action: () -> Unit) {
        permissionHelper.requests(requestedPermissions)
            .listener(object : PermissionListener.Request {
                override fun granted() = action.invoke()
                override fun showRequestPermissionRationale() = showDeniedPermission()
                override fun denied() = showDeniedPermission {
                    startActivity(
                        Intent(
                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                        )
                    )
                }
            })
    }

    private fun showDeniedPermission(action: (() -> Unit)? = null) {
        showDialog(getString(R.string.label_camera_permission)) {
            onBackPressed()
            action?.invoke()
        }
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: VotingEvent?) {
        fetchData()
    }

    private fun fetchData() {
        viewModel.votingList()
    }
}