package org.sp2k.mobile.model

data class Anggaran(
    val anggaran_limit: Double,
    val saldo: Double,
    val sisa: Double,
    val name: String
)