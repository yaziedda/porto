package org.sp2k.mobile.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.exception.GeneralException
import org.sp2k.mobile.model.*
import org.sp2k.mobile.model.news.NewsComment
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository
import org.sp2k.mobile.utils.PreferenceManager
import javax.inject.Inject

@HiltViewModel
class NewsViewModel @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    private val preferenceManager: PreferenceManager,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _isRunning: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val isRunning: MutableLiveData<Boolean>
        get() = _isRunning

    private val _news = MutableLiveData<MutableList<News>>()
    val news: LiveData<MutableList<News>>
        get() = _news

    private val _newsDetail = MutableLiveData<NewsDetail?>()
    val newsDetail: LiveData<NewsDetail?>
        get() = _newsDetail

    private val _newsComment = MutableLiveData<MutableList<NewsComment>?>()
    val newsComment: LiveData<MutableList<NewsComment>?>
        get() = _newsComment

    private val _sendIDComment: MutableLiveData<NewsComment?> by lazy {
        MutableLiveData(null)
    }

    val sendIDComment: MutableLiveData<NewsComment?>
        get() = _sendIDComment

    private val _successDeletedComment = MutableLiveData<Boolean?>()
    val successDeletedComment: LiveData<Boolean?>
        get() = _successDeletedComment


    fun resetState() {
        _news.value = arrayListOf()
        _newsDetail.value = null
        setError(null)
        _newsComment.value = null
        _sendIDComment.value = null
        _successDeletedComment.value = false
    }

    fun fetchNews() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.news()
            if (response.state) {
                val listNews: MutableList<News> = arrayListOf()
                response.data?.let { listNews.addAll(it) }
                _news.value = listNews
                _loading.value = false
            } else {
                _loading.value = false
                _news.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }

    }

    fun fetchNewsById(id: String) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.newsById(id)
            if (response.state) {
                _newsDetail.value = response.data
                _loading.value = false
            } else {
                _loading.value = false
                _newsDetail.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun fetchNewsCommentById(id: String, page: Int) = viewModelScope.launch {
        _isRunning.value = true
        try {
            val response = remoteRepository.newsCommentById(id, page)
            if (response.state) {
                val listNewsComment: MutableList<NewsComment> = arrayListOf()
                response.data?.data?.let { listNewsComment.addAll(it) }
                _newsComment.value = listNewsComment
                _isRunning.value = false
            } else {
                _isRunning.value = false
                _newsComment.value = null
                showError(GeneralException(response.message))
            }
        } catch (exception: Exception) {
            _isRunning.value = false
            showError(exception)
        }
    }

    fun sendNewsComment(map: HashMap<String, String>) = viewModelScope.launch {
        val uid = authenticationRepository.currentUser()
        try {
            val response = remoteRepository.newsCommentSend(map)
            if (response.state) {
                _sendIDComment.value = response.data
            }
        } catch (exception: Exception) {
            showError(exception)
        }
    }

    fun deleteNewsComment(id: String) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.deleteCommentById(id)
            if (response.state) {
                _loading.value = false
                _successDeletedComment.value = response.state
            } else {
                _loading.value = false
            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun getIsRunning() = _isRunning.value

    fun getUser() = preferenceManager.getUser()
    fun generateCommentId() = (System.currentTimeMillis().toString()+authenticationRepository.currentUser()?.uid).replace(" ", "").lowercase()

}