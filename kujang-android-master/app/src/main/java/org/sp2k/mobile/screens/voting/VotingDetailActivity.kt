package org.sp2k.mobile.screens.voting

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.sp2k.mobile.R
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityVotingDetailBinding
import org.sp2k.mobile.viewmodel.ProkerViewModel


@AndroidEntryPoint
class VotingDetailActivity : BaseActivity<ActivityVotingDetailBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_voting_detail

    val viewModel by viewModels<ProkerViewModel>()
    private var calonNamaKetum: String? = ""
    private var calonNIKKetum: String? = ""
    private var calonJabatanSebelumnyaKetum: String? = ""
    private var calonNamaSekum: String? = ""
    private var calonNIKSekum: String? = ""
    private var calonJabatanSebelumnyaSekum: String? = ""
    private var calonUrut: String? = ""
    private var calonFoto: String? = ""
    private var calonDeskripsi: String? = ""

    companion object {
        const val KETUM_NAMA = "KETUM_NAMA"
        const val KETUM_NIK = "KETUM_NIK"
        const val KETUM_JABATAN_SEBELUMNYA = "KETUM_JABATAN_SEBELUMNYA"

        const val SEKUM_NAMA = "SEKUM_NAMA"
        const val SEKUM_NIK = "SEKUM_NIK"
        const val SEKUM_JABATAN_SEBELUMNYA = "SEKUM_JABATAN_SEBELUMNYA"

        const val URUT = "URUT"
        const val FOTO = "FOTO"
        const val DESKRIPSI = "DESKRIPSI"
    }

    override fun ActivityVotingDetailBinding.initializeView(savedInstanceState: Bundle?) {
        getExtras()
        setupActionBar()
        setUI()
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityVotingDetailBinding.setUI() {
        votingDetailKetuaNama.text = calonNamaKetum
        votingDetailKetuaNik.text = calonNIKKetum
        votingDetailKetuaJabatanSebelumnya.text = calonJabatanSebelumnyaKetum

        votingDetailSekumNama.text = calonNamaSekum
        votingDetailSekumNik.text = calonNIKSekum
        votingDetailSekumJabatanSebelumnya.text = calonJabatanSebelumnyaSekum

        votingDetailUrut.text = "Calon $calonUrut"

        Glide.with(applicationContext)
            .load(calonFoto)
            .into(activityVotingDetailDetailImage)

        activityVotingDetailDetailImage.setOnClickListener {
            goToShowImageActivity(calonFoto ?: "")
        }

        htmlDetailContent.apply {
            setHtml(calonDeskripsi ?: "")
            setOnClickATagListener { _, _, href ->
                goToWebViewActivity(href ?: "")
                return@setOnClickATagListener true
            }
        }
    }

    private fun getExtras() {
        calonNamaKetum = intent?.extras?.getString(KETUM_NAMA)
        calonNIKKetum = intent?.extras?.getString(KETUM_NIK)
        calonJabatanSebelumnyaKetum = intent?.extras?.getString(KETUM_JABATAN_SEBELUMNYA)

        calonNamaSekum = intent?.extras?.getString(SEKUM_NAMA)
        calonNIKSekum = intent?.extras?.getString(SEKUM_NIK)
        calonJabatanSebelumnyaSekum = intent?.extras?.getString(SEKUM_JABATAN_SEBELUMNYA)

        calonUrut = intent?.extras?.getString(URUT)
        calonFoto = intent?.extras?.getString(FOTO)
        calonDeskripsi = intent?.extras?.getString(DESKRIPSI)
    }


    @SuppressLint("SetTextI18n")
    private fun ActivityVotingDetailBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "Detail Calon"
        }
    }

}