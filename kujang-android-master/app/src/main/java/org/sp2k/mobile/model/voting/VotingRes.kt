package org.sp2k.mobile.model.voting

data class VotingRes (
    val voting: List<Voting>? = arrayListOf(),
    val hasVoted: Boolean? = true,
    val isLive: Boolean? = false,
    val isClose: Boolean? = false,
    val evotePublish: Boolean? = false,
    val title: String? = "",
    val subTitle: String? = ""
)