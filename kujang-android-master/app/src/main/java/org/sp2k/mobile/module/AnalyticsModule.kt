package org.sp2k.mobile.module

import android.app.Application
import com.google.firebase.analytics.FirebaseAnalytics
import com.moe.pushlibrary.MoEHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class AnalyticsModule {

    @Provides
    fun providesGoogleAnalytics(
        application: Application
    ): FirebaseAnalytics {
        return FirebaseAnalytics.getInstance(application)
    }

    @Provides
    fun providesMoengageAnalytics(
        application: Application
    ): MoEHelper {
        return MoEHelper.getInstance(application)
    }
}