package org.sp2k.mobile.model.prokeranggaran

import org.sp2k.mobile.model.Bidang
import org.sp2k.mobile.model.user.Staff
import java.io.Serializable

data class ProkerData(
    val usulan: Usulan?,
    val proker: Proker?,
    val lpj: Lpj?,
    val usulan_updated: Staff?,
    val proker_updated: Staff?,
    val lpj_updated: Staff?,
    val bidang: Bidang?,
    val image_kwitansi: List<String> = arrayListOf(),
    val image_dokumentasi: List<String> = arrayListOf(),
) : Serializable