package org.sp2k.mobile.screens.notification

import android.annotation.SuppressLint
import android.content.Intent
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseFragment
import org.sp2k.mobile.databinding.FragmentNotificationBinding
import org.sp2k.mobile.databinding.ItemNotificationBinding
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.KujangNotification
import org.sp2k.mobile.screens.mail.`in`.InComingMailDetailActivity
import org.sp2k.mobile.screens.mail.out.OutComingMailDetailActivity
import org.sp2k.mobile.screens.polling.PollingActivity
import org.sp2k.mobile.screens.proker.ProkerlDetailActivity
import org.sp2k.mobile.screens.voting.VotingActivity
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.DateUtils
import org.sp2k.mobile.viewmodel.MainViewModel

class NotificationFragment : BaseFragment<FragmentNotificationBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_notification
    private val viewModel by activityViewModels<MainViewModel>()
    private lateinit var recyclerViewAdapter: RecyclerViewAdapter<KujangNotification, ItemNotificationBinding>

    override fun FragmentNotificationBinding.initializeView() {
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupUI()
        setupObserver()
        fetcher()
    }

    private fun FragmentNotificationBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                this?.let {
                    if (it) {
                        containerLoading.visible()
                        notificationContainer.gone()
                        emptyState.gone()
                    } else {
                        containerLoading.gone()
                        notificationContainer.visible()
                        swipeContainer.isRefreshing = false
                    }
                }
            }

            notificationList.observe {
                this?.let {
                    if (it.isNotEmpty()) {
                        emptyState.gone()
                        recyclerViewAdapter.updateList(this)
                    } else {
                        emptyState.visible()
                    }
                }
            }
        }
    }

    private fun FragmentNotificationBinding.setupUI() {
        setupNewsAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            getNotificationList()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun FragmentNotificationBinding.setupNewsAdapter() {
        recyclerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_notification,
            BR.kujangNotification
        ) { itemView, itemModel ->

            itemView.itemNotificationTvTitle.text = itemModel.title
            itemView.itemNotificationTvSubTitle.text = itemModel.body
            itemView.itemNotificationTime.text = DateUtils.let { date ->
                date.convertLongDateToAgoString(
                    date.parseDate(
                        itemModel.created_at,
                        Constants.yyyy_MM_dd_HH_mm_ss
                    )?.time ?: 0
                )
            }

            when (itemModel.type) {
                "lpj" -> {
                    Glide.with(requireActivity())
                        .load(itemModel.image)
                        .error(R.drawable.ic_menu_1x_proker)
                        .into(itemView.itemNotificationIvImage)
                    goToProkerDetail(itemView, itemModel)
                }
                "proposal" -> {
                    Glide.with(requireActivity())
                        .load(itemModel.image)
                        .error(R.drawable.ic_menu_1x_proker)
                        .into(itemView.itemNotificationIvImage)
                    goToProkerDetail(itemView, itemModel)
                }
                "anggaran" -> {
                    Glide.with(requireActivity())
                        .load(itemModel.image)
                        .error(R.drawable.ic_anggaran)
                        .into(itemView.itemNotificationIvImage)
                    goToProkerDetail(itemView, itemModel)
                }
                "anggaran_bidang" -> {
                    Glide.with(requireActivity())
                        .load(itemModel.image)
                        .error(R.drawable.ic_anggaran_bidang)
                        .into(itemView.itemNotificationIvImage)
                    goToProkerDetail(itemView, itemModel)
                }
                "voting" -> {
                    Glide.with(requireActivity())
                        .load(itemModel.image)
                        .error(R.drawable.ic_menu_1x_voting)
                        .into(itemView.itemNotificationIvImage)
                    itemView.itemNotificationRoot.setOnClickListener {
                        startActivity(
                            Intent(requireContext(), VotingActivity::class.java)
                        )
                    }
                }
                "polling" -> {
                    Glide.with(requireActivity())
                        .load(itemModel.image)
                        .error(R.drawable.ic_menu_1x_polling)
                        .into(itemView.itemNotificationIvImage)
                    itemView.itemNotificationRoot.setOnClickListener {
                        startActivity(
                            Intent(requireContext(), PollingActivity::class.java)
                        )
                    }
                }
                "surat_masuk" -> {
                    Glide.with(requireActivity())
                        .load(itemModel.image)
                        .error(R.drawable.ic_menu_1x_surat_masuk)
                        .into(itemView.itemNotificationIvImage)
                    itemView.itemNotificationRoot.setOnClickListener {
                        startActivity(
                            Intent(requireContext(), InComingMailDetailActivity::class.java).apply {
                                putExtra(InComingMailDetailActivity.ID, itemModel.key)
                            }
                        )
                    }
                }
                "surat_keluar" -> {
                    Glide.with(requireActivity())
                        .load(itemModel.image)
                        .error(R.drawable.ic_menu_1x_surat_keluar)
                        .into(itemView.itemNotificationIvImage)
                    itemView.itemNotificationRoot.setOnClickListener {
                        startActivity(
                            Intent(requireContext(), OutComingMailDetailActivity::class.java).apply {
                                putExtra(OutComingMailDetailActivity.ID, itemModel.key)
                            }
                        )
                    }
                }
                else -> {
                    Glide.with(requireActivity())
                        .load(itemModel.image)
                        .error(R.drawable.logo)
                        .into(itemView.itemNotificationIvImage)
                    itemView.itemNotificationRoot.setOnClickListener {

                    }
                }
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )
        notificationRecyclerview.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerViewAdapter
            isNestedScrollingEnabled = false
        }
    }

    private fun goToProkerDetail(
        itemView: ItemNotificationBinding,
        itemModel: KujangNotification
    ) {
        itemView.itemNotificationRoot.setOnClickListener {
            startActivity(
                Intent(requireContext(), ProkerlDetailActivity::class.java).apply {
                    putExtra(ProkerlDetailActivity.ID, itemModel.key)
                }
            )
        }
    }

    private fun goToDestination(kujangNotification: KujangNotification) {
        startActivity(
            Intent(requireContext(), ProkerlDetailActivity::class.java).apply {
                putExtra(ProkerlDetailActivity.ID, kujangNotification.key)
            }
        )
    }
}