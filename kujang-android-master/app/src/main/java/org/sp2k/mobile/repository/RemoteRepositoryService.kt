package org.sp2k.mobile.repository

import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.sp2k.mobile.model.*
import org.sp2k.mobile.model.blog.Blog
import org.sp2k.mobile.model.company.CompanyData
import org.sp2k.mobile.model.donation.Donation
import org.sp2k.mobile.model.news.NewsComment
import org.sp2k.mobile.model.news.NewsCommentData
import org.sp2k.mobile.model.polling.Polling
import org.sp2k.mobile.model.polling.PollingRequest
import org.sp2k.mobile.model.prokeranggaran.ProkerData
import org.sp2k.mobile.model.request.DisposisiRequest
import org.sp2k.mobile.model.request.ProkerApproval
import org.sp2k.mobile.model.request.SuratKeluarApproveRequest
import org.sp2k.mobile.model.request.SuratKeluarRejectRequest
import org.sp2k.mobile.model.user.AuthData
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.model.voting.VotingRes
import retrofit2.http.*


interface RemoteRepositoryService {

    @GET("home")
    suspend fun getHome(): BaseResponse<Home>

    @GET("donasi")
    suspend fun getDonasi(): BaseResponse<List<Donation>>

    @GET("donasi/{id}")
    suspend fun getDonasiById(@Path("id") id: String): BaseResponse<Donation>

    @FormUrlEncoded
    @POST("checkout-donation")
    suspend fun sendDonasi(
        @FieldMap map: HashMap<String, String?>?
    ): BaseResponse<String>

    @GET("product")
    suspend fun getProducts(): BaseResponse<List<Product>>

    @GET("product/{id}")
    suspend fun getProductById(@Path("id") id: String): BaseResponse<Product>

    @FormUrlEncoded
    @POST("add-to-cart")
    suspend fun addToCart(
        @FieldMap map: HashMap<String, String?>?
    ): BaseResponse<String>

    @GET("cart")
    suspend fun getCart(): BaseResponse<List<Cart>>

    @GET("cartUnchecked/{id}/{status}")
    suspend fun cartUnchecked(
        @Path("id") id: String,
        @Path("status") status: String
    ): BaseResponse<String>

    @GET("cartUpdateQty/{id}/{qty}")
    suspend fun cartUpdateQty(
        @Path("id") id: String,
        @Path("qty") qty: String
    ): BaseResponse<String>

    @GET("province")
    suspend fun province(): BaseResponse<List<Province>>


    @GET("city/{id}")
    suspend fun city(@Path("id") id: String): BaseResponse<List<City>>

    @FormUrlEncoded
    @POST("cost")
    suspend fun cost(
        @FieldMap map: HashMap<String, String?>?
    ): BaseResponse<RajaOngkir>

    @FormUrlEncoded
    @POST("checkout")
    suspend fun checkout(
        @FieldMap map: HashMap<String, String?>?
    ): BaseResponse<String>

    @GET("history")
    suspend fun history(): BaseResponse<List<TransactionHistory>>

    @GET("blog")
    suspend fun blog(): BaseResponse<List<Blog>>

    @GET("jasa")
    suspend fun jasa(): BaseResponse<List<Jasa>>


    @GET("jasa/{id}")
    suspend fun jasaById(@Path("id") id: String): BaseResponse<Jasa>

    @FormUrlEncoded
    @POST("token-updated")
    suspend fun tokenUpdate(
        @FieldMap map: HashMap<String, String?>?
    ): BaseResponse<String>

    @FormUrlEncoded
    @POST("login")
    suspend fun login(
        @FieldMap map: HashMap<String, String>
    ): BaseResponse<AuthData>

    @GET("news")
    suspend fun news(): BaseResponse<List<News>>

    @GET("company")
    suspend fun company(): BaseResponse<CompanyData>

    @GET("news/{id}")
    suspend fun newsById(@Path("id") id: String): BaseResponse<NewsDetail>

    @GET("news/comment/{id}")
    suspend fun newsCommentById(@Path("id") id: String, @Query("page") page: Int): BaseResponse<NewsCommentData>

    @FormUrlEncoded
    @POST("news/comment/send")
    suspend fun newsCommentSend(
        @FieldMap map: HashMap<String, String>
    ): BaseResponse<NewsComment>

    @GET("search-user")
    suspend fun search(@Query("search") search: String): BaseResponse<List<Staff>>

    @GET("group")
    suspend fun groups(): BaseResponse<List<GroupMail>>

    @GET("surat-masuk")
    suspend fun suratMasuk(): BaseResponse<List<Mail>>

    @POST("disposisi")
    suspend fun disposisi(
        @Body disposisiRequest: DisposisiRequest
    ): BaseResponse<Any>

    @POST("surat-keluar/approve")
    suspend fun approveSuratKeluar(
        @Body suratKeluarApproveRequest: SuratKeluarApproveRequest
    ): BaseResponse<Any>

    @GET("surat-keluar")
    suspend fun suratKeluar(): BaseResponse<List<SuratKeluar>>

    @POST("surat-keluar/disposisi")
    suspend fun disposisiKeluar(
        @Body disposisiRequest: DisposisiRequest
    ): BaseResponse<Any>

    @POST("surat-keluar/reject")
    suspend fun rejectSuratKeluar(
        @Body suratKeluarRejectRequest: SuratKeluarRejectRequest
    ): BaseResponse<Any>

    @GET("news/comment/delete/{id}")
    suspend fun deleteCommentById(@Path("id") id: String): BaseResponse<Any>

    @GET("proker")
    suspend fun prokerList(@Query("type") type: String): BaseResponse<ProkerResponse>

    @GET("proker/{id}")
    suspend fun prokerById(@Path("id") id: String): BaseResponse<ProkerData>

    @POST("proker/approve")
    suspend fun prokerApproval(
        @Body prokerApproval: ProkerApproval
    ): BaseResponse<Any>

    @GET("anggaran")
    suspend fun anggaran(): BaseResponse<AnggaranBidang>

    @GET("polling")
    suspend fun pollingList(): BaseResponse<List<Polling>>

    @GET("evote")
    suspend fun votingList(): BaseResponse<VotingRes>

    @POST("polling/vote")
    suspend fun pollingVote(
        @Body pollingRequest: PollingRequest
    ): BaseResponse<Any>


    @Multipart
    @POST("evote/vote")
    suspend fun voteVote(
        @Part("status") status: RequestBody?,
        @Part("id") id: RequestBody?,
        @Part file: MultipartBody.Part?
    ): BaseResponse<Any>

    @Multipart
    @POST("profile/change-profile")
    suspend fun changeProfile(
        @Part file: MultipartBody.Part?
    ): BaseResponse<String>

    @POST("profile/change-password")
    suspend fun changePassword(
        @Body changePasswordRequest: ChangePasswordRequest
    ): BaseResponse<Any>

    @GET("surat-masuk/{id}")
    suspend fun suratMasukById(@Path("id") id: String): BaseResponse<Mail>

    @GET("surat-keluar/{id}")
    suspend fun suratKeluarById(@Path("id") id: String): BaseResponse<SuratKeluar>

    @GET("notification")
    suspend fun notificationList(): BaseResponse<List<KujangNotification>>
}