package org.sp2k.mobile.screens.mail.`in`

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseActivity
import org.sp2k.mobile.databinding.ActivityInComingMailDisposisiBinding
import org.sp2k.mobile.databinding.ItemInComingMailDisposisiBinding
import org.sp2k.mobile.event.SuratEvent
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.GroupMail
import org.sp2k.mobile.model.request.DisposisiRequest
import org.sp2k.mobile.utils.StatusBar
import org.sp2k.mobile.viewmodel.SuratMainViewModel

@AndroidEntryPoint
class InComingMailDisposisiActivity : BaseActivity<ActivityInComingMailDisposisiBinding>() {

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter<GroupMail, ItemInComingMailDisposisiBinding>
    val viewModel by viewModels<SuratMainViewModel>()
    private var idSurat: Int = 0
    private var idGroup: String = ""

    override fun getLayoutId(): Int = R.layout.activity_in_coming_mail_disposisi

    companion object {
        const val ID_SURAT = "ID_SURAT"
    }

    override fun ActivityInComingMailDisposisiBinding.initializeView(savedInstanceState: Bundle?) {
        getExtras()
        setupActionBar()
        setupUI()
        setupObserver()
        viewModel.fetchGroups()
    }

    private fun getExtras() {
        idSurat = intent?.extras?.getInt(ID_SURAT) ?: 0
    }

    private fun ActivityInComingMailDisposisiBinding.setupObserver() {
        viewModel.apply {
            resetState()
            loading.observe {
                this?.let {
                    swipeContainer.isRefreshing = it
                    if (it) {
                        activityInComingMailDisposisiRecycler.gone()
                        activityInComingMailDisposisiAccept.gone()
                        containerLoading.visible()
                    } else {
                        activityInComingMailDisposisiRecycler.visible()
                        containerLoading.gone()

                    }
                }
            }

            groupList.observe {
                this?.let {
                    recyclerViewAdapter.updateList(it)
                }
            }

            disposisiSuccess.observe {
                this?.let {
                    if (it) {
                        showDialog("Berhasil melakukan disposisi") {
                            EventBus.getDefault().post(SuratEvent(SuratEvent.KEY))
                        }
                    }
                }
            }

            error.observe {
                showError(this)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun ActivityInComingMailDisposisiBinding.setupActionBar(
    ) {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.toolbar_text_title.text = "Pilih Disposisi"
        }
    }

    private fun ActivityInComingMailDisposisiBinding.setupUI() {
        StatusBar.setLightStatusBar(window)
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                viewModel.fetchGroups()
            }
        }

        activityInComingMailDisposisiAccept.setOnClickListener {
            showDialogOptionBottomSheet("Apakah anda yakin akan mendisposisi?", yeButtonClick = {
                val disposisiRequest = DisposisiRequest(
                    id_surat = idSurat.toString(),
                    id_group = idGroup,
                    users = arrayListOf()
                )
                viewModel.sendDisposisi(disposisiRequest)
            })
        }
    }

    private fun ActivityInComingMailDisposisiBinding.setupAdapter() {
        recyclerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_in_coming_mail_disposisi,
            BR.groupMail
        ) { itemView, itemModel ->

            itemView.itemInComingMailDisposisiRadio.isFocusable = false
            itemView.itemInComingMailDisposisiText.text = itemModel.name
            if (itemModel.isChecked) {
                itemView.itemInComingMailDisposisiRadio.isChecked = true
                itemModel.isChecked = false
            } else {
                itemView.itemInComingMailDisposisiRadio.isChecked = false
            }

            itemView.itemInComingMailDisposisiRadio.setOnClickListener {
                setValidateChoiceSelection(itemModel)
            }

            itemView.root.setOnClickListener {
                setValidateChoiceSelection(itemModel)
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            this@InComingMailDisposisiActivity,
            LinearLayoutManager.VERTICAL,
            false
        )

        activityInComingMailDisposisiRecycler.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerViewAdapter
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun ActivityInComingMailDisposisiBinding.setValidateChoiceSelection(
        itemModel: GroupMail
    ) {
        idGroup = itemModel.code
        itemModel.isChecked = true
        recyclerViewAdapter.notifyDataSetChanged()

        if (itemModel.code == "-1") {
            activityInComingMailDisposisiAccept.gone()
            startActivity(
                Intent(
                    applicationContext,
                    MailDisposisiSearchActivity::class.java
                ).apply {
                    putExtra(MailDisposisiSearchActivity.ID_SURAT, idSurat)
                }
            )
        } else {
            activityInComingMailDisposisiAccept.visible()
        }
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(event: SuratEvent?) {
        finish()
    }
}