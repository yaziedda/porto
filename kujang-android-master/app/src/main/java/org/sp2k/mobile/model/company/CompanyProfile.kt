package org.sp2k.mobile.model.company

data class CompanyProfile(
    val created_at: String,
    val description: String,
    val id: Int,
    val mision: String,
    val structure_org: String,
    val updated_at: String,
    val vision: String
)