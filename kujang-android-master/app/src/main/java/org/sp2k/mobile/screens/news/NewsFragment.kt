package org.sp2k.mobile.screens.news

import android.annotation.SuppressLint
import android.content.Intent
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import org.sp2k.mobile.BR
import org.sp2k.mobile.R
import org.sp2k.mobile.adapters.RecyclerViewAdapter
import org.sp2k.mobile.base.BaseFragment
import org.sp2k.mobile.databinding.FragmentNewsBinding
import org.sp2k.mobile.databinding.ItemNewsBinding
import org.sp2k.mobile.extensions.gone
import org.sp2k.mobile.extensions.visible
import org.sp2k.mobile.model.News
import org.sp2k.mobile.utils.Constants
import org.sp2k.mobile.utils.DateUtils
import org.sp2k.mobile.viewmodel.NewsViewModel

class NewsFragment : BaseFragment<FragmentNewsBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_news
    private val viewModel by activityViewModels<NewsViewModel>()
    private lateinit var recyclerNewsViewAdapter: RecyclerViewAdapter<News, ItemNewsBinding>

    override fun FragmentNewsBinding.initializeView() {
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupUI()
        setupObserver()
        fetcher()
    }

    private fun FragmentNewsBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                this?.let {
                    if (it) {
                        containerLoading.visible()
                        newsContainer.gone()
                    } else {
                        containerLoading.gone()
                        newsContainer.visible()
                        swipeContainer.isRefreshing = false
                    }
                }
            }
            news.observe {
                this.getOrNull(0)?.let {
                    setNewsHighlight(it)
                }

                if (this.isNotEmpty()) {
                    this.apply {
                        if (this.size > 1) {
                            removeAt(0)
                        }
                        recyclerNewsViewAdapter.updateList(this)
                    }
                }
            }
        }
    }

    private fun FragmentNewsBinding.setNewsHighlight(it: News) {
        newsHighlightTitle.apply {
            text = it.berita.judul
            setOnClickListener { _ ->
                goToNewsDetail(it.berita.id)
            }
        }
        newsHighlightDesc.apply {
            text = it.berita.ringkasan
            setOnClickListener { _ ->
                goToNewsDetail(it.berita.id)
            }
        }
        newsHighlightTime.apply {
            text = DateUtils.let { date ->
                date.convertLongDateToAgoString(
                    date.parseDate(
                        it.berita.updated_at,
                        Constants.yyyy_MM_dd_HH_mm_ss
                    )?.time ?: 0
                )
            }
            setOnClickListener { _ ->
                goToNewsDetail(it.berita.id)
            }
        }


        Glide.with(this@NewsFragment)
            .load(it.berita.image)
            .into(newsHighlightImage)

        newsHighlightImage.setOnClickListener { _ ->
            goToNewsDetail(it.berita.id)
        }
        newsHighlightContainer.setOnClickListener { _ ->
            goToNewsDetail(it.berita.id)
        }
        newsHighlightWrapper.setOnClickListener { _ ->
            goToNewsDetail(it.berita.id)
        }
    }

    private fun FragmentNewsBinding.setupUI() {
        setupNewsAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchNews()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun FragmentNewsBinding.setupNewsAdapter() {
        recyclerNewsViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_news,
            BR.news
        ) { itemView, itemModel ->

            itemView.itemNewsTvTitle.text = itemModel.berita.judul
            itemView.itemNewsTvSubTitle.text = if (itemModel.berita.ringkasan?.length ?: 0 > 39) {
                itemModel.berita.ringkasan?.substring(0, 39) + "..."
            } else {
                itemModel.berita.ringkasan
            }
            itemView.itemNewsTitle2.text = itemModel.kategori.kategori
            itemView.itemNewsTitle3.text =
                DateUtils.let { date ->
                    date.convertLongDateToAgoString(
                        date.parseDate(
                            itemModel.berita.updated_at,
                            Constants.yyyy_MM_dd_HH_mm_ss
                        )?.time ?: 0
                    )
                }

            Glide.with(requireActivity())
                .load(itemModel.berita.image)
                .into(itemView.itemNewsIvImage)

            itemView.root.setOnClickListener {
                goToNewsDetail(itemModel.berita.id)
            }
        }

        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )
        newsRecyclerview.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerNewsViewAdapter
            isNestedScrollingEnabled = false
        }
    }

    private fun goToNewsDetail(id: String) {
        startActivity(
            Intent(requireContext(), NewsDetailActivity::class.java).apply {
                putExtra(NewsDetailActivity.KEY_NEWS_ID, id)
            }
        )
    }
}