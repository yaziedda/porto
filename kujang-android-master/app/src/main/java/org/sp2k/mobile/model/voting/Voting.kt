package org.sp2k.mobile.model.voting

data class Voting(
    val created_at: String,
    val created_by: String,
    val deskripsi: String,
    val foto: String,
    val id: Int,
    val nama_ketua: String,
    val nik_ketua: String,
    val jabatan_ketua: String,
    val nama_sekum: String,
    val nik_sekum: String,
    val jabatan_sekum: String,
    val no_urut: Int,
    val status: Int,
    val umur: Int,
    val updated_at: String,
    val percentage: String? = "0",
    val hasVoted: Boolean,
    var isChecked: Boolean = false
)