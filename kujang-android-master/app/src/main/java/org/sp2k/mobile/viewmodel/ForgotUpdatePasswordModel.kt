package org.sp2k.mobile.viewmodel

import android.text.Selection
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.material.textfield.TextInputEditText
import org.sp2k.mobile.base.BaseViewModel
import org.sp2k.mobile.extensions.afterTextChanged
import org.sp2k.mobile.model.user.Staff
import org.sp2k.mobile.repository.AnalyticsRepository
import org.sp2k.mobile.repository.AuthenticationRepository
import org.sp2k.mobile.repository.RemoteRepository

class ForgotUpdatePasswordModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private var validPasswordFormat: Boolean = false
    private var validPasswordConfirmationFormat: Boolean = false
    private var onChangedDisabled: Boolean = false

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _staffData: MutableLiveData<Staff?> by lazy {
        MutableLiveData(null)
    }
    val staffData: MutableLiveData<Staff?>
        get() = _staffData

    private val _passwordValue = MutableLiveData<String>()
    private val passwordValue: MutableLiveData<String>
        get() = _passwordValue

    private val _passwordConfirmationValue = MutableLiveData<String>()
    private val passwordConfirmationValue: MutableLiveData<String>
        get() = _passwordConfirmationValue

    private val _hideErrorPasswordformat = MutableLiveData<Boolean>()
    val hideErrorPasswordformat: LiveData<Boolean>
        get() = _hideErrorPasswordformat

    private val _hideErrorPasswordConfirmationFormat = MutableLiveData<Boolean>()
    val hideErrorPasswordConfirmationFormat: LiveData<Boolean>
        get() = _hideErrorPasswordConfirmationFormat

    private val _isValid = MutableLiveData<Boolean>()
    val isValid: LiveData<Boolean>
        get() = _isValid

    private val _passwordIsUpdate = MutableLiveData<Boolean>()
    val passwordIsUpdate: LiveData<Boolean>
        get() = _passwordIsUpdate

    fun resetState() {
        setError(null)
    }

    fun attachInputFormat(
        passwordInputEditText: TextInputEditText,
        passwordConfirmationInputEditText: TextInputEditText
    ) {

        passwordInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _passwordValue.value = this
                    setText(passwordValue.value)
                    validPasswordFormat = length() >= 6
                    _hideErrorPasswordformat.value = validPasswordFormat
                    _isValid.value = validPasswordFormat && validPasswordConfirmationFormat
                    Selection.setSelection(passwordInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }

        passwordConfirmationInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _passwordConfirmationValue.value = this
                    setText(passwordConfirmationValue.value)
                    validPasswordConfirmationFormat =
                        passwordConfirmationValue.value == passwordValue.value
                    _hideErrorPasswordConfirmationFormat.value = validPasswordConfirmationFormat
                    _isValid.value = validPasswordFormat && validPasswordConfirmationFormat
                    Selection.setSelection(passwordConfirmationInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }
    }



}