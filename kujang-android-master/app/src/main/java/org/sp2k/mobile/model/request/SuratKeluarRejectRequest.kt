package org.sp2k.mobile.model.request

data class SuratKeluarRejectRequest(
    val id_surat: String = "",
    val alasan: String = "",
)