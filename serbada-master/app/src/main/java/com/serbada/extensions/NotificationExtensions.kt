package com.serbada.extensions

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.serbada.R

fun Context.sendNotification(
    channelId: String,
    pendingIntent: PendingIntent,
    title: String,
    content: String
) {
    val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(channelId, title, importance).apply {
            description = content
        }
        notificationManager.createNotificationChannel(channel)
    }
    val builder = NotificationCompat.Builder(this, channelId)
        .setSmallIcon(R.drawable.ic_notification)
        .setAutoCancel(true)
        .setStyle(NotificationCompat.BigTextStyle().bigText(content))
        .setContentTitle(title)
        .setContentText(content)
        .setContentIntent(pendingIntent)
        .setPriority(NotificationCompat.PRIORITY_HIGH)
    notificationManager.notify(0, builder.build())
}

fun Context.clearNotifications() {
    val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.cancelAll()
}