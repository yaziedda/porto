package com.serbada.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.serbada.model.Product

@Database(entities = [Product::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun localDao(): LocalDao
}