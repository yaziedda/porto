package com.serbada.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.serbada.model.Product

@Dao
interface LocalDao {
    @Query("SELECT * FROM product")
    fun getAll(): List<Product>

    @Query("SELECT * FROM product WHERE uid IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<Product>

    @Query("SELECT * FROM user WHERE first_name LIKE :first AND " +
            "last_name LIKE :last LIMIT 1")
    fun findByName(first: String, last: String): Product

    @Insert
    fun insertAll(vararg products: Product)

    @Delete
    fun delete(product: Product)
}