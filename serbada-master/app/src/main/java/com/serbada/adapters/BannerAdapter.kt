package com.serbada.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.serbada.R
import com.serbada.model.home.Banner

class BannerAdapter(
    private val models: List<Banner>,
    private val context: Context
) : PagerAdapter() {
    override fun getCount(): Int {
        return models.size
    }

    override fun isViewFromObject(
        view: View,
        `object`: Any
    ): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater = LayoutInflater.from(context).inflate(R.layout.item_home_banner,container,false)
        val imageView = layoutInflater.findViewById<AppCompatImageView>(R.id.fragment_home_banner_image)
//        val title: TextView
//        val desc: TextView
//        imageView = mView.findViewById(R.id.image)
//        title = mView.findViewById(R.id.title)
//        desc = mView.findViewById(R.id.txt_des)
//        imageView.setImageResource(models[position].image)
//        title.text = models[position].title
//        desc.text = models[position].desc
        Glide.with(context)
            .load(models[position].image)
            .into(imageView)

        if (position == models.lastIndex) {

        }

        container.addView(layoutInflater, 0)
        return layoutInflater
    }

    override fun destroyItem(
        container: ViewGroup,
        position: Int,
        `object`: Any
    ) {
        container.removeView(`object` as View)
    }

}