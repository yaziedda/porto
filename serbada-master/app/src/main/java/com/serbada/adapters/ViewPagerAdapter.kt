package com.serbada.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class ViewPagerAdapter(
    fm: FragmentManager,
    val screens: Array<Fragment>,
    val tabTitles: Array<String>
): FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return screens[position]
    }

    override fun getCount(): Int {
        return screens.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }
}