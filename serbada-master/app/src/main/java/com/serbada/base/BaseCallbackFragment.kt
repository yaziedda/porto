package com.serbada.base

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.util.Patterns
import android.view.inputmethod.InputMethodManager
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.afollestad.materialdialogs.MaterialDialog
import com.serbada.transition.TransitionType
import com.serbada.utils.FragmentCallback
import com.serbada.utils.FragmentHelper

abstract class BaseCallbackFragment<binding : ViewDataBinding, Action> : BaseFragment<binding>(),
    FragmentHelper<Action> {

    override val fragment: Fragment
        get() = this
    override var animation: TransitionType = TransitionType.StartSheetNavigationTransition
    override var fragmentCallback: FragmentCallback<Action>? = null
    override var isAlreadyAdded: Boolean = false
    override val listenBackPressed: Boolean = true
    lateinit var materialDialog: MaterialDialog

    override fun onBackPressed() = closeFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        materialDialog = MaterialDialog(requireContext())
            .message(null, "Loading...")
            .cancelable(false);
    }

    fun showProgressDialog() {
        materialDialog.show()
    }

    fun showProgressDialog(s: String) {
        materialDialog.title(null, s)
        materialDialog.show()
    }

    fun dissmissProgressDialog() {
        materialDialog.dismiss()
    }

    fun String.isValidEmail() =
        !TextUtils.isEmpty(this) && Patterns.EMAIL_ADDRESS.matcher(this).matches()

    fun showKeyboard() {
        val inputMethodManager: InputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    open fun fromHtml(html: String?): Spanned? {
        return if (html == null) {
            SpannableString("")
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }
}