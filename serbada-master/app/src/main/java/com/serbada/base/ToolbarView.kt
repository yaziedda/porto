package com.serbada.base

interface ToolbarView {
    fun showToolbar(title: CharSequence)
}