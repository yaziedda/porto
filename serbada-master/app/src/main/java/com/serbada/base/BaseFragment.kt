package com.serbada.base

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.text.SpannableStringBuilder
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import com.serbada.R
import com.serbada.extensions.bold
import com.serbada.extensions.color
import com.serbada.extensions.hideKeyboard
import com.serbada.screens.feature.WebViewActivity
import com.serbada.screens.showimage.ShowImageActivity
import com.serbada.utils.BlockingFragment
import com.serbada.utils.DialogUtils
import com.serbada.utils.FragmentCallback
import com.serbada.utils.FragmentSupportManager
import okhttp3.internal.http2.ConnectionShutdownException
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

abstract class BaseFragment<binding : ViewDataBinding> : CoreFragment<binding>() {

    private var _navigationConfig: NavigationConfig? = null
    protected val navigationConfig get() = _navigationConfig!!
    private val blockingFragment by lazy { BlockingFragment() }
    private val blockingSupportManager by lazy {
        FragmentSupportManager(
            requireActivity(),
            blockingFragment
        )
    }

    protected open val paid: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(
                getString(R.string.app_name).color(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.colorPrimary
                    )
                ).bold()
            )
    }

    protected val onProgress: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(
                getString(R.string.app_name).color(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.done
                    )
                ).bold()
            )
    }

    protected val waitingPayment: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(
                getString(R.string.app_name).replace("\n", " ").color(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.proces
                    )
                ).bold()
            )
    }

    protected val waitingConfirmation: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(
                getString(R.string.app_name).color(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.done
                    )
                ).bold()
            )
    }

    protected val notPaid: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(
                getString(R.string.app_name).replace("\n", " ").color(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.wait
                    )
                ).bold()
            )
    }

    protected val rejected: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(
                getString(R.string.app_name).replace("\n", " ").color(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.wait
                    )
                ).bold()
            )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        _navigationConfig = context as NavigationConfig
    }

    override fun onDetach() {
        super.onDetach()
        _navigationConfig = null
    }

    protected open fun showBlockingPage(action: (() -> Unit)? = null) {
        requireActivity().hideKeyboard()
        blockingSupportManager.show {
            object : FragmentCallback<Unit> {
                override fun action() {
                    action?.invoke()
                }
            }
        }
    }

    fun showError(exception: Exception?, action: (() -> Unit)? = null) {
        exception?.let {
            it.message?.let { message ->
                showError(
                    it,
                    message,
                    ContextCompat.getDrawable(requireContext(), R.drawable.logo),
                    action
                )
            }
        }
    }

    fun showError(exception: Exception?, message: CharSequence, action: (() -> Unit)? = null) {
        showError(
            exception,
            message,
            ContextCompat.getDrawable(requireContext(), R.drawable.logo),
            action
        )
    }

    fun showError(
        exception: Exception?,
        message: CharSequence,
        icon: Drawable?,
        action: (() -> Unit)? = null
    ) {
        exception?.let {
            when (it) {
                is SocketTimeoutException, is UnknownHostException, is ConnectionShutdownException, is IOException, is IllegalStateException -> showBlockingPage(
                    action
                )
                else -> DialogUtils.showBasicAlertDialog(
                    requireActivity(),
                    icon,
                    message
                )
            }
        }
    }

    fun showDialog(
        message: CharSequence,
        drawable: Drawable? = null,
        title: CharSequence? = "",
        note: CharSequence? = "",
        action: (() -> Unit)? = null
    ) =
        DialogUtils.showBasicAlertDialog(
            requireActivity(),
            drawable,
            message,
            title,
            note,
            action
        )

    fun isBetween(x: Int, lower: Int, upper: Int): Boolean {
        return x in lower..upper
    }


    fun goToShowImageActivity(url: String) {
        startActivity(
            Intent(requireContext(), ShowImageActivity::class.java).apply {
                putExtra(ShowImageActivity.IMAGE_URL, url)
            }
        )
    }

    fun goToWebViewActivity(url: String, title: String = "") {
        startActivity(
            Intent(requireContext(), WebViewActivity::class.java).apply {
                putExtra(WebViewActivity.KEY_URL, url)
                putExtra(WebViewActivity.KEY_TITLE, title)
            }
        )
    }
}