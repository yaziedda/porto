package com.serbada.base

interface NavigationConfig {
    fun setAppbarTitle(title: String) {}
}