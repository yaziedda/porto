package com.serbada.exception

class LoginException :
    Exception("Terjadi kesalahan saat login. Mungkin sedang terjadi kesalahan pada sistem kami.")