package com.serbada.exception

class FeedbackException(message: String = "Failed to submit") : Exception(message) {
    companion object {
        const val FEEDBACK_NOT_COMPLETED = "FEEDBACK_NOT_COMPLETED"
        const val FEEDBACK_FAILED = "FEEDBACK_FAILED"
    }
}