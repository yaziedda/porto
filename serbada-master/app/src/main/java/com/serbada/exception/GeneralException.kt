package com.serbada.exception

class GeneralException(message: String = "Error") : Exception(message)