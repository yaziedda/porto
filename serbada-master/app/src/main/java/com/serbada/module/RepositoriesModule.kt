package com.serbada.module

import NanoNanoUtils
import android.app.Application
import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GetTokenResult
import com.google.gson.GsonBuilder
import com.moe.pushlibrary.MoEHelper
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import com.serbada.BuildConfig
import com.serbada.model.Token
import com.serbada.repository.*
import com.serbada.utils.PreferenceManager
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.io.File
import java.net.HttpURLConnection
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class RepositoriesModule {

    @Singleton
    @Provides
    fun provideAuthenticationRepository(
        mGoogleSignInClient: GoogleSignInClient,
        auth: FirebaseAuth,
        token: Token
    ): AuthenticationRepository = AuthenticationRepository(mGoogleSignInClient, auth, token)

    @Singleton
    @Provides
    fun provideAnalyticsRepository(
        firebaseAnalytics: FirebaseAnalytics,
        moEHelper: MoEHelper
    ): AnalyticsRepository = AnalyticsRepository(moEHelper, firebaseAnalytics)

    @Singleton
    @Provides
    fun provideResourceRepository(
        application: Application
    ): ResourceRepository = ResourceRepository(application.baseContext)


    @Provides
    fun provideBaseUrl() = BuildConfig.BASE_URL

    @Provides
    @Singleton
    fun cache(cacheFile: File): Cache = Cache(cacheFile, 10 * 1000 * 1000) //10MB Cahe

    @Provides
    @Singleton
    fun cacheFile(@ApplicationContext context: Context): File =
        File(context.cacheDir, "okhttp_cache")

    @Provides
    @Singleton
    fun provideOkHttpClient(
        @ApplicationContext context: Context,
        cache: Cache?,
        token: Token
    ): OkHttpClient {
        return OkHttpClient.Builder().apply {
            addInterceptor(AuthorizationInterceptor(token, context))
            if (BuildConfig.DEBUG) addInterceptor(ChuckInterceptor(context))
            cache(cache)
            connectTimeout(180, TimeUnit.SECONDS)
            writeTimeout(180, TimeUnit.SECONDS)
            readTimeout(180, TimeUnit.SECONDS)
        }.build()
    }

    private class AuthorizationInterceptor(val token: Token, val context: Context) : Interceptor {
        fun getAuthorizationToken(refresh: Boolean): String? = try {
            val firebaseAuth = FirebaseAuth.getInstance().currentUser
            val task: Task<GetTokenResult>? = firebaseAuth?.getIdToken(refresh)
            val tokenResult: GetTokenResult = Tasks.await(task as Task<GetTokenResult>)
            token.token = tokenResult.token
            tokenResult.token
        } catch (exception: Exception) {
            Timber.tag(this::class.java.name).e(exception)
            ""
        }

        override fun intercept(chain: Interceptor.Chain): Response {
            var response: Response
            val newToken = when {
                token.token?.isNotEmpty() == true -> token.token
                else -> getAuthorizationToken(refresh = false)
            }
            response = createRequest(chain, newToken ?: "")
            try {
                if (response.code == HttpURLConnection.HTTP_UNAUTHORIZED || response.code == HttpURLConnection.HTTP_FORBIDDEN) {
                    response = createRequest(chain, getAuthorizationToken(refresh = true) ?: "")
                }
            } catch (exception: Exception) {
                Timber.tag(this::class.java.name).e(exception)
                response.close()
            }
            return response
        }

        private fun createRequest(chain: Interceptor.Chain, newToken: String): Response {
            val newRequest: Request = chain.request().newBuilder()
                .header("Authorization", "Bearer da39a3ee5e6b4b0d3255bfef95601890afd80709")
                .build()
            return chain.proceed(newRequest)
        }
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, BASE_URL: String): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .build()

    @Provides
    @Singleton
    fun provideRemoteRepositoryService(retrofit: Retrofit): RemoteRepositoryService =
        retrofit.create(RemoteRepositoryService::class.java)

    @Provides
    @Singleton
    fun provideRemoteRepositoryImpl(remoteRepositoryImpl: RemoteRepositoryDaoImpl): RemoteRepositoryDao =
        remoteRepositoryImpl

    @Singleton
    @Provides
    fun providePreferenceManager(
        @ApplicationContext context: Context
    ): PreferenceManager = PreferenceManager(context)



}