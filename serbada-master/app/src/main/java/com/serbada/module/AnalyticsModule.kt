package com.serbada.module

import android.app.Application
import com.google.firebase.analytics.FirebaseAnalytics
import com.moe.pushlibrary.MoEHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AnalyticsModule {
    @Provides
    @Singleton
    fun providesGoogleAnalytics(
        application: Application
    ): FirebaseAnalytics {
        return FirebaseAnalytics.getInstance(application)
    }

    @Provides
    @Singleton
    fun providesMoengageAnalytics(
        application: Application
    ): MoEHelper {
        return MoEHelper.getInstance(application)
    }
}