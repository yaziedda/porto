package com.serbada.model.productdetail


import com.google.gson.annotations.SerializedName

data class ProductDetailGalleryX(
    @SerializedName("caption")
    val caption: String?,
    @SerializedName("slug")
    val slug: String?,
    @SerializedName("url_250x320")
    val url250x320: String?,
    @SerializedName("url_375x375")
    val url375x375: String?,
    @SerializedName("url_68x68")
    val url68x68: String?,
    @SerializedName("url_original")
    val urlOriginal: String?,
    @SerializedName("url_originals")
    val urlOriginals: String?
)