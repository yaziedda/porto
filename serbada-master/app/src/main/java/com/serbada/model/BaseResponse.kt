package com.serbada.model

import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    val state: Boolean = true,
    val message: String = "",
    @SerializedName("queryString")
    val queryString: QueryString?,
    @field:SerializedName("response")
    val data: T? = null,
    @SerializedName("version")
    val version: Int?
)