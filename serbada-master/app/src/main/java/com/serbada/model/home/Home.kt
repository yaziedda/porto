package com.serbada.model.home


import com.google.gson.annotations.SerializedName

data class Home(
    @SerializedName("banner")
    val banner: List<Banner>
)