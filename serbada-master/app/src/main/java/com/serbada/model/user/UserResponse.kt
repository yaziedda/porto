package com.serbada.model.user

import com.serbada.model.user.User

data class UserResponse(
    val `data`: User,
    val message: String,
    val state: Boolean,
    val token: String
)