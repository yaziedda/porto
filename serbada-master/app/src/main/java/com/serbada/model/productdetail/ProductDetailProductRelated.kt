package com.serbada.model.productdetail


import com.google.gson.annotations.SerializedName

data class ProductDetailProductRelated(
    @SerializedName("count")
    val count: Int?,
    @SerializedName("product_id")
    val productId: String?,
    @SerializedName("results")
    val productDetailResults: List<ProductDetailResult>?
)