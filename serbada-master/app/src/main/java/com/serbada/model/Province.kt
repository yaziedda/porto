package com.serbada.model


import com.google.gson.annotations.SerializedName

data class Province(
    @SerializedName("province")
    val province: String = "",
    @SerializedName("province_id")
    val provinceId: String = ""
) {
    override fun toString(): String {
        return province
    }
}