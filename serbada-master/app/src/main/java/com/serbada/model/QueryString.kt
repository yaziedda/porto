package com.serbada.model


import com.google.gson.annotations.SerializedName

data class QueryString(
    @SerializedName("brand")
    val brand: String?,
    @SerializedName("category")
    val category: String?,
    @SerializedName("exclude_id")
    val excludeId: String?,
    @SerializedName("limit")
    val limit: String?,
    @SerializedName("page")
    val page: String?,
    @SerializedName("pricesort")
    val pricesort: String?,
    @SerializedName("q")
    val q: String?
)