package com.serbada.model


import com.google.gson.annotations.SerializedName

data class BannerResponse(
    @SerializedName("bottom")
    val bottom: BannerHeader? = null,
    @SerializedName("middle")
    val middle: BannerHeader? = null,
    @SerializedName("middletop")
    val middletop: BannerHeader? = null,
    @SerializedName("top")
    val top: BannerHeader? = null
)