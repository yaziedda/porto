package com.serbada.model.home


import com.google.gson.annotations.SerializedName

data class Banner(
    @SerializedName("created_at")
    val createdAt: String? = "",
    @SerializedName("id")
    val id: Int? = 0,
    @SerializedName("image")
    val image: String? = "",
    @SerializedName("squence")
    val squence: String? = "",
    @SerializedName("status")
    val status: Int? = 0,
    @SerializedName("title")
    val title: String? = "",
    @SerializedName("updated_at")
    val updatedAt: String? = ""
)