package com.serbada.model


import com.google.gson.annotations.SerializedName

data class ProductTag(
    @SerializedName("name")
    val name: String?,
    @SerializedName("slug")
    val slug: String?
)