package com.serbada.model

import com.google.gson.annotations.SerializedName

data class Home(
    @field:SerializedName("banner")
    val banner: List<Banner> = arrayListOf(),
    @field:SerializedName("product")
    val product: List<Product> = arrayListOf()
)