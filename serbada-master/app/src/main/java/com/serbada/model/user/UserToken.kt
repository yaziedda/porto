package com.serbada.model.user

data class UserToken(
    val user: User,
    val token: String
)