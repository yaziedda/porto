package com.serbada.model


import com.google.gson.annotations.SerializedName

data class BannerHeader(
    @SerializedName("count")
    val count: Int? = 0,
    @SerializedName("limit")
    val limit: String? = "",
    @SerializedName("page")
    val page: Int? = 0,
    @SerializedName("promo_id")
    val promoId: String? = "",
    @SerializedName("results")
    val results: List<Banner>? = arrayListOf()
)