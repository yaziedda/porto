package com.serbada.model

import com.google.gson.annotations.SerializedName

data class ProductResponse(
    @SerializedName("count")
    val count: Int? = 0,
    @SerializedName("limit")
    val limit: Int? = 0,
    @SerializedName("page")
    val page: String? = "",
    @SerializedName("product_id")
    val productId: String? = "",
    @SerializedName("results")
    val results: List<Product>? = arrayListOf()
)