package com.serbada.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Product(
    @PrimaryKey val uid: Int,
    @SerializedName("author")
    val author: String? = "",
    @SerializedName("author_id")
    val authorId: Int? = 0,
    @SerializedName("category_path")
    val categoryPath: List<String>? = arrayListOf(),
    @SerializedName("category_path_id")
    val categoryPathId: List<String>? = arrayListOf(),
    @SerializedName("create_date")
    val createDate: String? = "",
    @SerializedName("description")
    val description: String? = "",
    @SerializedName("detailUrl")
    val detailUrl: String? = "",
    val image: String? = "",
    @SerializedName("diskon")
    val diskon: Int? = 0,
    @SerializedName("gallery")
    val productGallery: List<ProductGallery>? = arrayListOf(),
    @SerializedName("in_stock")
    val inStock: Int? = 0,
    @SerializedName("is_recomended")
    val isRecomended: Int? = 0,
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("options")
    val options: List<Any>?,
    @SerializedName("price")
    val price: Int = 0,
    @SerializedName("price_normal")
    val priceNormal: Int? = 0,
    @SerializedName("price_normal_rp")
    val priceNormalRp: String? = "",
    @SerializedName("price_rp")
    val priceRp: String? = "",
    @SerializedName("product_id")
    val productId: Int? = 0,
    @SerializedName("publish_date")
    val publishDate: String? = "",
    @SerializedName("publish_timestamp")
    val publishTimestamp: String? = "",
    @SerializedName("rating")
    val rating: Int? = 0,
    @SerializedName("slug")
    val slug: String? = "",
    @SerializedName("summary")
    val summary: String? = "",
    @SerializedName("tags")
    val productTags: List<ProductTag>? = arrayListOf()
)