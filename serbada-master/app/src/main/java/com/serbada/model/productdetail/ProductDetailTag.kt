package com.serbada.model.productdetail


import com.google.gson.annotations.SerializedName

data class ProductDetailTag(
    @SerializedName("name")
    val name: String?,
    @SerializedName("slug")
    val slug: String?
)