package com.serbada.model.user

import com.google.gson.annotations.SerializedName

data class User(

    @field:SerializedName("status_message")
    val statusMessage: Any,

    @field:SerializedName("rt")
    val rt: String,

    @field:SerializedName("education")
    val education: Any,

    @field:SerializedName("kabupaten_ket")
    val kabupatenKet: Any,

    @field:SerializedName("id_pekerjaan")
    val idPekerjaan: Any,

    @field:SerializedName("no_hp")
    val noHp: Any,

    @field:SerializedName("rw")
    val rw: String,

    @field:SerializedName("golongan_darah")
    val golonganDarah: Any,

    @field:SerializedName("nama_lengkap")
    val namaLengkap: String,

    @field:SerializedName("nik")
    val nik: String,

    @field:SerializedName("agama")
    val agama: Any,

    @field:SerializedName("created_at")
    val createdAt: String,

    @field:SerializedName("id_provinsi")
    val idProvinsi: Any,

    @field:SerializedName("local")
    val local: Int,

    @field:SerializedName("id_desa")
    val idDesa: String,

    @field:SerializedName("id_kecamatan")
    val idKecamatan: String,

    @field:SerializedName("provinsi_ket")
    val provinsiKet: Any,

    @field:SerializedName("tempat_lahir")
    val tempatLahir: Any,

    @field:SerializedName("updated_at")
    val updatedAt: String,

    @field:SerializedName("pendapatan")
    val pendapatan: Any,

    @field:SerializedName("forgot_passwrod")
    val forgotPasswrod: Int,

    @field:SerializedName("kecamatan_ket")
    val kecamatanKet: String,

    @field:SerializedName("id")
    val id: Int,

    @field:SerializedName("id_education")
    val idEducation: Any,

    @field:SerializedName("jenis_kelamin")
    val jenisKelamin: Any,

    @field:SerializedName("tanggal_lahir")
    val tanggalLahir: Any,

    @field:SerializedName("id_kabupaten")
    val idKabupaten: Any,

    @field:SerializedName("email")
    val email: Any,

    @field:SerializedName("lat")
    val lat: Int,

    @field:SerializedName("instansi")
    val instansi: Any,

    @field:SerializedName("desa_ket")
    val desaKet: String,

    @field:SerializedName("email_verified")
    val emailVerified: Int,

    @field:SerializedName("lng")
    val lng: Int,

    @field:SerializedName("ktp")
    val ktp: Any,

    @field:SerializedName("ktp_selfie")
    val ktpSelfie: Any,

    @field:SerializedName("verified")
    val verified: Int,

    @field:SerializedName("alamat")
    val alamat: Any,

    @field:SerializedName("token")
    val token: String,

    @field:SerializedName("status_kawin")
    val statusKawin: Any,

    @field:SerializedName("pekerjaan")
    val pekerjaan: Any,

    @field:SerializedName("email_verified_token")
    val emailVerifiedToken: Any,

    @field:SerializedName("token_fcm")
    val tokenFcm: Any,

    @field:SerializedName("forgot_password_token")
    val forgotPasswordToken: Any,

    @field:SerializedName("last_active")
    val lastActive: String,

    @field:SerializedName("username")
    val username: String,

    @field:SerializedName("status")
    val status: Int
)