package com.serbada.model.productdetail


import com.google.gson.annotations.SerializedName

data class ProductDetailMarketplace(
    @SerializedName("link")
    val link: String?,
    @SerializedName("name")
    val name: String?
)