package com.serbada.model


import com.google.gson.annotations.SerializedName

data class Banner(
    @SerializedName("author")
    val author: String? = "",
    @SerializedName("author_id")
    val authorId: Int? = 0,
    @SerializedName("category_path")
    val categoryPath: List<String>? = arrayListOf(),
    @SerializedName("category_path_id")
    val categoryPathId: List<String>? = arrayListOf(),
    @SerializedName("create_date")
    val createDate: String? = "",
    @SerializedName("image")
    val image: String? = "",
    @SerializedName("image_desktop")
    val imageDesktop: String? = "",
    @SerializedName("image_mobile")
    val imageMobile: String? = "",
    @SerializedName("link")
    val link: String? = "",
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("position")
    val position: String? = "",
    @SerializedName("position_order")
    val positionOrder: Int? = 0,
    @SerializedName("promo_id")
    val promoId: Int? = 0,
    @SerializedName("publish_date")
    val publishDate: String? = "",
    @SerializedName("publish_timestamp")
    val publishTimestamp: String? = "",
    @SerializedName("slug")
    val slug: String? = ""
)