package com.serbada.model.productdetail


import com.google.gson.annotations.SerializedName

data class ProductDetail(
    @SerializedName("author")
    val author: String?,
    @SerializedName("author_id")
    val authorId: Int?,
    @SerializedName("brand")
    val brand: String?,
    @SerializedName("brand_id")
    val brandId: Int?,
    @SerializedName("category_path")
    val categoryPath: List<String>?,
    @SerializedName("category_path_id")
    val categoryPathId: List<String>?,
    @SerializedName("create_date")
    val createDate: String?,
    @SerializedName("description")
    val description: List<String>?,
    @SerializedName("detailUrl")
    val detailUrl: String?,
    @SerializedName("diskon")
    val diskon: Int?,
    @SerializedName("gallery")
    val productDetailGallery: List<ProductDetailGallery>?,
    @SerializedName("in_stock")
    val inStock: Int?,
    @SerializedName("marketplace")
    val productDetailMarketplace: List<ProductDetailMarketplace>?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("options")
    val options: List<Any>?,
    @SerializedName("price")
    val price: Int?,
    @SerializedName("price_normal")
    val priceNormal: Int?,
    @SerializedName("price_normal_rp")
    val priceNormalRp: String?,
    @SerializedName("price_rp")
    val priceRp: String?,
    @SerializedName("product_id")
    val productId: Int?,
    @SerializedName("product_related")
    val productDetailProductRelated: ProductDetailProductRelated?,
    @SerializedName("publish_date")
    val publishDate: String?,
    @SerializedName("publish_timestamp")
    val publishTimestamp: String?,
    @SerializedName("rating")
    val rating: Int?,
    @SerializedName("slug")
    val slug: String?,
    @SerializedName("summary")
    val summary: String?,
    @SerializedName("tags")
    val productDetailTags: List<ProductDetailTag>?,
    @SerializedName("videos")
    val videos: List<Any>?
)