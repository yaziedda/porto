package com.serbada.model

import com.google.gson.annotations.SerializedName
import com.serbada.model.productdetail.ProductDetail

data class ProductDetailResponse(
    @SerializedName("count")
    val count: Int? = 0,
    @SerializedName("limit")
    val limit: Int? = 0,
    @SerializedName("page")
    val page: String? = "",
    @SerializedName("product_id")
    val productId: String? = "",
    @SerializedName("results")
    val results: List<ProductDetail>? = arrayListOf()
)