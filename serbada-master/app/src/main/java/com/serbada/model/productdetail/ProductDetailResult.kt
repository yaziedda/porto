package com.serbada.model.productdetail


import com.google.gson.annotations.SerializedName

data class ProductDetailResult(
    @SerializedName("create_date")
    val createDate: String?,
    @SerializedName("gallery")
    val productDetailGallery: List<ProductDetailGalleryX>?,
    @SerializedName("in_stock")
    val inStock: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("price")
    val price: Int?,
    @SerializedName("price_normal")
    val priceNormal: Int?,
    @SerializedName("price_normal_rp")
    val priceNormalRp: String?,
    @SerializedName("price_rp")
    val priceRp: String?,
    @SerializedName("product_id")
    val productId: Int?,
    @SerializedName("publish_date")
    val publishDate: String?,
    @SerializedName("rating")
    val rating: Int?,
    @SerializedName("slug")
    val slug: String?
)