package com.serbada.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun <T> debounce(
    delayMillis: Long = 300L,
    scope: CoroutineScope,
    action: (T) -> Unit
): (T) -> Unit {
    var debounceJob: Job? = null
    return { param: T ->
        if (debounceJob == null) {
            debounceJob = scope.launch {
                action(param)
                delay(delayMillis)
                debounceJob = null
            }
        }
    }
}

fun delay(
    delayMillis: Long = 300L,
    scope: CoroutineScope,
    action: () -> Unit
): () -> Unit {
    var delayJob: Job? = null
    return {
        if (delayJob == null) {
            delayJob = scope.launch {
                delay(delayMillis)
                action()
                delayJob = null
            }
        }
    }
}