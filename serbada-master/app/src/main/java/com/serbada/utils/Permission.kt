package com.serbada.utils

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class Permission {
    companion object {
        private val PERMISSION_REQ_ID = 22
        private val REQUESTED_PERMISSIONS = arrayOf(
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        private fun checkSelfPermission(
            activity: AppCompatActivity,
            permission: String,
            requestCode: Int
        ): Boolean {
            if (ContextCompat.checkSelfPermission(activity, permission) !=
                PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(activity, REQUESTED_PERMISSIONS, requestCode)
                return false
            }
            return true
        }

        fun largeClassActivityPermissions(activity: AppCompatActivity): Boolean {
            return checkSelfPermission(activity, REQUESTED_PERMISSIONS[0], PERMISSION_REQ_ID) &&
                    checkSelfPermission(activity, REQUESTED_PERMISSIONS[1], PERMISSION_REQ_ID) &&
                    checkSelfPermission(activity, REQUESTED_PERMISSIONS[2], PERMISSION_REQ_ID)
        }

    }
}