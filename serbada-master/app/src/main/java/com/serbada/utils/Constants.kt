package com.serbada.utils

import com.serbada.BuildConfig


object Constants {
    const val DATE_FORMATTED_LIVE_AT = "dd MMMM yyyy"
    const val URL_PLAY_STORE = "https://play.google.com/store/apps/details?id="+BuildConfig.APPLICATION_ID
    const val YA = "YA"
    const val TIDAK = "TIDAK"
    const val SPLASH_SCREEN_DELAY_DURATION = 2000L
    const val WHATS_APP_URL = "https://api.whatsapp.com/send?phone=6289662549895&text="
    const val TEXT_PLAIN = "text/plain"
    const val COM_WHATSAPP = "com.whatsapp"
    const val DATABASE_NAME = "serbada.db"

}