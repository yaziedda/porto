package com.serbada.utils

fun String.cleanPhone(prefix: String): String {
    val regexNumeric = Regex("[^0-9]")
    val result = regexNumeric.replace(this, "")
    return when {
        result.take(1) == "0" -> {
            result.removePrefix("0")
        }
        result.take(2) == prefix -> {
            result.removePrefix(prefix)
        }
        else -> {
            result
        }
    }
}