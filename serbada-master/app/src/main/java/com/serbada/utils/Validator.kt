package com.serbada.utils

class Validator {
    companion object {
        fun isValidEmail(email: String): Boolean {
            val regex: Regex = Regex("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$")
            return email.matches(regex)
        }

        fun isValidPassword(password: String): Boolean {
            return password.length >= 8
        }

        fun isValidName(name: String): Boolean {
            return name.length >= 3
        }
    }
}