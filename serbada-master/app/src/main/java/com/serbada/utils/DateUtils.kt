package com.serbada.utils

import java.text.SimpleDateFormat
import java.util.*

class DateUtils {
    companion object {
        fun getDateNow(): String{
            val dateNow = Calendar.getInstance().time
            val formatter = SimpleDateFormat(Constants.DATE_FORMATTED_LIVE_AT)
            return formatter.format(dateNow)
        }
    }
}