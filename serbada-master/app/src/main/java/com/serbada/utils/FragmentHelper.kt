package com.serbada.utils

import androidx.fragment.app.Fragment
import com.serbada.transition.TransitionType

interface FragmentHelper<Result> {
    val fragment: Fragment
    var animation: TransitionType
    var fragmentCallback: FragmentCallback<Result>?
    var isAlreadyAdded: Boolean

    fun closeFragment() {
        fragment.parentFragmentManager.beginTransaction().setCustomAnimations(
            animation.enterTransition,
            animation.exitTransition
        ).remove(fragment).commit()
        isAlreadyAdded = false
    }
}