package com.serbada.utils

interface FragmentCallback<T> {
    fun result(result: T) {}
    fun action() {}
}