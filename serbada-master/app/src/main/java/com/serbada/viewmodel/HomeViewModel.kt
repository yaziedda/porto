package com.serbada.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.serbada.base.BaseViewModel
import com.serbada.model.Banner
import com.serbada.model.Home
import com.serbada.model.Product
import com.serbada.repository.AnalyticsRepository
import com.serbada.repository.AuthenticationRepository
import com.serbada.repository.RemoteRepository
import kotlinx.coroutines.launch

class HomeViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _isRunning: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val isRunning: MutableLiveData<Boolean>
        get() = _isRunning

    private val _isNeedUpdate = MutableLiveData<Boolean>()
    val isNeedUpdate: LiveData<Boolean>
        get() = _isNeedUpdate

    private val _home = MutableLiveData<Home?>()
    val home: LiveData<Home?>
        get() = _home

    private val _bannerList = MutableLiveData<List<Banner>>()
    val bannerList: LiveData<List<Banner>>
        get() = _bannerList

    private val _productList = MutableLiveData<List<Product>?>()
    val productList: LiveData<List<Product>?>
        get() = _productList

    private val _recommendationList = MutableLiveData<List<Product>?>()
    val recommendationList: LiveData<List<Product>?>
        get() = _recommendationList


    fun resetState() {
        _bannerList.value = arrayListOf()
        _productList.value = arrayListOf()
        _home.value = null
        setError(null)
    }

    fun fetchBanner() = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.getBanner()
            val listBanner: MutableList<Banner> = arrayListOf()
            listBanner.addAll(response.data?.top?.results ?: arrayListOf())
            _bannerList.value = listBanner
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }

    fun fetchProduct(
        page: Int,
        isNext: Boolean = false,
        search: String
    ) = viewModelScope.launch {
        _isRunning.value = true
        if (!isNext) {
            _productList.value = null
            _loading.value = true
        }
        try {
            val response = remoteRepository.getProduct(page, search)
            val listProduct: MutableList<Product> = arrayListOf()
            listProduct.addAll(response.data?.results ?: arrayListOf())
            _productList.value = listProduct
            _loading.value = false
            _isRunning.value = false
        } catch (exception: Exception) {
            _loading.value = false
            _isRunning.value = false
            showError(exception)
        }
    }

    fun fetchRecommendation(
        page: Int,
        isNext: Boolean = false
    ) = viewModelScope.launch {
        _isRunning.value = true
        if (!isNext) {
            _recommendationList.value = null
            _loading.value = true
        }
        try {
            val response = remoteRepository.getRecommendation(page)
            val listProduct: MutableList<Product> = arrayListOf()
            listProduct.addAll(response.data?.results ?: arrayListOf())
            _recommendationList.value = listProduct
            _loading.value = false
            _isRunning.value = false
        } catch (exception: Exception) {
            _loading.value = false
            _isRunning.value = false
            showError(exception)
        }
    }

    fun getIsRunning() = _isRunning.value
    fun isAnonymous() = authenticationRepository.isAnonymousUser()

}