package com.serbada.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.serbada.base.BaseViewModel
import com.serbada.model.Cart
import com.serbada.model.City
import com.serbada.model.Product
import com.serbada.model.Province
import com.serbada.model.productdetail.ProductDetail
import com.serbada.repository.AnalyticsRepository
import com.serbada.repository.AuthenticationRepository
import com.serbada.repository.RemoteRepository
import kotlinx.coroutines.launch

class ProductViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _loadingCart: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loadingCart: MutableLiveData<Boolean>
        get() = _loadingCart


    private val _product = MutableLiveData<Product?>()
    val product: LiveData<Product?>
        get() = _product

    private val _productList = MutableLiveData<List<Product>>()
    val productList: LiveData<List<Product>>
        get() = _productList

    private val _isSubmit = MutableLiveData<Boolean>()
    val isSubmit: LiveData<Boolean>
        get() = _isSubmit

    private val _cartList = MutableLiveData<List<Cart>>()
    val cartList: LiveData<List<Cart>>
        get() = _cartList

    private val _total = MutableLiveData<Double?>()
    val total: LiveData<Double?>
        get() = _total

    private val _weight = MutableLiveData<Int?>()
    val weight: LiveData<Int?>
        get() = _weight

    private val _provinces = MutableLiveData<List<Province>?>()
    val provinces: LiveData<List<Province>?>
        get() = _provinces


    private val _cites = MutableLiveData<List<City>?>()
    val cites: LiveData<List<City>?>
        get() = _cites

    private val _productDetails = MutableLiveData<List<ProductDetail>?>()
    val productDetails: LiveData<List<ProductDetail>?>
        get() = _productDetails


    var ongkir: Double = 0.0
    fun resetState() {
        setError(null)
        _product.value = null
        _isSubmit.value = false
    }

    fun isAnonymous(): Boolean = authenticationRepository.isAnonymousUser()

    fun fetchProducts() = viewModelScope.launch {
        _loading.value = true
        try {
//            val response = remoteRepository.getProducts()
//            _loading.value = false
//            if (response.state) {
//                val listProduct: MutableList<Product> = arrayListOf()
//                listProduct.addAll(response.data ?: arrayListOf())
//                _productList.value = listProduct
//            } else {
//                _product.value = null
//                showError(GeneralException(response.message ?: ""))
//            }
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }


    fun fetchProduct(id: String) = viewModelScope.launch {
        _loading.value = true
        try {
            val response = remoteRepository.getProductDetail(id)
            _loading.value = false
            val listProduct : MutableList<ProductDetail> = arrayListOf()
            listProduct.addAll(response.data ?: arrayListOf())
            _productDetails.value = listProduct
        } catch (exception: Exception) {
            _loading.value = false
            showError(exception)
        }
    }
}