package com.serbada.viewmodel

import android.content.Context
import android.content.Intent
import android.text.Selection
import androidx.databinding.ObservableField
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.common.api.ApiException
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.serbada.R
import com.serbada.base.BaseViewModel
import com.serbada.exception.GeneralException
import com.serbada.exception.LoginException
import com.serbada.extensions.afterTextChanged
import com.serbada.model.user.User
import com.serbada.repository.AnalyticsRepository
import com.serbada.repository.AuthenticationRepository
import com.serbada.repository.RemoteRepository
import com.serbada.repository.ResourceRepository
import com.serbada.utils.PreferenceManager
import com.serbada.utils.Validator
import kotlinx.coroutines.launch
import kotlin.collections.HashMap

class AuthViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val resourceRepository: ResourceRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository,
    private var preferenceManager: PreferenceManager
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    companion object {
        const val LOGIN_PAGE = 0
        const val REGISTER_PAGE = 1
        const val RC_GOOGLE_SIGN_IN = 2
    }


    private var validNIKFormat: Boolean = false
    private var validPasswordFormat: Boolean = false
    private var onChangedDisabled: Boolean = false

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _userData: MutableLiveData<User?> by lazy {
        MutableLiveData(null)
    }
    val userData: MutableLiveData<User?>
        get() = _userData

    private val _nikValue = MutableLiveData<String>()
    val nikValue: MutableLiveData<String>
        get() = _nikValue
    private val _hideErrorNIKformat = MutableLiveData<Boolean>()
    val hideErrorNIKformat: LiveData<Boolean>
        get() = _hideErrorNIKformat
    private val _passwordValue = MutableLiveData<String>()
    val passwordValue: MutableLiveData<String>
        get() = _passwordValue
    private val _hideErrorPasswordformat = MutableLiveData<Boolean>()
    val hideErrorPasswordformat: LiveData<Boolean>
        get() = _hideErrorPasswordformat
    private val _isValid = MutableLiveData<Boolean>()
    val isValid: LiveData<Boolean>
        get() = _isValid

    private val _isLoginFormValid: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    private val _isRegisterFormValid: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    private val _email: ObservableField<String> by lazy {
        ObservableField<String>("")
    }

    private val _phone: ObservableField<String> by lazy {
        ObservableField<String>("")
    }

    private val _password: ObservableField<String> by lazy {
        ObservableField<String>("")
    }

    private val _nameError: MutableLiveData<Exception?> by lazy {
        MutableLiveData<Exception?>(null)
    }

    private val _phoneError: MutableLiveData<Exception?> by lazy {
        MutableLiveData<Exception?>(null)
    }

    private val _emailError: MutableLiveData<Exception?> by lazy {
        MutableLiveData<Exception?>(null)
    }

    private val _passwordError: MutableLiveData<Exception?> by lazy {
        MutableLiveData<Exception?>(null)
    }

    private val _loginError: MutableLiveData<Exception?> by lazy {
        MutableLiveData<Exception?>(null)
    }

    private val _isSuccess: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    private val _page: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>(LOGIN_PAGE)
    }

    private val _googleSignInIntent: MutableLiveData<Intent> by lazy {
        MutableLiveData<Intent>(null)
    }

    val isLoginFormValid: LiveData<Boolean>
        get() = _isLoginFormValid

    val isRegisterFormValid: LiveData<Boolean>
        get() = _isRegisterFormValid


    val email: ObservableField<String>
        get() = _email

    val phone: ObservableField<String>
        get() = _phone

    val password: ObservableField<String>
        get() = _password

    val page: LiveData<Int>
        get() = _page


    val emailError: LiveData<Exception?>
        get() = _emailError

    val passwordError: LiveData<Exception?>
        get() = _passwordError

    val loginError: LiveData<Exception?>
        get() = _loginError

    val isSuccess: LiveData<Boolean>
        get() = _isSuccess

    val googleSignInIntent: LiveData<Intent>
        get() = _googleSignInIntent

    fun resetState() {
        setError(null)
        _page.value = 1
        _loading.value = false
        _loginError.value = null
        _emailError.value = null
        _passwordError.value = null
        _nameError.value = null
        _googleSignInIntent.value = null
        _isSuccess.value = false
        setError(null)
    }

    fun attachInputFormat(
        nikInputEditText: TextInputEditText,
        passwordInputEditText: TextInputEditText
    ) {
        nikInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _nikValue.value = this
                    setText(nikValue.value)
                    validNIKFormat = length() == 16
                    _hideErrorNIKformat.value = validNIKFormat
                    _isValid.value = validNIKFormat && validPasswordFormat
                    Selection.setSelection(nikInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }

        passwordInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _passwordValue.value = this
                    setText(passwordValue.value)
                    validPasswordFormat = length() >= 6
                    _hideErrorPasswordformat.value = validPasswordFormat
                    _isValid.value = validNIKFormat && validPasswordFormat
                    Selection.setSelection(passwordInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }
    }

    fun isAnonymous(context: Context): Boolean? {
        return PreferenceManager(context).isLoggedIn()
    }
    fun loginPage() {
        setError(null)
        _page.value = LOGIN_PAGE
    }

    fun registerPage() {
        setError(null)
        _page.value = REGISTER_PAGE
    }

    fun loginWithGoogle() {
        _loading.value = true
        setError(null)
        _googleSignInIntent.value = authenticationRepository.googleSignInIntent
    }

    fun onGoogleLoginActivityResult(data: Intent) {
        try {
            val callback: ((success: Boolean, err: Exception?) -> Unit) = { success, err ->
                updateUniqueUser()
                updateView(success)
            }
            authenticationRepository.processGoogleSignInResult(data, callback)
        } catch (e: ApiException) {
            updateView(false)
        }
    }

    private fun updateView(success: Boolean) {
        _loading.value = false
        _isSuccess.value = success
        if (!success) {
            showError(LoginException())
        }
    }

    fun isLoggedIn() = preferenceManager.isLoggedIn()


}