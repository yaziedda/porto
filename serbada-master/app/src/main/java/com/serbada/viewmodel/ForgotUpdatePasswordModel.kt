package com.serbada.viewmodel

import android.content.Context
import android.text.Selection
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.material.textfield.TextInputEditText
import com.serbada.base.BaseViewModel
import com.serbada.exception.GeneralException
import com.serbada.extensions.afterTextChanged
import com.serbada.model.user.User
import com.serbada.repository.AnalyticsRepository
import com.serbada.repository.AuthenticationRepository
import com.serbada.repository.RemoteRepository
import com.serbada.utils.PreferenceManager
import kotlinx.coroutines.launch

class ForgotUpdatePasswordModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private var validPasswordFormat: Boolean = false
    private var validPasswordConfirmationFormat: Boolean = false
    private var onChangedDisabled: Boolean = false

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _userData: MutableLiveData<User?> by lazy {
        MutableLiveData(null)
    }
    val userData: MutableLiveData<User?>
        get() = _userData

    private val _passwordValue = MutableLiveData<String>()
    private val passwordValue: MutableLiveData<String>
        get() = _passwordValue

    private val _passwordConfirmationValue = MutableLiveData<String>()
    private val passwordConfirmationValue: MutableLiveData<String>
        get() = _passwordConfirmationValue

    private val _hideErrorPasswordformat = MutableLiveData<Boolean>()
    val hideErrorPasswordformat: LiveData<Boolean>
        get() = _hideErrorPasswordformat

    private val _hideErrorPasswordConfirmationFormat = MutableLiveData<Boolean>()
    val hideErrorPasswordConfirmationFormat: LiveData<Boolean>
        get() = _hideErrorPasswordConfirmationFormat

    private val _isValid = MutableLiveData<Boolean>()
    val isValid: LiveData<Boolean>
        get() = _isValid

    private val _passwordIsUpdate = MutableLiveData<Boolean>()
    val passwordIsUpdate: LiveData<Boolean>
        get() = _passwordIsUpdate

    fun resetState() {
        setError(null)
    }

    fun attachInputFormat(
        passwordInputEditText: TextInputEditText,
        passwordConfirmationInputEditText: TextInputEditText
    ) {

        passwordInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _passwordValue.value = this
                    setText(passwordValue.value)
                    validPasswordFormat = length() >= 6
                    _hideErrorPasswordformat.value = validPasswordFormat
                    _isValid.value = validPasswordFormat && validPasswordConfirmationFormat
                    Selection.setSelection(passwordInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }

        passwordConfirmationInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _passwordConfirmationValue.value = this
                    setText(passwordConfirmationValue.value)
                    validPasswordConfirmationFormat =
                        passwordConfirmationValue.value == passwordValue.value
                    _hideErrorPasswordConfirmationFormat.value = validPasswordConfirmationFormat
                    _isValid.value = validPasswordFormat && validPasswordConfirmationFormat
                    Selection.setSelection(passwordConfirmationInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }
    }



}