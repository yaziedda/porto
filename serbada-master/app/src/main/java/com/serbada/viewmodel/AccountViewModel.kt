package com.serbada.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.serbada.base.BaseViewModel
import com.serbada.repository.AnalyticsRepository
import com.serbada.repository.AuthenticationRepository
import com.serbada.repository.RemoteRepository

class AccountViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }

    val loading: MutableLiveData<Boolean>
        get() = _loading

    fun resetState(){
        setError(null)
    }

    private val _displayName = MutableLiveData<String>()
    val displayName: LiveData<String>
        get() = _displayName

    private val _signOutSuccess: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    val signOutSuccess: LiveData<Boolean>
        get() = _signOutSuccess

    fun isAnonymous(): Boolean {
        return authenticationRepository.isAnonymousUser()
    }

    fun getDisplayName() {
        _displayName.value = when {
            authenticationRepository.currentUser()?.displayName != null ->
                authenticationRepository.currentUser()?.displayName ?: ""
            authenticationRepository.currentUser()?.email != null ->
                authenticationRepository.currentUser()?.email ?: ""
            else -> authenticationRepository.currentUser()!!.uid.substring(0, 5)
        }
    }

    fun signOut() {
        authenticationRepository.signOut()
        _signOutSuccess.value = true
    }
}