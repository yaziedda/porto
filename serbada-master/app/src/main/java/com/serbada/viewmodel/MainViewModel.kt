package com.serbada.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.serbada.base.BaseViewModel
import com.serbada.exception.GeneralException
import com.serbada.repository.AnalyticsRepository
import com.serbada.repository.AuthenticationRepository
import com.serbada.repository.RemoteRepository
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private val _isNeedUpdate = MutableLiveData<Boolean>()
    val isNeedUpdate: LiveData<Boolean>
        get() = _isNeedUpdate

    fun forceUpdate() {
        _isNeedUpdate.value = true
    }

    fun tokenUpdate(map: HashMap<String, String?>?) = viewModelScope.launch {
        try {
//            remoteRepository.tokenUpdate(map)
        } catch (exception: Exception) {
        }
    }

    fun isAnonymous() = authenticationRepository.isAnonymousUser()

    fun getEmail(): String {
        return authenticationRepository.currentUser()?.email ?: ""
    }
}