package com.serbada.viewmodel

import android.content.Context
import android.text.Selection
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.material.textfield.TextInputEditText
import com.serbada.base.BaseViewModel
import com.serbada.exception.GeneralException
import com.serbada.extensions.afterTextChanged
import com.serbada.model.user.User
import com.serbada.model.user.UserToken
import com.serbada.repository.AnalyticsRepository
import com.serbada.repository.AuthenticationRepository
import com.serbada.repository.RemoteRepository
import com.serbada.utils.PreferenceManager
import kotlinx.coroutines.launch
import kotlin.collections.HashMap

class ForgotPasswordViewModel @ViewModelInject constructor(
    private val remoteRepository: RemoteRepository,
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {

    private var validNIKFormat: Boolean = false
    private var onChangedDisabled: Boolean = false

    private val _loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val loading: MutableLiveData<Boolean>
        get() = _loading

    private val _startCountDown: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val startCountDown: MutableLiveData<Boolean>
        get() = _startCountDown

    private val _userData: MutableLiveData<User?> by lazy {
        MutableLiveData(null)
    }
    val userData: MutableLiveData<User?>
        get() = _userData

    private val _nikValue = MutableLiveData<String>()
    val nikValue: MutableLiveData<String>
        get() = _nikValue
    private val _hideErrorNIKformat = MutableLiveData<Boolean>()
    val hideErrorNIKformat: LiveData<Boolean>
        get() = _hideErrorNIKformat
    private val _isValid = MutableLiveData<Boolean>()
    val isValid: LiveData<Boolean>
        get() = _isValid

    private val _otpIsValid = MutableLiveData<Boolean>()
    val otpIsValid: LiveData<Boolean>
        get() = _otpIsValid

    fun resetState() {
        _userData.value = null
        setError(null)
        _startCountDown.value = false
    }

    fun attachInputFormat(
        nikInputEditText: TextInputEditText
    ) {
        nikInputEditText.apply {
            afterTextChanged {
                if (!onChangedDisabled) {
                    onChangedDisabled = true
                    _nikValue.value = this
                    setText(nikValue.value)
                    validNIKFormat = length() == 16
                    _hideErrorNIKformat.value = validNIKFormat
                    _isValid.value = validNIKFormat
                    Selection.setSelection(nikInputEditText.text, length())
                    onChangedDisabled = false
                }
            }
        }
    }


}