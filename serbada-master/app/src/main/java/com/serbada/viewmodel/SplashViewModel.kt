package com.serbada.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.serbada.base.BaseViewModel
import com.serbada.repository.AnalyticsRepository
import com.serbada.repository.AuthenticationRepository

class SplashViewModel @ViewModelInject constructor(
    private val authenticationRepository: AuthenticationRepository,
    analyticsRepository: AnalyticsRepository
) : BaseViewModel(authenticationRepository, analyticsRepository) {
    companion object {
        const val DESTINATION_LOGIN = 1
        const val DESTINATION_MAIN = 2
    }

    private val _destination: MutableLiveData<Int> by lazy {
        MutableLiveData(0)
    }

    val destination: LiveData<Int>
        get() = _destination

    fun checkAuthenticated() {

        _destination.value = DESTINATION_MAIN

    }
}
