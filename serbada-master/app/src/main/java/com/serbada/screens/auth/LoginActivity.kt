package com.serbada.screens.auth

import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import dagger.hilt.android.AndroidEntryPoint
import com.serbada.R
import com.serbada.base.BaseActivity
import com.serbada.databinding.ActivityLoginBinding
import com.serbada.exception.LoginException
import com.serbada.extensions.click
import com.serbada.extensions.color
import com.serbada.extensions.underline
import com.serbada.screens.MainActivity
import com.serbada.utils.StatusBar
import com.serbada.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_login.*


@AndroidEntryPoint
class LoginActivity : BaseActivity<ActivityLoginBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_login
    private val viewModel by viewModels<AuthViewModel>()
    private var isDirectToLogin: Boolean? = false

    private val errorMessage: SpannableStringBuilder by lazy {
        SpannableStringBuilder()
            .append(getString(R.string.label_prefix_blocking_description))
            .append(
                getString(R.string.label_here).color(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.colorPrimary
                    )
                )
                    .underline()
                    .click { }
            )
    }

    companion object {
        const val KEY_IS_DIRECT_TO_LOGIN = "KEY_IS_DIRECT_TO_LOGIN"
    }

    override fun ActivityLoginBinding.initializeView(savedInstanceState: Bundle?) {
        StatusBar.setLightStatusBar(window)
        setupUI()
        setupObserver()
    }

    private fun ActivityLoginBinding.setupObserver() {
        viewModel.apply {
            resetState()
            googleSignInIntent.observe(this@LoginActivity, { googleSignInIntent ->
                if (googleSignInIntent != null) {
                    startActivityForResult(googleSignInIntent, AuthViewModel.RC_GOOGLE_SIGN_IN)
                }
            })

            viewModel.isSuccess.observe(this@LoginActivity, { isSuccess ->
                if (isSuccess) {
                    val mainActivityIntent = Intent(this@LoginActivity, MainActivity::class.java).apply {
                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    }
                    startActivity(mainActivityIntent)
                    finish()
                }
            })

            error.observe { if (this is LoginException) showBlockingPage { viewModel.loginWithGoogle() } }

            isValid.observe {
                btnLogin.isEnabled = this
            }

            hideErrorNIKformat.observe {
                when {
                    this -> nikWarning.visibility = View.GONE
                    etEmail.text?.isEmpty() == true -> nikWarning.visibility =
                        View.GONE
                    else -> nikWarning.visibility = View.VISIBLE
                }
            }

            hideErrorPasswordformat.observe {
                when {
                    this -> passwordWarning.visibility = View.GONE
                    etPassword.text?.isEmpty() == true -> passwordWarning.visibility =
                        View.GONE
                    else -> passwordWarning.visibility = View.VISIBLE
                }
            }

            error.observe {
                if (this != null) {
                    this@LoginActivity.showError(
                        this,
                        message ?: errorMessage,
                        ContextCompat.getDrawable(
                            applicationContext,
                            R.drawable.ic_not_logged_in
                        )
                    ) {

                    }
                }
            }

            userData.observe {
                if (this != null) {
                    Toast.makeText(
                        applicationContext,
                        "Berhasil registrasi, selamat datang " + (namaLengkap
                            ?: "") + " di " + getString(R.string.app_name),
                        Toast.LENGTH_LONG
                    ).show()
                    goToHomeActivity()

                }
            }
        }
    }

    private fun ActivityLoginBinding.setupUI() {
        viewModel.attachInputFormat(etEmail, etPassword)
        var passwordHidden = false
        ivPasswordToggle.setOnClickListener {
            passwordHidden = if (passwordHidden) {
                passwordToggleVisibility(ivPasswordToggle, etPassword, false)
            } else {
                passwordToggleVisibility(ivPasswordToggle, etPassword, true)
            }
        }

        btnLoginGoogle.setOnClickListener {
            viewModel.loginWithGoogle()
        }
    }
    private fun goToHomeActivity() {
        val intent = Intent(
            this,
            MainActivity::class.java
        ).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AuthViewModel.RC_GOOGLE_SIGN_IN) {
            if (data != null) {
                viewModel.onGoogleLoginActivityResult(data)
            }
        }
    }


}