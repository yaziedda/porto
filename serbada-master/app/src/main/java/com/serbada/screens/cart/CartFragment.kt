package com.serbada.screens.cart

import android.content.Intent
import android.net.Uri
import com.serbada.R
import com.serbada.base.BaseFragment
import com.serbada.databinding.FragmentCartBinding


class CartFragment : BaseFragment<FragmentCartBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_cart
    override fun FragmentCartBinding.initializeView() {

    }


    private fun onClickWhatsApp(string: String, phone: String) {
        val url = "whatsapp://send?text=${string}&phone=${phone.replace("-", "").replace(" ", "")}"
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            val appPackageName = "com.whatsapp"
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$appPackageName")
                    )
                )
            } catch (e: android.content.ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }
        }
    }

}