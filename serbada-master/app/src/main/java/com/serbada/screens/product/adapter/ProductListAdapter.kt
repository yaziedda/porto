package com.serbada.screens.product.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.serbada.R
import com.serbada.extensions.toCurrency
import com.serbada.model.Product
import com.serbada.screens.product.ProductActivity

class ProductListAdapter(
    val activity: ProductActivity,
    val product: Product
) :
    AbstractItem<ProductListAdapter.ViewHolder>() {

    override val layoutRes: Int = R.layout.item_product
    override val type: Int = R.id.view_product_adapter
    override fun getViewHolder(v: View): ViewHolder =
        ViewHolder(v)

    class ViewHolder(view: View) : FastAdapter.ViewHolder<ProductListAdapter>(view) {
        val imageView: ImageView =
            view.findViewById(R.id.item_catalogue_iv_image)
        private val tvTitle: TextView =
            view.findViewById(R.id.item_catalogue_tv_title)
        private val tvPrice: TextView =
            view.findViewById(R.id.item_catalogue_tv_price)

        override fun bindView(item: ProductListAdapter, payloads: List<Any>) {
            with(item) {
                tvTitle.text = product.name
                tvPrice.text = product.price.toString().toCurrency()
                Glide.with(itemView.context)
                    .load(product.productGallery?.getOrNull(0)?.url375x375 ?: "")
                    .into(imageView)
                itemView.setOnClickListener {
                    activity.goToProductDetail(product)
                }
            }
        }

        override fun unbindView(item: ProductListAdapter) {
            tvTitle.text = ""
            tvPrice.text = ""
        }
    }
}