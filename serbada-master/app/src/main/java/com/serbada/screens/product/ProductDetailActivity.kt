package com.serbada.screens.product

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.Toast
import androidx.activity.viewModels
import androidx.room.Room
import com.bumptech.glide.Glide
import com.serbada.R
import com.serbada.base.BaseActivity
import com.serbada.database.AppDatabase
import com.serbada.databinding.ActivityProductDetailBinding
import com.serbada.extensions.gone
import com.serbada.extensions.toCurrency
import com.serbada.extensions.visible
import com.serbada.model.productdetail.ProductDetail
import com.serbada.screens.auth.BlockingPageAnonymousFragment
import com.serbada.utils.Constants
import com.serbada.utils.FragmentSupportManager
import com.serbada.viewmodel.AuthViewModel
import com.serbada.viewmodel.ProductViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.loading_view.*
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*

@AndroidEntryPoint
class ProductDetailActivity : BaseActivity<ActivityProductDetailBinding>() {
    override fun getLayoutId(): Int = R.layout.activity_product_detail
    private val viewModel by viewModels<ProductViewModel>()
    private val viewModelAuth by viewModels<AuthViewModel>()
    private lateinit var id: String

    companion object {
        const val KEY_ID = "KEY_ID"
    }

    override fun ActivityProductDetailBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        getExtras()
        initObserver()
        viewModel.fetchProduct(id)
    }

    private fun getExtras() {
        id = intent?.extras?.getString(KEY_ID) ?: ""
    }

    private fun ActivityProductDetailBinding.initObserver() {
        with(viewModel) {
            resetState()
            loading.observe {
                setLoading(this)
            }

            productDetails.observe {
                if (this != null && this.isNotEmpty()) {
                    setDetail(this.getOrNull(0))
                }
            }

            loadingCart.observe {
                if (this) {
                    showProgressDialog()
                } else {
                    dissmissProgressDialog()
                }
            }
        }
    }

    private fun ActivityProductDetailBinding.setDetail(productDetail: ProductDetail?) {
        productDetail?.let { product ->
            toolbar.toolbar_support.toolbar_text_title.text = product.name
            if (product.productDetailGallery?.isNotEmpty() == true) {
                activityProductDetailCarousel.setImageListener { position, imageView ->
                    val url = product.productDetailGallery.getOrNull(position)?.urlOriginal ?: ""
                    Glide.with(applicationContext)
                        .load(url)
                        .into(imageView)

                    imageView.setOnClickListener {
                        goToShowImageActivity(url)
                    }
                }
                activityProductDetailCarousel.pageCount = product.productDetailGallery.size
            }

            activityProductDetailTvPrice.text = product.price.toString().toCurrency()
            activityProductDetailTvTitle.text = product.name
            activityProductDetailBerat.text = product.author
            activityProductDetailDesc.isClickable = true
            activityProductDetailDesc.movementMethod = LinkMovementMethod.getInstance()
            activityProductDetailDesc.text = fromHtml(product.description?.getOrNull(0) ?: "")

            activityProductDetailBtnCart.setOnClickListener {
                if (viewModelAuth.isLoggedIn() == true) {
                    val phone = "628118076633"
                    val text =
                        "Hi Admin Saya Tertarik Membeli Barang Ini https://www.serbada.com/product/" + product.slug
                    onClickWhatsApp(text, phone)
                } else {
                    FragmentSupportManager(
                        this@ProductDetailActivity,
                        BlockingPageAnonymousFragment()
                    ).show()
                }
            }
        }
    }

    private fun onClickWhatsApp(string: String, phone: String) {
        val url = "whatsapp://send?text=${string}&phone=${phone.replace("-", "").replace(" ", "")}"
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            val appPackageName = "com.whatsapp"
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$appPackageName")
                    )
                )
            } catch (e: android.content.ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }
        }
    }

    private fun ActivityProductDetailBinding.setLoading(
        b: Boolean
    ) {
        if (b) {
            progressBar.visible()
            activityProductDetailWrapper.gone()
        } else {
            progressBar.gone()
            activityProductDetailWrapper.visible()
        }
    }

    private fun ActivityProductDetailBinding.setupActionBar() {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
        }
    }
}