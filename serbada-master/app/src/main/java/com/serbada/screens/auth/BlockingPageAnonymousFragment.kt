package com.serbada.screens.auth

import android.content.Intent
import com.bumptech.glide.Glide
import com.serbada.R
import com.serbada.base.BaseCallbackFragment
import com.serbada.databinding.FragmentBlockingPageAnonymousBinding
import com.serbada.utils.FragmentHelper
import kotlinx.android.synthetic.main.view_toobar_blocking_page.view.*

class BlockingPageAnonymousFragment(): BaseCallbackFragment<FragmentBlockingPageAnonymousBinding, Unit>(),FragmentHelper<Unit> {

    override fun getLayoutId(): Int = R.layout.fragment_blocking_page_anonymous

    override fun FragmentBlockingPageAnonymousBinding.initializeView() {
        blockingToolbar.close_sheet.setOnClickListener { closeFragment() }
        Glide.with(requireContext())
            .load(R.drawable.ic_not_logged_in)
            .into(starImageView)
        buttonLogin.setOnClickListener {
            startActivity(Intent(requireActivity(), LoginActivity::class.java))
            closeFragment()
        }
    }
}