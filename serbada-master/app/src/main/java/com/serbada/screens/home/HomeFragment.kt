package com.serbada.screens.home

import android.annotation.SuppressLint
import android.content.Intent
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.mikepenz.fastadapter.ui.items.ProgressItem
import com.serbada.BR
import com.serbada.R
import com.serbada.adapters.RecyclerViewAdapter
import com.serbada.base.BaseFragment
import com.serbada.databinding.FragmentHomeBinding
import com.serbada.databinding.ItemHomeBannerBinding
import com.serbada.extensions.gone
import com.serbada.extensions.visible
import com.serbada.model.Banner
import com.serbada.model.Product
import com.serbada.screens.home.adapter.ProductAdapter
import com.serbada.screens.product.ProductActivity
import com.serbada.screens.product.ProductDetailActivity
import com.serbada.viewmodel.HomeViewModel
import timber.log.Timber

class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_home
    private val viewModel by activityViewModels<HomeViewModel>()
    private lateinit var recyclerBannerViewAdapter: RecyclerViewAdapter<Banner, ItemHomeBannerBinding>
    private lateinit var fastItemAdapter: FastItemAdapter<IItem<*>>

    private var page = 1
    private var firstPage = 1
    lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener
    private var footerAdapter = ItemAdapter<ProgressItem>()

    override fun FragmentHomeBinding.initializeView() {
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                page = firstPage
                initAdapter()
                fetcher()
            }
        }
        setupUI()
        setupObserver()
        fetcher()
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchBanner()
            fetchProductUI(false)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun FragmentHomeBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    containerLoading.visibility = View.VISIBLE
                    fragmentHomeWrapperContainer.visibility = View.GONE
                    skeletonLayout.showSkeleton()
                } else {
                    containerLoading.visibility = View.GONE
                    fragmentHomeWrapperContainer.visibility = View.VISIBLE
                    skeletonLayout.showOriginal()
                    swipeContainer.isRefreshing = false
                    showLoadMoreLoading(false)
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }

            bannerList.observe {
                recyclerBannerViewAdapter.updateList(this)
            }

            recommendationList.observe {
                if (this != null) {
                    if (this.isNotEmpty()) {
                    val collections: MutableList<IItem<*>> = arrayListOf()
                        if (page == firstPage) fastItemAdapter.clear()
                        this.forEach { itemData ->
                            collections.add(
                                ProductAdapter(
                                    this@HomeFragment,
                                    itemData
                                )
                            )
                        }
                        fastItemAdapter.add(collections)
                    }
                }
            }

        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupUI() {
        binding.apply {
            setupBannerAdapter()
            initAdapter()
            productViewAll.setOnClickListener {
                startActivity(Intent(requireContext(), ProductActivity::class.java))
            }
        }
    }

    private fun FragmentHomeBinding.setupBannerAdapter() {
        recyclerBannerViewAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_home_banner,
            BR.banner
        ) { itemView, itemModel ->
            Glide.with(requireActivity())
                .load(itemModel.imageMobile)
                .into(itemView.fragmentHomeBannerImage)
        }
        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        fragmentHomeBannerRecyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerBannerViewAdapter
        }
        LinearSnapHelper().attachToRecyclerView(fragmentHomeBannerRecyclerView)
        indicator.attachToRecyclerView(fragmentHomeBannerRecyclerView)
    }

    private fun FragmentHomeBinding.initAdapter() {
        fastItemAdapter = FastItemAdapter<IItem<*>>().apply {
            setHasStableIds(true)
            val eventHook = object : ClickEventHook<ProductAdapter>() {
                override fun onBind(viewHolder: RecyclerView.ViewHolder): View? =
                    (viewHolder as? ProductAdapter.ViewHolder)?.itemView

                override fun onClick(
                    v: View,
                    position: Int,
                    fastAdapter: FastAdapter<ProductAdapter>,
                    item: ProductAdapter
                ) {

                }
            }
            addEventHook(eventHook)
        }

        fragmentHomeRecyclerViewLayanan.apply {
            isNestedScrollingEnabled = false
            itemAnimator = null
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = fastItemAdapter
            endlessRecyclerOnScrollListener =
                object : EndlessRecyclerOnScrollListener(footerAdapter) {
                    override fun onLoadMore(currentPage: Int) {
                        if (viewModel.getIsRunning() == true) {
                            return
                        }
//                        showLoadMoreLoading(true)
//                        fetchNextPageVacancies()
                    }
                }
            addOnScrollListener(endlessRecyclerOnScrollListener)
        }
    }

    private fun fetchNextPageVacancies() {
        page += 1
        fetchProductUI(isNext = true)
    }

    private fun fetchProductUI(isNext: Boolean = false) {
        viewModel.fetchRecommendation(isNext = isNext, page = page)
    }

    private fun FragmentHomeBinding.showLoadMoreLoading(loading: Boolean) {
        if (loading) {
            homeLoadMore.visible()
        } else {
            homeLoadMore.gone()
        }
    }

    fun goToProductDetail(itemModel: Product) {
        startActivity(
            Intent(requireContext(), ProductDetailActivity::class.java).apply {
                putExtra(ProductDetailActivity.KEY_ID, itemModel.productId.toString())
            }
        )
    }
}