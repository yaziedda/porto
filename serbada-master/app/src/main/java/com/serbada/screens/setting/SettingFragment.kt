package com.serbada.screens.setting

import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import dagger.hilt.android.AndroidEntryPoint
import com.serbada.R
import com.serbada.base.BaseFragment
import com.serbada.databinding.FragmentSettingBinding
import com.serbada.extensions.clearNotifications
import com.serbada.screens.SplashScreenActivity
import com.serbada.screens.auth.LoginActivity
import com.serbada.utils.DialogUtils
import com.serbada.viewmodel.AccountViewModel
import org.jetbrains.anko.support.v4.toast

@AndroidEntryPoint
class SettingFragment : BaseFragment<FragmentSettingBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_setting
    private val viewModel by activityViewModels<AccountViewModel>()

    override fun FragmentSettingBinding.initializeView() {
        initObserver()
        checkAnonymous()
        initLogout()
    }

    private fun FragmentSettingBinding.initObserver() {
        viewModel.displayName.observe {
            tvName.text = this
        }

        viewModel.signOutSuccess.observe(viewLifecycleOwner, { signOutSuccess ->
            if (signOutSuccess) {
                val intent = Intent(activity, SplashScreenActivity::class.java)
                activity?.startActivity(intent)
                activity?.finish()
            }
        })
    }

    private fun FragmentSettingBinding.initLogout() {
        buttonLogout.setOnClickListener {
            requireContext().clearNotifications()
            DialogUtils.showBasicAlertConfirmationDialog(
                activity = requireActivity(),
                ContextCompat.getDrawable(requireContext(), R.drawable.logo),
                "Apakah anda yakin akan logout?"
            ) {
                viewModel.signOut()
            }
        }
    }

    private fun FragmentSettingBinding.checkAnonymous() {
        val isAnonymous = viewModel.isAnonymous()
        if (!isAnonymous) {
            contentAnonymous.visibility = View.GONE
            contentMain.visibility = View.VISIBLE
            viewModel.getDisplayName()
        } else {
            contentAnonymous.visibility = View.VISIBLE
            contentMain.visibility = View.GONE
            contentAnonymous.apply {
                buttonLogin.setOnClickListener {
                    toast("Under development")
                }
            }
        }
    }


}