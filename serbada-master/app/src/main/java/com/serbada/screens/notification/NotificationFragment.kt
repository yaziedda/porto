package com.serbada.screens.notification

import com.serbada.R
import com.serbada.base.BaseFragment
import com.serbada.databinding.FragmentNotificationBinding

class NotificationFragment : BaseFragment<FragmentNotificationBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_notification
    override fun FragmentNotificationBinding.initializeView() {

    }

}