package com.serbada.screens.recommendation

import android.annotation.SuppressLint
import android.content.Intent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.mikepenz.fastadapter.ui.items.ProgressItem
import com.serbada.R
import com.serbada.base.BaseFragment
import com.serbada.database.AppDatabase
import com.serbada.databinding.FragmentRecommendationBinding
import com.serbada.extensions.gone
import com.serbada.extensions.hideKeyboard
import com.serbada.extensions.visible
import com.serbada.model.Product
import com.serbada.screens.product.ProductDetailActivity
import com.serbada.screens.recommendation.adapter.RecommendationAdapter
import com.serbada.utils.Constants
import com.serbada.viewmodel.HomeViewModel
import timber.log.Timber

class RecommendationFragment : BaseFragment<FragmentRecommendationBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_recommendation
    private val viewModel by activityViewModels<HomeViewModel>()
    private lateinit var fastItemAdapter: FastItemAdapter<IItem<*>>

    private var page = 1
    private var firstPage = 1
    lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener
    private var footerAdapter = ItemAdapter<ProgressItem>()
    private var searchProduct = ""

    override fun FragmentRecommendationBinding.initializeView() {
        setupUI()
        setupObserver()
        fetcher()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                resetFetcher()
            }
        }
        searchName.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val text = searchName.text.toString()
                if (text.isNotEmpty()) {
                    searchProduct = text
                    requireActivity().hideKeyboard()
                    resetFetcher()
                }
                return@OnEditorActionListener true
            }
            false
        })
    }

    private fun FragmentRecommendationBinding.resetFetcher() {
        page = firstPage
        setupProductAdapter()
        fetcher()
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchProduct(page, isNext = false, search = searchProduct)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    requireActivity().hideKeyboard()
                    binding.apply {
                        containerLoading.visible()
                        fragmentHomeWrapperContainer.gone()
                    }
                } else {
                    binding.apply {
                        containerLoading.gone()
                        fragmentHomeWrapperContainer.visible()
                        swipeContainer.isRefreshing = false
                        showLoadMoreLoading(false)
                    }
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }

            productList.observe {
                if (this != null) {
                    if (this.isNotEmpty()) {
                        val collections: MutableList<IItem<*>> = arrayListOf()
                        if (page == firstPage) fastItemAdapter.clear()
                        this.forEach { itemData ->
                            collections.add(
                                RecommendationAdapter(
                                    this@RecommendationFragment,
                                    itemData
                                )
                            )
                        }
                        fastItemAdapter.add(collections)
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupUI() {
        binding.apply {
            setupProductAdapter()
        }
    }

    private fun FragmentRecommendationBinding.setupProductAdapter() {
        fastItemAdapter = FastItemAdapter<IItem<*>>().apply {
            setHasStableIds(true)
            val eventHook = object : ClickEventHook<RecommendationAdapter>() {
                override fun onBind(viewHolder: RecyclerView.ViewHolder): View? =
                    (viewHolder as? RecommendationAdapter.ViewHolder)?.itemView

                override fun onClick(
                    v: View,
                    position: Int,
                    fastAdapter: FastAdapter<RecommendationAdapter>,
                    item: RecommendationAdapter
                ) {

                }
            }
            addEventHook(eventHook)
        }

        fragmentRecommendationRecyclerView.apply {
            itemAnimator = null
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = fastItemAdapter
            endlessRecyclerOnScrollListener =
                object : EndlessRecyclerOnScrollListener(footerAdapter) {
                    override fun onLoadMore(currentPage: Int) {
                        if (viewModel.getIsRunning() == true) {
                            return
                        }
                        showLoadMoreLoading(true)
                        fetchNextPageVacancies()
                    }
                }
            addOnScrollListener(endlessRecyclerOnScrollListener)
        }
    }

    private fun fetchNextPageVacancies() {
        page += 1
        fetchProductUI(isNext = true)
    }

    private fun fetchProductUI(isNext: Boolean = false) {
        viewModel.fetchProduct(isNext = isNext, page = page, search = searchProduct)
    }

    private fun FragmentRecommendationBinding.showLoadMoreLoading(loading: Boolean) {
        if (loading) {
            homeLoadMore.visible()
        } else {
            homeLoadMore.gone()
        }
    }

    fun goToProductDetail(itemModel: Product) {
        startActivity(
            Intent(requireContext(), ProductDetailActivity::class.java).apply {
                putExtra(ProductDetailActivity.KEY_ID, itemModel.productId.toString())
            }
        )
    }

}