package com.serbada.screens.product

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import com.mikepenz.fastadapter.ui.items.ProgressItem
import com.serbada.R
import com.serbada.base.BaseActivity
import com.serbada.databinding.ActivityProductBinding
import com.serbada.extensions.gone
import com.serbada.extensions.visible
import com.serbada.model.Product
import com.serbada.screens.product.adapter.ProductListAdapter
import com.serbada.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.loading_view.*
import kotlinx.android.synthetic.main.view_toolbar_activity.view.*
import timber.log.Timber

@AndroidEntryPoint
class ProductActivity : BaseActivity<ActivityProductBinding>() {
    private val viewModel by viewModels<HomeViewModel>()
    override fun getLayoutId(): Int = R.layout.activity_product
    private lateinit var fastItemAdapter: FastItemAdapter<IItem<*>>

    private var page = 1
    private var firstPage = 1
    lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener
    private var footerAdapter = ItemAdapter<ProgressItem>()

    override fun ActivityProductBinding.initializeView(savedInstanceState: Bundle?) {
        setupActionBar()
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupObserver()
        fetcher()
    }

    private fun ActivityProductBinding.setupActionBar() {
        with(toolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBar
            )
            toolbar_support.title = getString(R.string.produk)
        }
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchProductUI(false)
        }
    }

    private fun ActivityProductBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    progressBar.visible()
                    swipeContainer.isRefreshing = true
                    recyclerView.gone()
                } else {
                    progressBar.gone()
                    skeletonLayout.showOriginal()
                    recyclerView.visible()
                    swipeContainer.isRefreshing = false
                    showLoadMoreLoading(false)
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }

            productList.observe {
                if (this != null) {
                    if (this.isNotEmpty()) {
                        val collections: MutableList<IItem<*>> = arrayListOf()
                        if (page == firstPage) fastItemAdapter.clear()
                        this.forEach { itemData ->
                            collections.add(
                                ProductListAdapter(
                                    this@ProductActivity,
                                    itemData
                                )
                            )
                        }
                        fastItemAdapter.add(collections)
                    }
                }
            }
        }
    }

    private fun ActivityProductBinding.setupAdapter() {
        fastItemAdapter = FastItemAdapter<IItem<*>>().apply {
            setHasStableIds(true)
            val eventHook = object : ClickEventHook<ProductListAdapter>() {
                override fun onBind(viewHolder: RecyclerView.ViewHolder): View? =
                    (viewHolder as? ProductListAdapter.ViewHolder)?.itemView

                override fun onClick(
                    v: View,
                    position: Int,
                    fastAdapter: FastAdapter<ProductListAdapter>,
                    item: ProductListAdapter
                ) {

                }
            }
            addEventHook(eventHook)
        }

        recyclerView.apply {
            isNestedScrollingEnabled = false
            itemAnimator = null
            layoutManager = GridLayoutManager(applicationContext, 2)
            adapter = fastItemAdapter
            endlessRecyclerOnScrollListener =
                object : EndlessRecyclerOnScrollListener(footerAdapter) {
                    override fun onLoadMore(currentPage: Int) {
                        if (viewModel.getIsRunning() == true) {
                            return
                        }
                        showLoadMoreLoading(true)
                        fetchNextPageVacancies()
                    }
                }
            addOnScrollListener(endlessRecyclerOnScrollListener)
        }
    }

    private fun fetchNextPageVacancies() {
        page += 1
        fetchProductUI(isNext = true)
    }

    private fun fetchProductUI(isNext: Boolean = false) {
        viewModel.fetchRecommendation(isNext = isNext, page = page)
    }

    private fun ActivityProductBinding.showLoadMoreLoading(loading: Boolean) {
        if (loading) {
            homeLoadMore.visible()
        } else {
            homeLoadMore.gone()
        }
    }

    fun goToProductDetail(itemModel: Product) {
        startActivity(
            Intent(applicationContext, ProductDetailActivity::class.java).apply {
                putExtra(ProductDetailActivity.KEY_ID, itemModel.productId.toString())
            }
        )
    }

}