package com.serbada.screens

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import com.google.firebase.messaging.FirebaseMessaging
import com.kucingapes.ankodrawer.*
import com.kucingapes.ankodrawer.AnDrawerView.anDrawerLayout
import com.serbada.R
import com.serbada.base.BaseActivity
import com.serbada.databinding.ActivityMainBinding
import com.serbada.extensions.setupWithNavController
import com.serbada.utils.Constants
import com.serbada.utils.StatusBar
import com.serbada.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.*
import org.jetbrains.anko.design.coordinatorLayout

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>(), AnDrawerClickListener {

    override fun onDrawerClick(identifier: Int) {
        super.onDrawerClick(identifier)
        when (identifier) {
            1 -> toast("wah")
            2 -> toast("gile")
            3 -> toast("lu")
        }
    }


    companion object {
        const val FRAGMENT_DESTINATION = "FRAGMENT_DESTINATION"
    }

    override fun getLayoutId(): Int = R.layout.activity_main
    private val viewModel by viewModels<MainViewModel>()
    private var currentNavController: LiveData<NavController>? = null
    private val labelUpdateTitle by lazy { getString(R.string.label_update_title) }
    private val labelUpdateDescription by lazy { getString(R.string.label_update_description) }
    private var fragmentDestination: Int = 0

    override fun ActivityMainBinding.initializeView(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) setupBottomNavigationBar()
        StatusBar.setLightStatusBar(window)
        with(viewModel) {
            isNeedUpdate.observe {
                if (this) {
                    showDialog(
                        title = labelUpdateTitle,
                        message = labelUpdateDescription
                    ) {
                        val browserIntent =
                            Intent(Intent.ACTION_VIEW, Uri.parse(Constants.URL_PLAY_STORE))
                        startActivity(browserIntent)
                    }
                }
            }
            // TODO FORCE UPDATE
//            forceUpdate()
        }
        if (!viewModel.isAnonymous()) {
            FirebaseMessaging.getInstance().subscribeToTopic(viewModel.getEmail())
        }

//        setupNavSide()
    }

    private fun setupNavSide() {
        val drawer = AnDrawer(this@MainActivity, R.color.colorPrimary)
        frameLayout { anDrawerLayout(drawer) }
        AnDrawerInit.setupMainView(this@MainActivity, MainUi())

        drawer.setNavigationStyle(AnDrawerView.STYLE.NEW_MATERIAL)
        drawer.setDrawerStatusBar(R.color.colorPrimary)

        AnDrawerInit.setupHeader(this@MainActivity, HeaderUi())

        drawer.addItems().apply {
            val item1 = AnDrawerItem("Item 1")
                .addIcon(R.drawable.ic_home_black_24dp)
                .addIdentifier(1)

            val item2 = AnDrawerItem("Item 2")
                .addIcon(R.drawable.ic_home_black_24dp)
                .addIdentifier(2)

            val item3 = AnDrawerItem("Item 3")
                .addIcon(R.drawable.ic_home_black_24dp)
                .addIdentifier(3)

            val item4 = AnDrawerItem("Item 4")
                .addIcon(R.drawable.ic_home_black_24dp)
                .addIdentifier(4)
                .setFocusable(false)

            val item5 =
                AnDrawerItem("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua")
                    .addIcon(R.drawable.ic_home_black_24dp)
                    .addIdentifier(5)

            val item6 = AnDrawerItem("Item 6")
                .addIcon(R.drawable.ic_home_black_24dp)
                .addIdentifier(6)

            val divider = AnDrawerItem(AnDrawerItem.DIVIDER)

            add(divider)
            add(item1)
            add(item2)
            add(item3)
            add(item4)
            add(divider)
            add(item5)
            add(item6)
            drawer.setSelectedItem(3)
        }
    }

    class MainUi : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
            coordinatorLayout {
                themedToolbar(R.style.ThemeOverlay_AppCompat_ActionBar) {
                    //backgroundColorResource = R.color.customColor
                    id = R.id.toolbar
                    title = context.getString(R.string.app_name)
                }.lparams(matchParent, dimenAttr(R.attr.actionBarSize))

                relativeLayout {
                    textView("MAIN VIEW").lparams { centerInParent() }
                }.lparams(matchParent, matchParent)
            }
        }
    }

    class HeaderUi : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
            relativeLayout {
                lparams(matchParent, dip(200))
                backgroundColorResource = R.color.colorPrimary
                textView("CUSTOM HEADER") {
                    typeface = Typeface.DEFAULT_BOLD
                    textSize = 20f
                }.lparams { centerInParent() }
            }
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        binding.apply {
            setupBottomNavigationBar()
        }
    }

    private fun ActivityMainBinding.setupBottomNavigationBar() {
        val navGraphIds = listOf(
            R.navigation.home_nav,
            R.navigation.recommendation_nav,
            R.navigation.cart_nav,
            R.navigation.notification_nav,
            R.navigation.setting_nav
        )
        val controller = mainBottomNav.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = R.id.main_nav_host_container,
            intent = intent
        )
        currentNavController = controller
        fragmentDestination = intent.getIntExtra(FRAGMENT_DESTINATION, 0)
        if (fragmentDestination != 0) mainBottomNav.selectedItemId = fragmentDestination
    }

    override fun onSupportNavigateUp(): Boolean {
        return currentNavController?.value?.navigateUp() ?: false
    }

    private fun appInstalledOrNot(context: Context, uri: String): Boolean {
        val pm = context.packageManager
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
        }
        return false
    }
}