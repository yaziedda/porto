package com.serbada.screens.product

import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.serbada.BR
import com.serbada.R
import com.serbada.adapters.RecyclerViewAdapter
import com.serbada.base.BaseFragment
import com.serbada.databinding.FragmentProductBinding
import com.serbada.databinding.ItemProductBinding
import com.serbada.extensions.toCurrency
import com.serbada.model.Product
import com.serbada.viewmodel.ProductViewModel
import timber.log.Timber

class ProductFragment: BaseFragment<FragmentProductBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_product
    private lateinit var recyclerAdapter: RecyclerViewAdapter<Product, ItemProductBinding>
    private val viewModel by activityViewModels<ProductViewModel>()
    override fun FragmentProductBinding.initializeView() {
        setupAdapter()
        swipeContainer.apply {
            setOnRefreshListener {
                isRefreshing = true
                fetcher()
            }
        }
        setupObserver()
        fetcher()
    }

    private fun fetcher() {
        viewModel.apply {
            resetState()
            fetchProducts()
        }
    }

    private fun FragmentProductBinding.setupObserver() {
        viewModel.apply {
            loading.observe {
                if (this) {
                    swipeContainer.isRefreshing = true
                    skeletonLayout.showSkeleton()
                } else {
                    skeletonLayout.showOriginal()
                    swipeContainer.isRefreshing = false
                }
            }

            error.observe {
                if (this != null) {
                    Timber.tag(this::class.java.name).e(this)
                    showError(this) {
                        fetcher()
                    }
                }
            }

            productList.observe {
                recyclerAdapter.updateList(this)
            }
        }
    }
    private fun FragmentProductBinding.setupAdapter() {
        recyclerAdapter = RecyclerViewAdapter(
            arrayListOf(),
            R.layout.item_product,
            BR.product
        ) { itemView, itemModel ->
            itemView.itemCatalogueTvTitle.text = itemModel.name
            itemView.itemCatalogueTvPrice.text = itemModel.price.toString().toCurrency()
            Glide.with(requireActivity())
                .load(itemModel.image)
                .into(itemView.itemCatalogueIvImage)
        }

        val linearLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = recyclerAdapter
        }
    }
}