package com.serbada.screens

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.activity.viewModels
import com.serbada.BuildConfig
import com.serbada.R
import com.serbada.utils.Constants
import com.serbada.utils.StatusBar
import com.serbada.viewmodel.SplashViewModel
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        StatusBar.setLightStatusBar(window)
        app_version_name.text = "Version " + BuildConfig.VERSION_NAME

        Handler().postDelayed({
            goToNextDestination(
                Intent(this, MainActivity::class.java)
            )
        }, Constants.SPLASH_SCREEN_DELAY_DURATION)
    }

    private fun goToNextDestination(intent: Intent) {
        startActivity(intent)
        finish()
    }
}