package com.serbada.screens.recommendation.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.serbada.R
import com.serbada.extensions.toCurrency
import com.serbada.model.Product
import com.serbada.screens.recommendation.RecommendationFragment

class RecommendationAdapter(
    val fragment: RecommendationFragment,
    val product: Product
) :
    AbstractItem<RecommendationAdapter.ViewHolder>() {

    override val layoutRes: Int = R.layout.item_product
    override val type: Int = R.id.view_product_adapter
    override fun getViewHolder(v: View): ViewHolder =
        ViewHolder(v)

    class ViewHolder(view: View) : FastAdapter.ViewHolder<RecommendationAdapter>(view) {
        val imageView: ImageView =
            view.findViewById(R.id.item_catalogue_iv_image)
        private val tvTitle: TextView =
            view.findViewById(R.id.item_catalogue_tv_title)
        private val tvPrice: TextView =
            view.findViewById(R.id.item_catalogue_tv_price)

        override fun bindView(item: RecommendationAdapter, payloads: List<Any>) {
            with(item) {
                tvTitle.text = product.name
                tvPrice.text = product.price.toString().toCurrency()
                Glide.with(itemView.context)
                    .load(product.productGallery?.getOrNull(0)?.url375x375 ?: "")
                    .into(imageView)
                itemView.setOnClickListener {
                    fragment.goToProductDetail(product)
                }
            }
        }

        override fun unbindView(item: RecommendationAdapter) {
            tvTitle.text = ""
            tvPrice.text = ""
        }
    }
}