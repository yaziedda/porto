package com.serbada.screens.feature

import android.annotation.SuppressLint
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.*
import com.serbada.R
import com.serbada.base.BaseActivity
import com.serbada.databinding.ActivityWebViewBinding
import kotlinx.android.synthetic.main.toolbar.view.*


class WebViewActivity : BaseActivity<ActivityWebViewBinding>() {

    private var url: String = ""
    private var title: String = ""

    override fun getLayoutId(): Int = R.layout.activity_web_view

    companion object {
        const val KEY_URL = "value"
        const val KEY_TITLE = "name"
    }

    override fun ActivityWebViewBinding.initializeView(savedInstanceState: Bundle?) {
        loading.max = 100
        settings()
        getExtras()
//        FinestWebView.Builder(this@WebViewActivity).show(url)
//        finish()
        setupActionBar()
        loadWebview()
    }

    private fun getExtras() {
        url = intent?.extras?.getString("value") ?: ""
        title = intent?.extras?.getString("name") ?: ""
    }

    private fun ActivityWebViewBinding.setupActionBar() {
        with(layoutToolbar) {
            setupActionBar(
                toolbar_support,
                toolbar_support.toolbar_text_title,
                appBarLayout
            )
            toolbar_text_title.text = title
        }
    }

    private fun setupWebView() {
        binding.webViewContent.loadUrl("file:///android_asset/privacypolice/privacy_police_content.html")
        binding.webViewContent.setPadding(10, 10, 10, 10)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun ActivityWebViewBinding.settings() {
        val webView = webViewContent.settings
        webView.javaScriptEnabled = true
        webView.allowContentAccess = true
        webView.useWideViewPort = true
        webView.loadsImagesAutomatically = true
        webView.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.setRenderPriority(WebSettings.RenderPriority.HIGH)
        webView.setEnableSmoothTransition(true)
        webView.domStorageEnabled = true
    }

    @SuppressLint("ObsoleteSdkInt")
    private fun ActivityWebViewBinding.loadWebview() {
        if (Build.VERSION.SDK_INT >= 19) {
            webViewContent.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        } else {
            webViewContent.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
        webViewContent.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                loading.visibility = View.VISIBLE
                loading.progress = newProgress
                if (newProgress == 100) {
                    loading.visibility = View.INVISIBLE
                }
                super.onProgressChanged(view, newProgress)
            }

        }
        webViewContent.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, URL: String?): Boolean {
                view?.loadUrl(URL)
                loading.visibility = View.VISIBLE
                return true
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    view?.loadUrl(request?.url.toString())
                }
                loading.visibility = View.VISIBLE
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                loading.visibility = View.INVISIBLE
            }

            override fun onReceivedSslError(
                view: WebView?,
                handler: SslErrorHandler,
                error: SslError?
            ) {
                handler.proceed()
            }
        }

        webViewContent.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webViewContent.loadUrl(url)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}