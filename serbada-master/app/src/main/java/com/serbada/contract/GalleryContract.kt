package com.serbada.contract

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.activity.result.contract.ActivityResultContract

class GalleryContract : ActivityResultContract<Int, Uri?>() {

    override fun createIntent(context: Context, input: Int): Intent = Intent
        .createChooser(
            Intent().apply {
                type = "image/*"
                action = Intent.ACTION_PICK
                putExtra(Intent.EXTRA_LOCAL_ONLY, true)
            }, "Select Image"
        )

    override fun parseResult(resultCode: Int, intent: Intent?): Uri? = intent?.data?.takeIf { resultCode == RESULT_OK }
}