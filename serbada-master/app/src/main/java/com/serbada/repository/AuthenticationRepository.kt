package com.serbada.repository

import android.content.Intent
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase
import com.serbada.model.Token
import com.serbada.utils.PreferenceManager

class AuthenticationRepository constructor(
    private var mGoogleSignInClient: GoogleSignInClient,
    private var auth: FirebaseAuth,
    private var token: Token,
) {

    fun createUserWithEmailAndPassword(
        email: String,
        password: String,
        callback: ((success: Boolean, err: Exception?) -> Unit)
    ) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { t ->
                if (t.isSuccessful) {
                    val user = t.result?.user
                    val profileUpdates = userProfileChangeRequest {
                        displayName = email
                    }
                    user?.updateProfile(profileUpdates)
                }
                callback(t.isSuccessful, t.exception)
            }
    }

    fun signInWithEmailAndPassword(
        email: String,
        password: String,
        callback: ((success: Boolean, err: Exception?) -> Unit)
    ) {
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { t ->
                callback(t.isSuccessful, t.exception)
            }
    }

    val googleSignInIntent: Intent
        get() = mGoogleSignInClient.signInIntent

    fun processGoogleSignInResult(
        data: Intent?,
        callback: ((success: Boolean, err: Exception?) -> Unit)
    ) {
        val task = GoogleSignIn.getSignedInAccountFromIntent(data)
        val account = task.getResult(ApiException::class.java)!!
        val credential = GoogleAuthProvider.getCredential(account.idToken!!, null)
        auth.signInWithCredential(credential).addOnCompleteListener { t ->
            if (t.isSuccessful) token.token = ""
            callback(t.isSuccessful, t.exception)
        }
    }

    fun signOut() {
        mGoogleSignInClient.signOut()
        Firebase.auth.signOut()
        token.token = ""
    }

    fun isAuthenticated(callback: ((success: Boolean, err: Exception?) -> Unit)) {
        if (Firebase.auth.currentUser == null) {
            Firebase.auth.signInAnonymously()
                .addOnCompleteListener { t ->
                    callback(false, t.exception)
                }
        } else {
            callback(!Firebase.auth.currentUser!!.isAnonymous, null)
        }
    }

    fun currentUser(): FirebaseUser? {
        return Firebase.auth.currentUser
    }

    fun isAnonymousUser(): Boolean {
        return Firebase.auth.currentUser == null || Firebase.auth.currentUser!!.isAnonymous
    }
}