package com.serbada.repository

import com.serbada.model.BannerResponse
import com.serbada.model.ProductResponse
import com.serbada.model.Response
import com.serbada.model.productdetail.ProductDetail
import javax.inject.Inject

class RemoteRepository @Inject constructor(
    private val remoteRepositoryDao: RemoteRepositoryDao
) : RemoteRepositoryDao {
    override suspend fun getBanner(): Response<BannerResponse> {
        return remoteRepositoryDao.getBanner()
    }

    override suspend fun getProduct(page: Int, search: String): Response<ProductResponse> {
        return remoteRepositoryDao.getProduct(page = page, search = search)
    }

    override suspend fun getPopular(page: Int): Response<ProductResponse> {
        return remoteRepositoryDao.getPopular(page)
    }

    override suspend fun getRecommendation(page: Int): Response<ProductResponse> {
        return remoteRepositoryDao.getRecommendation(page = page)
    }

    override suspend fun getProductDetail(id: String): Response<List<ProductDetail>> {
        return remoteRepositoryDao.getProductDetail(id)
    }
}