package com.serbada.repository

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.moe.pushlibrary.MoEHelper

class AnalyticsRepository(
    private var moEHelper: MoEHelper,
    private var googleAnalytics: FirebaseAnalytics,
) {
    fun setUniqueUser(uid: String) {
        moEHelper.setUniqueId(uid)
        googleAnalytics.setUserId(uid)
    }

    fun trackEvent(uid: String, event: String, bundle: Bundle) {
        googleAnalytics.logEvent(event, bundle.apply {
            putString("uid", uid)
        })
    }

}