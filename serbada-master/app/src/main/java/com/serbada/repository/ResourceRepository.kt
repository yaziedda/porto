package com.serbada.repository

import android.content.Context
import com.moe.pushlibrary.MoEHelper
import com.moengage.core.MoEngage

class ResourceRepository(
    private var context: Context
) {
    fun getString(resId: Int): String? {
        return context.getString(resId)
    }
}