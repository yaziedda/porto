package com.serbada.repository

import com.serbada.model.BannerResponse
import com.serbada.model.ProductResponse
import com.serbada.model.Response
import com.serbada.model.productdetail.ProductDetail
import javax.inject.Inject

class RemoteRepositoryDaoImpl @Inject constructor(
    private val remoteRepositoryService: RemoteRepositoryService
) : RemoteRepositoryDao {
    override suspend fun getBanner(): Response<BannerResponse> {
        return remoteRepositoryService.getBanner()
    }

    override suspend fun getProduct(page: Int, search: String): Response<ProductResponse> {
        return remoteRepositoryService.getProduct(page = page, search)
    }

    override suspend fun getPopular(page: Int): Response<ProductResponse> {
        return remoteRepositoryService.getPopular(page.toString())
    }

    override suspend fun getRecommendation(page: Int): Response<ProductResponse> {
        return remoteRepositoryService.getRecommendation(page = page)
    }

    override suspend fun getProductDetail(id: String): Response<List<ProductDetail>> {
        return remoteRepositoryService.getProductDetail(id)
    }
}