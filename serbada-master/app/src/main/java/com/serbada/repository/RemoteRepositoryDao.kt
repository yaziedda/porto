package com.serbada.repository

import com.serbada.model.BannerResponse
import com.serbada.model.ProductResponse
import com.serbada.model.Response
import com.serbada.model.productdetail.ProductDetail

interface RemoteRepositoryDao {
    suspend fun getBanner(): Response<BannerResponse>
    suspend fun getProduct(page: Int, search: String): Response<ProductResponse>
    suspend fun getPopular(page: Int): Response<ProductResponse>
    suspend fun getRecommendation(page: Int): Response<ProductResponse>
    suspend fun getProductDetail(id: String): Response<List<ProductDetail>>
}