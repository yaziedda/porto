package com.serbada.repository

import com.serbada.model.BannerResponse
import com.serbada.model.ProductResponse
import com.serbada.model.Response
import com.serbada.model.productdetail.ProductDetail
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteRepositoryService {
    @GET("banner")
    suspend fun getBanner(): Response<BannerResponse>

    @GET("product")
    suspend fun getProduct(@Query("page") page:Int, @Query("search") search: String): Response<ProductResponse>

    @GET("product")
    suspend fun getPopular(@Query("page") page: String): Response<ProductResponse>

    @GET("product")
    suspend fun getRecommendation(@Query("is_recomended") isRecommended: String = "1", @Query("page") page: Int): Response<ProductResponse>

    @GET("detail")
    suspend fun getProductDetail(@Query("product_id") id: String): Response<List<ProductDetail>>
}